<tr>
    <td>{{ __t('property.nearby-trans', 'Nearby transportation') }}</td>
    <td colspan="3">
        <div class="nearby-trans">
            <div class="trans-item">
                <label>{{ __t('property.nearby-bts', 'Near BTS') }}</label> <span>{{ $details->nearby_bts() }}</span>
            </div>
            <div class="trans-item">
                <label>{{ __t('property.nearby-mrt', 'Near MRT') }}</label> <span>{{ $details->nearby_mrt() }}</span>
            </div>
            <div class="trans-item">
                <label>{{ __t('property.nearby-apl', 'Near APL') }}</label> <span>{{ $details->nearby_apl() }}</span>
            </div>
        </div>
    </td>
</tr>