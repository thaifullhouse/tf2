<?php
$latlng = $property->getLatLng();
$currentUser = Auth::user();
?>
<div class="row preview-header">
    <div class="col-md-6 title">
        <h3>{{ $details->getTranslatedField('title') }}</h3>
        <p>
            <span class="listing-type">
                <?php
                echo trans('property.listing_type.' . $property->listing_type);
                ?>
            </span>
            <?php
            if ($currentUser && $property->isInUserFavorites($currentUser)) {
                ?>
                <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                <?php
            } else {
                ?>
                <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                <?php
            }
            ?>
            <button class="share"><i class="fa fa-share-alt"></i> {{ trans('property.share') }}</button>
        </p>
    </div>
    <div class="col-md-6">
        <span class="listing-price">

            <?php
            if ($property->listing_type == 'rent') {
                ?>
                {{ __t('property.rental', 'Rental') }} : ฿ {{ number_format($details->listing_price) }}
                <?php
            } else {
                ?>
                {{ __t('property.starting-price', 'Sale price') }} : ฿ {{ number_format($details->listing_price) }}
                <?php
            }
            ?>
        </span>
    </div>
</div>
<div class="media-container">
    @include('visitor.property.slider')
</div>
<div class="details-container">    
    <div class="details-box">
        <table class="table">
            <tr>                
                <td class="plabel">{{ __t('property.deposit', 'Deposit') }}</td>
                <td class="value">฿ {{ number_format($details->deposite) }}</td>
                <td class="plabel">{{ __t('property.bedrooms', 'Bedrooms') }}</td>
                <td class="value">{{ $details->bedroom_num }}</td>
            </tr>
            <tr>
                <?php
                if ($property->listing_type == 'rent') {
                    ?>
                    <td>{{ __t('property.price-rent', 'Rental') }}</td>
                    <?php
                } else {
                    ?>
                    <td>{{ __t('property.price-sale', 'Sale') }}</td>
                    <?php
                }
                ?>
                <td class="value">฿ {{ number_format($details->listing_price) }}</td>
                <td>{{ __t('property.size', 'Size') }}</td>
                <td class="value">{{ $details->total_size }} sqm</td>
            </tr>
            <tr>
                <td>{{ __t('property.psf', 'PSF') }}</td>
                <td class="value">{{ number_format($details->psf) }} ฿/sqm</td>
                <td>{{ __t('property.bathrooms', 'Bathrooms') }}</td>
                <td class="value">{{ $details->bathroom_num }}</td>
            </tr>
            <tr>
                <td>{{ __t('property.floors', 'Floors') }}</td>
                <td class="value">{{ $details->floor_num }}</td>
                <td>{{ __t('property.year-built', 'Year built') }}</td>
                <td class="value">{{ $details->year_built }}</td>
            </tr>
            <tr>                
                <td>{{ __t('property.tower', 'Tower') }}</td>
                <td class="value">{{ $details->tower_num }}</td>
                <td>{{ __t('property.developer', 'Developer') }}</td>
                <td class="value">{{ $poster->getName() }}</td>
            </tr>
            
            <tr>
                <td>{{ __t('property.listed', 'Listed on') }}</td>
                <td class="value">{{ $property->getListedOn() }}</td>
                <td>{{ __t('property.property-id', 'Property ID') }}</td>
                <td class="value">No. {{ $property->id }}</td>
            </tr>

            @include('visitor.property.facilities')
            
            <tr>
                <td>{{ __t('property.nearby-trans', 'Nearby transportation') }}</td>
                <td colspan="3">
                    <div class="nearby-trans">
                        <div class="trans-item">
                            <label>{{ __t('property.nearby-bts', 'Near BTS') }}</label> <span>Asoke</span>
                        </div>
                        <div class="trans-item">
                            <label>{{ __t('property.nearby-mrt', 'Near MRT') }}</label> <span>Asoke</span>
                        </div>
                        <div class="trans-item">
                            <label>{{ __t('property.nearby-apl', 'Near APL') }}</label> <span>Asoke</span>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <?php
                if ($property->listing_type == 'rent') {
                    ?>
                    <td>{{ __t('property.availability-date', 'Availability date') }}</td>
                    <td class="value">{{ $details->getAvailabilityDate() }}</td>
                    <td>{{ __t('property.minimum-rental-period', 'Min. rent period') }}</td>
                    <td class="value">{{ $details->minimal_rental_period }}</td>
                    <?php
                } else {
                    ?>
                    <td>{{ __t('property.availability-date', 'Availability date') }}</td>
                    <td class="value">{{ $details->getAvailabilityDate() }}</td>
                    <td></td>
                    <td class="value"></td>
                    <?php
                }
                ?>

            </tr>
            <tr>
                <td>{{ __t('property.details', 'Details') }}</td>
                <td colspan="3">{{ $details->getTranslatedField('other_details') }}</td>
            </tr>
            <tr>
                <td>{{ __t('property.address', 'Address') }}</td>
                <td colspan="3">{{ $details->getTranslatedField('address') }}</td>
            </tr>
            <tr>
                <td>{{ __t('property.map-street-view', 'Map / Street view') }}</td>
                <td colspan="3">
                    <div id="property-map" class="street-view"></div>
                    @include('visitor.preview.places')
                </td>
            </tr>
        </table>
    </div>
    <div class="contact-box">
        <h4>{{ __t('property.information', 'Information') }}</h4>
        <div class="contact-box-header">
            <div class="row">
                <div class="col-md-4">
                    <img src="{{ $poster->getContactPicture() }}" class="img-responsive">
                </div>
                <div class="col-md-8">
                    {{ $poster->getName() }}<br/>
                    <a href="{{ $poster->website }}" class="single-line" target="_blank">{{ $poster->website }}</a>
                </div>
            </div>
            <p class="tel-btn">
                <button type="button" onclick="showPhone(this);" data-tel="{{ $poster->phone }}">{{ __t('message.tel', 'Tel.') }}</button>
            </p>
        </div>
        <h5>{{ __t('property.contact', 'Contact') }}</h5>
        <div class="contact-box-body">
            <form id="property-message">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" value="" placeholder="{{ __t('message.user-name', 'Name') }}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="phone" value="" placeholder="{{ __t('message.user-tel', 'Your phone number') }}">
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" value="" placeholder="{{ __t('message.user-email', 'E-mail') }}">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{ __t('property.message', 'Message') }}</label>
                    <textarea class="form-control" rows="5" name="message_body"><?php echo __dbconf('default_message', true); ?></textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="subscribe" value="yes"> {{ __t('property.send-newsletter', 'You want to receive our newsletter') }}
                    </label>
                </div>
                <p class="text-center">
                    <input type="hidden" name="poster_id" value="{{ $property->poster_id }}"/>
                    <input type="hidden" name="property_id" value="{{ $property->id }}"/>
                    <button type="button" class="btn btn-send" onclick="sendMessage();"><i class="fa fa-check"></i> {{ __t('button.submit', 'Submit') }}</button>
                </p>
            </form>
        </div>
    </div>
</div>