@extends('layouts.default')

@section('title', 'Page Title')

<?php $active = 'points'; ?>

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.condo-details')}}</li>

@endsection

@section('content')

<div class="container agent-container agent-search">

    <h1>Agents in Bangkok</h1>

    <div class="row search-filter-titles">
        <div class="col-md-2">
            <label>Name</label>
        </div>
        <div class="col-md-3">
            <label>Location</label>            
        </div>
        <div class="col-md-3">
            <label>Agent type</label>
        </div>
    </div>
    <div class="search-filters">
        <form action="/agent/search" method="get" class="row form-inline">
            <div class="col col-md-2">
                <input type="text" name="nam" value="{{ array_get($search, 'nam')}}" placeholder="Agent name" class="form-control"/>                
            </div>
            <div class="col col-md-2">
                <input type="text" name="loc" value="{{ array_get($search, 'loc')}}" placeholder="Location" class="form-control"/>
            </div>
            <div class="col col-md-1">
                <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
            </div>
            <div class="col col-md-3">
                <div class="agent-types">

                    <label <?php echo array_get($search, 'typ', 'all') == 'all' ? 'class="active"' : ''; ?> data-type="all">Both</label>

                    <label <?php echo array_get($search, 'typ') == \App\Models\User\Profile::FREELANCE_AGENT ? 'class="active"' : ''; ?>
                        data-type="{{ \App\Models\User\Profile::FREELANCE_AGENT }}">Freelance</label>

                    <label <?php echo array_get($search, 'typ') == \App\Models\User\Profile::COMPANY_AGENT ? 'class="active"' : ''; ?>
                        data-type="{{ \App\Models\User\Profile::COMPANY_AGENT }}">Agency</label>

                    <input type="hidden" id="typ" name="typ" value="{{ array_get($search, 'typ', 'all')}}"/>

                </div>
            </div>
        </form>
    </div>

    <h3>
        Freelance Agent Search Results
    </h3>

    <div class="row agent-list">
        <?php
        if (count($agents)) {

            foreach ($agents as $agent) {

                $user = \App\Models\User::find($agent->user_id);

                $profile = $user->getProfile();
                ?>
                <div class="col-md-4">
                    <div class="agent-cell">
                        <div class="rating">
                            <i class="fa fa-star active"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="agent-picture">
                            <a href="/agent/details/<?php echo $agent->user_id; ?>">
                                <img src="<?php echo $profile->getProfilePicture(); ?>"/>
                            </a>
                        </div>
                        <div class="agent-name">
                            <a href="/agent/details/<?php echo $agent->user_id; ?>"><?php echo $profile->getName(); ?></a>
                        </div>
                        <div class="row agent-stat">
                            <div class="col-sm-4 agent-status">
                                <?php
                                if ($agent->agent_verified) {
                                    ?>
                                    <span class="verified">
                                        <i class="fa fa-check"></i> Verified
                                    </span>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="col-sm-4 agent-posts">
                                <?php
                                $postings = $user->countProperty();
                                if ($postings > 1) {
                                    echo '(', $postings, ' postings)';
                                } else if ($postings == 1) {
                                    echo '(1 posting)';
                                } else {
                                    echo '(No posting)';
                                }
                                ?>
                            </div>
                            <div class="col-sm-4">

                            </div>
                        </div>
                        <div class="agent-info">
                            <p class="tags">
                                <em>Expert :</em> <?php echo $profile->getTags(); ?>
                            </p>
                            <p class="tags">
                                <em>Review :</em> <?php echo $profile->countReview(); ?> reviews
                            </p>
                            <p class="agent-intro">
                                <?php echo $profile->introduction; ?>
                            </p>
                        </div>
                        <a href="" class="btn btn-details">Contact</a>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <div class="agent-list-pagination">
        <?php echo $agents->appends($search)->links(); ?>
    </div>

</div>



@endsection

@section('scripts')
<script src="/js/search/agent.js"></script>
@endsection