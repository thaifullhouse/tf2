@extends('layouts.default')

@section('title', 'Page Title')

<link rel="stylesheet" type="text/css" href="/lib/bootstrap/dist/css/bootstrap.min.css">
<script src="/lib/angular/angular.js"></script>
<script src="/lib/angular-animate/angular-animate.js"></script>
<script src="/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="/lib/lodash/lodash.js"></script>
<script src="/lib/chart.js/dist/Chart.js"></script>
<script src="/lib/angular-chart.js/dist/angular-chart.js"></script>
<?php $active = 'points'; ?>

@section('breadcrumb')
<li><a href="/">{{ trans('nav.breadcrumb.home') }}</a></li>
<li><a href="/agent/search">{{ trans('nav.breadcrumb.agent') }}</a></li>
<li class="active">Agent Wonder</li>

@endsection

@section('content')

<div class="container" ng-app="myApp" ng-controller="myController">
    <div class="agent-header">
        <div class="agent-logo-container" ng-attr-back-img="@{{cover ? cover : undefined}}">
            <div class="agent-logo" ng-attr-back-img="@{{logo ? logo : undefined}}">
            </div>
            <div class="agent-circle" ng-attr-back-img="@{{profile ? profile : undefined}}">
            </div>
        </div>
        <div class="agent-title">
            <div class="agent-badge glyphicon glyphicon-user"></div>
            <span class="agent-name">@{{agent.name}}</span>
            <span ng-repeat="i in getNumber(agent.ranking) track by $index"><img class="agent-star" src="/img/star.png"></img></span>
            <span class="agent-posting">(Posting @{{agent.posts}})</span>
        </div>
        <div style="width:100%;" ng-if="verified">
            <div class="agent-verified">
                <div class="glyphicon glyphicon-ok"></div>Verified
            </div>
        </div>
        <div class="agent-details">
            <div class="agent-left-details">
                <p class="agent-detail-title">Agent Name&nbsp;<span class="single">: @{{agent.name}}</span></p>
                <p class="agent-detail-title">Agent ID&nbsp;<span class="single">: @{{agent.id}}</span></p>
                <p class="agent-detail-title">E-mail&nbsp;<span class="single">: @{{agent.email}}</span></p>
            </div>
            <div class="agent-right-details">
                <p class="agent-detail-title">Website&nbsp;<span class="single">: @{{agent.website}}</span></p>
                <p class="agent-detail-title">Language Ability&nbsp;<span class="single">: <span ng-repeat="lang in agent.langs">@{{lang}}@{{$last ? '' : ', '}}</span></span></p>
                <p class="agent-detail-title">Tel&nbsp;<span class="single">: @{{agent.tel}}</span></p>
            </div>
        </div>
        <div class="listing-details">
            <div class="listing-info">
                <uib-tabset active="0">
                    <uib-tab index="0" classes="listing-tab">
                        <uib-tab-heading>
                            <div class="listing-tab-title">Total Property Items</div>
                        </uib-tab-heading>
                        <div class="listing-tab-content" style="border-bottom:none;">
                            <div style="width:100%;">
                                <div style="height:380px;padding:30px 30px 30px 30px;" class="row">
                                    <div class="col-xs-6">
                                        <canvas id="doughnut" class="chart chart-doughnut" chart-data="data" chart-labels="labels" chart-colors="backgroundColor"></canvas>
                                    </div>
                                    <div style="display:flex;padding-left:20px;text-align:center;" class="col-xs-6">
                                        <div style="flex:1;">
                                            <div style="height:50%;">
                                                <img src="/img/apartment.png" style="width:50%;"></img><br/>
                                                <div style="display: inline-block;background-color:#cbff7a;border-radius:50%;width:20px;height:20px;margin:0 10px 0 -30px;vertical-align: middle;"></div>
                                                <div style="display: inline-block;vertical-align: middle;">Apartment</div>
                                                <br style="clear:both" />
                                                <div>@{{apartment}}</div>
                                            </div>
                                            <div style="height:50%;">
                                                <img src="/img/detachedhouse.png" style="width:50%;"></img><br/>
                                                <div style="display: inline-block;background-color:#179aed;border-radius:50%;width:20px;height:20px;margin:0 10px 0 -30px;vertical-align: middle;"></div>
                                                <div style="display: inline-block;vertical-align: middle;">Single House</div>
                                                <br style="clear:both" />
                                                <div>@{{house}}</div>
                                            </div>
                                        </div>
                                        <div style="flex:1;">
                                            <div style="height:50%;">
                                                <img src="/img/condo.png" style="width:50%;"></img><br/>
                                                <div style="display: inline-block;background-color:#ff5d83;border-radius:50%;width:20px;height:20px;margin:0 10px 0 -30px;vertical-align: middle;"></div>
                                                <div style="display: inline-block;vertical-align: middle;">Condominium</div>
                                                <br style="clear:both" />
                                                <div>@{{condominium}}</div>
                                            </div>
                                            <div style="height:50%;">
                                                <img src="/img/townhouse.png" style="width:50%;"></img><br/>
                                                <div style="display: inline-block;background-color:#93bcd6;border-radius:50%;width:20px;height:20px;margin:0 10px 0 -30px;vertical-align: middle;"></div>
                                                <div style="display: inline-block;vertical-align: middle;">Townhouse</div>
                                                <br style="clear:both" />
                                                <div>@{{townhouse}}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="listing-tab-content" style="border-top:none;border-bottom:none;">
                            <div style="width:100%;padding:30px 50px 30px 50px;">
                                <div style="display:flex;height:40px;">
                                    <div ng-style="{ flex: round(sale * 100 / (sale + rent))}" style="background-color:#d2f7ff;text-align:center;line-height:40px;">
                                        @{{round(sale * 100 / (sale + rent)) + '%'}}
                                    </div>
                                    <div ng-style="{ flex: round(rent * 100 / (sale + rent))}" style="background-color:#1191AE;text-align:center;color:white;line-height:40px;">
                                        @{{round(rent * 100 / (sale + rent)) + '%'}}
                                    </div>
                                </div>
                                <div style="float:left;">Sale</div><div style="float:right;">Rent</div>
                            </div>
                        </div>
                        <div class="listing-tab-content" style="border-top:none;">
                            <div style="width:100%;padding:30px 10px 30px 10px;">
                                <p style="margin-left:15px;color:gray;font-size:16px;">Search</p>
                                <div style="display:flex;background-color:#d2f7ff;text-align:center;height:40px;align-items: center; ">
                                    <div style="flex:1;">
                                        <select style="width:180px;height:26px;">
                                            <option>Type (Rent, Buy)</option>
                                        </select>
                                    </div>
                                    <div style="flex:1;">
                                        <select style="width:180px;height:26px;">
                                            <option>Property Type</option>
                                        </select>
                                    </div>
                                    <div style="flex:1;">
                                        <select style="width:180px;height:26px;">
                                            <option>Order</option>
                                        </select>
                                    </div>
                                </div>
                                <div style="text-align:right;margin:20px 20px 20px 20px;">
                                    <div style="float:right;margin-right:10px;">List</div>
                                    <div style="float:right;margin-right:5px;">
                                        <input type="radio" name="view" ng-model="search.type" value="list" />
                                    </div>
                                    <div style="float:right;margin-right:10px;">Map</div>
                                    <div style="float:right;margin-right:5px;">
                                        <input type="radio" name="view" ng-model="search.type" value="map" />
                                    </div>
                                    <div style="float:right;margin-right:10px;">Show on</div>
                                </div>
                                <div style="width:100%;margin-top:40px;padding:20px 10px 20px 10px;">
                                    <div ng-show="search.type == 'map'" style="height:637px;overflow-y:auto;">
                                        <img src="/img/mapview.png" style="width:100%"></img>
                                    </div>
                                    <div ng-show="search.type == 'list'" style="height:637px;overflow-y:auto;">
                                        <!--
                                        <div style="background-color:#4990E2;color:white;padding:10px;margin-bottom:15px;">
                                            <h1 style="font-size:24px;margin:0;margin-bottom:10px;">Spotlight Apartments</h1>
                                            <p style="text-align:center;padding:0 20px;font-size:13px;">NOW OPEN &amp; LEASING! New luxury building in Downtown NOLA. TWO-BEDROOM SPECIAL - $1000 off first month's rent when you sign a 12-month lease on a two-bedroom apartment!</p>
                                            <h2 style="font-size:22px;margin:5px auto;">The Beacon at South Market</h2>
                                            <p style="padding:0;font-size:13px;">1000 Girod Street, New Orleans, Louisiana 70113</p>
                                            <div style="display:flex">
                                                <div style="position:relative;flex:1;height:200px;" back-img="@{{search.img[search.index]}}">
                                                    <div style="z-index:2;position:absolute;height:100%;width:20%;top:0;left:0;cursor:pointer;" ng-click="decIndex()">
                                                        <img src="/img/left-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                    </div>
                                                    <div style="z-index:2;position:absolute;height:100%;width:20%;top:0;right:0;cursor:pointer;" ng-click="incIndex()">
                                                        <img src="/img/right-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                    </div>
                                                    <div style="z-index:3;text-align:center;position:absolute;height:35px;line-height:35px;width:35px;top:10px;left:10px;font-size:20px;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));border-radius:50%;padding-top:7px;padding-right:2px;cursor:pointer;" ng-click="favorite()" id="favorite">
                                                        <div class="glyphicon glyphicon-heart"></div>
                                                    </div>
                                                    <div style="z-index:1;text-align:center;position:absolute;height:30px;line-height:30px;width:60px;bottom:0;left:0;background:linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6))">
                                                        @{{(search.index + 1) + ' of ' + search.size}}
                                                    </div>
                                                </div>
                                                <div style="flex:1;height:200px;padding:10px;" back-img-blur="@{{search.img[0]}}">
                                                    <h2 style="font-size:22px;margin:auto;margin-bottom:10px;">From $1700</h2>
                                                    <div style="display:flex;font-size:13px;">
                                                        <p style="flex:18;margin:0;">1 Bed</p>
                                                        <p style="flex:32;margin:0;">| $1700 - $2320</p>
                                                        <p style="flex:50;margin:0;">| 4 units available now</p>
                                                    </div>
                                                    <div style="display:flex;font-size:13px;">
                                                        <p style="flex:18;margin:0;">2 Beds</p>
                                                        <p style="flex:32;margin:0;">| From $2375</p>
                                                        <p style="flex:50;margin:0;">| 9 units available now</p>
                                                    </div>
                                                    <div style="display:flex;font-size:13px;">
                                                        <p style="flex:18;margin:0;">3 Beds</p>
                                                        <p style="flex:32;margin:0;">| Please Call</p>
                                                        <p style="flex:50;margin:0;"></p>
                                                    </div>
                                                    <div style="font-size:13px;margin-top:5px;">
                                                        <p style="margin:0;">Current Rent Specials | Reserve Now</p>
                                                    </div>
                                                    <div style="text-align:right;font-size:10px;">
                                                        <p>Updated 2 days ago</p>
                                                    </div>
                                                    <hr style="border-bottom:1px solid white;margin:0;margin-bottom:10px;" />
                                                    <div style="display:flex;padding:0;">
                                                        <div style="flex:3;font-size:20px;font-weight:bold;text-align:center;">(504) 702-1661</div>
                                                        <div style="flex:2;font-size:14px;">
                                                            <button style="background-color:#4990E2;border:none;border-radius:20px;text-align:center;height:28px;">Check Availability</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        -->
                                        <div ng-repeat="property in properties">
                                            <div ng-init="property.index = 0" style="display:none;"></div>
                                            <h2 style="font-size:22px;margin:5px auto;">@{{property.property_name ? property.property_name : 'No title'}}</h2>
                                            <p style="padding:0;font-size:13px;">@{{property.details.address_en ? property.details.address_en : 'No address'}}</p>
                                            <div style="display:flex;color:white;margin-bottom:15px;">
                                                <div style="position:relative;flex:1;height:200px;" back-img="@{{property.images[property.index]}}">
                                                    <div ng-show="property.images.length > 0" style="z-index:2;position:absolute;height:100%;width:20%;top:0;left:0;cursor:pointer;" ng-click="decIndex($index)">
                                                        <img src="/img/left-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                    </div>
                                                    <div ng-show="property.images.length > 0" style="z-index:2;position:absolute;height:100%;width:20%;top:0;right:0;cursor:pointer;" ng-click="incIndex($index)">
                                                        <img src="/img/right-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                    </div>
                                                    <div style="z-index:3;text-align:center;position:absolute;height:35px;line-height:35px;width:35px;top:10px;left:10px;font-size:20px;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));border-radius:50%;padding-top:7px;padding-right:2px;cursor:pointer;" ng-click="favorite($index)" ng-attr-id="@{{'favorite' + $index}}">
                                                        <div class="glyphicon glyphicon-heart"></div>
                                                    </div>
                                                    <div style="z-index:1;text-align:center;position:absolute;height:30px;line-height:30px;width:60px;bottom:0;left:0;background:linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6))">
                                                        @{{property.images.length > 0 ? (property.index + 1) + ' of ' + property.images.length : 'No Image'}}
                                                    </div>
                                                </div>
                                                <div style="flex:1;height:200px;padding:10px;" back-img-blur="@{{property.images[((property.index + 1) % property.images.length)]}}">
                                                    <div style="margin-top:100px;font-size:13px;">
                                                        <p style="margin:0;">@{{property.property_name ? property.property_name : 'No title'}}</p>
                                                    </div>
                                                    <div style="display:flex;align-items: center;font-size:13px;width:100%;">
                                                        <p style="flex:42;margin:0 auto;font-size:28px;font-weight:bold;">&#3647; @{{property.details.listing_price ? property.details.listing_price : '-'}}</p>
                                                        <p style="flex:18;margin:0 auto;">| @{{property.details.bedroom_num ? property.details.bedroom_num + ' Beds' : '-'}}</p>
                                                        <p style="flex:18;margin:0 auto;"> @{{property.details.bedroom_num ? property.details.bedroom_num + ' Baths' : '-'}}</p>
                                                        <p style="flex:22;margin:0 auto;">| @{{property.details.total_size ? property.details.total_size + ' sqm' : '-'}}</p>
                                                    </div>
                                                    <div style="font-size:13px;">
                                                        <p style="">@{{property.details.address_en ? property.details.address_en : 'No address'}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--
                                        <h2 style="font-size:22px;margin:5px auto;">The Beacon at South Market<span style="padding:0;font-size:13px;margin-left:10px;">(French Quarter - CBD, Central Business District)</span></h2>
                                        <p style="padding:0;font-size:13px;">1000 Girod Street, New Orleans, Louisiana 70113</p>
                                        <div style="display:flex;color:white;margin-bottom:15px;">
                                            <div style="position:relative;flex:1;height:200px;" back-img="@{{search.img[search.index]}}">
                                                <div style="z-index:2;position:absolute;height:100%;width:20%;top:0;left:0;cursor:pointer;" ng-click="decIndex()">
                                                    <img src="/img/left-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                </div>
                                                <div style="z-index:2;position:absolute;height:100%;width:20%;top:0;right:0;cursor:pointer;" ng-click="incIndex()">
                                                    <img src="/img/right-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
                                                </div>
                                                <div style="z-index:3;text-align:center;position:absolute;height:35px;line-height:35px;width:35px;top:10px;left:10px;font-size:20px;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));border-radius:50%;padding-top:7px;padding-right:2px;cursor:pointer;" ng-click="favorite3()" id="favorite3">
                                                    <div class="glyphicon glyphicon-heart"></div>
                                                </div>
                                                <div style="z-index:1;text-align:center;position:absolute;height:30px;line-height:30px;width:60px;bottom:0;left:0;background:linear-gradient(rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0.6))">
                                                    @{{(search.index + 1) + ' of ' + search.size}}
                                                </div>
                                            </div>
                                            <div style="flex:1;height:200px;padding:10px;" back-img-blur="@{{search.img[0]}}">
                                                <h2 style="font-size:22px;margin:auto;margin-bottom:10px;">From $1700</h2>
                                                <div style="display:flex;font-size:13px;">
                                                    <p style="flex:18;margin:0;">1 Bed</p>
                                                    <p style="flex:32;margin:0;">| $1700 - $2320</p>
                                                    <p style="flex:50;margin:0;">| 4 units available now</p>
                                                </div>
                                                <div style="display:flex;font-size:13px;">
                                                    <p style="flex:18;margin:0;">2 Beds</p>
                                                    <p style="flex:32;margin:0;">| From $2375</p>
                                                    <p style="flex:50;margin:0;">| 9 units available now</p>
                                                </div>
                                                <div style="display:flex;font-size:13px;">
                                                    <p style="flex:18;margin:0;">3 Beds</p>
                                                    <p style="flex:32;margin:0;">| Please Call</p>
                                                    <p style="flex:50;margin:0;"></p>
                                                </div>
                                                <div style="font-size:13px;margin-top:5px;">
                                                    <p style="margin:0;">Current Rent Specials | Reserve Now</p>
                                                </div>
                                                <div style="text-align:right;font-size:10px;">
                                                    <p>Updated 2 days ago</p>
                                                </div>
                                                <hr style="border-bottom:1px solid white;margin:0;margin-bottom:10px;" />
                                                <div style="display:flex;padding:0;">
                                                    <div style="flex:3;font-size:20px;font-weight:bold;text-align:center;">(504) 702-1661</div>
                                                    <div style="flex:2;font-size:14px;">
                                                        <button style="background-color:#4990E2;border:none;border-radius:20px;text-align:center;height:28px;">Check Availability</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </uib-tab>
                    <uib-tab index="1" classes="listing-tab">
                        <uib-tab-heading>
                            <div class="listing-tab-title">Review</div>
                        </uib-tab-heading>
                        <div class="listing-tab-content" style="border-bottom:none;" id="reply-ratings">
                            <div style="width: 100%;padding: 20px 5px 0px 5px;">
                                <div class="agent-comment" ng-repeat="comment in comments.slice(((currentPage - 1) * itemsPerPage), (((currentPage - 1) * itemsPerPage) + itemsPerPage))">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <div style="width:80%;padding: 40% 0;" back-img="@{{comment.user.image}}"></div>
                                        </div>
                                        <div class="col-xs-10">
                                            <div class="commenter-email">
                                                @{{comment.user.name}}
                                            </div>
                                            <div>
                                                <fieldset class="rating-blue-passive">
                                                        <input type="radio" disabled ng-checked="comment.rating == 10" ng-attr-id="@{{$index + 'star51'}}" ng-attr-name="@{{$index + 'rating1'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star51'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 9" ng-attr-id="@{{$index + 'star41half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star41half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 8" ng-attr-id="@{{$index + 'star41'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star41'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 7" ng-attr-id="@{{$index + 'star31half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star31half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 6" ng-attr-id="@{{$index + 'star31'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star31'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 5" ng-attr-id="@{{$index + 'star21half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star21half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 4" ng-attr-id="@{{$index + 'star21'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star21'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 3" ng-attr-id="@{{$index + 'star11half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star11half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 2" ng-attr-id="@{{$index + 'star11'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star11'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.rating == 1" ng-attr-id="@{{$index + 'starhalf1'}}" ng-attr-name="@{{$index + 'rating1'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf1'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                                <div style="margin-top: 7px;margin-left: 145px;">(2 days ago)</div>
                                            <div style="margin:20px 0 20px 0;">
                                                @{{comment.user.message}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="comments-end" style="display: table;margin: 20px auto;" items-per-page="itemsPerPage" uib-pagination boundary-links="true" max-size="maxSize" rotate="true" total-items="totalItems" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                            </div>
                        </div>
                        <div class="listing-tab-content" style="border-top:none;">
                            <div style="width:100%;">
                                <p style="display:table;margin: 30px 30px 10px 10px;text-transform:uppercase;font-weight:bold;font-size:17px;">Write Comments</p>
                                <div style="margin: 0px 10px 20px 10px;background-color:#F6FDFF;padding: 20px 20px 0px 20px;" class="agent-commenting">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div>Give us your recommendation</div>
                                        </div>
                                        <div class="col-xs-8">
                                            <div style="margin-top:-8px;margin-left:-55px;">
                                                <fieldset class="rating-blue">
                                                    <input type="radio" ng-click="reply.rating = 10" id="star51" name="rating1" value="5" /><label class = "full" for="star51" title="Awesome - 5 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 9" id="star41half" name="rating1" value="4 and a half" /><label class="half" for="star41half" title="Pretty good - 4.5 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 8" id="star41" name="rating1" value="4" /><label class = "full" for="star41" title="Pretty good - 4 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 7" id="star31half" name="rating1" value="3 and a half" /><label class="half" for="star31half" title="Meh - 3.5 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 6" id="star31" name="rating1" value="3" /><label class = "full" for="star31" title="Meh - 3 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 5" id="star21half" name="rating1" value="2 and a half" /><label class="half" for="star21half" title="Kinda bad - 2.5 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 4" id="star21" name="rating1" value="2" /><label class = "full" for="star21" title="Kinda bad - 2 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 3" id="star11half" name="rating1" value="1 and a half" /><label class="half" for="star11half" title="Meh - 1.5 stars"></label>
                                                    <input type="radio" ng-click="reply.rating = 2" id="star11" name="rating1" value="1" /><label class = "full" for="star11" title="Sucks big time - 1 star"></label>
                                                    <input type="radio" ng-click="reply.rating = 1" id="starhalf1" name="rating1" value="half" /><label class="half" for="starhalf1" title="Sucks big time - 0.5 stars"></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <p>Describe in detail your experience with this agent</p>
                                    <textarea placeholder="Your comment" style="resize:none;width:400px;height:160px;" ng-model="reply.user.message"></textarea>
                                    <br/>
                                    <button class="btn" style="margin-left:250px;width:150px;background-color:#9d9d9d;" ng-click="sendReview()">Send The Review</button>
                                </div>
                            </div>
                        </div>
                    </uib-tab>
                </uib-tabset>
            </div>
            <div class="agent-contact">
                <p>Contact</p>
                <form name="form">
                    <label for="name">Name</label><input name="name" type="text" placeholder="Name" ng-model="contact.name" />
                    <label for="tel">Tel.</label><input name="tel" type="tel" placeholder="Telephone" ng-model="contact.phone" />
                    <label for="email">E-mail</label><input name="email" type="email" placeholder="E - mail" ng-model="contact.email" />
                    <label for="message">Message</label><textarea name="message" placeholder="Your message" ng-model="contact.message" ></textarea>
                    <input class="news" name="news" type="checkbox" /><label class="news" for="news" ng-model="contact.newsletter">You want us to send News Letters</label>
                    <button class="submit btn btn-primary" name="submit" type="submit" ng-click="submitMessage()"><i class="glyphicon glyphicon-ok"></i>Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="/agent.js"></script>

@endsection