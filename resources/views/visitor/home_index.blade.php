@extends('layouts.default')

@section('title', 'Page Title')

<?php $active = 'points'; ?>

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')
    <div class="big-banner-container">
        <img class="" src="/samples/zillow.jpg"/>
        <div class="container options-container">
            <p class="home-search-options text-center">
                <a href="#" class="active">BUY</a>
                <a href="#">RENT</a>
                <a href="#">NEW PROJECT</a>
                <a href="#">CONDO</a>
            </p>
            <form class="home-search-options">
                <div class="input-group">
                    <input type="text" class="form-control input-lg" id="search" name="search" value="" placeholder="Enter a neighborhood, city, address or ZIP code">
                    <span class="input-group-btn">
                        <button class="btn btn-blue btn-lg" type="button">SEARCH</button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="container">
        <div class="home-transportation-container">
            <a href="/search?tr=bts" class="bts"><img src="/img/bts.jpg"></a>
            <a href="/search?tr=mrt" class="mrt"><img src="/img/mrt.png"></a>
            <a href="/search?tr=arl" class="apl"><img src="/img/apl.jpg"></a>
        </div>
    </div>

@endsection

@section('scripts')
<link href="/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/js/home.js"></script>
@endsection
