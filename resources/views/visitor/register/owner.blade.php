@extends('layouts.default')

@section('breadcrumb')
<li><a href="/">{{ __t('visitor_breadcrumb.home', 'Home') }}</a></li>
<li>{{ __t('visitor_breadcrumb.register', 'Register') }}</li>
<li class="active">{{ __t('visitor_breadcrumb.register.owner', 'Owner') }}</li>
@endsection

@section('content')

<div class="container registration-container">

    <div class="row">
        <div class="col-md-12">
            <h1 class="registration-title">{{ __t('visitor_registration.register-title', 'Register') }}</h1>
        </div>
    </div>

    <div class="registration-banner owner-banner">
        <h1>{{ __t('visitor_registration.register.owner-title', 'FOR OWNER') }}</h1>
        <p>
            {{ __t('visitor_registration.register.owner-description', 'By simple registration, you can start listing your property free of charge.') }}
        </p>
    </div>

    <form action="/auth/register/owner" method="post">

        <div class="row registration-form-container registration-form">

            @if (count($errors) > 0)
            <div class="col-md-offset-2 col-md-8">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>        
            @endif

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="firstname">{{ __t('visitor_registration.owner.firstname', 'First name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">{{ __t('visitor_registration.owner.lastname', 'Last name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="phone">{{ __t('visitor_registration.owner.telephone', 'Tel.') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile">{{ __t('visitor_registration.owner.mobile', 'Mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ __t('visitor_registration.owner.line-id', 'Line ID') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>
            
        </div>
        <div class="row registration-form with-marging-top">
            
            <div class="col-md-offset-2 col-md-8 with-marging-bottom">
                <h4>{{ __t('visitor_registration.owner.address-title', 'Address') }}</h4>
            </div>
            
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="street_no">{{ __t('visitor_registration.owner.street-no', 'No. / Building') }}</label>
                    <input type="text" class="form-control" name="street_no" value="{{ old('street_no') }}" placeholder="">
                </div>
            </div>
            
            <div class="col-md-4">
                <div class="form-group">
                    <label for="street_name">{{ __t('visitor_registration.owner.road', 'Road') }}</label>
                    <input type="text" class="form-control" name="street_name" value="{{ old('street_name') }}" placeholder="">
                </div>
            </div>

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="area">{{ __t('visitor_registration.owner.area', 'Area') }}</label>
                    <input type="text" class="form-control" name="area" value="{{ old('area') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="district">{{ __t('visitor_registration.owner.district', 'District') }}</label>
                    <input type="text" class="form-control" name="district" value="{{ old('district') }}" placeholder="">
                </div>
            </div>

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="province">{{ __t('visitor_registration.owner.province', 'Province') }}</label>
                    <input type="text" class="form-control" name="province" value="{{ old('province') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="postal_code">{{ __t('visitor_registration.owner.postal-code', 'Postal Code') }}</label>
                    <input type="text" class="form-control" name="postal_code" value="{{ old('postal_code') }}" placeholder="">
                </div>
            </div>

        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="email">{{ __t('visitor_registration.owner.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="password">{{ __t('visitor_registration.owner.password', 'Password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_confirmation">{{ __t('visitor_registration.owner.password-confirmation', 'Re-password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" value=""  placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-3 col-md-6 text-center">
                <div class="checkbox accept-registration-condition">
                    <label>
                        <input type="checkbox" class="accept_condition" name="accept_condition" value="yes"> 
                        {!! __t('visitor_registration.owner.password-confirmation'
                        , 'I am the owner or an authorized person for the property that is listed in this website<br/>
                        * Otherwise, please acknowledge that your listing shall be removed from the platform.') !!}

                    </label>
                </div>
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="submit" id="submit-registration" class="btn btn-gray" disabled>{{ __t('visitor_registration.owner.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>
</div>

@endsection

@section('scripts')
<script>
    $(function () {
        $('.accept_condition').on('click', function () {
            if ($(this).is(':checked')) {
                $('#submit-registration').prop('disabled', false);
            }
            else {
                $('#submit-registration').prop('disabled', true);
            }
        });
    });
</script>
@endsection