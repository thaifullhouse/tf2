@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row free-listing-hero">
        <div class="col-md-6 col-md-offset-3">
            <h3>Who are you ?</h3>
            <form id="usertype-form" action="/auth/register/type" method="post">
                <div class="radio">
                    <label class="user-type-owner">
                        <input type="radio" name="utp" value="owner" class="user-type-selector">
                        I am an owner
                    </label>
                </div>
                <div class="radio">
                    <label class="user-type-agent">
                        <input type="radio" name="utp" value="freelance" class="user-type-selector">
                        I am a Freelance Agent
                    </label>
                </div>
                <div class="radio">
                    <label class="user-type-realestate">
                        <input type="radio" name="utp" value="realestate" class="user-type-selector">
                        I am an Real Estate Company Agent
                    </label>
                </div>

                <div class="radio">
                    <label class="user-type-developer">
                        <input type="radio" name="utp" value="developer" class="user-type-selector">
                        I am a land developer
                    </label>
                </div>
                {{ csrf_field() }}
                <button type="button" id="submit-user-type" class="btn btn-primary pull-right">NEXT <i class="fa fa-angle-double-right"></i></button>
            </form>
        </div>
    </div>


</div>

@endsection

@section('scripts')

<script>

    $(function () {
        var userType = null;

        $('.user-type-selector').on('click', function () {
            userType = $(this).val();
        });

        $('#submit-user-type').on('click', function () {
            if (userType) {
                $('#usertype-form').submit();
            }
            else {
                alert('Please select you profile');
            }
        });
    });



</script>

@endsection