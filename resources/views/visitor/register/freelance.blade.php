@extends('layouts.default')

@section('breadcrumb')
<li><a href="/">{{ __t('visitor_breadcrumb.home', 'Home') }}</a></li>
<li>{{ __t('visitor_breadcrumb.register', 'Register') }}</li>
<li class="active">{{ __t('visitor_breadcrumb.register.freelance-agent', 'Agent') }}</li>
@endsection

@section('content')

<div class="container registration-container">
    
    <div class="row">
        <div class="col-md-12">
            <h1 class="registration-title">{{ __t('visitor_registration.register-title', 'Register') }}</h1>
        </div>
    </div>
    
    <div class="registration-banner agent-banner">
        <h1>{{ __t('visitor_registration.register.freelance-title', 'FOR FREELANCE AGENT') }}</h1>
        <p>
            {{ __t('visitor_registration.register.freelance-description', 'By simple registration, you can start listing your property free of charge.') }}            
        </p>
    </div>

    <form action="/auth/register/agent" method="post" enctype="multipart/form-data">

        <div class="row registration-form-container registration-form">

            @if (count($errors) > 0)
            <div class="col-md-offset-2 col-md-8">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>        
            @endif

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="firstname">{{ __t('visitor_registration.freelance.firstname', 'First name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">{{ __t('visitor_registration.freelance.lastname', 'Last name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="phone">{{ __t('visitor_registration.freelance.telephone', 'Tel.') }} </label>
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile">{{ __t('visitor_registration.freelance.mobile', 'Mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ __t('visitor_registration.freelance.line-id', 'Line ID') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="photo_file">{{ __t('visitor_registration.freelance.upload-photo', 'Upload photo') }} <span class="text-danger">*</span></label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="citizen_id_preview" placeholder="{{ __t('visitor_registration.freelance.upload-placeholder', 'Select a file ...') }}" disabled>
                        <span class="input-group-btn">
                            <label title="Upload image file" for="photo_file" class="btn btn-default upload">
                                <input type="file" accept="image/*" name="photo_file" id="photo_file" class="hide uploader">
                                upload
                            </label>
                        </span>
                    </div>
                </div>
                <p>
                    {!! __t('visitor_registration.freelance.upload-photo-description', 
                                '* Please upload your profile picture to get verified mark, this mark will give the customer confidence in your identity.') !!}
                    
                </p>
            </div>
        </div>

        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="email">{{ __t('visitor_registration.freelance.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="password">{{ __t('visitor_registration.freelance.password', 'Password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_confirmation">{{ __t('visitor_registration.freelance.password-confirmation', 'Re-password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-3 col-md-6 text-center">
                
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-gray">{{ __t('visitor_registration.freelance.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>

</div>



@endsection


@section('scripts')

<script>

    $('.uploader').on('change', function () {
        if (this.files.length > 0) {
            var file = this.files[0];
            $('#citizen_id_preview').val(file.name);
        }
    });

</script>

@endsection
