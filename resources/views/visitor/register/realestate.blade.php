@extends('layouts.default')

@section('breadcrumb')
<li><a href="/">{{ __t('visitor_breadcrumb.home', 'Home') }}</a></li>
<li>{{ __t('visitor_breadcrumb.register', 'Register') }}</li>
<li class="active">{{ __t('visitor_breadcrumb.register.realestate-agent', 'Real Estate Agent') }}</li>
@endsection

@section('content')

<div class="container registration-container">
    
    <div class="row">
        <div class="col-md-12">
            <h1 class="registration-title">{{ __t('visitor_registration.register-title', 'Register') }}</h1>
        </div>
    </div>
    
    <div class="registration-banner realestate-banner">
        <h1>{{ __t('visitor_registration.register.realestate-title', 'For real estate company') }}</h1>
        <p>{{ __t('visitor_registration.register.realestate-description', 'By simple registration, you can start listing your property free of charge.') }}</p>
    </div>

    <form action="/auth/register/company" method="post">

        <div class="row registration-form-container registration-form">

            @if (count($errors) > 0)
            <div class="col-md-offset-2 col-md-8">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>        
            @endif

            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="company_name">{{ __t('visitor_registration.realestate.company-name', 'Company name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="company_registration">{{ __t('visitor_registration.realestate.company-registration', 'Company registration') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="company_registration" value="{{ old('company_registration') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="firstname">{{ __t('visitor_registration.realestate.firstname', 'Firstname') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="firstname" value="{{ old('firstname') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="lastname">{{ __t('visitor_registration.realestate.lastname', 'Lastname') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="lastname" value="{{ old('lastname') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="phone">{{ __t('visitor_registration.realestate.telephone', 'Tel.') }}</label>
                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="mobile">{{ __t('visitor_registration.realestate.mobile', 'Mobile') }}</label>
                    <input type="text" class="form-control" name="mobile" value="{{ old('mobile') }}" placeholder="">
                </div>
            </div>
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="line_id">{{ __t('visitor_registration.realestate.lineid', 'Line ID') }}</label>
                    <input type="text" class="form-control" name="line_id" value="{{ old('line_id') }}" placeholder="">
                </div>
            </div>
        </div>
        

        <div class="row registration-form with-marging-top">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="email">{{ __t('visitor_registration.realestate.email', 'ID (E-mail)') }} <span class="text-danger">*</span></label>
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form">
            <div class="col-md-offset-2 col-md-4">
                <div class="form-group">
                    <label for="password">{{ __t('visitor_registration.realestate.password', 'Password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password" value="" placeholder="">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="password_confirmation">{{ __t('visitor_registration.realestate.password-confirm', 'Re-password') }} <span class="text-danger">*</span></label>
                    <input type="password" class="form-control" name="password_confirmation" value="" placeholder="">
                </div>
            </div>
        </div>
        <div class="row registration-form with-marging-top  register-action">
            <div class="col-md-offset-3 col-md-6 text-center">
                <div class="checkbox accept-registration-condition">
<!--                    <label>
                        <input type="checkbox" name="accept_condition" value="yes"> I am the owner or an authorized person for the property that is listed in this website<br/>
                        * Otherwise, please acknowledge that your listing shall be removed from the platform.
                    </label>-->
                </div>
            </div>
            <div class="col-md-offset-3 col-md-6 text-center with-marging-top">
                {{ csrf_field() }}
                <button type="submit" class="btn btn-gray">{{ __t('visitor_registration.realestate.button-next', 'NEXT') }}</button>
            </div>
        </div>
    </form>
</div>



@endsection

@section('scripts')

<script>

    $('.uploader').on('change', function () {
        if (this.files.length > 0) {
            var file = this.files[0];
            $('#citizen_id_preview').val(file.name);
        }
    });

</script>

@endsection