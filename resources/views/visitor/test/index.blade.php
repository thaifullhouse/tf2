@extends('layouts.default')

@section('title', 'Page Title')

<?php $active = 'points'; ?>

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.condo-details') }}</li>

@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <form id="" action="/rest/condo/review" method="post">
                <div class="form-group">
                    <label for="review">Review</label>
                    <input type="text" class="form-control" id="review" name="review" value="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="condo_id">Condo_id</label>
                    <input type="text" class="form-control" id="condo_id" name="condo_id" value="" placeholder="">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" name="images" id="exampleInputFile">
                    <p class="help-block">Example block-level help text here.</p>
                </div>
                <button type="button" id="submit-condo-review" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>

    var submitReview = function () {
        if (userIsOnline) {
            postCondoReview({review: 'sdsdfsdfsdf', 'condo_id': 1});
        }
    };

    var postCondoReview = function (data, results, error) {

        if (userIsOnline) {
            var xhr = $.post('/rest/condo/review', data, function (response, status, xhr) {

            });
        } else {

        }
    };

    $(document).ready(function () {
        $('#submit-condo-review').on('click', function () {
            submitReview();
        });
    });

</script>

@endsection