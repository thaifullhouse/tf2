@extends('layouts.default')

@section('title', 'Page Title')

<link rel="stylesheet" type="text/css" href="/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/lib/angularjs-slider/dist/rzslider.min.css">
<script src="https://maps.google.com/maps/api/js?key=AIzaSyC9efZV5sak_Geyb6y7UMSfAaNt7fFUcfM&libraries=places"></script>
<script src="/lib/angular/angular.js"></script>
<script src="/lib/angular-animate/angular-animate.js"></script>
<script src="/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="/lib/ngmap/build/scripts/ng-map.js"></script>
<script src="/lib/lodash/lodash.js"></script>
<script src="/lib/angularjs-slider/dist/rzslider.js"></script>
<script src="/lib/chart.js/dist/Chart.js"></script>
<script src="/lib/angular-chart.js/dist/angular-chart.js"></script>
<?php $active = 'points'; ?>

@section('breadcrumb')

<!-- <li class="active">{{ trans('nav.breadcrumb.condo-details') }}</li> -->
<div>Home >> Condominium >> condo_name</div>
<div class="title-link"></div>

@endsection

@section('content')

<div class="container" ng-app="myApp" ng-controller="myController">
    <div class="listing-header">
        <button class="btn btn-primary back-button"><< BACK</button>
        <div class="listing-header-title">
            <h3>@{{condo.name ? condo.name : '-'}}</h3>
            <span>Start Price: &#3647; @{{condo.salesPrice ? beautify(condo.salesPrice) : '-'}}</span>
        </div>
        <div class="ranking">
            <h4>RANKING <img src="/img/star.png" class="ranking-star" ng-repeat="i in getNumber(condo.ranking ? condo.ranking : 0) track by $index"></img></h4>
        </div>     
    </div>
    <div style="height:400px;width:100%;" back-img="@{{condo.images[0]}}">
    </div>
    <div class="listing-highlights">
        <div class="listing-highlight">
        <img src="/img/money.png"></img>
        </br>
        </br>
        <p>Sales Price Start From</p>
        <h4>@{{condo.salesPrice ? formatPrice(condo.salesPrice) : '-'}}</h4>        
        </div>
        <div class="listing-highlight">
        <img src="/img/circular-clock.png"></img>
        </br>
        </br>
        <p>Office Operating Hours</p>
        <h4>@{{condo.hours ? condo.hours : '-'}}</h4>  
        </div>
        <div class="listing-highlight">
        <img src="/img/crane.png"></img>
        </br>
        </br>
        <p>Year Built</p>
        <h4>@{{condo.built ? condo.built : '-'}}</h4>
        </div>
        <div class="listing-highlight">
        <img src="/img/building-3.png"></img>
        </br>
        </br>
        <p>Tower</p>
        <h4>@{{condo.tower ? condo.tower : '-'}}</h4>
        </div>
        <div class="listing-highlight">
        <img src="/img/lift.png"></img>
        </br>
        </br>
        <p>Floor</p>
        <h4>@{{condo.floor ? condo.floor : '-'}}</h4>
        </div>
    </div>
    <div class="listing-details">
        <div class="listing-info">
            <uib-tabset active="0">
                <uib-tab index="0" classes="listing-tab">
                    <uib-tab-heading>
                        <div class="listing-tab-title">Information</div>
                    </uib-tab-heading>
                    <div class="listing-tab-content" style="border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Total Units</p>
                            <p>Room Type</p>
                            <p>Parking Space</p>
                            <p>Website</p>
                        </div>
                        <div class="content-titles2" style="border-bottom: 1px solid gray;">
                            <p>@{{condo.totalUnits ? condo.totalUnits : '-'}}</p>
                            <p>@{{condo.roomType ? condo.roomType : '-'}}</p>
                            <p>@{{condo.parkingSpace ? condo.parkingSpace : '-'}}</p>
                            <p>@{{condo.website ? condo.website : '-'}}</p>
                        </div>
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Total Floors</p>
                            <p>District</p>
                            <p>Contact</p>
                        </div>
                        <div class="content-titles2" style="border-bottom: 1px solid gray;">
                            <p>@{{condo.totalFloors ? condo.totalFloors : '-'}}</p>
                            <p>@{{condo.district ? condo.district : '-'}}</p>
                            <p>@{{condo.contact ? condo.contact : '-'}}</p>
                        </div>
                    </div>
                    <div class="listing-tab-content" style="border-top: none; border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Facilities</p>
                        </div>
                        <div class="content-facilities" style="border-bottom: 1px solid gray;">
                            <div class="facility-line">
                                <div class="facility" ng-if="condo.facilities.gym">
                                    <img src="/img/gym.png"></img>
                                    <p>Gym</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.pool">
                                    <img src="/img/gym.png"></img>
                                    <p>Swimming Pool</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.sauna">
                                    <img src="/img/gym.png"></img>
                                    <p>Sauna</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.playground">
                                    <img src="/img/gym.png"></img>
                                    <p>Playground</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.garden">
                                    <img src="/img/gym.png"></img>
                                    <p>Garden</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.readingRoom">
                                    <img src="/img/gym.png"></img>
                                    <p>Reading Room</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.elevator">
                                    <img src="/img/gym.png"></img>
                                    <p>Elevator</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.security">
                                    <img src="/img/gym.png"></img>
                                    <p>24 Security</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.cctv">
                                    <img src="/img/gym.png"></img>
                                    <p>CCTV</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.parking">
                                    <img src="/img/gym.png"></img>
                                    <p>Parking</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.supermarket">
                                    <img src="/img/gym.png"></img>
                                    <p>Supermarket</p>
                                </div>
                                <div class="facility" ng-if="condo.facilities.laundry">
                                    <img src="/img/gym.png"></img>
                                    <p>Laundry</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Details</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <p>@{{condo.details ? condo.details : '-'}}</p>
                        </div>                          
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Address</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <p>@{{condo.address ? condo.address : '-'}}</p>
                        </div>                          
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Map / Street View</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <div id="map" class="map-container" map-lazy-load="https://maps.google.com/maps/api/js" map-lazy-load-params="@{{googleMapsUrl}}">
                                <ng-map class="map" style="height: 100%;width:100%;" zoom="12" center="@{{condo.location.lat + ', ' + condo.location.lng}}">
                                </ng-map>
                            </div>
                            <div ng-repeat="(locKey, locValue) in locations" style="display:block;margin:20px;height:40px;">
                                <div style="inline-block;float:left;width:130px;height:40px;text-transform:capitalize;background-color:#6BA9B6;color:white;text-align: center;vertical-align: middle;line-height:40px;">
                                    @{{locKey + ' (Total ' + locations[locKey].length + ' )'}}
                                </div>
                                <select style="inline-block;float:left;width:calc(100% - 130px);height:40px;padding-left:20px;" ng-model="selectedItem[$index]" ng-change="goToLocation(selectedItem[$index])">
                                    <option value="">---Please select---</option>
                                    <option ng-repeat="(key, value) in locations[locKey]" value="@{{value.lat + ' ' + value.lng}}">@{{($index + 1) + '. ' + value.name}}</option>
                                </select>
                            </div>
                        </div>                          
                    </div> 
                    <div class="listing-tab-content" style="border-top: none;">
                        <div class="content-mortgage">
                            <p>Mortgage Payment Calculator</p>
                            <div class="mortgage-calculator">
                                <div class="mortgage-result">
                                    <canvas id="doughnut" class="chart chart-doughnut" chart-data="data" chart-labels="labels" chart-colors="backgroundColor"></canvas>
                                    <div style="position: absolute;
                                                border-radius: 50%;
                                                top: 82px;
                                                left: 82px;
                                                background: #e8fbff;
                                                width: 164px;
                                                height: 164px;
                                                display: block;
                                                clip: auto;
                                                padding: 40px 20px 30px 20px;
                                                box-shadow: 0px 0px 10px 3px rgba(0,0,0,0.5);">
                                        <div style="word-wrap: break-word;text-align: center;">
                                            <p style="font-weight: 700;font-size: 13px;">Your payment per month</p>
                                            <!-- payment per month logic -->
                                            <p style="font-weight: 700;font-size: 17px;">&#3647; @{{beautify(monthlyPayment)}}</p>
                                        </div>
                                    </div>
                                    <div style="position:absolute; bottom: 24px; left: 0px; display: table;">
                                        <div style="background-color: #343f41;width: 18px;display: table-cell;"></div><div style="display: table-cell;color: #36bedc;font-weight: 700;font-size: 13px;padding-left:10px;">Loan Amount</div>
                                    </div>
                                    <div style="position:absolute; bottom: 0px; left: 0px; display: table;">
                                        <div style="background-color: #36bedc;width: 18px;display: table-cell;"></div><div style="display: table-cell;color: #36bedc;font-weight: 700;font-size: 13px;padding-left:10px;">Total Amount</div>
                                    </div>
                                </div>
                                <div class="mortgage-options">
                                    <p style="float: left;">Principal Price</p>
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(condo.principlePrice)}}</div>
                                    <div style="clear: both;"></div>
                                    <!--
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(slider.price.value)}}</div>
                                    <rzslider rz-slider-model="slider.price.value" rz-slider-options="slider.price.options"></rzslider>
                                    -->
                                    <p style="float: left;">Loan Amount</p>
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(slider.loan.value)}}</div>
                                    <rzslider rz-slider-model="slider.loan.value" rz-slider-options="slider.loan.options"></rzslider>
                                    <p style="float: left;">Rate (ARPR)</p>
                                    <div class="mortgage-label" style="float: right;">@{{parseInt(slider.rate.value * 100) + '%'}}</div>
                                    <rzslider rz-slider-model="slider.rate.value" rz-slider-options="slider.rate.options"></rzslider>
                                    <p style="float: left;">Term</p>
                                    <div class="mortgage-label" style="float: right;">@{{slider.term.value}} year@{{(slider.term.value === 1 ? '' : 's')}}</div>
                                    <rzslider rz-slider-model="slider.term.value" rz-slider-options="slider.term.options"></rzslider>
                                    <p style="float: left;margin-top: 14px;">Total Amount</p>
                                    <div class="mortgage-label mortgage-total">&#3647; @{{beautify(totalAmount)}}</div>
                                </div>
                            </div>
                        </div>                          
                    </div>
                </uib-tab>
                <uib-tab index="1" classes="listing-tab">
                    <uib-tab-heading>
                        <div class="listing-tab-title">Review</div>
                    </uib-tab-heading>
                    <div class="listing-tab-content" style="display:inherit;padding: 30px 30px 0px 30px;">
                        <div class="content-review">
                            <p>How to get there</p>
                            <div back-img="@{{condo.review.howToGetThere.images[0]}}" style="margin-bottom:10px;height:400px;"></div>
                            <div class="details">@{{condo.review.howToGetThere.details}}</div>
                        </div>
                        <div class="content-review">
                            <p>Room Type</p>
                            <div back-img="@{{condo.review.roomType.images[0]}}" style="margin-bottom:10px;height:400px;"></div>
                            <div class="details">@{{condo.review.roomType.details}}</div>
                        </div>
                        <div class="content-review">
                            <p>Facilities &amp; Landscape</p>
                            <div back-img="@{{condo.review.facilities.images[0]}}" style="margin-bottom:10px;height:400px;"></div>
                            <div class="details">@{{condo.review.facilities.details}}</div>
                        </div>
                        <div class="content-review">
                            <p>Floor Plans</p>
                            <div back-img="@{{condo.review.floorPlan.images[0]}}" style="margin-bottom:10px;height:400px;"></div>
                            <div class="details">@{{condo.review.floorPlan.details}}</div>
                        </div>
                        <div class="content-review">
                            <p>Project Video</p>
                            <div class="video-container">
                                <iframe src="@{{condo.review.video.url | trustUrl}}" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="content-review">
                            <p>Overview</p>
                            <div class="details">@{{condo.review.overview.details}}</div>
                        </div>
                    </div>
                </uib-tab>
            </uib-tabset>
            <div class="listing-tab-content" style="border: none;" ng-show="condo.comments.totalItems > 0" id="comments">
                <div style="width: 100%;">
                    <div style="display: table;margin: 20 auto 20 auto;" items-per-page="condo.comments.itemsPerPage" uib-pagination boundary-links="true" max-size="condo.comments.maxSize" rotate="true" total-items="condo.comments.totalItems" ng-model="condo.comments.currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                    <div style="margin: 0 5px 0 5px;">
                        <div ng-repeat="comment in condo.comments.commentList.slice(((condo.comments.currentPage - 1) * condo.comments.itemsPerPage), (((condo.comments.currentPage - 1) * condo.comments.itemsPerPage) + condo.comments.itemsPerPage)) | orderBy:'id'">
                            <div class="comment-box">
                                <div class="reply-no"># Reply @{{((condo.comments.currentPage - 1) * condo.comments.itemsPerPage) + $index + 1}}</div>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div style="padding: 10px 10px 10px 10px;">
                                            <div style="width:200px;height:60px;background-color:black;text-align:center;color:white;">
                                                <div ng-style="{ width: comment.likes == 0 ? '50%' : parseInt(comment.likes * 100 / (comment.likes + comment.dislikes)) + '%'}" style="height:100%;background-color:#36bedc;float:left;padding-top: 10px;">
                                                    <div class="glyphicon glyphicon-heart" style="font-size:1.5em;"></div>
                                                    <div>@{{comment.likes}}</div>
                                                </div>
                                                <div ng-style="{ width: comment.dislikes == 0 ? '50%' : parseInt(comment.dislikes * 100 / (comment.likes + comment.dislikes)) + '%'}" style="height:100%;background-color:#343f41;float:left;padding-top: 10px;">
                                                    <div class="glyphicon glyphicon-thumbs-down" style="font-size:1.5em;"></div>
                                                    <div>@{{comment.dislikes}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="commenter">
                                            <div class="commenter-email">
                                                @{{comment.email}}
                                            </div>
                                            <div>
                                                Posted on @{{comment.created_at}}
                                            </div>
                                        </div>
                                        <div style="word-break: break-word;margin-bottom:20px;">
                                            @{{comment.review}}
                                        </div>
                                        <div style="height:100px;width:100%;">
                                            <div style="float:left;width:calc((100% - 40px) / 3);height:100%;" ng-style="{ 'margin-right': $last ? '0px' : '20px'}" ng-repeat="image in comment.images" back-img="@{{image}}"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:30px;padding-left:10px;color:#0DB9F0;">
                                    <div class="col-xs-9">
                                        <div style="display:flex;">
                                            <div style="flex:1">
                                                <div>Condo Facilities</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 10" ng-attr-id="@{{$index + 'star51'}}" ng-attr-name="@{{$index + 'rating1'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star51'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 9" ng-attr-id="@{{$index + 'star41half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star41half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 8" ng-attr-id="@{{$index + 'star41'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star41'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 7" ng-attr-id="@{{$index + 'star31half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star31half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 6" ng-attr-id="@{{$index + 'star31'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star31'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 5" ng-attr-id="@{{$index + 'star21half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star21half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 4" ng-attr-id="@{{$index + 'star21'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star21'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 3" ng-attr-id="@{{$index + 'star11half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star11half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 2" ng-attr-id="@{{$index + 'star11'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star11'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 1" ng-attr-id="@{{$index + 'starhalf1'}}" ng-attr-name="@{{$index + 'rating1'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf1'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Room Design</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 10" ng-attr-id="@{{$index + 'star52'}}" ng-attr-name="@{{$index + 'rating2'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star52'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 9" ng-attr-id="@{{$index + 'star42half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star42half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 8" ng-attr-id="@{{$index + 'star42'}}" ng-attr-name="@{{$index + 'rating2'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star42'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 7" ng-attr-id="@{{$index + 'star32half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star32half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 6" ng-attr-id="@{{$index + 'star32'}}" ng-attr-name="@{{$index + 'rating2'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star32'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 5" ng-attr-id="@{{$index + 'star22half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star22half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 4" ng-attr-id="@{{$index + 'star22'}}" ng-attr-name="@{{$index + 'rating2'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star22'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 3" ng-attr-id="@{{$index + 'star12half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star12half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 2" ng-attr-id="@{{$index + 'star12'}}" ng-attr-name="@{{$index + 'rating2'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star12'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 1" ng-attr-id="@{{$index + 'starhalf2'}}" ng-attr-name="@{{$index + 'rating2'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf2'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Condo Management</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 10" ng-attr-id="@{{$index + 'star53'}}" ng-attr-name="@{{$index + 'rating3'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star53'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 9" ng-attr-id="@{{$index + 'star43half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star43half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 8" ng-attr-id="@{{$index + 'star43'}}" ng-attr-name="@{{$index + 'rating3'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star43'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 7" ng-attr-id="@{{$index + 'star33half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star33half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 6" ng-attr-id="@{{$index + 'star33'}}" ng-attr-name="@{{$index + 'rating3'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star33'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 5" ng-attr-id="@{{$index + 'star23half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star23half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 4" ng-attr-id="@{{$index + 'star23'}}" ng-attr-name="@{{$index + 'rating3'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star23'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 3" ng-attr-id="@{{$index + 'star13half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star13half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 2" ng-attr-id="@{{$index + 'star13'}}" ng-attr-name="@{{$index + 'rating3'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star13'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 1" ng-attr-id="@{{$index + 'starhalf3'}}" ng-attr-name="@{{$index + 'rating3'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf3'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display:flex;">
                                            <div style="flex:1">
                                                <div>Security</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 10" ng-attr-id="@{{$index + 'star54'}}" ng-attr-name="@{{$index + 'rating4'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star54'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 9" ng-attr-id="@{{$index + 'star44half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star44half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 8" ng-attr-id="@{{$index + 'star44'}}" ng-attr-name="@{{$index + 'rating4'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star44'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 7" ng-attr-id="@{{$index + 'star34half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star34half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 6" ng-attr-id="@{{$index + 'star34'}}" ng-attr-name="@{{$index + 'rating4'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star34'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 5" ng-attr-id="@{{$index + 'star24half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star24half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 4" ng-attr-id="@{{$index + 'star24'}}" ng-attr-name="@{{$index + 'rating4'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star24'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 3" ng-attr-id="@{{$index + 'star14half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star14half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 2" ng-attr-id="@{{$index + 'star14'}}" ng-attr-name="@{{$index + 'rating4'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star14'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 1" ng-attr-id="@{{$index + 'starhalf4'}}" ng-attr-name="@{{$index + 'rating4'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf4'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Nearby Amenities</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 10" ng-attr-id="@{{$index + 'star55'}}" ng-attr-name="@{{$index + 'rating5'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star55'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 9" ng-attr-id="@{{$index + 'star45half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star45half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 8" ng-attr-id="@{{$index + 'star45'}}" ng-attr-name="@{{$index + 'rating5'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star45'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 7" ng-attr-id="@{{$index + 'star35half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star35half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 6" ng-attr-id="@{{$index + 'star35'}}" ng-attr-name="@{{$index + 'rating5'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star35'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 5" ng-attr-id="@{{$index + 'star25half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star25half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 4" ng-attr-id="@{{$index + 'star25'}}" ng-attr-name="@{{$index + 'rating5'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star25'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 3" ng-attr-id="@{{$index + 'star15half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star15half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 2" ng-attr-id="@{{$index + 'star15'}}" ng-attr-name="@{{$index + 'rating5'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star15'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 1" ng-attr-id="@{{$index + 'starhalf5'}}" ng-attr-name="@{{$index + 'rating5'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf5'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Transport Accessibility</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 10" ng-attr-id="@{{$index + 'star56'}}" ng-attr-name="@{{$index + 'rating6'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star56'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 9" ng-attr-id="@{{$index + 'star46half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star46half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 8" ng-attr-id="@{{$index + 'star46'}}" ng-attr-name="@{{$index + 'rating6'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star46'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 7" ng-attr-id="@{{$index + 'star36half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star36half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 6" ng-attr-id="@{{$index + 'star36'}}" ng-attr-name="@{{$index + 'rating6'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star36'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 5" ng-attr-id="@{{$index + 'star26half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star26half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 4" ng-attr-id="@{{$index + 'star26'}}" ng-attr-name="@{{$index + 'rating6'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star26'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 3" ng-attr-id="@{{$index + 'star16half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star16half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 2" ng-attr-id="@{{$index + 'star16'}}" ng-attr-name="@{{$index + 'rating6'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star16'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 1" ng-attr-id="@{{$index + 'starhalf6'}}" ng-attr-name="@{{$index + 'rating6'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf6'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                    </div>
                                </div>
                                <div style="text-align:right;">
                                    <span style="cursor:pointer;">Edit</span><span style="cursor:pointer;"><span ng-click="replyActive = !replyActive" class="glyphicon glyphicon-share-alt" style="margin-left: 20px;"></span> <span ng-click="replyActive = !replyActive">Reply</span></span>
                                </div>
                            </div>
                            <div class="reply-box" ng-show="replyActive">
                                <div class="commenter-email" style="margin-bottom:10px;">You@Navxx.com</div>
                                <textarea style="width:100%;height:100px;margin-bottom:10px;resize: none;"></textarea>
                                <button style="float:right;color:white;background-color:#686D70;border:none;padding:0px 20px 0px 20px;">Post</button>
                            </div>
                        </div>
                    </div>
                    <div id="comments-end" style="display: table;margin: 20 auto 20 auto;" items-per-page="condo.comments.itemsPerPage" uib-pagination boundary-links="true" max-size="condo.comments.maxSize" rotate="true" total-items="condo.comments.totalItems" ng-model="condo.comments.currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                </div>
            </div>
            <div class="listing-tab-content" style="border: none;" id="reply-ratings">
                <div style="width:100%;">
                    <div style="display:table;margin: 30px 30px 10px 30px;text-transform:uppercase;font-weight:bold;font-size:17px;">
                        Give Us Your Comment
                    </div>
                    <div style="margin: 0px 10px 10px 10px;background-color:#F6FDFF;padding: 20px 20px 0px 20px;">
                        <div style="text-align: right;">
                            <div class="commenter-email">
                                @{{reply.user.email}}
                            </div>
                        </div>
                        <div style="margin: 0px 30px 30px 30px;">
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:10px;">
                                Your Comment About This Condominium
                            </div>
                            <textarea style="width:100%;height:100px;margin-bottom:20px;resize:none;" ng-model="reply.review"></textarea>
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:10px;">
                                Give Your Score Review
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Condo Facilities</div>
                                    <div style="margin-bottom:5px;" class="rating">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.facilities = 10" id="star51" name="rating1" value="5" /><label class = "full" for="star51" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 9" id="star41half" name="rating1" value="4 and a half" /><label class="half" for="star41half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 8" id="star41" name="rating1" value="4" /><label class = "full" for="star41" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 7" id="star31half" name="rating1" value="3 and a half" /><label class="half" for="star31half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 6" id="star31" name="rating1" value="3" /><label class = "full" for="star31" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 5" id="star21half" name="rating1" value="2 and a half" /><label class="half" for="star21half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 4" id="star21" name="rating1" value="2" /><label class = "full" for="star21" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 3" id="star11half" name="rating1" value="1 and a half" /><label class="half" for="star11half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 2" id="star11" name="rating1" value="1" /><label class = "full" for="star11" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 1" id="starhalf1" name="rating1" value="half" /><label class="half" for="starhalf1" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Room Design</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.design = 10" id="star52" name="rating2" value="5" /><label class = "full" for="star52" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 9" id="star42half" name="rating2" value="4 and a half" /><label class="half" for="star42half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 8" id="star42" name="rating2" value="4" /><label class = "full" for="star42" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 7" id="star32half" name="rating2" value="3 and a half" /><label class="half" for="star32half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 6" id="star32" name="rating2" value="3" /><label class = "full" for="star32" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 5" id="star22half" name="rating2" value="2 and a half" /><label class="half" for="star22half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 4" id="star22" name="rating2" value="2" /><label class = "full" for="star22" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 3" id="star12half" name="rating2" value="1 and a half" /><label class="half" for="star12half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 2" id="star12" name="rating2" value="1" /><label class = "full" for="star12" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 1" id="starhalf2" name="rating2" value="half" /><label class="half" for="starhalf2" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Transport Accessibility</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.accessibility = 10" id="star53" name="rating3" value="5" /><label class = "full" for="star53" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 9" id="star43half" name="rating3" value="4 and a half" /><label class="half" for="star43half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 8" id="star43" name="rating3" value="4" /><label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 7" id="star33half" name="rating3" value="3 and a half" /><label class="half" for="star33half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 6" id="star33" name="rating3" value="3" /><label class = "full" for="star33" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 5" id="star23half" name="rating3" value="2 and a half" /><label class="half" for="star23half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 4" id="star23" name="rating3" value="2" /><label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 3" id="star13half" name="rating3" value="1 and a half" /><label class="half" for="star13half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 2" id="star13" name="rating3" value="1" /><label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 1" id="starhalf3" name="rating3" value="half" /><label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Condo Management</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.management = 10" id="star54" name="rating4" value="5" /><label class = "full" for="star54" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 9" id="star44half" name="rating4" value="4 and a half" /><label class="half" for="star44half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 8" id="star44" name="rating4" value="4" /><label class = "full" for="star44" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 7" id="star34half" name="rating4" value="3 and a half" /><label class="half" for="star34half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 6" id="star34" name="rating4" value="3" /><label class = "full" for="star34" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 5" id="star24half" name="rating4" value="2 and a half" /><label class="half" for="star24half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 4" id="star24" name="rating4" value="2" /><label class = "full" for="star24" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 3" id="star14half" name="rating4" value="1 and a half" /><label class="half" for="star14half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 2" id="star14" name="rating4" value="1" /><label class = "full" for="star14" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 1" id="starhalf4" name="rating4" value="half" /><label class="half" for="starhalf4" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Security</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.security = 10" id="star55" name="rating5" value="5" /><label class = "full" for="star55" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 9" id="star45half" name="rating5" value="4 and a half" /><label class="half" for="star45half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 8" id="star45" name="rating5" value="4" /><label class = "full" for="star45" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 7" id="star35half" name="rating5" value="3 and a half" /><label class="half" for="star35half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 6" id="star35" name="rating5" value="3" /><label class = "full" for="star35" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 5" id="star25half" name="rating5" value="2 and a half" /><label class="half" for="star25half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 4" id="star25" name="rating5" value="2" /><label class = "full" for="star25" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 3" id="star15half" name="rating5" value="1 and a half" /><label class="half" for="star15half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 2" id="star15" name="rating5" value="1" /><label class = "full" for="star15" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 1" id="starhalf5" name="rating5" value="half" /><label class="half" for="starhalf5" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Nearby Amenities</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.amenities = 10" id="star56" name="rating6" value="5" /><label class = "full" for="star56" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 9" id="star46half" name="rating6" value="4 and a half" /><label class="half" for="star46half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 8" id="star46" name="rating6" value="4" /><label class = "full" for="star46" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 7" id="star36half" name="rating6" value="3 and a half" /><label class="half" for="star36half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 6" id="star36" name="rating6" value="3" /><label class = "full" for="star36" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 5" id="star26half" name="rating6" value="2 and a half" /><label class="half" for="star26half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 4" id="star26" name="rating6" value="2" /><label class = "full" for="star26" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 3" id="star16half" name="rating6" value="1 and a half" /><label class="half" for="star16half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 2" id="star16" name="rating6" value="1" /><label class = "full" for="star16" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 1" id="starhalf6" name="rating6" value="half" /><label class="half" for="starhalf6" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:20px;margin-top:20px;">
                                Upload Picture
                            </div>
                            <div class="row">
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg" />
                                    <input type="file" name="userprofile_picture" id="filePhoto" />
                                </div>
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg2" />
                                    <input type="file" name="userprofile_picture" id="filePhoto2" />
                                </div>
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg3" />
                                    <input type="file" name="userprofile_picture" id="filePhoto3" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:10px;margin-top:40px;color:gray;">
                                    <input type="checkbox" style="margin-right:10px;" ng-model="reply.agree" /><span>I agree with ThaiFullHouse's Terms and Conditions</span>
                                </div>
                                <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:20px;margin-top:10px;color:gray;">
                                    <input type="submit" class="btn btn-primary" style="border-radius:0;background-color:#06B4FF;width: 80px;" value="POST" ng-click="submitReply()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="listing-rooms">
            <div class="listing-rooms-content">
                <div class="listing-rooms-title">
                    Listing In This Condo
                </div>
                <button class="listing-rooms-button btn" ng-click="listingType = 'rent'">For Rent (@{{forRent}})</button>
                <button class="listing-rooms-button btn" ng-click="listingType = 'sale'">For Sale (@{{forSale}})</button>
                <div class="listing-room-list">
                    <div class="listing-room" ng-repeat="listing in condo.listings | filter:{ 'listing_type': listingType }">
                        <div class="listing-room-image" back-img="@{{listing.images[0]}}"></div>
                        <div class="listing-room-details">
                            <div style="font-size:16px;font-weight:bold;">@{{listing.property_name}}</div>
                            <div style="font-size:14px;font-weight:bold;color:gray;">@{{listing.address_en}}</div>
                            <div style="font-size:16px;font-weight:bold;">&#3647; @{{beautify(listing.details.listing_price)}}@{{listing.listing_type == 'rent' ? ' / month' : ''}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/condo.js"></script>

@endsection