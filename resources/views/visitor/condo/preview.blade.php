<?php
$latlng = $condo->get_location();
$currentUser = Auth::user();
?>
<div class="row preview-header">
    <div class="col-md-6 title">
        <h3><?php echo $details->getTranslatedField('title'); ?></h3>
        <p>
            <?php echo __t('property.ranking', 'RANKING'); ?>
        </p>
    </div>
    <div class="col-md-6">
        <span class="listing-price">

            <?php echo __t('property.starting-price', 'Sale price'); ?> : ฿ <?php echo number_format($details->starting_sales_price); ?>
        </span>
    </div>
</div>
<div class="media-container">
    @include('visitor.condo.slider')
</div>

<div class="listing-highlights">
    <div class="listing-highlight">
        <div class="icon">
            <img src="/img/money.png"/>
        </div>
        <p><?php echo __t('property.starting-price', 'Sales Price Start From'); ?></p>
        <h4> ฿ <?php echo number_format($details->starting_sales_price); ?></h4>        
    </div>
    <div class="listing-highlight">
        <div class="icon">
            <img src="/img/circular-clock.png"/>
        </div>
        <p><?php echo __t('property.office-opening-hours', 'Office Operating Hours'); ?></p>
        <h4><?php echo $details->office_hours; ?></h4>  
    </div>
    <div class="listing-highlight">
        <div class="icon">
            <img src="/img/crane.png"/>
        </div>
        <p><?php echo __t('property.year-built', 'Year Built'); ?></p>
        <h4><?php echo $details->year_built; ?></h4>
    </div>
    <div class="listing-highlight">
        <div class="icon">
            <img src="/img/building-3.png"/>
        </div>
        <p><?php echo __t('property.tower', 'Tower'); ?></p>
        <h4><?php echo $details->number_of_tower; ?></h4>
    </div>
    <div class="listing-highlight">
        <div class="icon">
            <img src="/img/lift.png"/>
        </div>
        <p><?php echo __t('property.floors', 'Floors'); ?></p>
        <h4><?php echo $details->total_floors; ?></h4>
    </div>
</div>
<div class="details-container">    
    <div class="details-box">
        <table class="table">
            <tr>                
                <td class="plabel"><?php echo __t('property.total-unit', 'Total units'); ?></td>
                <td class="value"><?php echo $details->total_units; ?></td>
                <td class="plabel"><?php echo __t('property.floors', 'Floors'); ?></td>
                <td class="value"><?php echo $details->total_floors; ?></td>
            </tr>
            <tr>
                <td><?php echo __t('property.room-types', 'Room types'); ?></td>
                <td class="value"><?php echo $details->room_types; ?></td>
                <td><?php echo __t('property.district', 'District'); ?></td>
                <td class="value"><?php echo $details->district; ?></td>
            </tr>
            <tr>
                <td><?php echo __t('property.parking-space', 'Parking space'); ?></td>
                <td class="value"><?php echo $details->parking_space; ?></td>
                <td><?php echo __t('property.contact', 'Contact'); ?></td>
                <td class="value"><?php echo $details->contact_person; ?></td>
            </tr>
            <tr>                
                <td><?php echo __t('property.website', 'Website'); ?></td>
                <td class="value"><?php echo $details->website; ?></td>
                <td></td>
                <td class="value"></td>
            </tr>

            @include('visitor.condo.facilities')

            <tr>
                <td><?php echo __t('property.nearby-trans', 'Nearby transportation'); ?></td>
                <td colspan="3">
                    <div class="nearby-trans">
                        <div class="trans-item">
                            <label><?php echo __t('property.nearby-bts', 'Near BTS'); ?></label> <span>Asoke</span>
                        </div>
                        <div class="trans-item">
                            <label><?php echo __t('property.nearby-mrt', 'Near MRT'); ?></label> <span>Asoke</span>
                        </div>
                        <div class="trans-item">
                            <label><?php echo __t('property.nearby-apl', 'Near APL'); ?></label> <span>Asoke</span>
                        </div>
                    </div>
                </td>
            </tr>


            <tr>
                <td><?php echo __t('property.details', 'Details'); ?></td>
                <td colspan="3"><?php echo $details->getTranslatedField('other_details'); ?></td>
            </tr>
            <tr>
                <td><?php echo __t('property.address', 'Address'); ?></td>
                <td colspan="3"><?php echo $details->getTranslatedField('address'); ?></td>
            </tr>
            <tr>
                <td><?php echo __t('property.map-street-view', 'Map / Street view'); ?></td>
                <td colspan="3">
                    <div id="property-map" class="street-view"></div>
                    @include('visitor.condo.places')
                </td>
            </tr>
        </table>
    </div>

</div>

<div class="property-listing">
    <h4>LISTING IN THIS CONDO</h4>
    
    <?php
    
    $count = $condo->countListings();
    ?>
    <p class="filters">
        <button class="btn rent">For rent (<?php echo array_get($count, 'rent', 0); ?>)</button>
        <button class="btn pull-right sale">For sale (<?php echo array_get($count, 'sale', 0); ?>)</button>
    </p>
    <?php
    
    
    foreach ($properties as $property) {
        $details = $property->getDetails();
        ?>
        <div class="listing-room listing-<?php echo $property->listing_type; ?>">
            <div class="listing-room-image"></div>
            <div class="listing-room-details">
                <p><?php echo __trget($property->name); ?></p>
                <p><?php echo __trget($details->address); ?></p>
                <p>
                    &#3647; <?php echo __trget($property->listing_price); ?>  <?php echo $property->listing_type == 'rent' ? ' / month' : ''; ?>
                </p>
            </div>
        </div>
        <?php
    }
    ?>
</div>