<div class="nearby-places">
    <?php
    $list = $condo->getNearbyPlaces();

    $places = [];

    foreach ($list as $place) {
        $places[$place->type][] = $place;
    }

    foreach ($places as $type => $list) {
        ?>
        <div class="input-group">
            <span class="input-group-addon"> 
                <?php
                switch ($type) {
                    case 'bts':
                    case 'mrt':
                    case 'apl':
                        ?>
                        <i class="fa fa-bus"></i>
                        <?php
                        break;
                    case 'bank':
                        ?>
                        <i class="fa fa-bank"></i>
                        <?php
                        break;
                    case 'dept-store':
                        ?>
                        <i class="fa fa-shopping-cart"></i>
                        <?php
                        break;
                    case 'hospital':
                        ?>
                        <i class="fa fa-h-square"></i>
                        <?php
                        break;
                    case 'school':
                        ?>
                        <i class="fa fa-graduation-cap"></i>
                        <?php
                        break;
                    default:
                        ?>
                        <i class="fa fa-map-pin"></i>
                        <?php
                        break;
                }
                ?>

                Total ({{ count($list) }})</span>
            <select type="text" class="form-control poi-list" placeholder="Username" aria-describedby="basic-addon1" >
                <option value="0">{{ __t('property.select-poi', 'Select a POI') }}</option>
                <?php
                foreach ($list as $place) {
                    $names = json_decode($place->name, true)
                    ?>
                    <option value="{{ $place->id }}" 
                            data-lat="{{ $place->lat }}"
                            data-lng="{{ $place->lng }}"
                            data-type="{{ $place->type }}">{{ __trget($names) }}</option>
                            <?php
                        }
                        ?>
            </select>
        </div>
        <?php
    }
    ?>
</div>