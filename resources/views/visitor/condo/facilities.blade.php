<tr>
    <td>{{ __t('property.facilities', 'Facilities') }}</td>
    <td colspan="3">
        <div class="row facility-list">
            <?php
            if (count($facilities)) {
                foreach ($facilities as $facility) {

                    if (in_array($facility->id, $selected_facilities)) {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item facilities active">
                                <div class="icon-container">
                                    <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item facilities">
                                <div class="icon-container">
                                    <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </td>
</tr>