@extends('layouts.default')

@section('title', 'Page Title')

<?php $active = 'points'; ?>

@section('breadcrumb')

<li class="active">{{ __t('nav.breadcrumb.home', 'Home') }}</li>

@endsection

@section('content')
<div class="big-banner-container">
    <img class="main-bg rent img-responsive" src="/image/{{ __trimg('home_image', 'rent', 'rent') }}"/>
    <img class="main-bg sale img-responsive" src="/image/{{ __trimg('home_image', 'sale', 'rent') }}" style="display: none;"/>
    <img class="main-bg cd img-responsive" src="/image/{{ __trimg('home_image', 'condo', 'rent') }}" style="display: none;"/>
    <img class="main-bg np img-responsive" src="/image/{{ __trimg('home_image', 'new_project', 'rent') }}" style="display: none;"/>
    <div class="container options-container">
        <p class="home-search-options text-center">
            <span class="option" data-value="sale" data-name="lt">{{ __t('home.options.sale', 'SALE') }}</span>
            <span class="option active" data-value="rent" data-name="lt">{{ __t('home.options.rent', 'RENT') }}</span>
            <span class="option" data-value="np" data-name="np">{{ __t('home.options.new-project', 'NEW PROJECT') }}</span>
            <span class="option" data-value="cd" data-name="pt">{{ __t('home.options.condo', 'CONDO') }}</span>
        </p>
        <form class="home-search-options" metho="get" action="/search">
            <div class="input-group">
                <input type="text" class="form-control input-lg" id="search" name="q" value="" 
                       placeholder="{{ __t('home.input.search-placeholder', 'Enter a neighborhood, city, address or ZIP code') }}">
                <span class="input-group-btn">
                    <button class="btn btn-blue btn-lg" type="submit">{{ __t('home.button.search', 'SEARCH') }}</button>
                </span>
                <input type="hidden" id="lt" name="lt" value="sale"/>
                <input type="hidden" id="pr" name="pr" value=""/>
            </div>
        </form>
    </div>
</div>

<div class="home-container">
    <div class="front-hero">
        <h1>{{ __t('home.hero.title', 'HOW IS THAIFULLHOUSE DIFFERENT ?') }}</h1>
        <div class="hero-cell">
            <img src="/img/home/searching.png">
            <h3>{{ __t('home.hero.search.title', 'SEARCH FASTER') }}</h3>
            <p>
                {{ __t('home.hero.search.text', 'we provide a searching map services to reach your dream house faster') }}
            </p>
        </div>
        <div class="hero-cell">
            <img src="/img/home/free.png">
            <h3>{{ __t('home.hero.free.title', 'FREE POSTING') }}</h3>
            <p>
                {{ __t('home.hero.free.text', 'You can post a listing for free on Thaifullhouse website') }}
            </p>
        </div>
        <div class="hero-cell">
            <img src="/img/home/thumbs-up.png">
            <h3>{{ __t('home.hero.info.title', 'FULL INFORMATION') }}</h3>
            <p>
                {{ __t('home.hero.info.text', 'Our website provide all needed information to you for the better decision') }} 
            </p>
        </div>
        <div class="hero-cell">
            <img src="/img/home/trophy.png">
            <h3>{{ __t('home.hero.agents.title', 'BEST AGENTS') }}</h3>
            <p>
                {{ __t('home.hero.agents.text', 'We provide you the information of proffesional agents, trustable and ready to be your best partner') }}
            </p>
        </div>
    </div>
    <div class="row premium">
        <div class="col-md-12">
            <h3>{!! trans('home.home.premium-listing') !!}</h3>            
        </div>

        <div class="col-md-12 premium-listings">
            <?php
            if (count($premium)) {

                foreach ($premium as $property) {
                    $title = __trget(json_decode($property->details->title, true));
                    $address = __trget(json_decode($property->details->address, true));
                    //$name = $property->name ? $property->name : $title;

                    $name = __trget(json_decode($property->name, true));

                    $images = $property->thumbs;
                    $image = false;

                    if (count($images)) {
                        $image = array_shift($images);
                    }
                    ?>
                    <div class="listing-item premium-item" data-id="{{ $property->id }}">
                        <div class="ribbon"><span>Exclusive</span></div>
                        <div class="listing-img">
                            <?php
                            if ($image) {
                                ?>
                                <img src="{{ $image }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="/img/default-property.png" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="listing-info">
                            <div class="row">
                                <div class="col-xs-6 property-type">
                                    <i class="fa fa-home"></i> {{ $name }}
                                </div>
                                <div class="col-xs-6 location text-right">
                                    <i class="fa fa-map-marker"></i> {{ $property->street_name }}
                                </div>
                            </div>
                            <p class="lname">
                                {{ $name  }}
                            </p>
                            <div class="row">
                                <div class="col-xs-6 price">

                                    <?php
                                    if ($property->listing_type == 'rent') {
                                        echo __t('home.rental-price', 'Rental price');
                                    } else {
                                        echo __t('home.sale-price', 'Starting price');
                                    }
                                    ?>
                                </div>
                                <div class="col-xs-6 price text-right">
                                    ฿ {{ number_format($property->details->listing_price) }}
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>


    <div class="row home-banner">
        <div class="col-md-12">
            {{ __banner('home') }}
        </div>        
    </div>

    <div class="row home-sale">
        <div class="col-md-12">
            <h3>{{ __t('home.for-sale', 'For sale') }}</h3>
        </div>        
        <div class="col-md-12">
            <div class="exclusive-listings">
                <div class="listing current-listing main-sale">
                    <img id="main-sale-image" src="http://placehold.it/618x370?text=ICON">
                    <div class="info">
                        <p class="name"></p>
                        <p>
                            <span class="location"></span> <span class="price pull-right"></span>
                        </p>
                    </div>
                </div>

                <?php
                if (count($sales)) {
                    foreach ($sales as $property) {
                        ?>
                        <div class="listing pending-listing" id="exclusive-{{ $property->id }}" data-ref="{{ $property->id }}">
                            <img src="{{ $property->thumb }}">
                        </div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>

    <div class="row home-rent">
        <div class="col-md-12">
            <h3>{{ __t('home.for-rent', 'For rent') }}</h3>
        </div>
        <div class="col-md-12">
            <div class="exclusive-listings">
                <div class="listing current-listing main-rent">
                    <img src="http://placehold.it/618x370?text=ICON">
                    <div class="info">

                    </div>
                </div>

                <?php
                if (count($rentals)) {
                    foreach ($rentals as $property) {
                        ?>
                        <div class="listing pending-listing" id="exclusive-{{ $property->id }}" data-ref="{{ $property->id }}">
                            <img src="{{ $property->thumb }}">
                        </div>
                        <?php
                    }
                }
                ?>

            </div>
        </div>
    </div>

    <div class="row condo-community">
        <div class="col-md-12">
            <h3>{!! __t('home.condo-community', 'Condo <em>community</em>') !!}</h3>            
        </div>

        <div class="col-md-12">
            <?php
            $image = __trimg('condo_image');

            if ($image) {
                ?>
                <div class="">
                    <a href="/search?pt=cd">
                        <img src="/image/{{ $image }}" />
                    </a>
                </div>
                <?php
            }
            ?>
        </div>
    </div>

</div>

<div id="property-preview" class="modal fade property-preview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="action dismiss" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> CLOSE</button>
                <button type="button" class="action expand" onclick="showFullProperty();"><i class="fa fa-external-link"></i> EXPAND</button>
                <h4 class="modal-title" id="property-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection

@section('scripts')
<link href="/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/home.js"></script>
<script src="/js/search/suggestion.js"></script>

<script>

                    var searchOptions = {
                        lt: 'sale',
                        np: '',
                        pt: ''
                    };

                    $(function () {

                        //var suggest = new SearchSuggestion('search');

                        $('span.option').on('click', function (event) {
                            event.preventDefault()
                            $('span.option').removeClass('active');
                            $(this).addClass('active');

                            searchOptions.pt = searchOptions.lt = searchOptions.np = '';

                            searchOptions[$(this).data('name')] = $(this).data('value');

                            //console.log(searchOptions);
                            
                            $('.main-bg').hide();
                            $('.main-bg.' + $(this).data('value')).show();
                        });

                        runExclusiveSalesGallery();
                    });

                    var exclusiveSales = <?php echo json_encode($sales); ?>;
                    var exclusiveRetanls = <?php echo json_encode($rentals); ?>;
                    var galleryTrasitionDuration = 3000;
                    var currentSalesIndex = 0;

                    var updateProgress = function (event) {
                        console.log(event);
                    };

                    var runExclusiveSalesGallery = function () {

                        var list = exclusiveSales;
                        var index = currentSalesIndex;
                        var property = list[index];

                        if (!property) {
                            return;
                        }

                        index++;

                        if (index >= list.length) {
                            index = 0;
                        }

                        var mainImage = document.getElementById('main-sale-image');

                        var xhr = new XMLHttpRequest();

                        xhr.responseType = 'blob';

                        xhr.addEventListener("progress", updateProgress);
                        /*xhr.addEventListener("load", transferComplete);
                         xhr.addEventListener("error", transferFailed);
                         xhr.addEventListener("abort", transferCanceled);*/

                        xhr.open("GET", property.main, true);


                        xhr.onload = function () {
                            var urlCreator = window.URL || window.webkitURL;
                            var imageUrl = urlCreator.createObjectURL(this.response);

                            $(mainImage).fadeOut('slow', function () {
                                mainImage.src = imageUrl;
                                $(mainImage).show();

                                $('.main-sale').find('.name').html(property.property_name);
                                $('.main-sale').find('.location').html(property.address);
                                $('.main-sale').find('.price').html('‎‎฿ ' + numberWithCommas(property.listing_price));

                            });



                            setTimeout(runExclusiveSalesGallery, galleryTrasitionDuration);
                            currentSalesIndex = index;
                        };


                        xhr.onerror = function () {
                            runExclusiveSalesGallery();
                        };
                        xhr.send();
                    };

                    var numberWithCommas = function (x) {
                        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                    };

</script>


@endsection
