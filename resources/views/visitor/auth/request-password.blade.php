@extends('layouts.default')

@section('breadcrumb')
<li class="active">{{ trans('auth.forget_password_title') }}</li>
@endsection

@section('content')
<div class="container auth-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>{{ __t('title.forget-password-title', 'Forget my password') }}</h1>
            <p>
                {{ __t('text.forget-password-text', 'Please input your email below to receive a link to reset your password') }}
            </p>
            <form action="/auth/request-reset-link" method="post">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                        <li>{{ __t('text.email_not_found_error', 'The email you provided was not found in our system') }}</li>
                    </ul>
                </div>
                @endif
                <div class="form-group">
                    <label for="email">{{ __t('label.email-address', 'Email') }}</label>
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button type="submit" class="btn btn-primary">{{ __t('button.submit', 'Submit') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
