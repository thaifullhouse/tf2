@extends('layouts.default')

@section('content')
<div class="container auth-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Verify your email</h1>
            <p>
                Please check your email ({{ $user->email }}), a password reset link has been sent there to verify your email.
            </p>
        </div>
    </div>
</div>
@endsection
