@extends('layouts.default')

@section('content')
<div class="container auth-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?php
            if ($email_verified) {
                ?>
                <h3 class="text-success">Congratulations</h3>
                <p>
                    Your email ({{ $user->email }}) have been successfully verified.
                </p>
                <p>
                    You can now sign in with your email and password.
                </p>
                <?php
            } else {
                ?>
                <h3 class="text-danger">Sorry</h3>
                <p>
                    We could not verify your email ({{ $user->email }}), please check that you correctly spell it.
                </p>
                <?php
            }
            ?>
        </div>
    </div>

</div>
@endsection
