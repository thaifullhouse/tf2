@extends('layouts.default')

@section('breadcrumb')
<li class="active">{{ trans('auth.update_password_title') }}</li>
@endsection

@section('content')
<div class="container auth-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>{{ __t('title.update_password_title', 'Update password') }}</h1>
            <div class="alert alert-success">
                {{ __t('text.updated_password_text', 'Your password have been updated successfully, you can login with your new password') }}
            </div>
        </div>
    </div>
</div>
@endsection
