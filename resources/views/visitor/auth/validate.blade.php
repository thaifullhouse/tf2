@extends('layouts.default')

@section('content')
<div class="container auth-container">
    <h1>Check your email</h1>
    <p>
        Please check your email ({{ $user->email }}), a validation link has been send there to verify your email.
    </p>
    <p>
        If you did not receive it within 5 minutes, check in your spam box, 
        or <a href="/auth/request-email/{{ $user->id }}">request a new verification link</a>
    </p>
</div>

@endsection
