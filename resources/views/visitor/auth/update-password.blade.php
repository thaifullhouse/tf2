@extends('layouts.default')

@section('breadcrumb')
<li class="active">{{ trans('auth.update_password_title') }}</li>
@endsection

@section('content')
<div class="container auth-container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>{{ __t('title.update_password_title', 'Update password') }}</h1>
            <p>
                {{ __t('text.update_password_text', 'Please ipunt your new password below') }}
            </p>
            <form action="/auth/update-password" method="post">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">
                    <label for="password">{{ __t('label.password', 'Password') }}</label>
                    <input type="password" class="form-control" name="password" value="{{ old('password') }}" placeholder="New password">
                </div>
                <div class="form-group">
                    <label for="password_confirmation">{{ __t('label.password_confirmation', 'Confirm passqword') }}</label>
                    <input type="password" class="form-control" name="password_confirmation" value="{{ old('password_confirmation') }}" placeholder="Confirm your password">
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="token" value="{{ $password_reset_token }}">
                <input type="hidden" name="id" value="{{ $id }}">
                <button type="submit" class="btn btn-primary">{{ __t('button.submit', 'Submit') }}</button>
            </form>
        </div>
    </div>
</div>
@endsection
