<tr>
    <td>{{ __t('property.key-features', 'Key features') }}</td>
    <td colspan="3">
        <div class="row facility-list">
            <?php
            if (count($features)) {
                foreach ($features as $feature) {

                    if (in_array($feature->id, $selected_features)) {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item features active">
                                <div class="icon-container">
                                    <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item features">
                                <div class="icon-container">
                                    <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </td>
</tr>
<tr>
    <td>{{ __t('property.facilities', 'Facilities') }}</td>
    <td colspan="3">
        <div class="row facility-list">
            <?php
            if (count($facilities)) {
                foreach ($facilities as $facility) {

                    if (in_array($facility->id, $selected_facilities)) {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item facilities active">
                                <div class="icon-container">
                                    <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    } else {
                        ?>
                        <div class="col-md-2">
                            <div class=" text-center facility-item facilities">
                                <div class="icon-container">
                                    <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                         class="img-responsive"/>
                                </div>                                            
                                <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                            </div>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
    </td>
</tr>
<?php
if ($property->listing_type == 'rent') {
    ?>
    <tr>
        <td>{{ __t('property.optional-features', 'Optional features') }}</td>
        <td colspan="3">
            <div class="row facility-list">
                <?php
                if (count($optional_features)) {
                    foreach ($optional_features as $feature) {

                        if (in_array($feature->id, $selected_optional_features)) {
                            ?>
                            <div class="col-md-2">
                                <div class=" text-center facility-item features active">
                                    <div class="icon-container">
                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                             class="img-responsive"/>
                                    </div>                                                
                                    <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="col-md-2">
                                <div class=" text-center facility-item features">
                                    <div class="icon-container">
                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                             class="img-responsive"/>
                                    </div>                                                
                                    <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                </div>
                            </div>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </td>
    </tr>
    <?php
}
?>