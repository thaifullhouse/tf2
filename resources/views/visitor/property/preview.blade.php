<div class="row preview-header">
    <div class="col-md-6 title">
        <h3>{{ $details->getTranslatedField('title') }}</h3>
        <p>
            <span class="listing-type">
                <?php
                echo trans('property.listing_type.' . $property->listing_type);
                ?>
            </span>
            <button class="favorite"><i class="fa fa-heart-o"></i></button>
            <button class="share"><i class="fa fa-share-alt"></i> {{ trans('property.share') }}</button>
        </p>
    </div>
    <div class="col-md-6">
        <span class="listing-price">
            
            <?php
            if ($property->listing_type == 'rent') {
                ?>
                {{ trans('property.rent') }} : {{ number_format($details->listing_price) }}
                <?php
            } else {
                ?>
                {{ trans('property.starting-price') }} : {{ number_format($details->listing_price) }}
                <?php
            }
            ?>
        </span>
    </div>
</div>
<div class="media-container">
    <div class="media-slider">
        <div class="media-group big-media">
            <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                <img src="http://placehold.it/400x400">
            </a>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
    </div>
    <span class="handler left" onclick="mediaScrollLeft();">
        <i class="fa fa-angle-left"></i>
    </span>
    <span class="handler right" onclick="mediaScrollRight();">
        <i class="fa fa-angle-right"></i>
    </span>
</div>
<div class="details-container">
    <div class="details-box">
        <table class="table">
            <tr>
                <td class="plabel">Deposit</td>
                <td class="value">2 months</td>
                <td class="plabel">Bedrooms</td>
                <td class="value">2</td>
            </tr>
            <tr>
                <td>Rental</td>
                <td class="value">2,000</td>
                <td>Bathrooms</td>
                <td class="value">1</td>
            </tr>
            <tr>
                <td>Floors</td>
                <td class="value">2/20 floors</td>
                <td>Size</td>
                <td class="value">39sqm</td>
            </tr>
            <tr>
                <td>Built In</td>
                <td class="value">2012</td>
                <td>Tower</td>
                <td class="value">2</td>
            </tr>
            <tr>
                <td>Key features</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Facilities</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Optional</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Available</td>
                <td class="value">fsdfsdfsdf</td>
                <td>Min. rental</td>
                <td class="value">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Details</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Address</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>Map / Street view</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
        </table>
    </div>
    <div class="contact-box">
        <h4>Information</h4>
        <div class="contact-box-header">
            <div class="row">
                <div class="col-md-4">
                    <img src="http://placehold.it/64x64">
                </div>
                <div class="col-md-8">
                    Phuket 9 Okey Development<br/>
                    www.phuket.com<br/>
                    Start price: 3,650,000
                </div>
            </div>
            <p class="tel-btn">
                <button type="button">TEL.</button>
            </p>
        </div>
        <h5>Contact</h5>
        <div class="contact-box-body">
            <form>
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Name">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Tel.">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Message</label>
                    <textarea class="form-control" rows="5">Default text message</textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> You want us to send Newsletters
                    </label>
                </div>
                <p class="text-center">
                    <button type="submit" class="btn btn-send"><i class="fa fa-check"></i> Submit</button>
                </p>
            </form>
        </div>
    </div>
</div>
