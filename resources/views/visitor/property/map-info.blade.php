
<?php
if (is_object($data)) {
    ?>
    <div class="marker-info">
        <div class="marker-img">
            <img src="{{ $image }}" class="img-responsive">
        </div>
        <div class="marker-details">
            <h6>{{ $data->name }}</h6>
            <span class="price">฿ {{ number_format($data->listing_price) }}</span><br/>
            <span class="beds">{{ $data->bedroom_num }}</span> bd | <span class="baths">{{ $data->bathroom_num }}</span> ba<br/>
            <span class="size">{{ $data->total_size }}</span>sqm
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="marker-info">

    </div>
    <?php
}
?>