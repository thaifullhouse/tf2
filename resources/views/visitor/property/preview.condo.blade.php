<div class="row preview-header">
    <div class="col-md-6 title">
        <h3>{{ $details->getTranslatedField('title') }}</h3>
        <p>
            <span class="listing-type">
                <?php
                echo trans('property.listing_type.' . $property->listing_type);
                ?>
            </span>
            <button class="favorite"><i class="fa fa-heart-o"></i></button>
            <button class="share"><i class="fa fa-share-alt"></i> {{ trans('property.share') }}</button>
        </p>
    </div>
    <div class="col-md-6">
        <span class="listing-price">
            
            <?php
            if ($property->listing_type == 'rent') {
                ?>
                {{ trans('property.rent') }} : {{ number_format($details->listing_price) }}
                <?php
            } else {
                ?>
                {{ trans('property.starting-price') }} : {{ number_format($details->listing_price) }}
                <?php
            }
            ?>
        </span>
    </div>
</div>
<div class="media-container">
    <div class="media-slider">
        <div class="media-group big-media">
            <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                <img src="http://placehold.it/400x400">
            </a>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
        <div class="media-group">
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
            <div class="small-media">
                <a class="fancybox" rel="group" href="http://placehold.it/900x700">
                    <img src="http://placehold.it/300x200">
                </a>
            </div>
        </div>
    </div>
    <span class="handler left" onclick="mediaScrollLeft();">
        <i class="fa fa-angle-left"></i>
    </span>
    <span class="handler right" onclick="mediaScrollRight();">
        <i class="fa fa-angle-right"></i>
    </span>
</div>
<div class="details-container">
    <div class="details-box">
        <table class="table">
            <tr>                
                <td class="plabel">{{ trans('property.deposit-' . $property->listing_type) }}</td>
                <td class="value">{{ $details->deposite }}</td>
                <td class="plabel">{{ trans('property.bedrooms') }}</td>
                <td class="value">{{ $details->bedroom_num }}</td>
            </tr>
            <tr>
                <td>{{ trans('property.price-label-' . $property->listing_type) }}</td>
                <td class="value">{{ number_format($details->listing_price) }}</td>
                <td>{{ trans('property.bathrooms') }}</td>
                <td class="value">{{ $details->bathroom_num }}</td>
            </tr>
            <tr>
                <td>{{ trans('property.floors') }}</td>
                <td class="value">2/20 floors</td>
                <td>{{ trans('property.size') }}</td>
                <td class="value">39sqm</td>
            </tr>
            <tr>
                <td>{{ trans('property.built-in') }}</td>
                <td class="value">2012</td>
                <td>{{ trans('property.tower') }}</td>
                <td class="value">2</td>
            </tr>
            <tr>
                <td>{{ trans('property.key-features') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.facilities') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.optional-features') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.availability-date') }}</td>
                <td class="value">fsdfsdfsdf</td>
                <td>{{ trans('property.minimum-retal-period') }}</td>
                <td class="value">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.details') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.address') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
            <tr>
                <td>{{ trans('property.map-street-view') }}</td>
                <td colspan="3">fsdfsdfsdf</td>
            </tr>
        </table>
    </div>
    <div class="contact-box">
        <h4>{{ trans('property.information') }}</h4>
        <div class="contact-box-header">
            <div class="row">
                <div class="col-md-4">
                    <img src="http://placehold.it/64x64">
                </div>
                <div class="col-md-8">
                    Phuket 9 Okey Development<br/>
                    www.phuket.com<br/>
                    Start price: 3,650,000
                </div>
            </div>
            <p class="tel-btn">
                <button type="button">{{ trans('property.tel') }}</button>
            </p>
        </div>
        <h5>{{ trans('property.contact') }}</h5>
        <div class="contact-box-body">
            <form>
                <div class="form-group">
                    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Name">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Tel.">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="E-mail">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">{{ trans('property.message') }}</label>
                    <textarea class="form-control" rows="5">Default text message</textarea>
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> {{ trans('property.send-newsletter') }}
                    </label>
                </div>
                <p class="text-center">
                    <button type="submit" class="btn btn-send"><i class="fa fa-check"></i> {{ trans('property.submit') }}</button>
                </p>
            </form>
        </div>
    </div>
</div>
