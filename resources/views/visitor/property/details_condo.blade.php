@extends('layouts.details')

@section('title', 'Page Title')

@section('content')

<?php
$latlng = $property->getLatLng();
$images = $property->getImages();
$currentUser = Auth::user();
?>
<div class="property-details">
    <div class="row preview-header">
        <div class="col-md-6 title">
            <h3>{{ $details->getTranslatedField('title') }}</h3>
            <p>
                <span class="listing-type">
                    <?php
                    echo trans('property.listing_type.' . $property->listing_type);
                    ?>
                </span>
                <?php
                if ($currentUser && $property->isInUserFavorites($currentUser)) {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart"></i></button>
                    <?php
                } else {
                    ?>
                    <button class="favorite" onclick="propertyToggleFavorite({{ $property->id }})"><i class="fa fa-heart-o"></i></button>
                    <?php
                }
                ?>

                <button class="share" onclick="propertyShare({{ $property->id }})"><i class="fa fa-share-alt"></i> {{ trans('property.share') }}</button>
            </p>
        </div>
        <div class="col-md-6">
            <span class="listing-price">

                <?php
                if ($property->listing_type == 'rent') {
                    ?>
                    {{ trans('property.rental') }} : ฿ {{ number_format($details->listing_price) }}
                    <?php
                } else {
                    ?>
                    {{ trans('property.starting-price') }} : ฿ {{ number_format($details->listing_price) }}
                    <?php
                }
                ?>
            </span>
        </div>
    </div>

    <div class="media-container">
        @include('visitor.property.slider')
    </div>
    <div class="details-container">
        <div class="details-box">
            <table class="table">
                <tr>                
                    <td class="plabel">{{ trans('property.deposit-' . $property->listing_type) }}</td>
                    <td class="value">{{ $details->deposite }}</td>
                    <td class="plabel">{{ trans('property.bedrooms') }}</td>
                    <td class="value">{{ $details->bedroom_num }}</td>
                </tr>
                <tr>
                    <td>{{ trans('property.price-label-' . $property->listing_type) }}</td>
                    <td class="value">฿ {{ number_format($details->listing_price) }}</td>
                    <td>{{ trans('property.bathrooms') }}</td>
                    <td class="value">{{ $details->bathroom_num }}</td>
                </tr>
                <tr>
                    <td>{{ trans('property.floors') }}</td>
                    <td class="value">{{ $details->floor_num }}</td>
                    <td>{{ trans('property.size') }}</td>
                    <td class="value">{{ $details->total_size }}</td>
                </tr>
                <tr>
                    <td>{{ trans('property.built-in') }}</td>
                    <td class="value">{{ $details->year_built }}</td>
                    <td>{{ trans('property.tower') }}</td>
                    <td class="value">{{ $details->tower_num }}</td>
                </tr>

                @include('visitor.property.facilities')

                <tr>
                    <?php
                    if ($property->listing_type == 'rent') {
                        ?>
                        <td>{{ trans('property.availability-date') }}</td>
                        <td class="value">{{ $details->getAvailabilityDate() }}</td>
                        <td>{{ trans('property.minimum-rental-period') }}</td>
                        <td class="value">{{ $details->minimal_rental_period }}</td>
                        <?php
                    } else {
                        ?>
                        <td>{{ trans('property.availability-date') }}</td>
                        <td class="value">{{ $details->getAvailabilityDate() }}</td>
                        <td></td>
                        <td class="value"></td>
                        <?php
                    }
                    ?>

                </tr>
                <tr>
                    <td>{{ trans('property.details') }}</td>
                    <td colspan="3">{{ $details->getTranslatedField('other_details') }}</td>
                </tr>
                <tr>
                    <td>{{ trans('property.address') }}</td>
                    <td colspan="3">{{ $details->getTranslatedField('address') }}</td>
                </tr>
                <tr>
                    <td>{{ trans('property.map-street-view') }}</td>
                    <td colspan="3"><div id="street-view" class="street-view"></div></td>
                </tr>
            </table>
        </div>
        <div class="contact-box">
            <?php
            $publisher = $property->getPublisherProfile();

            //var_dump($publisher);
            ?>
            <h4>{{ trans('property.information') }}</h4>
            <div class="contact-box-header">
                <div class="row">
                    <div class="col-md-4">
                        <img src=" {{ $publisher->getProfilePicture() }}" class="img-responsive">
                    </div>
                    <div class="col-md-8">
                        {{ $publisher->getName() }}<br/>
                        {{ $publisher->getTextAddress() }}<br/>
                        Start price: 3,650,000
                    </div>
                </div>
                <p class="tel-btn">
                    <button type="button" data-tel="{{ $publisher->phone }}" onclick="showPhone(this)">{{ trans('property.tel') }}</button>
                </p>
            </div>
            <h5>{{ trans('property.contact') }}</h5>
            <div class="contact-box-body">
                <form id="property-message">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="" placeholder="Name">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" value="" placeholder="Tel.">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" value="" placeholder="E-mail">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">{{ trans('property.message') }}</label>
                        <textarea class="form-control" rows="5" name="message_body"><?php echo __dbconf('default_message', true); ?></textarea>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="subscribe" value="yes"> {{ trans('property.send-newsletter') }}
                        </label>
                    </div>
                    <p class="text-center">
                        <input type="hidden" name="poster_id" value="{{ $property->poster_id }}"/>
                        <button type="button" class="btn btn-send" onclick="sendMessage();"><i class="fa fa-check"></i> {{ trans('property.submit') }}</button>
                    </p>
                </form>
            </div>
        </div>
    </div>
    <div class="row similars">
        <div class="col-md-12">
            <h4>{{ trans('property.similar-listing') }}</h4>
        </div>

        <div class="col-md-12 similar-listings">
            <?php
            if (count($related)) {

                foreach ($related as $_property) {
                    $title_field = session('lang') ? 'title_' . session('lang') : 'title_th';
                    $address_field = session('lang') ? 'address_' . session('lang') : 'address_th';
                    $title = $_property->details->$title_field;
                    $name = $_property->property_name ? $_property->property_name : $title;

                    $images = $_property->thumbs;
                    $image = false;

                    if (count($images)) {
                        $image = array_shift($images);
                    }
                    ?>
                    <div class="listing-item">
                        <a href="/property/details/{{ $_property->id }}">
                            <div class="listing-img">
                                <?php
                                if ($image) {
                                    ?>
                                    <img src="{{ $image }}" class="img-responsive"/>
                                    <?php
                                }
                                ?>
                            </div>
                            <div class="listing-info">
                                <p class="lname">
                                    {{ $name  }}
                                </p>
                                {{ number_format($_property->details->listing_price) }}
                            </div>
                        </a>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>

</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" />
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/fancybox/jquery.fancybox.pack.js"></script>
<script>
                            var latlng = <?php echo json_encode($property->getLatLng()); ?>

                            $(document).ready(function () {
                            $('.fancybox').fancybox();
                            });
                        </script>
<script src="/js/property-details.js"></script>
<script src="/js/property-gallery.js"></script>
<script src="/js/message.js"></script>

@endsection