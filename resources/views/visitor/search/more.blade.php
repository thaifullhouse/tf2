<div class="dropdown listing-type">
    <button id="dLabel" type="button" class="more" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ trans('search.more') }}
        <span class="caret"></span>
    </button>
    
    <?php  
        
        $smin = array_get($all, 'smin');
        $smax = array_get($all, 'smax');
        
        $ymin = array_get($all, 'ymin');
        $ymax = array_get($all, 'ymax');
    ?>
    
    <div class="dropdown-menu more" aria-labelledby="dLabel">
        <form class="form-horizontal">           
            <div class="form-group">
                <label for="baths" class="col-sm-4 col control-label">{{ __t('map_search.listing.size', 'Size') }}</label>
                <div class="col-sm-4 col">
                    <input type="number" class="form-control" id="size_min" name="size_min" value="<?php echo $smin > 0 ? $smin : ''; ?>" placeholder="{{ __t('map_search.listing.min-size', 'Min size') }}">
                </div>
                <div class="col-sm-4 col">
                    <input type="number" class="form-control" id="size_max" name="size_max" value="<?php echo $smax > 0 ? $smax : ''; ?>" placeholder="{{ __t('map_search.listing.max-size', 'Max size') }}">
                </div>
            </div>
            <div class="form-group">
                <label for="baths" class="col-sm-4 col control-label">{{ __t('map_search.listing.year-built', 'Year built') }}</label>
                <div class="col-sm-4 col">
                    <input type="number" class="form-control" id="year_min" name="year_min" value="<?php echo $ymin > 0 ? $ymin : ''; ?>" placeholder="{{ __t('map_search.listing.min-year-built', 'Min year') }}">
                </div>
                <div class="col-sm-4 col">
                    <input type="number" class="form-control" id="year_max" name="year_max" value="<?php echo $ymax > 0 ? $ymax : ''; ?>" placeholder="{{ __t('map_search.listing.max-year-built', 'Max year') }}">
                </div>
            </div>
            <div class="form-group filter-action">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="button" class="btn btn-default btn-block btn-apply btn-map-search">{{ __t('map_search.button.search', 'Search') }}</button>
                </div>
            </div>
        </form>
    </div>
</div>