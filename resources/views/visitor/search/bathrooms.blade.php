<?php
$bath = array_get($all, 'bath');
?>

<div class="dropdown">
    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ __t('map_search.bathrooms', 'BATHS') }} <span class="bath-count"><?php echo $bath != 0 ? '(' . $bath . ')' : ''; ?></span>
        <span class="caret"></span>
    </button>


    <ul class="dropdown-menu list-group bathrooms">
        <li class="list-group-item bath-item<?php echo $bath == '0' ? ' active' : ''; ?>" data-filter="0">0</li>
        <li class="list-group-item bath-item<?php echo $bath == '1' ? ' active' : ''; ?>" data-filter="1">1</li>
        <li class="list-group-item bath-item<?php echo $bath == '2' ? ' active' : ''; ?>" data-filter="2">2</li>
        <li class="list-group-item bath-item<?php echo $bath == '3' ? ' active' : ''; ?>" data-filter="3">3</li>
        <li class="list-group-item bath-item<?php echo $bath == '4+' ? ' active' : ''; ?>" data-filter="4+">4+</li>
    </ul>
</div>