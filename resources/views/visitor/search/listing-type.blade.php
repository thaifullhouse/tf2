<div class="dropdown listing-type">
    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-circle listing-type-sale" aria-hidden="true"></i> <i class="fa fa-circle listing-type-rent" aria-hidden="true"></i>  {{ trans('search.listing.type') }}
        <span class="caret"></span>
    </button>
    <div class="dropdown-menu option-dropdown" aria-labelledby="dLabel">

        <?php
        $lp = array_filter(explode(',', array_get($all, 'lt')));

        if (count($lp) == 0) {
            $lp = ['rent', 'sale'];
        }
        ?>
        <h5>Listing type</h5>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="lt-sale" name="lt" value="sale" class="lt" <?php echo in_array('sale', $lp) ? 'checked' : ''; ?> >
            </div>
            <div class="colxs-10">
                <i class="fa fa-circle type-sale" aria-hidden="true"></i> {{ __t('map_search.listing.for-sale', 'Sale') }}
            </div>
        </div>
        <div class="row search-option-row">
            <div class="col-xs-2 text-center">
                <input type="checkbox" id="lt-rent" name="lt" value="rent" class="lt" <?php echo in_array('rent', $lp) ? 'checked' : ''; ?> >
            </div>
            <div class="colxs-10">
                <i class="fa fa-circle type-rent" aria-hidden="true"></i> {{ __t('map_search.listing.for-rent', 'Rent') }}
            </div>
        </div>


    </div>
</div>