@extends('layouts.bigmap')

@section('title', 'Page Title')

@section('content')

<div class="map-search-container">
    <div class="filter-bar">

        <input type="text" class="search form-control" id="q" name="q" value="{{ array_get($all, 'q') }}" placeholder="Enter area, BTS station">
        <i class="fa fa-search"></i>

        @include('visitor.search.listing-type')

        @include('visitor.search.home-type')

        @include('visitor.search.price-range')

        @include('visitor.search.bedrooms')    

        @include('visitor.search.bathrooms')

        @include('visitor.search.more')           

        <button type="button" class="btn btn-map-search">
            {{ __t('map_search.search-button', 'SEARCH') }}
        </button>
        <div class="dropdown save-dropdown">
            <button type="button" class="search-action saved" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{ __t('map_search.saved', 'SAVED') }} <span class="saved-count"></span>
            </button>
            <div class="dropdown-menu seach-list" aria-labelledby="dLabel">

            </div>
        </div>
        <button type="button" class="search-action save">{{ __t('map_search.save', 'SAVE') }}</button>
    </div>
    <div class="search-result-container">
        <table>
            <tr>
                <td class="col-map">
                    <div class="map map-container" style="height: auto;">
                        <div id="map" class="map">
                        </div>
                        <div class="map-tools">
                            <button class="more-maps" type="button">
                                <span class="more">{{ __t('map_search.more-map', 'More map') }} &nbsp;&nbsp;<i class="fa fa-angle-right"></i></span>
                                <span class="less"><i class="fa fa-angle-left"></i>&nbsp;&nbsp; {{ __t('map_search.less-map', 'Less map') }}</span>
                            </button>
                            <button class="more-maps" type="button"><span>List view</span></button>
                        </div>
                    </div>
                </td>
                <td class="col-list">
                    <div class="property-listing">
                        {{ __banner('map') }}
                        <h3>
                            <span></span> <small class="property-count"></small>
                        </h3>
                        <ul class="nav nav-pills nav-filter">
                            <li role="presentation" id="sort-rank" class="active" onclick="sortby('year', 'rank');"><a href="#">{{ __t('map_search.sorter.home-for-you', 'Home') }}</a></li>
                            <li role="presentation" id="sort-new" onclick="sortby('year', 'new');"><a href="#">{{ __t('map_search.sorter.newest', 'Newest') }}</a></li>
                            <li role="presentation" id="sort-price" onclick="sortby('price', 'price');" class="cheap-filter"><a href="#">{{ __t('map_search.sorter.cheapest', 'Cheapest') }}</a></li>
                            <li role="presentation" id="sort-more"  class="dropdown more-filter">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">{{ __t('map_search.sorter.more', 'More') }}</a>
                                <ul class="dropdown-menu">
                                    <li onclick="sortby('beds', 'more');"><a href="#">{{ __t('map_search.sorter.bedrooms', 'Bedrooms') }}</a></li>
                                    <li onclick="sortby('baths', 'more');"><a href="#">{{ __t('map_search.sorter.bathrooms', 'Bathrooms') }}</a></li>
                                    <li onclick="sortby('size', 'more');"><a href="#">{{ __t('search.sorter.size', 'Size') }}</a></li>
                                    <li onclick="sortby('rental', 'more');"><a href="#">{{ __t('map_search.sorter.short-rental', 'Short rental') }}</a></li>
                                </ul>
                            </li>
                        </ul>
                        <div class="search-results">
                        </div>
                        <div class="no-search-results" style="display: none;">
                            <div class="alert alert-warning">
                                {{ __t('map_search.no-result', 'No result was found, please try to change some search options') }}
                            </div>
                        </div>
                        <div class="search-results-pagination">
                        </div>
                        <div class="loading-results">
                            <p>
                                <span><i class="fa fa-spinner fa-pulse fa-fw"></i> {{ __t('map_search.results.updating', 'Updating results') }}</span>
                            </p>
                        </div>
                    </div>
                </td>
            </tr>

        </table>


    </div>

</div>
<div id="property-template" class="hidden">
    <div class="property-item" data-year="" data-price="" data-beds="" data-baths="" data-size="" data-rental="">
        <img src="" class="prop-image img-responsive"/>
        <span class="img-count">2 images</span>
        <button class="favorite-btn"><i class="favorite fa"></i></button>
        <div class="details">
            <span class="listing-type"><i class="fa fa-circle"></i></span>  <span class="property"></span><br/>
            ‎<span class="listing-price"></span><br/>
            <div class="summary">
                <span class="prop-only">
                    <span class="beds"></span> <i class="fa fa-bed" aria-hidden="true"></i> |
                    <span class="bath"></span> <i class="fa fa-bath" aria-hidden="true"></i> |
                    <span class="size"></span>sqm | 
                </span>
                <span class="address"></span>
            </div>
        </div>
    </div>
</div>

<div id="property-preview" class="modal fade property-preview" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="action dismiss" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i> CLOSE</button>
                <button type="button" class="action expand" onclick="showFullProperty();"><i class="fa fa-external-link"></i> EXPAND</button>
                <h4 class="modal-title" id="property-title">&nbsp;</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="saved-message" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-check"></i> {{ __t('map_search.saved', 'Saved') }}</h4>
            </div>
            <div class="modal-body">
                <?php echo addslashes(__t('map_search.saved-message', 'Your search was saved on the right button')); ?>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<link rel="stylesheet" href="/scrollbar/jquery.mCustomScrollbar.css" />
<link rel="stylesheet" href="/fancybox/jquery.fancybox.css" />
<link href="/js/jquery-ui/jquery-ui.min.css" rel="stylesheet"/>

<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<link rel="stylesheet" href="/social/stylesheets/arthref.css">
<script src="/social/javascripts/socialShare.min.js"></script>
<script src="/social/javascripts/socialProfiles.min.js"></script>

<script src="/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="/js/underscore-min.js"></script>
<script src="/js/markerclusterer.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>
<script src="/fancybox/jquery.fancybox.pack.js"></script>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script src="/js/pagination/jquery.simplePagination.js"></script>
<script src="{{ __asset('/js/message.js') }}"></script>
<script src="{{ __asset('/js/infobox_packed.js') }}"></script>
<script>
    var domain = '<?php echo array_get($all, 'rel', ''); ?>';
    var currentId = <?php echo array_get($all, 'id', 'null') + 0; ?>;
    var searchCriteria = <?php echo json_encode($all); ?>;
    var currentPoiData = <?php echo $poi ? $poi->toJson() : 'null'; ?>;
    var currentProData;
<?php
if ($mapCenter) {
    ?>
    var lat0 = <?php echo $mapCenter->lat; ?>;
    var lng0 = <?php echo $mapCenter->lng; ?>;
    var zl0 = <?php echo $mapCenter->zl; ?>;
    <?php
} else {
    ?>
    var lat0 = 13.737998;
    var lng0 = 100.544032;
    var zl0 = 15;
    <?php
}
?>

</script>
<script src="{{ __asset('/js/property.js') }}"></script>
<script src="{{ __asset('/js/search/search.js') }}"></script>
<script src="{{ __asset('/js/property-gallery.js') }}"></script>

@endsection
