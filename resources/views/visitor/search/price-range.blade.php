<div class="dropdown price-range">
    <button id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="price-range-label">{{ __t('map_search.any-price' ,'Any price') }}</span>
        <span class="caret"></span>
    </button>
    
    
    <?php
    
    $pmin = array_get($all, 'pmin');
    $pmax= array_get($all, 'pmax');
    
    ?>
    
    <div class="dropdown-menu price" aria-labelledby="dLabel">
        <form class="form-horizontal">
            <div class="col-sm-6">
                <input type="text" class="form-control" id="price_min" name="price_min" value="<?php echo $pmin > 0 ? $pmin : ''; ?>" placeholder="{{ __t('map_search.listing.min-price', 'Min price') }}">
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="price_max" name="price_max" value="<?php echo $pmax > 0 ? $pmax : ''; ?>" placeholder="{{ __t('map_search.listing.max-price', 'Max price') }}">
            </div>
        </form>
        <div class="rent-prices">
            <?php
            $rental_prices = [2000, 4000, 6000, 8000, 10000, 15000, 20000, 25000, 30000, 35000, 40000, 45000,
                50000, 60000, 70000, 80000, 90000, 100000, 125000, 150000];
            ?>
            <ul class="list-group min-prices">
                <li class="list-group-item min-price" data-value="0" data-label="{{ __t('map_search.price-no-min-label', 'Any price') }}">
                    {{ __t('map_search.price-no-min', 'Any price') }}
                </li>
                <?php
                foreach ($rental_prices as $price) {
                    ?>
                    <li class="list-group-item min-price" data-value="{{ $price }}"data-label="{{ __knum($price) }}">฿ {{ number_format($price) }}</li>
                    <?php
                }
                ?>
            </ul>
            <ul class="list-group max-prices">
                <li class="list-group-item max-price" data-value="0" data-label="{{ __t('map_search.price-no-max-label', 'Any price') }}">
                    {{ __t('map_search.price-no-max', 'Any price') }}
                </li>
                <?php
                foreach ($rental_prices as $price) {
                    ?>
                    <li class="list-group-item max-price <?php echo $price < $pmax ? 'inactive' : ''; ?>" data-value="{{ $price }}" 
                        data-label="{{ __knum($price) }}">฿ {{ number_format($price) }}</li>
                    <?php
                }
                ?>
            </ul>
        </div>
        <div class="sale-prices">
            <?php
            $sales_prices = [
                500000, 1000000, 1500000, 2000000, 2500000, 3000000, 3500000, 4000000,
                4500000, 5000000, 5500000, 6000000, 6500000, 7000000, 7500000, 8000000,
                8500000, 9000000, 9500000, 10000000, 10500000, 12000000, 13000000,
                14000000, 15000000, 16000000, 15000000, 20000000, 50000000];
            ?>

            <ul class="list-group min-prices">
                <li class="list-group-item min-price" data-value="0" data-label="{{ __t('map_search.price-no-min-label', 'Any') }}">
                    {{ __t('map_search.price-no-min', 'No min price') }}
                </li>
                <?php
                foreach ($sales_prices as $price) {
                    ?>
                    <li class="list-group-item min-price" data-value="{{ $price }}" data-label="{{ __knum($price) }}">฿ {{ number_format($price) }}</li>
                    <?php
                }
                ?>
            </ul>
            <ul class="list-group max-prices">
                <li class="list-group-item max-price" data-value="0" data-label="{{ __t('map_search.price-no-max-label', 'Any') }}">
                    {{ __t('map_search.price-no-max', 'No max price') }}
                </li>
                <?php
                foreach ($sales_prices as $price) {
                    ?>
                    <li class="list-group-item max-price <?php echo $price < $pmax ? 'inactive' : ''; ?>" data-value="{{ $price }}" 
                        data-label="{{ __knum($price) }}">฿ {{ number_format($price) }}</li>
                        <?php
                    }
                    ?>
            </ul>
        </div>

    </div>

</div>