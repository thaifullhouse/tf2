@extends('layouts.default')

@section('title', 'Page Title')

<link rel="stylesheet" type="text/css" href="/lib/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/lib/angularjs-slider/dist/rzslider.min.css">
<script src="https://maps.google.com/maps/api/js?key=AIzaSyC9efZV5sak_Geyb6y7UMSfAaNt7fFUcfM&libraries=places"></script>
<script src="/lib/angular/angular.js"></script>
<script src="/lib/angular-animate/angular-animate.js"></script>
<script src="/lib/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="/lib/ngmap/build/scripts/ng-map.js"></script>
<script src="/lib/lodash/lodash.js"></script>
<script src="/lib/angularjs-slider/dist/rzslider.js"></script>
<script src="/lib/chart.js/dist/Chart.js"></script>
<script src="/lib/angular-chart.js/dist/angular-chart.js"></script>
<?php $active = 'points'; ?>

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.condo-details') }}</li>

@endsection

@section('content')

<div class="container" ng-app="myApp" ng-controller="myController">
    <div class="listing-header">
        <button class="btn btn-primary back-button"><< BACK</button>
        <div class="listing-header-title">
            <h3>@{{condo.name}}</h3>
            <span>Start Price: &#3647; @{{beautify(condo.salesPrice)}}</span>
        </div>
        <div class="ranking">
            <h4>RANKING <img src="/img/star.png" class="ranking-star" ng-repeat="i in getNumber(condo.ranking) track by $index"></img></h4>
        </div>     
    </div>
    <div class="listing-images">
        <img class="fit-to-parent" src="https://i.gyazo.com/27c253c0726381a71bdc26053aee49e2.jpg"></img>
    </div>
    <div class="listing-highlights">
        <div class="listing-highlight">
        <img src="/img/money.png"></img>
        </br>
        </br>
        <p>Sales Price Start From</p>
        <h4>@{{formatPrice(condo.salesPrice)}}</h4>        
        </div>
        <div class="listing-highlight">
        <img src="/img/circular-clock.png"></img>
        </br>
        </br>
        <p>Office Operating Hours</p>
        <h4>@{{condo.hours}}</h4>  
        </div>
        <div class="listing-highlight">
        <img src="/img/crane.png"></img>
        </br>
        </br>
        <p>Year Built</p>
        <h4>@{{condo.built}}</h4>
        </div>
        <div class="listing-highlight">
        <img src="/img/building-3.png"></img>
        </br>
        </br>
        <p>Tower</p>
        <h4>@{{condo.tower}}1</h4>
        </div>
        <div class="listing-highlight">
        <img src="/img/lift.png"></img>
        </br>
        </br>
        <p>Floor</p>
        <h4>@{{condo.floor}}</h4>
        </div>
    </div>
    <div class="listing-details">
        <div class="listing-info">
            <uib-tabset active="0">
                <uib-tab index="0" classes="listing-tab">
                    <uib-tab-heading>
                        <div class="listing-tab-title">Information</div>
                    </uib-tab-heading>
                    <div class="listing-tab-content" style="border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Total Units</p>
                            <p>Room Type</p>
                            <p>Parking Space</p>
                            <p>Website</p>
                        </div>
                        <div class="content-titles2" style="border-bottom: 1px solid gray;">
                            <p>330</p>
                            <p>32, 39, 40.4, 80 sq.m.</p>
                            <p>306 cars</p>
                            <p>www.ananda.co.th</p>
                        </div>
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Total Floors</p>
                            <p>District</p>
                            <p>Contact</p>
                        </div>
                        <div class="content-titles2" style="border-bottom: 1px solid gray;">
                            <p>33</p>
                            <p>Rama 2</p>
                            <p>02-777777</p>
                        </div>
                    </div>
                    <div class="listing-tab-content" style="border-top: none; border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Facilities</p>
                        </div>
                        <div class="content-facilities" style="border-bottom: 1px solid gray;">
                            <div class="facility-line">
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Gym</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Swimming Pool</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Sauna</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Playground</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Garden</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Reading Room</p>
                                </div>
                            </div>
                            <div class="facility-line">
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Elevator</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>24 Security</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>CCTV</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Parking</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Supermarket</p>
                                </div>
                                <div class="facility">
                                    <img src="/img/gym.png"></img>
                                    <p>Laundry</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Details</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa.</p>
                        </div>                          
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Address</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        </div>                          
                    </div>
                    <div class="listing-tab-content" style="border-top: none;border-bottom: none;">
                        <div class="content-titles1" style="border-bottom: 1px solid gray;border-right: 1px solid gray;">
                            <p>Map / Street View</p>
                        </div>
                        <div class="content-details" style="border-bottom: 1px solid gray;">
                            <div id="map" class="map-container" map-lazy-load="https://maps.google.com/maps/api/js" map-lazy-load-params="@{{googleMapsUrl}}">
                                <ng-map class="map" style="height: 100%;width:100%;" zoom="12" center="13.7563, 100.5018">
                                </ng-map>
                            </div>
                        </div>                          
                    </div> 
                    <div class="listing-tab-content" style="border-top: none;">
                        <div class="content-mortgage">
                            <p>Mortgage Payment Calculator</p>
                            <div class="mortgage-calculator">
                                <div class="mortgage-result">
                                    <canvas id="doughnut" class="chart chart-doughnut" chart-data="data" chart-labels="labels" chart-colors="backgroundColor"></canvas>
                                    <div style="position: absolute;
                                                border-radius: 50%;
                                                top: 74px;
                                                left: 74px;
                                                background: #e8fbff;
                                                width: 164px;
                                                height: 164px;
                                                display: block;
                                                clip: auto;
                                                padding: 40px 20px 30px 20px;
                                                box-shadow: 0px 0px 10px 3px rgba(0,0,0,0.5);">
                                        <div style="word-wrap: break-word;text-align: center;">
                                            <p style="font-weight: 700;font-size: 13px;">Your payment per month</p>
                                            <!-- payment per month logic -->
                                            <p style="font-weight: 700;font-size: 17px;">&#3647; @{{beautify(monthlyPayment)}}</p>
                                        </div>
                                    </div>
                                    <div style="position:absolute; bottom: 24px; left: 0px; display: table;">
                                        <div style="background-color: #343f41;width: 18px;display: table-cell;"></div><div style="display: table-cell;color: #36bedc;font-weight: 700;font-size: 13px;padding-left:10px;">Loan Amount</div>
                                    </div>
                                    <div style="position:absolute; bottom: 0px; left: 0px; display: table;">
                                        <div style="background-color: #36bedc;width: 18px;display: table-cell;"></div><div style="display: table-cell;color: #36bedc;font-weight: 700;font-size: 13px;padding-left:10px;">Total Amount</div>
                                    </div>
                                </div>
                                <div class="mortgage-options">
                                    <p style="float: left;">Principal Price</p>
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(condo.price)}}</div>
                                    <div style="clear: both;"></div>
                                    <!--
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(slider.price.value)}}</div>
                                    <rzslider rz-slider-model="slider.price.value" rz-slider-options="slider.price.options"></rzslider>
                                    -->
                                    <p style="float: left;">Loan Amount</p>
                                    <div class="mortgage-label" style="float: right;">&#3647; @{{beautify(slider.loan.value)}}</div>
                                    <rzslider rz-slider-model="slider.loan.value" rz-slider-options="slider.loan.options"></rzslider>
                                    <p style="float: left;">Rate (ARPR)</p>
                                    <div class="mortgage-label" style="float: right;">@{{parseInt(slider.rate.value * 100) + '%'}}</div>
                                    <rzslider rz-slider-model="slider.rate.value" rz-slider-options="slider.rate.options"></rzslider>
                                    <p style="float: left;">Term</p>
                                    <div class="mortgage-label" style="float: right;">@{{slider.term.value}} year@{{(slider.term.value === 1 ? '' : 's')}}</div>
                                    <rzslider rz-slider-model="slider.term.value" rz-slider-options="slider.term.options"></rzslider>
                                    <p style="float: left;margin-top: 14px;">Total Amount</p>
                                    <div class="mortgage-label mortgage-total">&#3647; @{{beautify(totalAmount)}}</div>
                                </div>
                            </div>
                        </div>                          
                    </div>
                </uib-tab>
                <uib-tab index="1" classes="listing-tab">
                    <uib-tab-heading>
                        <div class="listing-tab-title">Review</div>
                    </uib-tab-heading>
                    <div class="listing-tab-content" style="display:inherit;padding: 30px 30px 0px 30px;">
                        <div class="content-review">
                            <p>How to get there</p>
                            <img src="/img/howtoget.png" style="margin-bottom:10px"></img>
                            <div class="details">DETAILS: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa</div>
                        </div>
                        <div class="content-review">
                            <p>Room Type</p>
                            <img src="/img/roomtype.png" style="margin-bottom:10px"></img>
                            <div class="details">DETAILS: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa</div>
                        </div>
                        <div class="content-review">
                            <p>Facilities &amp; Landscape</p>
                            <img src="/img/roomtype.png" style="margin-bottom:10px"></img>
                            <div class="details">DETAILS: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa</div>
                        </div>
                        <div class="content-review">
                            <p>Floor Plans</p>
                            <img src="/img/floorplan.png" style="margin-bottom:10px"></img>
                            <div class="details">DETAILS: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa</div>
                        </div>
                        <div class="content-review">
                            <p>Project Video</p>
                            <div class="video-container">
                                <iframe src="https://www.youtube.com/embed/DiK1K665YB4" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                        <div class="content-review">
                            <p>Overview</p>
                            <div class="details">DETAILS: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed fringilla ligula in tincidunt blandit. Integer vitae mi augue. Etiam nec nibh mi. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed enim nisl. Morbi et tellus et orci feugiat rutrum. Vestibulum massa libero, facilisis eget tristique id, feugiat vel risus. Nulla eu feugiat mi, vel suscipit massa</div>
                        </div>
                    </div>
                </uib-tab>
            </uib-tabset>
            <div class="listing-tab-content" style="border: none;" ng-show="totalItems > 0" id="comments">
                <div style="width: 100%;">
                    <div style="display: table;margin: 20 auto 20 auto;" items-per-page="itemsPerPage" uib-pagination boundary-links="true" max-size="maxSize" rotate="true" total-items="totalItems" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                    <div style="margin: 0 5px 0 5px;">
                        <div ng-repeat="comment in comments.data.slice(((currentPage - 1) * itemsPerPage), (((currentPage - 1) * itemsPerPage) + itemsPerPage)) | orderBy:'id'">
                            <div class="comment-box">
                                <div class="reply-no"># Reply @{{((currentPage - 1) * itemsPerPage) + $index + 1}}</div>
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div style="padding: 10px 10px 10px 10px;">
                                            <div style="width:200px;height:60px;background-color:black;text-align:center;color:white;">
                                                <div ng-style="{ width: parseInt(likes * 100 / (likes + dislikes)) + '%'}" style="height:100%;background-color:#36bedc;float:left;padding-top: 10px;">
                                                    <div class="glyphicon glyphicon-heart" style="font-size:1.5em;"></div>
                                                    <div>@{{likes}}</div>
                                                </div>
                                                <div ng-style="{ width: parseInt(dislikes * 100 / (likes + dislikes)) + '%'}" style="height:100%;background-color:#343f41;float:left;padding-top: 10px;">
                                                    <div class="glyphicon glyphicon-thumbs-down" style="font-size:1.5em;"></div>
                                                    <div>@{{dislikes}}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="commenter">
                                            <div class="commenter-email">
                                                @{{comment.user.email}}
                                            </div>
                                            <div>
                                                Posted on @{{comment.created_at}}
                                            </div>
                                        </div>
                                        <div style="word-break: break-word;">
                                            @{{comment.review}}
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:30px;padding-left:10px;color:#0DB9F0;">
                                    <div class="col-xs-9">
                                        <div style="display:flex;">
                                            <div style="flex:1">
                                                <div>Condo Facilities</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 10" ng-attr-id="@{{$index + 'star51'}}" ng-attr-name="@{{$index + 'rating1'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star51'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 9" ng-attr-id="@{{$index + 'star41half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star41half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 8" ng-attr-id="@{{$index + 'star41'}}" ng-attr-name="@{{$index + 'rating1'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star41'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 7" ng-attr-id="@{{$index + 'star31half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star31half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 6" ng-attr-id="@{{$index + 'star31'}}" ng-attr-name="@{{$index + 'rating1'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star31'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 5" ng-attr-id="@{{$index + 'star21half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star21half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 4" ng-attr-id="@{{$index + 'star21'}}" ng-attr-name="@{{$index + 'rating1'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star21'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 3" ng-attr-id="@{{$index + 'star11half'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star11half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 2" ng-attr-id="@{{$index + 'star11'}}" ng-attr-name="@{{$index + 'rating1'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star11'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.facilities == 1" ng-attr-id="@{{$index + 'starhalf1'}}" ng-attr-name="@{{$index + 'rating1'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf1'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Room Design</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 10" ng-attr-id="@{{$index + 'star52'}}" ng-attr-name="@{{$index + 'rating2'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star52'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 9" ng-attr-id="@{{$index + 'star42half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star42half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 8" ng-attr-id="@{{$index + 'star42'}}" ng-attr-name="@{{$index + 'rating2'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star42'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 7" ng-attr-id="@{{$index + 'star32half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star32half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 6" ng-attr-id="@{{$index + 'star32'}}" ng-attr-name="@{{$index + 'rating2'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star32'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 5" ng-attr-id="@{{$index + 'star22half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star22half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 4" ng-attr-id="@{{$index + 'star22'}}" ng-attr-name="@{{$index + 'rating2'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star22'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 3" ng-attr-id="@{{$index + 'star12half'}}" ng-attr-name="@{{$index + 'rating2'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star12half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 2" ng-attr-id="@{{$index + 'star12'}}" ng-attr-name="@{{$index + 'rating2'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star12'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.design == 1" ng-attr-id="@{{$index + 'starhalf2'}}" ng-attr-name="@{{$index + 'rating2'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf2'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Condo Management</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 10" ng-attr-id="@{{$index + 'star53'}}" ng-attr-name="@{{$index + 'rating3'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star53'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 9" ng-attr-id="@{{$index + 'star43half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star43half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 8" ng-attr-id="@{{$index + 'star43'}}" ng-attr-name="@{{$index + 'rating3'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star43'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 7" ng-attr-id="@{{$index + 'star33half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star33half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 6" ng-attr-id="@{{$index + 'star33'}}" ng-attr-name="@{{$index + 'rating3'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star33'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 5" ng-attr-id="@{{$index + 'star23half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star23half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 4" ng-attr-id="@{{$index + 'star23'}}" ng-attr-name="@{{$index + 'rating3'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star23'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 3" ng-attr-id="@{{$index + 'star13half'}}" ng-attr-name="@{{$index + 'rating3'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star13half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 2" ng-attr-id="@{{$index + 'star13'}}" ng-attr-name="@{{$index + 'rating3'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star13'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.management == 1" ng-attr-id="@{{$index + 'starhalf3'}}" ng-attr-name="@{{$index + 'rating3'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf3'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        <div style="display:flex;">
                                            <div style="flex:1">
                                                <div>Security</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 10" ng-attr-id="@{{$index + 'star54'}}" ng-attr-name="@{{$index + 'rating4'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star54'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 9" ng-attr-id="@{{$index + 'star44half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star44half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 8" ng-attr-id="@{{$index + 'star44'}}" ng-attr-name="@{{$index + 'rating4'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star44'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 7" ng-attr-id="@{{$index + 'star34half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star34half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 6" ng-attr-id="@{{$index + 'star34'}}" ng-attr-name="@{{$index + 'rating4'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star34'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 5" ng-attr-id="@{{$index + 'star24half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star24half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 4" ng-attr-id="@{{$index + 'star24'}}" ng-attr-name="@{{$index + 'rating4'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star24'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 3" ng-attr-id="@{{$index + 'star14half'}}" ng-attr-name="@{{$index + 'rating4'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star14half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 2" ng-attr-id="@{{$index + 'star14'}}" ng-attr-name="@{{$index + 'rating4'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star14'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.security == 1" ng-attr-id="@{{$index + 'starhalf4'}}" ng-attr-name="@{{$index + 'rating4'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf4'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Nearby Amenities</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 10" ng-attr-id="@{{$index + 'star55'}}" ng-attr-name="@{{$index + 'rating5'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star55'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 9" ng-attr-id="@{{$index + 'star45half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star45half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 8" ng-attr-id="@{{$index + 'star45'}}" ng-attr-name="@{{$index + 'rating5'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star45'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 7" ng-attr-id="@{{$index + 'star35half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star35half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 6" ng-attr-id="@{{$index + 'star35'}}" ng-attr-name="@{{$index + 'rating5'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star35'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 5" ng-attr-id="@{{$index + 'star25half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star25half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 4" ng-attr-id="@{{$index + 'star25'}}" ng-attr-name="@{{$index + 'rating5'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star25'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 3" ng-attr-id="@{{$index + 'star15half'}}" ng-attr-name="@{{$index + 'rating5'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star15half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 2" ng-attr-id="@{{$index + 'star15'}}" ng-attr-name="@{{$index + 'rating5'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star15'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.amenities == 1" ng-attr-id="@{{$index + 'starhalf5'}}" ng-attr-name="@{{$index + 'rating5'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf5'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <div style="flex:1">
                                                <div>Transport Accessibility</div>
                                                <div style="margin-bottom:5px;">
                                                    <fieldset class="rating-passive">
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 10" ng-attr-id="@{{$index + 'star56'}}" ng-attr-name="@{{$index + 'rating6'}}" value="5" /><label class = "full" ng-attr-for="@{{$index + 'star56'}}" title="Awesome - 5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 9" ng-attr-id="@{{$index + 'star46half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="4 and a half" /><label class="half" ng-attr-for="@{{$index + 'star46half'}}" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 8" ng-attr-id="@{{$index + 'star46'}}" ng-attr-name="@{{$index + 'rating6'}}" value="4" /><label class = "full" ng-attr-for="@{{$index + 'star46'}}" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 7" ng-attr-id="@{{$index + 'star36half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="3 and a half" /><label class="half" ng-attr-for="@{{$index + 'star36half'}}" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 6" ng-attr-id="@{{$index + 'star36'}}" ng-attr-name="@{{$index + 'rating6'}}" value="3" /><label class = "full" ng-attr-for="@{{$index + 'star36'}}" title="Meh - 3 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 5" ng-attr-id="@{{$index + 'star26half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="2 and a half" /><label class="half" ng-attr-for="@{{$index + 'star26half'}}" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 4" ng-attr-id="@{{$index + 'star26'}}" ng-attr-name="@{{$index + 'rating6'}}" value="2" /><label class = "full" ng-attr-for="@{{$index + 'star26'}}" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 3" ng-attr-id="@{{$index + 'star16half'}}" ng-attr-name="@{{$index + 'rating6'}}" value="1 and a half" /><label class="half" ng-attr-for="@{{$index + 'star16half'}}" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 2" ng-attr-id="@{{$index + 'star16'}}" ng-attr-name="@{{$index + 'rating6'}}" value="1" /><label class = "full" ng-attr-for="@{{$index + 'star16'}}" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" disabled ng-checked="comment.ratings.accessibility == 1" ng-attr-id="@{{$index + 'starhalf6'}}" ng-attr-name="@{{$index + 'rating6'}}" value="half" /><label class="half" ng-attr-for="@{{$index + 'starhalf6'}}" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                    </div>
                                </div>
                                <div style="text-align:right;">
                                    <span style="cursor:pointer;">Edit</span><span style="cursor:pointer;"><span ng-click="replyActive = !replyActive" class="glyphicon glyphicon-share-alt" style="margin-left: 20px;"></span> <span ng-click="replyActive = !replyActive">Reply</span></span>
                                </div>
                            </div>
                            <div class="reply-box" ng-show="replyActive">
                                <div class="commenter-email" style="margin-bottom:10px;">You@Navxx.com</div>
                                <textarea style="width:100%;height:100px;margin-bottom:10px;resize: none;"></textarea>
                                <button style="float:right;color:white;background-color:#686D70;border:none;padding:0px 20px 0px 20px;">Post</button>
                            </div>
                        </div>
                    </div>
                    <div id="comments-end" style="display: table;margin: 20 auto 20 auto;" items-per-page="itemsPerPage" uib-pagination boundary-links="true" max-size="maxSize" rotate="true" total-items="totalItems" ng-model="currentPage" class="pagination-sm" previous-text="&lsaquo;" next-text="&rsaquo;" first-text="&laquo;" last-text="&raquo;"></div>
                </div>
            </div>
            <div class="listing-tab-content" style="border: none;" id="reply-ratings">
                <div style="width:100%;">
                    <div style="display:table;margin: 30px 30px 10px 30px;text-transform:uppercase;font-weight:bold;font-size:17px;">
                        Give Us Your Comment
                    </div>
                    <div style="margin: 0px 10px 10px 10px;background-color:#F6FDFF;padding: 20px 20px 0px 20px;">
                        <div style="text-align: right;">
                            <div class="commenter-email">
                                @{{reply.user.email}}
                            </div>
                        </div>
                        <div style="margin: 0px 30px 30px 30px;">
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:10px;">
                                Your Comment About This Project
                            </div>
                            <textarea style="width:100%;height:100px;margin-bottom:20px;resize:none;" ng-model="reply.review"></textarea>
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:10px;">
                                Give Your Score Review
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Condo Facilities</div>
                                    <div style="margin-bottom:5px;" class="rating">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.facilities = 10" id="star51" name="rating1" value="5" /><label class = "full" for="star51" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 9" id="star41half" name="rating1" value="4 and a half" /><label class="half" for="star41half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 8" id="star41" name="rating1" value="4" /><label class = "full" for="star41" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 7" id="star31half" name="rating1" value="3 and a half" /><label class="half" for="star31half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 6" id="star31" name="rating1" value="3" /><label class = "full" for="star31" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 5" id="star21half" name="rating1" value="2 and a half" /><label class="half" for="star21half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 4" id="star21" name="rating1" value="2" /><label class = "full" for="star21" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 3" id="star11half" name="rating1" value="1 and a half" /><label class="half" for="star11half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 2" id="star11" name="rating1" value="1" /><label class = "full" for="star11" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.facilities = 1" id="starhalf1" name="rating1" value="half" /><label class="half" for="starhalf1" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Room Design</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.design = 10" id="star52" name="rating2" value="5" /><label class = "full" for="star52" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 9" id="star42half" name="rating2" value="4 and a half" /><label class="half" for="star42half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 8" id="star42" name="rating2" value="4" /><label class = "full" for="star42" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 7" id="star32half" name="rating2" value="3 and a half" /><label class="half" for="star32half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 6" id="star32" name="rating2" value="3" /><label class = "full" for="star32" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 5" id="star22half" name="rating2" value="2 and a half" /><label class="half" for="star22half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 4" id="star22" name="rating2" value="2" /><label class = "full" for="star22" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 3" id="star12half" name="rating2" value="1 and a half" /><label class="half" for="star12half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 2" id="star12" name="rating2" value="1" /><label class = "full" for="star12" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.design = 1" id="starhalf2" name="rating2" value="half" /><label class="half" for="starhalf2" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Transport Accessibility</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.accessibility = 10" id="star53" name="rating3" value="5" /><label class = "full" for="star53" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 9" id="star43half" name="rating3" value="4 and a half" /><label class="half" for="star43half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 8" id="star43" name="rating3" value="4" /><label class = "full" for="star43" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 7" id="star33half" name="rating3" value="3 and a half" /><label class="half" for="star33half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 6" id="star33" name="rating3" value="3" /><label class = "full" for="star33" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 5" id="star23half" name="rating3" value="2 and a half" /><label class="half" for="star23half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 4" id="star23" name="rating3" value="2" /><label class = "full" for="star23" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 3" id="star13half" name="rating3" value="1 and a half" /><label class="half" for="star13half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 2" id="star13" name="rating3" value="1" /><label class = "full" for="star13" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.accessibility = 1" id="starhalf3" name="rating3" value="half" /><label class="half" for="starhalf3" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Condo Management</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.management = 10" id="star54" name="rating4" value="5" /><label class = "full" for="star54" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 9" id="star44half" name="rating4" value="4 and a half" /><label class="half" for="star44half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 8" id="star44" name="rating4" value="4" /><label class = "full" for="star44" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 7" id="star34half" name="rating4" value="3 and a half" /><label class="half" for="star34half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 6" id="star34" name="rating4" value="3" /><label class = "full" for="star34" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 5" id="star24half" name="rating4" value="2 and a half" /><label class="half" for="star24half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 4" id="star24" name="rating4" value="2" /><label class = "full" for="star24" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 3" id="star14half" name="rating4" value="1 and a half" /><label class="half" for="star14half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 2" id="star14" name="rating4" value="1" /><label class = "full" for="star14" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.management = 1" id="starhalf4" name="rating4" value="half" /><label class="half" for="starhalf4" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="display:flex;">
                                <div style="flex:1">
                                    <div style="margin-bottom:5px;">Security</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.security = 10" id="star55" name="rating5" value="5" /><label class = "full" for="star55" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 9" id="star45half" name="rating5" value="4 and a half" /><label class="half" for="star45half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 8" id="star45" name="rating5" value="4" /><label class = "full" for="star45" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 7" id="star35half" name="rating5" value="3 and a half" /><label class="half" for="star35half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 6" id="star35" name="rating5" value="3" /><label class = "full" for="star35" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 5" id="star25half" name="rating5" value="2 and a half" /><label class="half" for="star25half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 4" id="star25" name="rating5" value="2" /><label class = "full" for="star25" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 3" id="star15half" name="rating5" value="1 and a half" /><label class="half" for="star15half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 2" id="star15" name="rating5" value="1" /><label class = "full" for="star15" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.security = 1" id="starhalf5" name="rating5" value="half" /><label class="half" for="starhalf5" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <div style="flex:2">
                                    <div style="margin-bottom:5px;">Nearby Amenities</div>
                                    <div style="margin-bottom:5px;">
                                        <fieldset class="rating">
                                            <input type="radio" ng-click="reply.ratings.amenities = 10" id="star56" name="rating6" value="5" /><label class = "full" for="star56" title="Awesome - 5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 9" id="star46half" name="rating6" value="4 and a half" /><label class="half" for="star46half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 8" id="star46" name="rating6" value="4" /><label class = "full" for="star46" title="Pretty good - 4 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 7" id="star36half" name="rating6" value="3 and a half" /><label class="half" for="star36half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 6" id="star36" name="rating6" value="3" /><label class = "full" for="star36" title="Meh - 3 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 5" id="star26half" name="rating6" value="2 and a half" /><label class="half" for="star26half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 4" id="star26" name="rating6" value="2" /><label class = "full" for="star26" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 3" id="star16half" name="rating6" value="1 and a half" /><label class="half" for="star16half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 2" id="star16" name="rating6" value="1" /><label class = "full" for="star16" title="Sucks big time - 1 star"></label>
                                            <input type="radio" ng-click="reply.ratings.amenities = 1" id="starhalf6" name="rating6" value="half" /><label class="half" for="starhalf6" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            <div style="text-transform:uppercase;font-weight:bold;font-size:14px;margin-bottom:20px;margin-top:20px;">
                                Upload Picture
                            </div>
                            <div class="row">
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg" />
                                    <input type="file" name="userprofile_picture" id="filePhoto" />
                                </div>
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg2" />
                                    <input type="file" name="userprofile_picture" id="filePhoto2" />
                                </div>
                                <div class="uploader col-xs-3">
                                    <img class="cover" src="http://www.freeiconspng.com/uploads/plus-icon-black-2.png"/>
                                    <img src="" class="uploaded" id="uploadedImg3" />
                                    <input type="file" name="userprofile_picture" id="filePhoto3" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:10px;margin-top:40px;color:gray;">
                                    <input type="checkbox" style="margin-right:10px;" /><span>I agree with ThaiFullHouse's Terms and Conditions</span>
                                </div>
                                <div class="col-xs-12" style="text-align:center;font-size:16px;margin-bottom:20px;margin-top:10px;color:gray;">
                                    <input type="submit" class="btn btn-primary" style="border-radius:0;background-color:#06B4FF;width: 80px;" value="POST" ng-click="submitReply()" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="project-page agent-contact">
            <div class="project-info">
                <p style="margin-bottom:0;">Information</p>
                <hr />
                <div class="project-info-flex">
                    <div class="project-info-img">
                        <img src="/img/condo.jpg"></img>
                    </div>
                    <div class="project-info-text">
                        <p>Phuket9 Okey Development Co., Ltd.</p>
                        <p>www.xxxx.com</p>
                        <p style="project-p-bold">Start Price: &#3647; 3,650,000</p>
                    </div>
                </div>
                <button class="submit btn btn-primary" name="tel">Tel.</button>
            </div>
            <hr />
            <p>Contact</p>
            <form name="form">
                <label for="name">Name</label><input name="name" type="text" placeholder="Name" />
                <label for="tel">Tel.</label><input name="tel" type="tel" placeholder="Telephone" />
                <label for="email">E-mail</label><input name="email" type="email" placeholder="E - mail" />
                <label for="message">Message</label><textarea name="message" placeholder="Your message"></textarea>
                <input class="news" name="news" type="checkbox" /><label class="news" for="news">You want us to send News Letters</label>
                <button class="submit btn btn-primary" name="submit" type="submit"><i class="glyphicon glyphicon-ok"></i>Submit</button>
            </form>
        </div>
    </div>
    <p style="margin: 30px auto;font-size:16px;font-weight:bold;">Nearby New Projects</p>
    <div style="position:relative;width:100%;height:350px;">
        <div style="z-index:2;position:absolute;height:100%;width:60px;top:0;left:0;cursor:pointer;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));" ng-click="scrollLeft()">
            <img src="/img/left-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
        </div>
        <div style="z-index:2;position:absolute;height:100%;width:60px;;top:0;right:0;cursor:pointer;background:linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3));" ng-click="scrollRight()">
            <img src="/img/right-arrow.png" style="width:80%;position: absolute;top: 0; bottom:0; left: 0; right:0;margin: auto;"></img>
        </div>
        <div style="position:relative;width:100%;height:350px;overflow-x: hidden;overflow-y:hidden;white-space: nowrap;" id="numWrap">
            <div class="listing-room" style="width:300px;margin-left:0;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
            <div class="listing-room" style="width:300px;margin-left:40px;display: inline-block;">
                <div class="listing-room-image" back-img="@{{search.img[0]}}"></div>
                <div class="listing-room-details">
                    <div style="font-size:16px;font-weight:bold;">Condo ideo verve Sukhumvit</div>
                    <div style="font-size:14px;font-weight:bold;color:gray;">Sukhumvit, Prakanhong, Bangkok</div>
                    <div style="font-size:16px;font-weight:bold;">&#3647; 18,000 / month</div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="/condo.js"></script>
@endsection