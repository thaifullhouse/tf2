@extends('layouts.error')

@section('title', 'Page Title')

@section('content')

<h4>{{ trans('errors.forbidden-access-title') }}</h4>
<p>
    {{ trans('errors.forbidden-access-text') }}. 
</p>
<p>
    If you think this is an error please contact our administrator: admin@thaifullhouse.com    
</p>
<p>
    <?php
    if (!\Auth::check()) {
        ?>
    <p>
        {!! trans('errors.forbidden-login-text') !!}
    </p>
    <?php
}
?>
</p>

@endsection