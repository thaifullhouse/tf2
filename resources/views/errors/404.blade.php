@extends('layouts.error')

@section('title', 'Page Title')

@section('content')

<h4>{{ trans('errors.not-found-title') }}</h4>
<p>
   {{ trans('errors.not-found-text') }} 
</p>


@endsection