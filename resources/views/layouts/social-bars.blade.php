<?php

$settings = [];

try {
    $settings = \DB::table('settings')->pluck('value', 'domain')->all();
} catch (\Exception $ex) {

} catch (\Illuminate\Database\QueryException $e) {
    
}

?>
<ul class="nav navbar-nav navbar-social">
    <li><a href="{{ array_get($settings, 'facebook_link') }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
    <li><a href="{{ array_get($settings, 'instagram_link') }}" target="_blank"><i class="fa fa-instagram"></i></a></li>
    <li><a href="{{ array_get($settings, 'twitter_link') }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
    <li><a href="{{ array_get($settings, 'google_link') }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
</ul>
