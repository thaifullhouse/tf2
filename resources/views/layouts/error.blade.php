<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{ trans('nav.appname') }} - @yield('title')</title>

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Signika:400,600,300' rel='stylesheet' type='text/css'>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/style.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="topbar">
            <nav class="navbar navbar-blue navbar-static-top">
                <div class="container">
                    <div id="navbar" class="collapse navbar-collapse">
                        @include('layouts.social-bars')
                        <ul class="nav navbar-nav navbar-right navbar-lang">
                            @include('layouts.lang_selector')
                            <li><a href="#"><i class="fa fa-search"></i></a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </nav>
        </div>
        <div class="container mainmenu-container">
            @include('layouts.mainmenu')
        </div>

        <div class="container main-container error-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    @yield('content')
                </div>
            </div>
        </div>

        <div class="container">
            @include('layouts.footer')
        </div>

        <script
            src="https://code.jquery.com/jquery-2.2.4.min.js"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/notify.min.js"></script>
        <script src="/fancybox/jquery.fancybox.js"></script>
        <script src="/js/jquery-ui/jquery-ui.min.js"></script>
        <script src="/js/app.js"></script>
        <script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var hl = '<?php echo session('lang'); ?>';
        </script>
        @yield('scripts')
    </body>
</html>
