<footer>
    <div class="row">
        <div class="col-md-3">
            <h5>Properties for Sales</h5>
            <ul class="list-unstyled">
                <li><a href="/search?lt=sale&pt=dh">Houses for Sales</a></li>
                <li><a href="/search?lt=sale&pt=co">Condos for Sales</a></li>
                <li><a href="/search?lt=sale&pt=th">Townhouses for Sales</a></li>
                <li><a href="/search?lt=sale&pt=ap">Apartments for Sales</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h5>Properties for Rent</h5>
            <ul class="list-unstyled">
                <li><a href="/search?lt=rent&pt=dh">Houses for Rent</a></li>
                <li><a href="/search?lt=rent&pt=co">Condos for Rent</a></li>
                <li><a href="/search?lt=rent&pt=th">Townhouses for Rent</a></li>
                <li><a href="/search?lt=rent&pt=ap">Apartments for Rent</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h5>Search Condos by Price</h5>
            <ul class="list-unstyled">
                <li><a href="/search?lt=sale&pt=co&price_min=1000000&price_max=5000000">Condos for sales 1-5M.</a></li>
                <li><a href="/search?lt=sale&pt=co&price_min=5000000&price_max=10000000">Condos for sales 6-10M.</a></li>
                <li><a href="/search?lt=sale&pt=co&price_min=11000000&price_max=15000000">Condos for sales 11-15M.</a></li>
                <li><a href="/search?lt=sale&pt=co&price_min=16000000&price_max=">Condos for sales 16M+</a></li>
            </ul>
        </div>
        <div class="col-md-3">
            <h5>Search Houses by Price</h5>
            <ul class="list-unstyled">
                <li><a href="/search?lt=sale&pt=dh&price_min=5000000&price_max=10000000">Houses for sales 5-10M.</a></li>
                <li><a href="/search?lt=sale&pt=dh&price_min=11000000&price_max=15000000">Houses for sales 11-15M.</a></li>
                <li><a href="/search?lt=sale&pt=dh&price_min=16000000&price_max=20000000">Houses for sales 16-20M.</a></li>
                <li><a href="/search?lt=sale&pt=dh&price_min=21000000&price_max=">Houses for sales 21M+</a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            <h5>Developers</h5>
        </div>
        <div class="col-md-3">
            <h5>Popular areas in Bangkok</h5>
        </div>
        <div class="col-md-3">
            <h5>Advertise with us</h5>
        </div>
        <div class="col-md-3">
            <h5>Company Info</h5>
        </div>
    </div>
</footer>
<footer class="address">
    <div class="row">
        <div class="col-md-6">
            <img src="/img/logo-small.png" class="bottom-logo"/>
        </div>
        <div class="col-md-6">
            <h3>Wonderpons Co.,Ltd</h3>
            <p>
                <?php echo nl2br(__dbconf('footer_contact', true)); ?>
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 text-center">
            <ul class="nav nav-pills footer-nav">
                <li role="presentation"><a href="#">Account Sign In</a></li>
                <li role="presentation"><a href="#">Privacy Policy</a></li>
                <li role="presentation"><a href="#">Terms and Conditions</a></li>
                <li role="presentation"><a href="#">Contact Us</a></li>
                <li role="presentation"><a href="#">Careers</a></li>
                <li role="presentation"><a href="#">Copyright &copy; Thaifullhouse Co., Ltd.</a></li>
            </ul>
        </div>
    </div>
</footer>
