<a href="/"><img src="/img/logo-small.png" class="logo"/></a>
<ul class="nav nav-pills main-nav">
    <li role="presentation"><a href="/search?lt=sale" class="main">{{ __t('topmenu.sale', 'SALE') }}</a></li>
    <li role="presentation"><a href="/search?lt=rent" class="main">{{ __t('topmenu.rent', 'Rent') }}</a></li>
    <li role="presentation"><a href="/search?np=yes" class="main">{{ __t('topmenu.new-project', 'New project') }}</a></li>
    <li role="presentation"><a href="/agent/search" class="main">{{ __t('topmenu.find-agent', 'Find agent') }}</a></li>
    <li role="presentation"><a href="/search?pt=cd" class="main">{{ __t('topmenu.condo-community', 'Condo community') }}</a></li>

    <?php
    $user_is_authenticated = Auth::check();



    if ($user_is_authenticated) {

        $user = Auth::user();
        $profile = $user->getProfile();

        $userType = $user->getType();

        $firstname = $user->realname ? $user->realname : $user->email;
       

        if ($userType->code == 'member') {

            if ($profile && $profile->firstname) {
                $firstname = $profile->firstname;
            }

            $new_messages = $user->countNewMessage();
            ?>
            <li role="presentation" class="pull-right">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $firstname }}</strong> <i class="fa fa-chevron-down"></i></a>
                <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                    <ul class="list-group">
                        <li class="list-group-item"><a href="/member/settings">{{ __t('menu.setting', 'Setting profile') }}</a></li>
                        <li class="list-group-item"><a href="/member/listing">{{ __t('menu.listing', 'My listings') }}</a></li>
                        <li class="list-group-item"><a href="/member/purchase">{{ __t('menu.purchase', 'My purchase') }}</a></li>
                        <li class="list-group-item"><a href="/member/message">{{ __t('menu.messages', 'Message') }}</a>
                            <?php
                            if ($new_messages > 0) {
                                ?>
                                <span class="badge">{{ $new_messages }}</span>
                                <?php
                            }
                            ?>
                        </li>
                        <li class="list-group-item"><a href="/member/favorite">{{ __t('menu.favorites', 'Favorites') }}</a></li>
                        <li class="list-group-item"><a href="/member/notice">{{ __t('menu.notices', 'Notice') }}</a></li>
                        <li class="list-group-item"><a href="/auth/logout">{{ __t('menu.logout', 'Logout') }}</a></li>
                    </ul>
                </div>
            </li>
            <?php
        } else {
            ?>
            <li role="presentation" class="pull-right">
                <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><strong>{{ $firstname }}</strong> <i class="fa fa-chevron-down"></i></a>
                <div class="dropdown-menu account-dropdown" aria-labelledby="drop3">
                    <ul class="list-group">
                        <?php
                        if ($userType->code == 'manager') {
                            ?>
                            <li class="list-group-item">
                                <a href="/manager/dashboard" class="main">Dashboard</a>
                            </li>
                            <?php
                        }
                        ?>
                        <li class="list-group-item">
                            <a href="/auth/logout" class="main">{{ trans('nav.menu.logout') }} (<?php echo $userType->code; ?>)</a>
                        </li>
                    </ul>
                </div>
            </li>
            <?php
        }
    } else {
        ?>
        <li role="presentation" class="pull-right">
            <a href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="main"><i class="fa fa-user"></i> {{ trans('nav.menu.login-register') }}</a>
            <div class="dropdown-menu session-dropdown" aria-labelledby="drop3">
                <div class="auth-box">
                    <div class="social-btn-container">
                        <p>
                            <a href="/auth/facebook" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> {{ trans('nav.menu.sign-facebook') }}</a>
                        </p>
                        <p>
                            <a href="/auth/twitter" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> {{ trans('nav.menu.sign-twitter') }}</a>
                        </p>
                        <p>
                            <a href="/auth/google" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus"></i> {{ trans('nav.menu.sign-google') }}</a>
                        </p>
                    </div>

                    <p class="text-center or-separator-container">
                        <span class="or-separator">or</span>
                    </p>

                    <form method="post" id="login-form">
                        <div class="alert alert-danger error-box" >
                            <ul id="login-errors-list" class="list-unstyled">
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="email">{{ trans('nav.menu.email') }}</label>
                            <input type="text" class="form-control" name="email" value="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="password">{{ trans('nav.menu.password') }}</label>
                            <input type="password" class="form-control" name="password" value="" placeholder="">
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> {{ trans('nav.menu.remember-me') }}
                            </label>
                        </div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="button" class="btn btn-default" id="ajax-login-submiter">
                            {{ trans('nav.menu.sign-in') }} <i class="fa fa-spinner fa-spin fa-fw"></i>
                        </button>
                        <p class="account-link">
                            <a href="/auth/request-password">{{ trans('nav.menu.forgot-password') }}</a>
                        </p>
                        <p class="text-right account-link">
                            <a href="#" class="auth-mode-switcher">{{ trans('nav.menu.dont-have-account') }}</a>
                        </p>
                    </form>
                </div>
                <div class="auth-box" style="display: none;">
                    <div class="social-btn-container">
                        <p>
                            <button type="button" class="btn btn-facebook btn-block"><i class="fa fa-facebook"></i> {{ trans('nav.menu.join-facebook') }}</button>
                        </p>
                        <p>
                            <button type="button" class="btn btn-twitter btn-block"><i class="fa fa-twitter"></i> {{ trans('nav.menu.join-twitter') }}</button>
                        </p>
                        <p>
                            <button type="button" class="btn btn-google-plus btn-block"><i class="fa fa-google-plus"></i> {{ trans('nav.menu.join-google') }}</button>
                        </p>
                    </div>

                    <p class="text-center or-separator-container">
                        <span class="or-separator">or</span>
                    </p>

                    <form method="post" id="registration-form">
                        <div class="alert alert-danger error-box" >
                            <ul id="registration-errors-list" class="list-unstyled">
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" name="email" value="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" name="password" value="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="email">Re-Password</label>
                            <input type="password" class="form-control" name="password_confirmation" value="" placeholder="">
                        </div>

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="button" class="btn btn-default" id="ajax-registration-submiter">Sign Up <i class="fa fa-spinner fa-spin fa-fw"></i></button>

                        <p class="text-right account-link">
                            <a href="#" class="auth-mode-switcher">Already have an account ?</a>
                        </p>
                    </form>
                </div>

            </div>
        </li>
        <?php
    }
    ?>


</ul>
