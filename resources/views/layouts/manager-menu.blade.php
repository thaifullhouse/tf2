<a href="/"><img src="/img/logo-small.png" class="logo"/></a>
<ul class="nav nav-pills main-nav">
    <li role="presentation"><a href="/search?lt=sale" class="main">{{ __t('topmenu.sale', 'SALE') }}</a></li>
    <li role="presentation"><a href="/search?lt=rent" class="main">{{ __t('topmenu.rent', 'Rent') }}</a></li>
    <li role="presentation"><a href="/search?np=yes" class="main">{{ __t('topmenu.new-project', 'New project') }}</a></li>
    <li role="presentation"><a href="/agent/search" class="main">{{ __t('topmenu.find-agent', 'Find agent') }}</a></li>
    <li role="presentation"><a href="/search?pt=cd" class="main">{{ __t('topmenu.condo-community', 'Condo community') }}</a></li>
    <li role="presentation" class="pull-right">
        <a href="/auth/logout" class="main">{{ trans('nav.menu.logout') }}</a>
    </li>
</ul>
