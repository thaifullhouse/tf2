<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{ trans('nav.appname') }} - @yield('title')</title>

        <!-- Bootstrap -->
        <link href='https://fonts.googleapis.com/css?family=Signika:400,600,300' rel='stylesheet' type='text/css'>
        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/js/jquery-ui/jquery-ui.min.css" rel="stylesheet">
        <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/css/style.min.css?t=<?php echo filemtime(public_path() . '/css/style.min.css'); ?>" rel="stylesheet">
        <link href="/css/admin.min.css?t=<?php echo filemtime(public_path() . '/css/admin.min.css'); ?>" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="topbar">
            <nav class="navbar navbar-admin navbar-static-top">
                <div class="container">
                    <div id="navbar" class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-social">
                            <li><a href="/">Back to website</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right navbar-lang">                            
                            <li><a href="/auth/logout">Logout</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </nav>
        </div>
        <div class="container mainmenu-container">
            @include('layouts.admin-menu')
        </div>
        <div class="container admin-container">
            <div class="row">
                <div class="col-md-3">
                    @include('admin.sidebar')
                </div>
                <div class="col-md-9 manager-content">
                    @yield('content')                    
                </div>
            </div>
        </div>
        
        <script src="/js/jquery-3.1.0.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/notify.min.js"></script>
        <script src="/fancybox/jquery.fancybox.js"></script>
        <script src="/js/jquery-ui/jquery-ui.min.js"></script>
        <script src="/js/app.js"></script>
        <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        </script>
        @yield('scripts')
    </body>
</html>
