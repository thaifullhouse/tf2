<?php
$langs = \App\Helpers\Language::getActives();

foreach ($langs as $code => $lang) {
    ?>
    <li><a href="/?hl={{ $code }}" class="lang"><img src="/img/lang/{{ $code }}.png" class="img-responsive"/></a></li>
    <?php
}
?>
