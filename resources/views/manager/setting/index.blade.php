@extends('layouts.manager')

@section('title', 'Page Title')

<?php 
$tab = 'basic'; 
?>

@section('content')

<h3>{{ trans('manager.setting.list-title') }}</h3>

<div class="manager-content">    

    @include('manager.setting.tab')
    
    <h4>{{ trans('manager.setting.social-network-title') }}</h4>

    <div class="form-container row">
        
        
        <div class="col-md-7">
            
            <form class="form-horizontal" method="post" action="/manager/setting/update">
                
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="facebook_link" class="col-sm-4">{{ trans('manager.setting.facebook-link-label') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="facebook_link" value="{{ array_get($settings, 'facebook_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="instagram_link" class="col-sm-4">{{ trans('manager.setting.instagram-link-label') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="instagram_link" value="{{ array_get($settings, 'instagram_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="twitter_link" class="col-sm-4">{{ trans('manager.setting.twitter-link-label') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="twitter_link" value="{{ array_get($settings, 'twitter_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="google_link" class="col-sm-4">{{ trans('manager.setting.google-link-label') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="google_link" value="{{ array_get($settings, 'google_link') }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-primary">{{ trans('form.update') }}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

</div>
@endsection