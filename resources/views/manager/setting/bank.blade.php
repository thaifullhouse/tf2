@extends('layouts.manager')

@section('title', 'Page Title')

<?php $tab = 'bank'; ?>

@section('content')

<h3>{{ trans('manager.setting.list-title') }}</h3>

<div class="manager-content">


    @include('manager.setting.tab')

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation" class="pull-right">
                <a href="/manager/setting/add-bank">{{ trans('manager.setting.add-bank') }}</a>
            </li>
        </ul>
    </div>

    <table class="table table-list">
        <thead>
            <tr>
                <th style="width: 200px;">Bank</th>
                <th>Name</th>
                <th style="width: 200px;">Account #</th>
                <th class="icon-col"></th>
            </tr>
        </thead>
        <?php
        if (count($banks)) {
            ?>
            <tbody>
                <?php
                foreach ($banks as $bank) {                     
                    ?>
                    <tr>
                        <td>{{ $bank->getBankNameByCode() }}</td>
                        <td>
                            <a href="/manager/setting/edit-bank/{{ $bank->id }}">
                                {{ $bank->name }}
                            </a>
                        </td>
                        <td>{{ $bank->account_no }}</td>
                        <td>
                            <a href="/manager/setting/edit-bank/{{ $bank->id }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
            <?php
        }
        ?>
    </table>

</div>
@endsection