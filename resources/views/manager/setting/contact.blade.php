@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'contact';

$langs = \App\Helpers\Language::getActives();
?>

@section('content')
<h3>{{ trans('manager.setting.list-title') }}</h3>

@include('manager.setting.tab')

<h4>{{ __t('manager_setting.default-contact-message', 'Default contact message') }}</h4>

<form method="post" action="/manager/setting/contact">
    <div class="form-group">
        <div class="">
            <ul id="details-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#details-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                $details = $default_message ? json_decode($default_message, true) : [];

                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}">
                        <textarea class="form-control" rows="7" name="details[{{ $code }}]">{{ old('address.' . $code, array_get($details, $code)) }}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="form-group clearfix">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary pull-right">{{ __t('form.update', 'UPDATE') }}</button>
    </div>
</form>

<h4>{{ __t('manager_setting.default-contact-footer', 'Footer contact') }}</h4>

<form method="post" action="/manager/setting/footer">
    <div class="form-group">
        <div class="">
            <ul id="footer-tab" class="nav nav-tabs" role="tablist">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <li role="presentation">
                        <a href="#footer-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <?php
                $details = $footer ? json_decode($footer, true) : [];

                foreach ($langs as $code => $lang) {
                    ?>
                    <div role="tabpanel" class="tab-pane active" id="footer-{{ $code }}">
                        <textarea class="form-control" rows="7" name="footer[{{ $code }}]">{{ old('address.' . $code, array_get($details, $code)) }}</textarea>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

    </div>
    <div class="form-group clearfix">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary pull-right">{{ __t('form.update', 'UPDATE') }}</button>
    </div>
</form>

@endsection

@section('scripts')

<script>

    $(function () {
        $('#details-tab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('#details-tab a:first').tab('show');
        
        
        $('#footer-tab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });

        $('#footer-tab a:first').tab('show');
        
    });
</script>

@endsection
