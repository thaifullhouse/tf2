@extends('layouts.manager')

@section('title', 'Page Title')

<?php $tab = 'bank'; ?>

@section('content')
<h3>{{ trans('manager.setting.list-title') }}</h3>

<div class="manager-content">
    @include('manager.setting.tab')

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation" class="pull-right">
                <a href="/manager/setting/bank"><i class="fa fa-long-arrow-left"></i> {{ trans('manager.setting.back-bank') }}</a>
            </li>
        </ul>
    </div>
    <div class="form-container row">
        <div class="col-md-5">
            <form method="post" action="/manager/setting/add-bank">
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.bank-label') }} *</label>
                    <?php
                    $banks = \App\Models\Bank::getBankList();
                    ?>
                    <select class="form-control" name="bank">
                        <option value="-">{{ trans('manager.setting.bank.bank-select') }}</option>
                        <?php
                        foreach ($banks as $code => $bank) {
                            if (old('bank') == $code) {
                                ?>
                                <option value="{{ $code }}" selected>{{ $bank }}</option>
                                <?php
                            } else {
                                ?>
                                <option value="{{ $code }}">{{ $bank }}</option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.name-label') }} *</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.account-no-label') }} *</label>
                    <input type="text" class="form-control" name="account_no" value="{{ old('account_no') }}">
                </div>
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.branch-label') }} *</label>
                    <input type="text" class="form-control" name="branch" value="{{ old('branch') }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary">{{ trans('manager.setting.bank.add-button') }}</button>
            </form>
        </div>
        <div class="col-md-7">
            <p>
                <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
            </p>
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection