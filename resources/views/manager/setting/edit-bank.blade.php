@extends('layouts.manager')

@section('title', 'Page Title')

<?php $tab = 'bank'; ?>

@section('content')
<h3>{{ trans('manager.setting.list-title') }}</h3>

<div class="manager-content">
    @include('manager.setting.tab')

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation">
                <a href="/manager/setting/add-bank">{{ trans('manager.setting.add-bank') }}</a>
            </li>
            <li role="presentation" class="pull-right">
                <a href="/manager/setting/bank"><i class="fa fa-long-arrow-left"></i> {{ trans('manager.setting.back-bank') }}</a>
            </li>
        </ul>
    </div>
    <div class="form-container row">
        <div class="col-md-5">
            <form method="post" action="/manager/setting/update-bank/{{ $bank->id }}">
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.bank-label') }} *</label>
                    <?php
                    $banks = \App\Models\Bank::getBankList();
                    ?>
                    <select class="form-control" name="bank">
                        <option value="-">{{ trans('manager.setting.bank.bank-select') }}</option>
                        <?php
                        foreach ($banks as $code => $name) {
                            if (old('bank', $bank->bank) == $code) {
                                ?>
                                <option value="{{ $code }}" selected>{{ $name }}</option>
                                <?php
                            } else {
                                ?>
                                <option value="{{ $code }}">{{ $name }}</option>
                                <?php
                            }
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.name-label') }} *</label>
                    <input type="text" class="form-control" name="name" value="{{ old('name', $bank->name) }}">
                </div>
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.account-no-label') }} *</label>
                    <input type="text" class="form-control" name="account_no" value="{{ old('account_no', $bank->account_no) }}">
                </div>
                
                <div class="form-group">
                    <label>{{ trans('manager.setting.bank.branch-label') }} *</label>
                    <input type="text" class="form-control" name="branch" value="{{ old('branch', $bank->branch) }}">
                </div>
                
                <p class="text-danger">
                    <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
                </p>
                
                {{ csrf_field() }}
                
                <button type="submit" class="btn btn-primary">{{ trans('manager.setting.bank.update-button') }}</button>
            </form>
        </div>
        <div class="col-md-offset-2 col-md-5">

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>- {{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            
            <div class="danger-zone">
                <p>
                    {{ trans('manager.setting.bank.delete-message') }}
                </p>
                <a href="/manager/setting/delete-bank/{{ $bank->id }}" class="btn btn-danger">
                    {{ trans('manager.setting.bank.delete-link') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection