

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'basic' ? 'class="active"' : ''; ?> >
        <a href="/manager/setting">{{ __t('manager_setting.basic-tab', 'Basic setting') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'contact' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/contact">{{ __t('manager_setting.contact-tab', 'Contact') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'bank' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/bank">{{ __t('manager_setting.bank-tab', 'Bank accounts') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'image' ? 'class="active"' : ''; ?>>
        <a href="/manager/setting/image">{{ __t('manager_setting.image-tab', 'Images') }}</a>
    </li>
</ul>