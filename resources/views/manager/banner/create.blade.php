@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'info';
$active = 'banner';
?>

@section('content')

<h3>{{ __t('manager_banner.create-title', 'Create banner') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/banner">{{ __t('manager_banner.back-to-list', 'Back to banner list') }}</a>
        </li>
    </ul>
</div>

<!--<div class="bs-wizard-container">
    <div class="row bs-wizard">


        <div class="col-md-6 bs-wizard-step active">
            <div class="text-center bs-wizard-stepnum">Step 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">{{ __t('manager.manage_banner.create.step_1', 'Step 1') }}</div>
        </div>

        <div class="col-md-6 bs-wizard-step"> complete 
            <div class="text-center bs-wizard-stepnum">Step 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
            <div class="bs-wizard-info text-center">{{ __t('manager.manage_banner.create.step_2', 'Step 2') }}</div>
        </div>
    </div>
</div>-->

<div class="form-container row">
    <div class="col-md-5">
        <form action="/manager/banner/create" method="post">

            <div class="form-group">
                <label for="name" class="control-label">{{ __t('manager_banner.name-label', 'Name') }} *</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" placeholder="">
            </div>

            <div class="form-group">
                <label for="">{{ __t('manager_banner.customer-label', 'Customer') }} *</label>
                <input type="text" class="form-control" name="customer" value="{{ old('customer') }}" placeholder="">
            </div>

            <div class="form-group">
                <label for="location" class="control-label">{{ __t('manager_banner.location-label', 'Location') }} *</label>
                <select class="form-control" id="location" name="location">
                    <option value="0">{{ __t('manager_banner.location-select', 'Select a location') }}</option>
                    <?php
                    foreach ($locations as $location => $name) {
                        ?>
                        <option value="{{ $location }}">{{ $name }}</option>
                        <?php
                    }
                    ?>
                </select>
            </div>

            <div class="form-group">
                <label for="date_start" class="control-label">{{ __t('manager_banner.start-label', 'Start date') }}</label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_start" name="date_start" value="{{ old('date_start') }}">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                        <p>
                            <span>Today</span>
                        </p>
                    </div>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <input type="text" class="form-control" id="time_start" name="time_start" value="{{ old('time_start') }}">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>


            </div>

            <div class="form-group">
                <label for="date_end" class="control-label">{{ __t('manager_banner.end-label', 'End date') }}</label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_end" name="date_end" value="{{ old('date_end') }}" placeholder="">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="input-group">
                            <input type="text" class="form-control" id="time_end" name="time_end" value="{{ old('time_end') }}">
                            <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
                        </div>
                    </div>
                </div>

            </div>

            <div class="form-group">
                <label for="status" class="control-label">{{ __t('manager/banner.status-label', 'Payment status') }}</label>
                <select class="form-control" id="status" name="status">
                    <option value="0">{{ __t('manager_banner.status-select', 'Select status') }}</option>
                    <option value="paid">Paid</option>
                    <option value="waiting">Waiting</option>
                </select>
            </div>
            
            {{ csrf_field() }}

            <button type="submit" class="btn btn-primary" id="submit-banner">{{ __t('form.create', 'Create') }}</button>

        </form>
    </div>

</div>
@endsection

@section('scripts')

<script>

    $(function () {
        var dateFormat = "dd/mm/yy";
        var from = $("#date_start")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: dateFormat
                })
                .on("change", function () {
                    to.datepicker("option", "minDate", getDate(this));
                });

        var to = $("#date_end")
                .datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 1,
                    dateFormat: dateFormat
                })
                .on("change", function () {
                    from.datepicker("option", "maxDate", getDate(this));
                });

        function getDate(element) {
            var date;
            try {
                date = $.datepicker.parseDate(dateFormat, element.value);
            } catch (error) {
                date = null;
            }

            return date;
        }
    });
</script>

@endsection
