@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager/banner.list-title') }}</h3>

<div class="toolbar">

    <ul class="nav nav-pills">
<!--        <li role="presentation">
            <span class="input-label">{{ trans('manager/condo.search-filter') }}</span>
        </li>-->
<!--        <li role="presentation" class="input">                
            <input type="text" name="q" value="" class="form-control search"/>
        </li>-->
        <li role="presentation" class="pull-right">
            <!--            <button type="button" class="btn btn-sm btn-blue"  data-toggle="modal" data-target="#create-banner">
                            {{ trans('manager/banner.create') }}
                        </button>-->
            <a href="/manager/banner/create" class="btn btn-sm btn-blue">{{ __t('manager_banner.create', 'Add new banner') }}</a>
        </li>
    </ul>

</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="tiny-col text-center">ID</th>
            <th>{{ __t('manager_banner.name-col', 'Banner name') }}</th>
            <th>{{ __t('manager_banner.customer-col', 'Customer') }}</th>
            <th class="text-center">{{ __t('manager_banner.start-col', 'Start') }}</th>
            <th class="text-center">{{ __t('manager_banner.end-col', 'End') }}</th>
            <th>{{ __t('manager_banner.location-col', 'Location') }}</th>
            <th class="text-center tiny-col">{{ __t('manager_banner.view-col', 'View') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($banners as $banner) {
            ?>
            <tr>
                <td class="text-center">{{ $banner->id }}</td>
                <td>{{ $banner->name }}</td>
                <td>{{ $banner->customer }}</td>
                <td class="text-center">{{ date('d/m/Y', strtotime($banner->date_start)) }}</td>
                <td class="text-center">{{ date('d/m/Y', strtotime($banner->date_end)) }}</td>
                <td>{{ $banner->getLocation() }}</td>
                <td class="text-center">{{ $banner->print }}</td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-xs btn-white"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i>
                        </button>
                        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                            <li>
                                <a href="/manager/banner/info/{{ $banner->id }}">
                                    {{ __t('manager_banner.info-tab', 'Info') }}
                                </a>
                            </li>
                            <li>
                                <a href="/manager/banner/image/{{ $banner->id }}">
                                    {{ __t('manager_banner.image-tab', 'Image') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<div id="create-banner" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.close') }}</button>
                <button type="button" class="btn btn-primary" id="submit-banner">{{ trans('form.create') }}</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>

    var bannerSelectImage = function () {
        $('#image').trigger('click');
    };

    $(function () {

        $('#create-banner').on('show.bs.modal', function (e) {
            $('.modal-body').load('/manager/banner/create', function (data) {

                

                $inputImage = $('#image');

                if (window.FileReader) {

                    $inputImage.on('change', function () {

                        var fileReader = new FileReader(),
                                files = this.files,
                                file;

                        console.log(files);

                        if (!files.length) {
                            return;
                        }

                        file = files[0];

                        if (/^image\/\w+$/.test(file.type)) {

                            fileReader.readAsDataURL(file);
                            fileReader.onload = function () {

                                var div = document.createElement("div");
                                $(div).addClass('img-preview');
                                var img = document.createElement("img");
                                $(img).prop('src', fileReader.result);
                                $(img).addClass('img-responsive');
                                $(div).append(img);
                                $('.banner-preview-container').html('');
                                $('.banner-preview-container').append(div);

                            };
                        }
                    });
                } else {
                    $inputImage.addClass("hide");
                }

            });
        });

        $('#submit-banner').on('click', function () {

            $('.alert').hide();

            var form = document.getElementById('create-banner-form');

            var data = new FormData(form);

            console.log(data);

            $.ajax({
                url: '/manager/banner/create',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data, status, xhr) {
                    window.location.reload();
                },
                error: function (xhr, status, error) {
                    $('.alert').show();
                }

            });
        });

    });

</script>

@endsection