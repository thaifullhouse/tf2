<div class="banner-create-box">
    <form class="form-horizontal" id="create-banner-form">
        
        <div class="form-group">
            <label for="developer_id" class="col-sm-4 control-label">{{ trans('manager/banner.developer-label') }} *</label>
            <div class="col-sm-8">
                <select class="form-control" id="developer_id" name="developer_id" value="">
                    <option value="0">{{ trans('manager/banner.developer-select') }}</option>
                    <?php
                    foreach ($developers as $developer) {
                        ?>
                        <option value="{{ $developer->user_id }}">{{ $developer->company_name }}</option>
                        <?php
                    }
                    ?>
                </select>
            </div>
        </div>
        
        <div class="form-group">
            <label for="image" class="col-sm-4 control-label">{{ trans('manager/banner.image-label') }} *</label>
            <div class="col-sm-2">
                <button class="btn btn-default btn-block" type="button" onclick="bannerSelectImage();"><i class="fa fa-upload"></i></button>
                <input type="file" class="hidden" id="image" name="image" value="">
            </div>
            <div class="col-sm-12 banner-preview-container">
            </div>            
        </div>
        <div class="form-group">
            <label for="link" class="col-sm-4 control-label">{{ trans('manager/banner.link-label') }}</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="link" name="link" placeholder="">
            </div>
        </div>
        <div class="form-group">
            <label for="effect" class="col-sm-4 control-label">{{ trans('manager/banner.effect-label') }}</label>
            <div class="col-sm-8">
                <select class="form-control" id="effect" name="effect" value="">
                    <option value="0">{{ trans('manager/banner.effect-select') }}</option>
                    <option value="fix">Fix</option>
                    <option value="random">Random</option>
                </select>
            </div>
        </div>
        

        <div class="form-group">
            <label for="date_start" class="col-sm-4 control-label">{{ trans('manager/banner.start-label') }}</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <input type="text" class="form-control" id="date_start" name="date_start" value="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="date_end" class="col-sm-4 control-label">{{ trans('manager/banner.end-label') }}</label>
            <div class="col-sm-5">
                <div class="input-group">
                    <input type="text" class="form-control" id="date_end" name="date_end" value="" placeholder="">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="status" class="col-sm-4 control-label">{{ trans('manager/banner.status-label') }}</label>
            <div class="col-sm-8">
                <select class="form-control" id="status" name="status">
                    <option value="0">{{ trans('manager/banner.status-select') }}</option>
                    <option value="paid">Paid</option>
                    <option value="waiting">Waiting</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="alert alert-danger">
                {!! trans('manager/banner.create-error-text') !!}
            </div>
        </div>

    </form>
</div>
