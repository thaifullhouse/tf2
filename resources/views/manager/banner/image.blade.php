@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'image';
$active = 'banner';

$langs = \App\Helpers\Language::getActives();

?>

@section('content')

<h3>{{ __t('manager_banner.details-title', 'Banner details') }}</h3>


@include('manager.banner.tab')

<h4>Banner : {{ $banner->name }}</h4>

<ul id="image-tab" class="nav nav-tabs" role="tablist">
    <?php
    foreach ($langs as $code => $lang) {
        ?>
        <li role="presentation"><a href="#image-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a></li>
        <?php
    }
    ?>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <?php
    $images = json_decode($images, true);

    foreach ($langs as $code => $lang) {

        $image = array_get($images, $code);
        ?>
        <div role="tabpanel" class="tab-pane" id="image-{{ $code }}">
            <div class="row">
                <div class="col-md-9">
                    <div class="manager-setting-preview">
                        <?php
                        if ($image) {
                            ?>
                            <img src="/thumb/400x100/{{ $image }}" class="img-responsive"/>
                            <a class="delete" href="/manager/banner/image-remove?lang={{ $code }}"><i class="fa fa-times"></i></a>
                            <?php
                        }
                        ?>                        
                    </div>
                </div>
                <div class="col-md-3">
                    <p>
                        <label for="input-{{ $code }}" class="btn btn-upload">{{ __t('manager_setting.select-an-image', 'Select an image') }}</label>
                        <input type="file" id="input-{{ $code }}" name="image" 
                               data-lang="{{ $code }}" data-id="{{ $banner->id }}" 
                               data-progress="#progress" class="banner-image hidden"/>
                    </p>
                    <p class="text-danger">
                        JPEG or PNG only
                    </p>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<div class="progress" style="margin-top: 25px;">
    <div id="progress" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
        <span class="sr-only"></span>
    </div>
</div>

@endsection


@section('scripts')
<script src="/js/manager/banner.js"></script>
@endsection