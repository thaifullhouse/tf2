<div class="list-group manager-sidebar">
    
    <a href="/manager/dashboard" class="list-group-item <?php echo $active=='dashboard' ? 'active' : ''; ?>">{{ trans('manager.sidebar.dashboard') }}</a>
    
    <h5>{{ trans('manager.sidebar.member-management') }}</h5>
    <a href="/manager/visitors" class="list-group-item <?php echo $active=='visitors' ? 'active' : ''; ?>">{{ trans('manager.sidebar.visitors') }} </a>
    <a href="/manager/owners" class="list-group-item <?php echo $active=='owners' ? 'active' : ''; ?>">{{ trans('manager.sidebar.owners') }} </a>
    <a href="/manager/agents" class="list-group-item <?php echo $active=='agents' ? 'active' : ''; ?>">{{ trans('manager.sidebar.agents') }} </a>
    <a href="/manager/developers" class="list-group-item <?php echo $active=='developers' ? 'active' : ''; ?>">{{ trans('manager.sidebar.developers') }} </a>    
    
    
    <h5>{{ trans('manager.sidebar.property-management') }}</h5>
    
    <a href="/manager/condo/list" class="list-group-item <?php echo $active=='condo' ? 'active' : ''; ?>">{{ trans('manager.sidebar.condo_list') }}</a>
    
    <a href="/manager/facility/list" class="list-group-item <?php echo $active=='facility' ? 'active' : ''; ?>">{{ trans('manager.sidebar.condo_facilities') }}</a> 
    <a href="/manager/feature/list" class="list-group-item <?php echo $active=='feature' ? 'active' : ''; ?>">{{ trans('manager.sidebar.property_features') }}</a>
    <a href="/manager/optional-feature/list" class="list-group-item <?php echo $active=='optional-feature' ? 'active' : ''; ?>">{{ trans('manager.sidebar.optional_features') }}</a>   
        
    <h5>{{ trans('manager.sidebar.new-project') }}</h5>
    
    <a href="/manager/project/list/condo/" class="list-group-item <?php echo $active=='condo_project' ? 'active' : ''; ?>">{{ trans('manager.sidebar.condo_project') }}</a>
    <a href="/manager/project/list/apartment" class="list-group-item <?php echo $active=='apartment_project' ? 'active' : ''; ?>">{{ trans('manager.sidebar.apartment_project') }}</a>
    <a href="/manager/project/list/house" class="list-group-item <?php echo $active=='house_project' ? 'active' : ''; ?>">{{ trans('manager.sidebar.house_project') }}</a>
    <a href="/manager/project/list/townhouse" class="list-group-item <?php echo $active=='townhouse_project' ? 'active' : ''; ?>">{{ trans('manager.sidebar.townhouse_project') }}</a>
    
    <h5>{{ trans('manager.sidebar.customer-data') }}</h5>
    
    <a href="/manager/new-condo/list" class="list-group-item <?php echo $active=='new-condo' ? 'active' : ''; ?>">{{ trans('manager.sidebar.new_condo_list') }}</a>
    <a href="/manager/apartment/list" class="list-group-item <?php echo $active=='apartment' ? 'active' : ''; ?>">{{ trans('manager.sidebar.apartment_list') }}</a>
    <a href="/manager/house/list" class="list-group-item <?php echo $active=='house' ? 'active' : ''; ?>">{{ trans('manager.sidebar.house_list') }}</a>
    <a href="/manager/townhouse/list" class="list-group-item <?php echo $active=='condo' ? 'active' : ''; ?>">{{ trans('manager.sidebar.townhouse_list') }}</a>
    
<!--    <a href="/manager/listing" class="list-group-item <?php echo $active=='listing' ? 'active' : ''; ?>">{{ trans('manager.sidebar.listings') }} </a>-->
    <a href="/manager/order" class="list-group-item <?php echo $active=='order' ? 'active' : ''; ?>">{{ trans('manager.sidebar.orders') }} </a>   
    
        
    <h5>{{ trans('manager.sidebar.geographic-data') }}</h5>
    
    <a href="/manager/location/province-list" class="list-group-item <?php echo $active=='province' ? 'active' : ''; ?>">{{ trans('manager.sidebar.provinces') }}</a>
    <a href="/manager/point" class="list-group-item <?php echo $active=='poi' ? 'active' : ''; ?>">{{ trans('manager.sidebar.points') }} </a>    
    
    <h5>{{ trans('manager.sidebar.other-data') }}</h5>    
    
    <a href="/manager/banner" class="list-group-item <?php echo $active=='banner' ? 'active' : ''; ?>">{{ trans('manager.sidebar.banners') }} </a>
    <a href="/manager/package/list" class="list-group-item <?php echo $active=='package' ? 'active' : ''; ?>">{{ trans('manager.sidebar.package') }}</a>
    <a href="/manager/staff/list" class="list-group-item <?php echo $active=='staff' ? 'active' : ''; ?>">{{ trans('manager.sidebar.staff') }}</a>
    <a href="/manager/setting" class="list-group-item <?php echo $active=='setting' ? 'active' : ''; ?>">{{ trans('manager.sidebar.website_settings') }}</a>
    <a href="/manager/translation/list" class="list-group-item <?php echo $active=='translation' ? 'active' : ''; ?>">Translations</a>

</div>
