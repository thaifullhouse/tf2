@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.feature.list-title') }}</h3>

<div class="row">
    <div class="col-md-8">
        <table class="table table-list">
            <thead>
                <tr>
                    <td width="25"></td>
                    <td width="56"></td>
                    <td>Name TH</td>
                    <td>Name EN</td>
                    <td></td>
                </tr>
            </thead>
            <tbody class="table-sortable">
                <?php
                foreach ($list as $feature) {
                    ?>
                    <tr data-id="{{ $feature->id }}" class="feature-item">
                        <td width="25" class="text-center sorting-handler" style="vertical-align: middle;">
                            <i class="fa fa-arrows-v text-muted"></a>
                        </td>
                        <td class="text-center"><img src="{{ $feature->getThumbUrl('48x48', true) }}" class="img-responsive"/></td>
                        <td contenteditable data-id="{{ $feature->id }}" data-domain="feature" data-field="name_th"
                            style="vertical-align: middle;">{{ $feature->name_th }}</td>
                        <td contenteditable data-id="{{ $feature->id }}" data-domain="feature" data-field="name_en"
                            style="vertical-align: middle;">{{ $feature->name_en }}</td>
                        <td style="vertical-align: middle;"><a href="/manager/feature/edit/{{ $feature->id }}">Edit</a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </div>
    <div class="col-md-4">
        <h3>Feature manager</h3>
        <form method="post" action="/manager/feature/create">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                <label for="name_th">Name TH</label>
                <input type="text" class="form-control" name="name_th" value="" placeholder="">
            </div>
            <div class="form-group">
                <label for="name_en">Name EN</label>
                <input type="text" class="form-control" name="name_en" value="" placeholder="">
            </div>
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Create</button>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script src="/js/content.js"></script>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script>
$(document).ready(function () {
    $("tbody").sortable({
        stop: function (event, ui) {
            var data = [];

            $.each($(".feature-item"), function (index, value) {
                data.push($(value).data('id'));

            });
            console.log(data);

            $.post('/manager/feature/order', {ranks : data},  function (data) {

            });
        }
    });
    $("tbody").disableSelection();
});
</script>
@endsection