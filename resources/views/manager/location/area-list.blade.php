@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>{{ __t('manager_province.area-list-title', 'Area list') }} : {{ $district->getJsonTranslatedField('name') }}</h3>

        <div class="toolbar">
            <ul class="nav nav-pills">
                <li role="presentation">
                    <a href="/manager/location/district-list/{{ $district->province_id }}">{{ __t('manager_location.back-to-district-list', 'Back to district list') }}</a>
                </li>
            </ul>
        </div>

        <table class="table table-list" id="province-list">
            <thead>
                <tr>
                    <th>Name</th>
                    <th class="text-center small-col">Area</th>
                    <th class="icon-col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($areas as $area) {
                    ?>
                    <tr>
                        <td contenteditable data-id="{{ $area->id }}" data-domain="area" data-field="name_th">{{ $area->getJsonTranslatedField('name') }}</td>
                        <td></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </div>
</div>
@endsection