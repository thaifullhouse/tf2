@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>{{ __t('manager_province.list-title', 'Province list') }}</h3>

        <table class="table table-list" id="province-list">
            <thead>
                <tr>
                    <th>Name</td>
                    <td class="text-center small-col">Districts</td>
                    <td class="icon-col"></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($provinces as $province) {
                    ?>
                    <tr>                        
                        <td contenteditable data-id="{{ $province->id }}" data-domain="province" data-field="name_th">{{ $province->getJsonTranslatedField('name') }}</td>
                        <td class="text-center">{{ $province->getDistrictCount() }}</td>
                        <td class="icon-col text-center"><a href="/manager/location/district-list/{{ $province->id }}"><i class="fa fa-list-ul"></i></a></td>                        
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </div>
</div>
@endsection