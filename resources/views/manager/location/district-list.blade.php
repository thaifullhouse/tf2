@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>{{ __t('manager_province.district-list-title', 'District list') }} : {{ $province->getJsonTranslatedField('name') }}</h3>
        
        <div class="toolbar">
            <ul class="nav nav-pills">
                <li role="presentation">
                    <a href="/manager/location/province-list">{{ __t('manager_location.back-to-province-list', 'Back to province list') }}</a>
                </li>
            </ul>
        </div>

        <table class="table table-list" id="province-list">
            <thead>
                <tr>
                    <th>Name</td>
                    <td class="text-center small-col">Area</td>
                    <td class="icon-col"></td>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($districts as $district) {
                    ?>
                    <tr>
                        <td contenteditable data-id="{{ $district->id }}" data-domain="district" data-field="name_th">{{ $district->getJsonTranslatedField('name') }}</td>
                        <td class="text-center">{{ $district->getAreaCount() }}</td>
                        <td class="icon-col text-center"><a href="/manager/location/area-list/{{ $district->id }}"><i class="fa fa-list-ul"></i></a></td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>

    </div>
</div>
@endsection