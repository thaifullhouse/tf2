@extends('layouts.manager')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>
@endsection

@section('content')
<div class="row poi-editor">

    <div class="col-md-9">
        <h3>
            {{ trans('manager.points.type_title') }}
        </h3>
    </div>
    <div class="col-md-3">
        <h3>
            {{ trans('manager.points.create_type') }}
        </h3>
    </div>

</div>
@endsection

@section('scripts')

@endsection