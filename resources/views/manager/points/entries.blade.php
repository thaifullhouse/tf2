@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>
    {{ trans('manager.points.title') }}
</h3>
<div class="manager-content poi-map">

    <div class="point-filters">  
        <strong>Filters:</strong>
        <label><input type="checkbox" name="filters" value="bts" class="poi-filter" checked> <span>BTS</span></label>
        <label><input type="checkbox" name="filters" value="mrt" class="poi-filter" checked> <span>MRT</span></label>
        <label><input type="checkbox" name="filters" value="apl" class="poi-filter" checked> <span>Airport Link</span></label>
        <label><input type="checkbox" name="filters" value="bank" class="poi-filter" checked> <span>Bank</span></label>
        <label><input type="checkbox" name="filters" value="dept-store" class="poi-filter" checked> <span>Department Store</span></label>
        <label><input type="checkbox" name="filters" value="hospital" class="poi-filter" checked> <span>Hospital</span></label>
        <label><input type="checkbox" name="filters" value="school" class="poi-filter" checked> <span>School</span></label>
        <span class="select-all text-primary pull-right">Select all</span>
    </div>
    <p class="text-success text-right">
        &nbsp; <span class="poi-message action-created">{{ trans('manager.points.action-created') }} <i class="fa fa-check"></i></span>
        <span class="poi-message action-updated">{{ trans('manager.points.action-updated') }} <i class="fa fa-check"></i></span>
    </p>
    <div id="map" class="map">
    </div>
    <div class="map-loader-mask">

    </div>
</div>
<div class="poi-editor row">
    <div class="col-sm-12">
        <div id="create-poi" class="alert alert-info">
            {{ trans('manager.points.create-poi') }}
        </div>

        <div id="name-error" class="alert alert-danger">
            {{ trans('manager.points.name-error') }}
        </div>

        <div id="marker-error" class="alert alert-danger">
            {{ trans('manager.points.marker-error') }}
        </div>
    </div>

    <form class="form">
        <div class="form-group col-sm-6">
            <input type="text" class="form-control" id="name_en" name="name_en" value="" placeholder="{{ trans('manager.points.name_en') }}">
        </div>
        <div class="form-group col-sm-4">
            <select class="form-control" id="type" name="type">


                <option value="bts">BTS</option>
                <option value="mrt">MRT</option>
                <option value="apl">Airport Link</option>
                <option value="bank">Bank</option>
                <option value="dept-store">Department Store</option>
                <option value="hospital">Hospital</option>
                <option value="school">School</option>
            </select>
        </div>
        <div class="form-group col-sm-2">
            <button type="button" class="btn btn-primary btn-block" onclick="savePoi()">{{ trans('manager.points.save-poi') }}</button>
        </div>
        <div class="form-group col-sm-6">
            <input type="text" class="form-control" id="name_th" name="name_th" value="" placeholder="{{ trans('manager.points.name_th') }}">
        </div>
        <div class="form-group col-sm-4">
            <input type="text" class="form-control" id="location" name="location" value="" disabled>
            <input type="hidden" id="poi-id" name="id" value=""/>
        </div>
        <div class="form-group col-sm-2">
            <input type="text" class="form-control" id="zl" name="zl" value="" disabled>
        </div>

    </form>
</div>
@endsection

@section('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('google.browser_key') }}"></script>
<script src="/js/markerclusterer.js"></script>
<script src="/js/manager-points.js"></script>
@endsection