<div class="row place-listing">
    <div class="col-md-12">

        <h5>{{ __t('manager.condo.places-bank-title', 'Banks') }}</h5>

        <?php
        $_list = array_get($places, 'bank');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ __t('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ __t('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ __t('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ __t('manager.condo.places-stores-title', 'Stores &amp; Department stores') }}</h5>
        
        <?php
        $_list = array_get($places, 'dept-store');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ __t('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ __t('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ __t('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ __t('manager.condo.places-schools-title', 'Schools &amp; Universities') }}</h5>
        
        <?php
        $_list = array_get($places, 'school');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ __t('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ __t('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ __t('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ __t('manager.condo.places-hospital-title', 'Hospitals &amp; Pharmacies') }}</h5>
        
        <?php
        $_list = array_get($places, 'hospital');

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ __t('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ __t('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ __t('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

        <h5>{{ __t('manager.condo.places-trains-title', 'Train stations') }}</h5>
        
        <?php
        $_list = array_merge(array_get($places, 'bts', []), array_get($places, 'mrt', []), array_get($places, 'apl', []));

        if (count($_list)) {
            ?>
            <table class="table table-list">
                <thead>
                    <tr>
                        <td>{{ __t('place_list.name-col', 'Place names') }}</td>
                        <td class="small-col">{{ __t('place_list.distance-col', 'Distance') }}</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($_list as $place) {

                        $d = $place->distance / 1000;

                        $names = json_decode($place->name, true);
                        $name = empty(array_get($names, $lang)) ? array_get($names, 'th') : array_get($names, $lang);
                        ?>
                        <tr>
                            <td>{{ $name }}</td>
                            <td>{{ sprintf("%0.2f km", $d) }}</td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
        } else {
            ?>
            <div class="alert alert-warning">{{ __t('sentences.no-places-nearby', 'No places found') }}</div>
            <?php
        }
        ?>

    </div>
</div>