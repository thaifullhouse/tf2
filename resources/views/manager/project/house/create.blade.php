@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ trans('manager/project.create-house-title') }}</h3>

<?php
$current_type = 'house';
$project_type = 'house';
?>

@include('manager.project.general')

@endsection


@section('scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}&libraries=places'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script>
var lat = <?php echo old('lat', 13.74603); ?>;
var lng = <?php echo old('lng', 100.53477); ?>;

$(function () {
    var dateFormat = "dd/mm/yy";
    var from = $("#date_start")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                to.datepicker("option", "minDate", getDate(this));
            });

    var to = $("#date_end")
            .datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: dateFormat
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

    function getDate(element) {
        var date;
        try {
            date = $.datepicker.parseDate(dateFormat, element.value);
        } catch (error) {
            date = null;
        }

        return date;
    }
    
    $('.set-date').on('click', function () {
        var target = $(this).data('target');
        var months = parseInt($(this).data('value'));

        var start = getDate(document.getElementById('date_start'));

        if (start) {
            var end = moment(start).add(months, 'M');       
            $(target).datepicker('setDate', end.toDate());
        }
    });
});

</script>
<script src="/js/condo-location.js"></script>

@endsection