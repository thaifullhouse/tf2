@extends('layouts.manager')

@section('title', 'Page Title')

<?php 
    $active = 'house_project';
?>

@section('content')
<h3>{{ __t('manager.project.house-list-title', 'House projects') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">

        <li role="presentation">
            <span class="input-label">{{ trans('manager/condo.search-filter') }}</span>
        </li>

        <li role="presentation" class="input">                
            <input type="text" name="q" value="" class="form-control search"/>
        </li>       

        <li role="presentation" class="pull-right">
            <a href="/manager/project/create/house" class="btn btn-sm btn-blue">
                <i class="fa fa-plus"></i> {{ __t('manager.project.create-house-button', 'Add house project') }} 
            </a>
        </li>

    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="icon-col"></th>
            <th>{{ trans('manager/project.name-col') }}</th>
            <th>{{ trans('manager/project.area-col') }}</th>
            <th class="small-col text-center">{{ trans('manager/project.photo-col') }}</th>            
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($properties as $property) {
            ?>
            <tr>
                <td class="text-center"><input type="checkbox" name="active" value="YES"/></td>
                <td>{{ $property->getTranslatedField('name') }}</td>
                <td>{{ $property->street_name }}</td>
                <td class="text-center">{{ $property->countImages() }}</td>
                <td class="text-center">
                    <div class="dropdown">
                        <button class="btn btn-xs btn-white"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                            <li>
                                <a href="/manager/project/house/info/{{ $property->id }}">
                                    {{ trans('manager/project.info-tab') }}
                                </a>
                            </li>
                            <li>
                                <a href="/manager/project/house/details/{{ $property->id }}">
                                    {{ trans('manager/project.details-tab') }}
                                </a>
                            </li>
                            <li>
                                <a href="/manager/project/house/review/{{ $property->id }}">
                                    {{ trans('manager/project.review-tab') }}
                                </a>
                            </li>
                            <li>
                                <a href="/manager/project/house/places/{{ $property->id }}">
                                    {{ trans('manager/project.places-tab') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

@endsection