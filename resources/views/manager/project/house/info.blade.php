@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'info';
$active = 'house_project';
$location = $property->getLatLng();
$langs = \App\Helpers\Language::getActives();
?>

@section('content')

<h3>{{ __t('manager_project.house-details-title', 'House project details') }}</h3>

@include('manager.project.house.tab')

<div class="form-container row">        
    <form action="/manager/project/house/info/{{ $property->id }}" method="post">            
        <div class="col-md-5">
            <div class="form-group">
                <label for="">{{ __t('manager_project.create-name-label', 'Names') }} <span class="text-danger">*</span></label>
            </div>
            <div class="well">
                <?php
                $name = json_decode($property->name, true);

                foreach ($langs as $code => $lang) {
                    ?>
                    <div class="form-group">
                        <label for="">{{ $lang }}</label>
                        <input type="text" class="form-control" name="name[{{ $code }}]" value="{{ old('name.' . $code, array_get($name, $code)) }}">
                    </div>

                    <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.latitude-label') }} *</label>
                <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat', $location->lat) }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.longitude-label') }} *</label>
                <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng', $location->lng) }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.area-label') }}</label>
                <input type="text" class="form-control" id="address" name="address" value="{{ old('address', $property->street_name) }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.postcode-label') }}</label>
                <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode', $property->postale_code) }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="date_end" class="control-label">{{ trans('manager/banner.start-label') }} *</label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_start" name="date_start" value="{{ old('date_start', date('d/m/Y', strtotime($property->project_start))) }}" placeholder="">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <button class="btn btn-default btn-block set-date" type="button" data-target="#date_start" data-value="6">
                            {{ __t('manager_project.expiration.today-six-month', '+ 6 months') }}
                        </button>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <label for="date_end" class="control-label">{{ trans('manager/banner.end-label') }} *</label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_end" name="date_end" value="{{ old('date_end', date('d/m/Y', strtotime($property->project_end))) }}" placeholder="">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <button class="btn btn-default btn-block set-date" type="button" data-target="#date_end" data-value="12">
                            {{ __t('manager_project.expiration.today-one-year', '+ 1 year') }}
                        </button>
                    </div>
                </div>

            </div>
            <p>
                <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
            </p>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <button type="submit" class="btn btn-primary">{{ trans('manager/project.submit-btn') }}</button>
        </div>
        <div class="col-md-7">
            <div id="map-picker" style="width: 100%; height: 400px; margin-bottom: 15px;"></div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" id="area" name="area" placeholder="Type the area name ...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="clear-area-btn" type="button">{{ trans('manager/project.clear-btn') }}</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>
<?php
$lat = old('lat', $location->lat);
$lng = old('lng', $location->lng);

$lat = $lat ? $lat : 13.746318;
$lng = $lng ? $lng : 100.534875;
?>

@endsection

@section('scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}&libraries=places'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script>

var lat = <?php echo old('lat', $lat); ?>;
var lng = <?php echo old('lng', $lng); ?>;

</script>
<script src="/js/condo-location.js"></script>
@endsection