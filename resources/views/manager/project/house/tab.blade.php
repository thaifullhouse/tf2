<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/project/list/house"><i class="fa fa-long-arrow-left"></i> {{ trans('manager/project.back-to-list') }}</a>
        </li>
    </ul>
</div>

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'info' ? 'class="active"' : ''; ?> >
        <a href="/manager/project/house/info/{{ $property->id }}">{{ trans('manager/project.info-tab') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'review' ? 'class="active"' : ''; ?>>
        <a href="/manager/project/house/review/{{ $property->id }}">{{ trans('manager/project.review-tab') }}</a>
    </li>
   <li role="presentation" <?php echo $tab == 'details' ? 'class="active"' : ''; ?>>
        <a href="/manager/project/house/details/{{ $property->id }}">{{ trans('manager/project.details-tab') }}</a>
    </li>
<!--    <li role="presentation" <?php echo $tab == 'media' ? 'class="active"' : ''; ?>>
        <a href="/manager/project/house/media/{{ $property->id }}">{{ trans('manager/project.media-tab') }}</a>
    </li>-->
    
    <li role="presentation" <?php echo $tab == 'places' ? 'class="active"' : ''; ?>>
        <a href="/manager/project/house/places/{{ $property->id }}">{{ trans('manager/project.places-tab') }}</a>
    </li>
</ul>