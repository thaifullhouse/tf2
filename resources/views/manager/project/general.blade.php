<?php
$langs = \App\Helpers\Language::getActives();
?>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/project/list">{{ trans('manager/project.back-to-list') }}</a>
        </li>
    </ul>
</div>
<div class="form-container row">        
    <form action="/manager/project/create/{{ $project_type }}" method="post">            
        <div class="col-md-5">

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="form-group">
                <label for="">{{ __t('manager_project.create-name-label', 'Names') }} <span class="text-danger">*</span></label>
            </div>

            <div class="well">
                <?php
                foreach ($langs as $code => $lang) {
                    ?>
                    <div class="form-group">
                        <label for="">{{ $lang }}</label>
                        <input type="text" class="form-control" name="name[{{ $code }}]" value="{{ old('name.' . $code) }}" aria-describedby="basic-addon2">
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.latitude-label') }} *</label>
                <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.longitude-label') }} *</label>
                <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.area-label') }}</label>
                <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ trans('manager/project.postcode-label') }}</label>
                <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="date_end" class="control-label">{{ trans('manager/banner.start-label') }} <span class="text-danger">*</span></label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_start" name="date_start" value="{{ old('date_start') }}" placeholder="">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>

                </div>

            </div>
            <div class="form-group">
                <label for="date_end" class="control-label">{{ trans('manager/banner.end-label') }} <span class="text-danger">*</span></label>
                <div class="row">
                    <div class="col-xs-7">
                        <div class="input-group">
                            <input type="text" class="form-control" id="date_end" name="date_end" value="{{ old('date_end') }}" placeholder="">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <div class="btn-group" role="group" aria-label="...">
                            <button class="btn btn-default set-date" type="button" data-target="#date_end" data-value="6">
                                {{ __t('manager_project.expiration.today-six-month', '+6M') }}
                            </button>
                            <button class="btn btn-default set-date" type="button" data-target="#date_end" data-value="12">
                                {{ __t('manager_project.expiration.today-one-year', '+1Y') }}
                            </button>
                        </div>

                    </div>
                </div>

            </div>
            <p>
                <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
            </p>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <button type="submit" class="btn btn-primary">{{ trans('manager/project.submit-btn') }}</button>
        </div>
        <div class="col-md-7">
            <div id="map-picker" style="width: 100%; height: 400px; margin-bottom: 15px;"></div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" id="area" name="area" placeholder="Type the area name ...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="clear-area-btn" type="button">{{ trans('manager/project.clear-btn') }}</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>



