@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'review';
$active = 'townhouse_project';
?>

@section('content')

<h3>{{ __t('manager_project.townhouse-details-title', 'Townhouse project details') }}</h3>

@include('manager.project.townhouse.tab')

<h4 class="blue">{{ trans('manager/project.review.how-to-get-there-title')  }}</h4>

<div class="review-media-list">
    <?php
    $images = $review->getLocationImages();

    if (count($images)) {
        foreach ($images as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="/manager/project/property/rm-image/{{ $image->id }}/location/{{ $property->id }}" class="rm-media">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <?php
        }
    }
    ?>
</div>
<p class="push-below">
<div class="progress">
    <div id="progress-1" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>
<button class="btn btn-sm btn-blue upload-trigger" 
        data-target="#location-image">
    <i class="fa fa-plus"></i> {{ trans('manager/project.review.add-image') }}        
</button> <span class="text-danger">(JPEG or PNG image only)</span>
<input type="file" id="location-image" name="images" class="hidden image-multiple" multiple data-progress="#progress-1"
       data-action="/manager/project/property/upload/{{ $property->id }}/location"/>
</p>

<h5>{{ trans('manager/project.review.details-title')  }}</h5>

<div class="review-form-container push-below row">
    <form action="/manager/project/property/review/{{ $review->id }}/location" method="post" class="col-md-8">
        <div class="form-group">
            <textarea class="form-control" name="location_details" rows="5">{{ $review->location_details }}</textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<h4 class="blue">{{ trans('manager/project.review.facility-landscape-title')  }}</h4>

<div class="review-media-list">
    <?php
    $images = $review->getFacilityImages();

    if (count($images)) {
        foreach ($images as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="/manager/project/property/rm-image/{{ $image->id }}/facility/{{ $property->id }}" class="rm-media">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <?php
        }
    }
    ?>
</div>

<p class="push-below">
<div class="progress">
    <div id="progress-2" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>
<button class="btn btn-sm btn-blue upload-trigger" 
        data-target="#facility-image">
    <i class="fa fa-plus"></i> {{ trans('manager/project.review.add-image') }}        
</button> <span class="text-danger">(JPEG or PNG image only)</span>
<input type="file" id="facility-image" name="images" class="hidden image-multiple"  multiple data-progress="#progress-2"
       data-action="/manager/project/property/upload/{{ $property->id }}/facility"/>
</p>

<h5>{{ trans('manager/project.review.details-title')  }}</h5>

<div class="review-form-container push-below row">
    <form action="/manager/project/property/review/{{ $review->id }}/facility" method="post" class="col-md-8">
        <div class="form-group">
            <textarea class="form-control" name="facility_details" rows="5">{{ $review->facility_details }}</textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<h4 class="blue">{{ trans('manager/project.review.floor-plan-title')  }}</h4>

<div class="review-media-list">
    <?php
    $images = $review->getFloorplanImages();

    if (count($images)) {
        foreach ($images as $image) {
            ?>
            <div class="media-item">
                <img src="/thumb/100x100/{{ $image->filepath }}"/>
                <a href="/manager/project/property/rm-image/{{ $image->id }}/floorplan/{{ $property->id }}" class="rm-media">
                    <i class="fa fa-times"></i>
                </a>
            </div>
            <?php
        }
    }
    ?>
</div>

<p class="push-below">
<div class="progress">
    <div id="progress-3" class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
        <span class="sr-only"></span>
    </div>
</div>
<button class="btn btn-sm btn-blue upload-trigger" 
        data-target="#floorplan-image">
    <i class="fa fa-plus"></i> {{ trans('manager/project.review.add-image') }}        
</button> <span class="text-danger">(JPEG or PNG image only)</span>
<input type="file" id="floorplan-image" name="images" class="hidden image-multiple" multiple data-progress="#progress-3"
       data-action="/manager/project/property/upload/{{ $property->id }}/floorplan"/>
</p>

<h5>{{ trans('manager/project.review.details-title')  }}</h5>

<div class="review-form-container push-below row">
    <form action="/manager/project/property/review/{{ $review->id }}/floorplan" method="post" class="col-md-8">
        <div class="form-group">
            <textarea class="form-control" name="floor_plan_details" rows="5">{{ $review->floor_plan_details }}</textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<h4 class="blue">{{ trans('manager/project.review.video-title')  }}</h4>

<div class="review-form-container push-below row">
    <form action="/manager/project/property/review/{{ $review->id }}/video" method="post" class="col-md-8">
        <div class="form-group">
            <input type="text" class="form-control" name="video_link" value="{{ $review->video_link }}">
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

<h4 class="blue">{{ trans('manager/project.review.overview-title')  }}</h4>

<div class="review-form-container push-below row">
    <form action="/manager/project/property/review/{{ $review->id }}/overview" method="post" class="col-md-8">
        <div class="form-group">
            <textarea class="form-control" name="overview" rows="5">{{ $review->overview }}</textarea>
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

@endsection

@section('scripts')
<script src="/js/uploader.js?t=<?php echo filemtime(public_path() . '/js/uploader.js'); ?>"></script>
@endsection