@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'details';
$active = 'condo_project';
$langs = \App\Helpers\Language::getActives();
?>

@section('content')

<h3>{{ trans('manager/project.details-title') }}</h3>

@include('manager.project.condo.tab')

<div class="form-container row">
    <div class="col-md-12">

        <h4>General info</h4>
        <form class="form-horizontal" action="/manager/project/condo/details/{{ $condo->id }}" method="post">
            <div class="form-group">
                <label for="starting_sales_price" class="col-sm-2 control-label">Sales price start</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="starting_sales_price" value="{{ $details->starting_sales_price }}">
                </div>

                <label for="total_units" class="col-sm-2 control-label">Total units</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="total_units" value="{{ $details->total_units }}">
                </div>
            </div>
            <div class="form-group">
                <label for="office_hours" class="col-sm-2 control-label">Office hours</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="office_hours" value="{{ $details->office_hours }}">
                </div>



            </div>
            <div class="form-group">
                <label for="year_built" class="col-sm-2 control-label">Year built</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="year_built" value="{{ $details->year_built }}" placeholder="">
                </div>

                <label for="website" class="col-sm-2 control-label">Website</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="website" value="{{ $details->website }}" placeholder="">
                </div>

            </div>
            <div class="form-group">
                <label for="number_of_tower" class="col-sm-2 control-label">Number of tower</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="number_of_tower" value="{{ $details->number_of_tower }}">
                </div>

                <label for="district" class="col-sm-2 control-label">District</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="district" value="{{ $details->district }}">
                </div>

            </div>
            <div class="form-group">
                <label for="total_floors" class="col-sm-2 control-label">Total floors</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="total_floors" value="{{ $details->total_floors }}">
                </div>

                <label for="contact_person" class="col-sm-2 control-label">Contact</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="contact_person" value="{{ $details->contact_person }}">
                </div>

            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>
        </form>
        <h4>Room types</h4>
        <form class="form-horizontal" action="/manager/condo/add-room/{{ $condo->id }}" method="post" id="room-types-form">
            <div id="room-type-container">
                <?php
                $rooms = $condo->getRoomTypes();

                $i = 1;

                foreach ($rooms as $room) {
                    ?>
                    <div class="form-group room-row-{{ $i }}">
                        <label for="inputEmail3" class="col-sm-2 control-label">Type # {{ $i }}</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control room_name" id="room_name_{{ $room->id }}" name="name" value="{{ $room->name }}" placeholder="Name" onkeyup="showUpdate('#update-button-{{ $i }}')">
                        </div>
                        <div class="col-sm-2">
                            <input type="text" class="form-control room_size" id="room_size_{{ $room->id }}" name="size" value="{{ $room->size }}" placeholder="Size (sqm)" onkeyup="showUpdate('#update-button-{{ $i }}')">
                        </div>
                        <div class="col-sm-2">
                            <button type="button" id="update-button-{{ $i }}" class="btn btn-room text-primary" onclick="updateRoom({{ $room->id }})" style="display: none;"><i class="fa fa-save"></i></button>
                            <button type="button" class="btn btn-room text-danger" onclick="deleteRoomType({{ $room->id }})"><i class="fa fa-trash"></i></button>
                        </div>

                    </div>
                    <?php
                    $i++;
                }
                ?>

            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">New room</label>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="room_name" name="name" value="" placeholder="Name">
                </div>
                <div class="col-sm-2">
                    <input type="text" class="form-control" id="room_size" name="size" value="" placeholder="Size (sqm)">
                </div>
                <div class="col-sm-2">
                    <button type="button" class="btn btn-primary" onclick="addRoomType()"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </form>
        <h4>Facilities</h4>
        <form class="form-horizontal" action="/manager/project/condo/facilities/{{ $condo->id }}" method="post">

            <?php
            if (count($facilities)) {
                foreach ($facilities as $facility) {
                    ?>
                    <div class="checkbox col-md-4">
                        <label>
                            <input type="checkbox" name="facilities[]" value="{{ $facility->id }}" <?php echo in_array($facility->id, $condo_facilities) ? 'checked' : ''; ?> > {{ $facility->name_en }}
                        </label>
                    </div>
                    <?php
                }
            }
            ?>

            <div class="form-group">
                <div class="col-sm-12">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>

        </form>

        <h4>Others</h4>

        <form class="form-horizontal" action="/manager/project/condo/others/{{ $condo->id }}" method="post">

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="address">Address</label>
                </div>
            </div>

            <div class="well">
                <?php
                $address = json_decode($details->address, true);

                foreach ($langs as $code => $lang) {
                    ?>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="address">{{ $lang }}</label>
                            <input type="text" class="form-control" name="address[{{ $code }}]" value="{{ old('address.' . $code, array_get($address, $code)) }}">
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="address">Details</label>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <div class="">
                        <ul id="details-tab" class="nav nav-tabs" role="tablist">
                            <?php
                            foreach ($langs as $code => $lang) {
                                ?>
                                <li role="presentation"><a href="#details-{{ $code }}" aria-controls="settings" role="tab" data-toggle="tab">{{ $lang }}</a></li>
                                <?php
                            }
                            ?>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php
                            $details = json_decode($details->details, true);

                            foreach ($langs as $code => $lang) {
                                ?>
                                <div role="tabpanel" class="tab-pane active" id="details-{{ $code }}">
                                    <textarea class="form-control" rows="7" name="details[{{ $code }}]">{{ old('address.' . $code, array_get($details, $code)) }}</textarea>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-sm-12">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary pull-right">Update</button>
                </div>
            </div>
        </form>

    </div>
</div>

@endsection

@section('scripts')

<script>
    var condo_id = <?php echo $condo->id; ?>

    $(function(){
        $('#details-tab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        });
        $('#details-tab a:first').tab('show');
    });</script>
<script src="/js/condo-setting.js"></script>

@endsection
