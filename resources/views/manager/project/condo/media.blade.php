@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'media';
$active = 'condo_project';
?>

@section('content')

<h3>{{ trans('manager/project.details-title') }}</h3>

@include('manager.project.condo.tab')


@endsection