@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'places';
$active = 'condo_project';
?>

@section('content')

<h3>{{ trans('manager/project.details-title') }}</h3>

@include('manager.project.condo.tab')

<?php
$list = $condo->getNearbyPlaces();

$places = [];

$lang = \App::getLocale();

foreach ($list as $place) {
    $places[$place->type][] = $place;
}
?>

<h4>{{ __t('manager.condo.current-places-title', 'Current places') }}</h4>

@include('manager.points.list')

@endsection