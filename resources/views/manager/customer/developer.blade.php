@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.developer.list-title') }}</h3>

<div class="manager-content">
    
    <div class="toolbar">
        <ul class="nav nav-pills">           
            <li role="presentation" class="pull-right input">                
                <input type="text" name="q" value="" class="form-control search"/>
            </li>
            <li role="presentation" class="pull-right">
                <span class="input-label">{{ trans('manager.developer.search-filter') }}</span>
            </li>
        </ul>
    </div>
    
    <table class="table table-list">
        <thead>
            <tr>
                <th class="tiny-col">{{ trans('manager.developer.id-col') }}</th>
                <th>{{ trans('manager.developer.name-col') }}</th>
                <th class="tiny-col">{{ trans('manager.developer.listing-col') }}</th>
                <th class="tiny-col">{{ trans('manager.developer.banner-col') }}</th>
                <th class="tiny-col">{{ trans('manager.developer.visit-col') }}</th>
                <th class="small-col">{{ trans('manager.developer.last-login-col') }}</th>
                <th class="icon-col">{{ trans('manager.developer.active-col') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($profiles as $profile) {
                $user = $profile->getUser();
                $last_login = date('d/m/Y', strtotime($user->last_login));
                ?>
                <tr>
                    <td>{{ $profile->getPublicID() }}</td><!-- id -->
                    <td>{{ $profile->getName() }}</td><!-- name -->
                    <td class="text-center">{{ $user->countProperty() }}</td><!-- listing -->
                    <td class="text-center">{{ $profile->countBanners() }}</td><!-- banners -->
                    <td class="text-center">{{ $user->getTotalVisit() }}</td><!-- visit -->                    
                    <td>{{ $last_login }}</td><!-- login -->                    
                    <td class="text-center">
                        <input type="checkbox" class="user-status" name="active" data-id="{{ $user->id }}"
                               value="true" <?php echo $user->active ? 'checked' : ''; ?>/>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    
</div>
@endsection

@section('scripts')
<script src="/js/manager/user.js"></script>
@endsection