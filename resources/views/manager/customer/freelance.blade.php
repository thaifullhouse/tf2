@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.agent.list-title') }}</h3>

<div class="manager-content">    

    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="/manager/agents">{{ trans('manager.agent.freelance-tab') }} ({{ $profiles->total() }})</a></li>
        <li role="presentation"><a href="/manager/agents/realestate">{{ trans('manager.agent.realestate-tab') }}</a></li>
    </ul>

    <div class="toolbar">
        <ul class="nav nav-pills">           
            <li role="presentation" class="pull-right input">                
                <input type="text" name="q" value="" class="form-control search"/>
            </li>
            <li role="presentation" class="pull-right">
                <span class="input-label">{{ trans('manager.agent.search-filter') }}</span>
            </li>
        </ul>
    </div>

    <table class="table table-list">
        <thead>
            <tr>
                <th class="tiny-col">{{ __t('manager_agent.id-col', 'ID') }}</th>
                <th>{{ __t('manager_agent.name-col', 'Name') }}</th>
                <th class="tiny-col text-center">{{ __t('manager_agent.listing-col', 'Listing') }}</th>
                <th class="tiny-col text-center">{{ __t('manager_agent.visit-col', 'Visits') }}</th>
                <th>{{ __t('manager_agent.company-col', 'Company') }}</th>
                <th class="icon-col">{{ __t('manager_agent.register-col', 'R') }}</th>
                <th class="icon-col">{{ __t('manager_agent.verified-col', 'V') }}</th>
                <th class="icon-col">{{ __t('manager_agent.matching-col', 'M') }}</th>
                <th class="small-col">{{ __t('manager_agent.last-login-col', 'Last login') }}</th>
                <th class="icon-col">{{ __t('manager_agent.active-col', 'Active') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($profiles as $profile) {
                $user = $profile->getUser();
                $last_login = date('d/m/Y', strtotime($user->last_login));
                ?>
                <tr>
                    <td>{{ $profile->getPublicID() }}</td><!-- id -->
                    <td>{{ $profile->getName() }}</td><!-- name -->
                    <td class="text-center">{{ $user->countProperty() }}</td><!-- listing -->
                    <td class="text-center">{{ $user->getTotalVisit() }}</td><!-- visit -->
                    <td>{{ $profile->company_name }}</td><!-- company -->
                    <td class="check-col text-center"><input type="checkbox" class="has_paid" name="has_paid" value="true" <?php echo $profile->has_paid ? 'checked' : ''; ?>/></td><!-- level -->
                    <td class="check-col text-center"><input type="checkbox" class="agent_verified" name="agent_verified" value="true" <?php echo $profile->agent_verified ? 'checked' : ''; ?>/></td><!-- level -->
                    <td class="check-col text-center">
                        <?php
                        if ($profile->accept_quick_matching) {
                            ?>
                            <i class="fa fa-check text-success"></i>
                            <?php
                        } else {
                            ?>
                            <i class="fa fa-times text-danger"></i>
                            <?php
                        }
                        ?>
                    </td><!-- level -->
                    <td>{{ $last_login }}</td><!-- login -->
                    <td class="text-center">
                        <input type="checkbox" class="user-status" name="active" data-id="{{ $user->id }}"
                               value="true" <?php echo $user->active ? 'checked' : ''; ?>/>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    {{ $profiles->links() }}

</div>
@endsection

@section('scripts')
<script src="/js/manager/user.js"></script>
@endsection