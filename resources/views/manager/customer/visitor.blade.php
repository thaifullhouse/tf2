@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.visitor.list-title') }}</h3>

<div class="manager-content">

    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ trans('manager.visitor.email-col') }}</th>
                <th>{{ trans('manager.visitor.lang-col') }}</th>
                <th>{{ trans('manager.visitor.visit-col') }}</th>
                <th>{{ trans('manager.visitor.comment-col') }}</th>
                <th>{{ trans('manager.visitor.last-login-col') }}</th>
                <th class="icon-col">{{ trans('manager.visitor.active-col') }}</th>
                <th class="icon-col"></th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($profiles as $profile) {
                $user = $profile->getUser();
                $last_login = date('d/m/Y', strtotime($user->last_login));
                ?>
                <tr>
                    <td>{{ $user->email }}</td>
                    <td>{{ strtoupper($user->prefered_lang) }}</td>
                    <td>{{ $user->total_visit }}</td>
                    <td></td>
                    <td>{{ $last_login }}</td>
                    <td class="text-center">
                        <input type="checkbox" class="user-status" name="active" data-id="{{ $user->id }}"
                               value="true" <?php echo $user->active ? 'checked' : ''; ?>/>
                    </td>
                    <td class="text-center"><a href="#"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    {{ $profiles->links() }}
</div>

<div class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('scripts')
<script src="/js/manager/user.js"></script>
@endsection