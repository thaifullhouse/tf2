@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.owner.list-title') }}</h3>

<div class="manager-owners">
    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ trans('manager.owner.id-col') }}</th>
                <th>{{ trans('manager.owner.name-col') }}</th>
                <th class="text-center">{{ trans('manager.owner.listing-col') }}</th>
                <th class="small-col">{{ trans('manager.owner.visit-col') }}</th>
                <th>{{ trans('manager.owner.last-login-col') }}</th>
                <th class="icon-col">{{ trans('manager.owner.active-col') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($profiles as $profile) {
                $user = $profile->getUser();
                $last_login = date('d/m/Y', strtotime($user->last_login));
                ?>
                <tr>
                    <td>{{ $profile->getPublicID() }}</td>
                    <td>{{ $profile->getName() }}</td>
                    <td class="text-center">{{ $user->countProperty() }}</td>
                    <td class="text-center">{{ $user->getTotalVisit() }}</td>
                    <td>{{ $last_login }}</td>
                    <td class="text-center">
                        <input type="checkbox" class="user-status" name="active" data-id="{{ $user->id }}"
                               value="true" <?php echo $user->active ? 'checked' : ''; ?>/>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
</div>
@endsection

@section('scripts')
<script src="/js/manager/user.js"></script>
@endsection