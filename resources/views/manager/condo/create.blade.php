@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'info';

$langs = \App\Helpers\Language::getActives();
?>

@section('content')

<h3>{{ __t('manager_condo.create-title', 'Create a condominium') }}</h3>


<div class="form-container row">        
    <form action="/manager/condo/create" method="post">            
        <div class="col-md-5">

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul class="list-unstyled">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <div class="form-group">
                <label for="">{{ __t('manager_condo.create-name-label', 'Condo names') }} <span class="text-danger">*</span></label>
            </div>
            
            <div class="well">
            <?php
            foreach ($langs as $code => $lang) {
                ?>
                <div class="form-group">
                    <label for="">{{ $lang }}</label>
                    <input type="text" class="form-control" name="name[{{ $code }}]" value="{{ old('name.' . $code) }}" aria-describedby="basic-addon2">
                </div>
                <?php
            }
            ?>
            </div>

            <div class="form-group">
                <label for="">{{ __t('manager_condo.create-latitude-label', 'Latitude') }} <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ __t('manager_condo.create-longitude-label', 'Longitude') }} <span class="text-danger">*</span></label>
                <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ __t('manager_condo.create-area-label', 'Area') }}</label>
                <input type="text" class="form-control" id="address" name="address" value="{{ old('address') }}" placeholder="">
            </div>
            <div class="form-group">
                <label for="">{{ __t('manager_condo.create-postal-code-label', 'Postcode') }}</label>
                <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode') }}" placeholder="">
            </div>
            <p>
                <span class="text-danger">*</span> {{ __t('form.required-fields-msg', 'are required fields') }}
            </p>
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <button type="submit" class="btn btn-primary">{{ __t('manager_condo.create-submit-button', 'Submit') }}</button>
        </div>
        <div class="col-md-7">
            <div id="map-picker" style="width: 100%; height: 400px; margin-bottom: 15px;"></div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" id="area" name="area" placeholder="{{ __t('manager_condo.create-map-placeholder', 'Type the area name ...') }}">
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="clear-area-btn" type="button">{{ __t('manager_condo.create-clear-button', 'Clear') }}</button>
                    </span>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@section('scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}&libraries=places'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script>

var updateArea = function (addressComponents)
{
    console.log(addressComponents);
    area = addressComponents.streetName + ', ' + addressComponents.district;
    $('#address').val(area);
    $('#postcode').val(addressComponents.postalCode);
};

$('#map-picker').locationpicker({

    location: {latitude: <?php echo old('lat', 13.74603); ?>, longitude: <?php echo old('lng', 100.53477); ?>},
    radius: 0,
    enableAutocomplete: true,
    autocompleteSetting: {
        componentRestrictions: {country: "th"}
    },
    inputBinding: {
        latitudeInput: $('#lat'),
        longitudeInput: $('#lng'),
        locationNameInput: $('#area')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        updateArea(addressComponents);
    },
    oninitialized: function (component) {
        var addressComponents = $(component).locationpicker('map').location.addressComponents;
        updateArea(addressComponents);
    }
});

$('#clear-area-btn').on('click', function () {
    $('#area').val('');
});
</script>
@endsection