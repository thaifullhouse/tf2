@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'media';
?>

@section('content')

<h3>{{ __t('manager.condo.media-title', 'Media list') }} : {{ $condo->getJsonTranslatedField('name') }}</h3>

@include('manager.condo.tab')

<h3>{{ __t('manager.condo.current-media-title', 'Current media') }}</h3>
<div class="row thumb-container-list">
    <?php
    if (count($images)) {
        foreach ($images as $image) {
            ?>
            <div class="col-md-2">
                <div class="thumb-container" data-id="{{ $image->id }}">
                    <a class="fancybox" rel="group" href="{{ $image->getUrl() }}">
                        <img src="{{ $image->getThumbUrl('113x100', true) }}" class="img-responsive"/>
                    </a>
                    <span class="text-danger delete-image" data-link="/manager/condo/media/delete/{{ $image->id }}">
                        <i class="fa fa-times"></i>
                    </span>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<h3>{{ __t('manager.condo.media-upload-title', 'Upload new images') }}</h3>
<form method="post">
    <div class="row img-preview-container">
        
    </div>
    <div class="row upload-progress-container">
        <div class="col-md-10">
            <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">

                </div>
            </div>
        </div>
        <div class="col-md-2 text-center">
            <span id="progress-label"></span>
        </div>
    </div>
    <div class="upload-actions">
        <div class="btn-group">
            <label title="Upload image file" for="condo_image" class="btn btn-primary">
                <input type="file" accept="image/*" name="image" id="condo_image" class="hide" multiple>
                Add an image
            </label>
        </div>
        <?php
        if ($condo->user_submited == true) {
            ?>
            <a href="/manager/condo/publish/{{ $condo->id }}" class="btn btn-warning pull-right">Publish</a></li>
            <?php
        }
        ?>
    </div>
</form>

@endsection

@section('scripts')
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script>
$(document).ready(function () {
    
    $('.delete-image').on('click', function(){
        var link = $(this).data('link');
        if (confirm('<?php echo addslashes(__t('manager.condo.media-delete-confirm', 'Do you really want to delete this image ?')); ?>')) {
            window.location = link;
        }
    });

    $(".thumb-container-list").sortable({
        stop: function (event, ui) {
            var data = [];

            $.each($(".thumb-container"), function (index, value) {
                data.push($(value).data('id'));

            });
            console.log(data);

            $.post('/manager/condo/media/sort', {ranks : data},  function (data) {

            });
        }
    });

    var $inputImage = $("#condo_image");

    var formData = new FormData();

    formData.append('condo_id', <?php echo $condo->id; ?>);

    var sendImages = function () {
        console.log(formData);

        $('.upload-progress-container').show();

        var xhr = $.ajax({
            url: '/manager/condo/media/upload',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            xhr: function () {
                var mxhr = $.ajaxSettings.xhr();
                if (mxhr.upload) {
                    mxhr.upload.addEventListener('progress', progress, false);
                }
                return mxhr;
            },
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');
                $.notify("Uploaded successfully", 'success', {elementPosition: 'bottom center', autoHideDelay: 1500});
                setTimeout(function () {
                    window.location.reload(true);
                }, 1500);
            }
        });
    };

    function progress(e) {

        if (e.lengthComputable) {
            var max = e.total;
            var current = e.loaded;
            var percent = (current * 100) / max;
            $('.progress-bar').css('width', percent + '%');
            $('#progress-label').html(percent.toFixed(2) + '%');

            if (percent >= 100) {
                $('#progress-label').html(percent.toFixed(2) + '%');
            }
        }
    }

    var currentFileIndex = 0;
    var totalFileNumber = 0;


    if (window.FileReader) {
        $inputImage.on('change', function () {
            var fileReader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            currentFileIndex = 0;
            totalFileNumber = files.length;

            readNextFile(files, fileReader);

            $inputImage.prop('disabled', true);


        });
    } else {
        $inputImage.addClass("hide");
    }

    var readNextFile = function (files, reader) {

        file = files[currentFileIndex];

        if (/^image\/\w+$/.test(file.type)) {

            formData.append('images[]', file);

            reader.readAsDataURL(file);
            reader.onload = function () {

                var div = document.createElement("div");
                $(div).addClass('col-md-3').addClass('img-preview');
                var img = document.createElement("img");
                $(img).addClass('img-responsive');
                $(img).prop('src', reader.result);
                $(div).append(img);
                $('.img-preview-container').append(div);

                currentFileIndex++;
                console.log(currentFileIndex, totalFileNumber);

                if (currentFileIndex < totalFileNumber) {
                    readNextFile(files, reader);
                }

                if (currentFileIndex == totalFileNumber) {
                    sendImages();
                }
            };

        } else {

        }
    };
});
</script>

@endsection