@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.new-condo.list-title') }}</h3>



<div class="manager-content">

    <div class="toolbar">
        <ul class="nav nav-pills">
            <li role="presentation">
                <a href="/manager/condo/create">{{ trans('manager.condo.create') }}</a>
            </li>
        </ul>
    </div>

    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ trans('manager.new-condo.name-col') }}</th>
                <th>{{ trans('manager.new-condo.area-col') }}</th>
                <th class="icon-col"></th>
            </tr>
        </thead>
    </table>

</div>
@endsection