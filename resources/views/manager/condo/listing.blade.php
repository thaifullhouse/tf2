@extends('layouts.manager')

@section('title', 'Page Title')

<?php 
$tab = 'listing'; 
?>

@section('content')

<h3>{{ trans('manager.condo.details-title') }}</h3>

@include('manager.condo.tab')

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ trans('manager/condo.name-col') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($properties as $property) {
            ?>
            <tr>
                <td>{{ $property->property_name }}</td>
                <td class="text-center">
                    <a href="/property/details/{{ $property->id }}" target="_blank">
                        <i class="fa fa-eye"></i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $properties->links() }}

@endsection