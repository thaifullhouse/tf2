@extends('layouts.manager')

@section('title', 'Page Title')

<?php

$tab = 'places';

$list = $condo->getNearbyPlaces();

$places = [];

$lang = \App::getLocale();

foreach ($list as $place) {
    $places[$place->type][] = $place;
}

?>

@section('content')

<h3>{{ __t('manager.condo.places-title', 'Nearby places') }}</h3>

@include('manager.condo.tab')

<h4>{{ __t('manager.condo.current-places-title', 'Current places') }}</h4>

@include('manager.points.list')

<h4>{{ __t('manager.condo.more-places-title', 'Available places') }}</h4>
<p class="text-muted">
    {{ __t('manager.condo.places-subtitle', 'List provided by Google Place Search') }}
</p>
<div class="row place-listing">
    <form class="form-horizontal" action="manager/condo/places" method="post">
        <div class="col-md-12">
            <h5>{{ __t('manager.condo.places-bank-title', 'Banks') }}</h5>
            <?php
            $places = $condo->getGooglePlacesByType('bank');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    if (strripos($place['name'], 'atm') === false) {
                        ?>
                        <div class="checkbox col-md-6">
                            <label>
                                <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                            </label>
                        </div>
                        <?php
                    }
                }
            }
            ?>
        </div>
        <div class="col-md-12 clearfix">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Update</button>
            <input type="hidden" name="id" value="{{ $condo->id }}"/>
            <input type="hidden" name="type" value="banks"/>
        </div>
    </form>
    <form class="form-horizontal" action="manager/condo/places" method="post">
        <div class="col-md-12">
            <h5>{{ __t('manager.condo.places-stores-title', 'Stores &amp; Department stores') }}</h5>
            <?php
            $places = $condo->getGooglePlacesByType('department_store');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }

            $places = $condo->getGooglePlacesByType('store');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-md-12 clearfix">               
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Update</button>
            <input type="hidden" name="id" value="{{ $condo->id }}"/>
            <input type="hidden" name="type" value="stores"/>
        </div>
    </form>
    <form class="form-horizontal" action="manager/condo/places" method="post">
        <div class="col-md-12">
            <h5>{{ __t('manager.condo.places-schools-title', 'Schools &amp; Universities') }}</h5>
            <?php
            $places = $condo->getGooglePlacesByType('school');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }

            $places = $condo->getGooglePlacesByType('university');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-md-12 clearfix">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Update</button>
            <input type="hidden" name="id" value="{{ $condo->id }}"/>
            <input type="hidden" name="type" value="school"/>
        </div>
    </form>
    <form class="form-horizontal" action="manager/condo/places" method="post">
        <div class="col-md-12">
            <h5>{{ __t('manager.condo.places-hospital-title', 'Hospitals &amp; Pharmacies') }}</h5>
            <?php
            $places = $condo->getGooglePlacesByType('hospital');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }

            $places = $condo->getGooglePlacesByType('pharmacy');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <div class="col-md-12 clearfix">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Update</button>
            <input type="hidden" name="id" value="{{ $condo->id }}"/>
            <input type="hidden" name="type" value="hospital"/>
        </div>
    </form>
    <form class="form-horizontal" action="manager/condo/places" method="post">
        <div class="col-md-12">
            <h5>{{ __t('manager.condo.places-trains-title', 'Trani stations') }}</h5>
            <?php
            $places = $condo->getGooglePlacesByType('train_station');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }

            $places = $condo->getGooglePlacesByType('bus_station');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }

            $places = $condo->getGooglePlacesByType('subway_station');

            if ($places['status'] == 'OK') {
                foreach ($places['results'] as $place) {
                    $types = $place['types']
                    ?>
                    <div class="checkbox col-md-6">
                        <label>
                            <input type="checkbox" name="places[{{ $place['id'] }}]" value="{{ $place['name'] }}"> {{ $place['name'] }}
                        </label>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

        <div class="col-md-12 clearfix">
            {{ csrf_field() }}
            <button type="submit" class="btn btn-primary pull-right">Update</button>
            <input type="hidden" name="id" value="{{ $condo->id }}"/>
            <input type="hidden" name="type" value="transportation"/>
        </div>

    </form>
</div>

@endsection