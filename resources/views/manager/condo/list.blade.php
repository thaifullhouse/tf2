@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager/condo.list-title') }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/condo/create">{{ trans('manager/condo.create') }}</a>
        </li>
        <li role="presentation" class="pull-right input">                
            <input type="text" name="q" value="" class="form-control search"/>
        </li>
        <li role="presentation" class="pull-right">
            <span class="input-label">{{ trans('manager/condo.search-filter') }}</span>
        </li>
    </ul>
</div>

<table class="table table-list">
    <thead>
        <tr>
            <th class="icon-col"></th>
            <th>{{ __t('manager.condo.name-col', 'Name') }}</th>
            <th>{{ __t('manager.condo.area-col', 'Area') }}</th>
            <th class="text-center">{{ __t('manager.condo.listed-col', 'Listed') }}</th>
            <th class="text-center">{{ __t('manager.condo.photo-col', 'Photo') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($condos as $condo) {
            ?>
            <tr>
                <td class="text-center">
                    <input type="checkbox" name="active" value="yes" <?php echo $condo->active ? 'checked' : ''; ?>/>
                </td>
                <td>{{ $condo->getJsonTranslatedField('name') }}</td>
                <td>{{ $condo->location_name }}</td>
                 <td class="tiny-col text-center">{{ $condo->countListedProperties() }}</td>
                <td class="tiny-col text-center">{{ $condo->countImages() }}</td>
                <td class="text-center">
                    <a href="/manager/condo/info/{{ $condo->id }}">
                        <i class="fa fa-pencil-square-o"></i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>
{{ $condos->links() }}

@endsection