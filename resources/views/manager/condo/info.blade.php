@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'info';
$location = $condo->get_location();
$langs = \App\Helpers\Language::getActives();
?>

@section('content')

<h3>{{ $condo->getTranslatedField('name') }}</h3>

<div class="manager-content">     

    @include('manager.condo.tab')

    <div class="form-container row">
        <div class="col-md-5">

            <form action="/manager/condo/info" method="post">

                <div class="form-group">
                    <label for="">{{ __t('manager_condo.create-name-label', 'Condo names') }} <span class="text-danger">*</span></label>
                </div>
                <div class="well">
                    <?php
                    $name = json_decode($condo->name, true);

                    foreach ($langs as $code => $lang) {
                        ?>
                        <div class="form-group">
                            <label for="">{{ $lang }}</label>
                            <input type="text" class="form-control" name="name[{{ $code }}]" value="{{ old('name.' . $code, array_get($name, $code)) }}">
                        </div>

                        <?php
                    }
                    ?>
                </div>
                <div class="form-group">
                    <label for="">Province</label>
                    <select class="form-control" id="province_id" name="province_id">

                        <?php
                        if ($condo->province_id == 0) {
                            ?>
                            <option value="0">Select a province</option>
                            <?php
                        }
                        foreach ($provinces as $id => $province) {
                            ?>
                            <option value="{{ $id }}" {{ (isset($condo->province_id) && $id == $condo->province_id) ? 'selected' : '' }}>{{ $province }}</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">District</label>
                    <select class="form-control" id="district_id" name="district_id" {{ isset($condo->district_id) ? '' : 'disabled' }}>

                            <?php
                            if (count($districts)) {
                                foreach ($districts as $id => $district) {
                                    ?>
                                    <option value="{{ $id }}" {{ (isset($condo->district_id) && $id == $condo->district_id) ? 'selected' : '' }}>{{ $district }}</option>
                                        <?php
                                    }
                                } else {
                                    ?>
                            <option value="0">Please select a province</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Area</label>
                    <select class="form-control" id="area_id" name="area_id" {{ isset($condo->area_id) ? '' : 'disabled' }}>

                            <?php
                            if (count($areas)) {
                                foreach ($areas as $id => $area) {
                                    ?>
                                    <option value="{{ $id }}" {{ (isset($condo->area_id) && $id == $condo->area_id) ? 'selected' : '' }}>{{ $area }}</option>
                                        <?php
                                    }
                                } else {
                                    ?>
                            <option value="0">Please select a district</option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="">Custom Area</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ old('address', $condo->location_name) }}" placeholder="">
                </div>
                <div class="form-group">
                    <label for="">Latitude</label>
                    <input type="text" class="form-control" id="lat" name="lat" value="{{ old('lat', $location['lat']) }}" placeholder="">
                </div>
                <div class="form-group">
                    <label for="">Longitude</label>
                    <input type="text" class="form-control" id="lng" name="lng" value="{{ old('lng', $location['lng']) }}" placeholder="">
                </div>

                <div class="form-group">
                    <label for="">Postcode</label>
                    <input type="text" class="form-control" id="postcode" name="postcode" value="{{ old('postcode', $condo->postcode) }}" placeholder="">
                </div>
                
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <input type="hidden" name="id" value="{{ $condo->id }}">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

        </div>
        <div class="col-md-7">
            <div id="map-picker" style="width: 100%; height: 400px; margin-bottom: 15px;"></div>
            <div class="form-group">
                <div class="input-group">
                    <input type="text" class="form-control" id="area" name="area" placeholder="Type the area name ...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="clear-area-btn" type="button">Clear</button>
                    </span>
                </div>
            </div>

            <?php
            if ($condo->user_submited == true) {
                ?>
                <a href="/manager/condo/publish/{{ $condo->id }}" class="btn btn-warning">Publish</a></li>
                <?php
            }
            ?>

        </div>
    </div>



</div>

@endsection

<?php
$lat = old('lat', $location['lat']);
$lng = old('lng', $location['lng']);

$lat = $lat ? $lat : 13.746318;
$lng = $lng ? $lng : 100.534875;
?>

@section('scripts')
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&key={{ config('google.browser_key') }}&libraries=places'></script>
<script src="/js/lp/locationpicker.jquery.js"></script>
<script src="/js/province.js"></script>
<script>

var updateArea = function (addressComponents)
        {
        console.log(addressComponents);
        area = addressComponents.streetName + ', ' + addressComponents.district;
        $('#address').val(area);
        $('#postcode').val(addressComponents.postalCode);
        };
var initArea = function (addressComponents)
        {
        if ($('#address').val().length == 0) {
        area = addressComponents.streetName + ', ' + addressComponents.district;
        $('#address').val(area);
        $('#postcode').val(addressComponents.postalCode);
        }
        };
$('#map-picker').locationpicker({
location: {latitude: {{ $lat }}, longitude: {{ $lng }}  },
        radius: 0,
        enableAutocomplete: true,
        autocompleteSetting: {
        componentRestrictions: {country: "th"}
        },
        inputBinding: {
        latitudeInput: $('#lat'),
                longitudeInput: $('#lng'),
                locationNameInput: $('#area')
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        updateArea(addressComponents);
        },
        oninitialized: function (component) {
        var addressComponents = $(component).locationpicker('map').location.addressComponents;
        initArea(addressComponents);
        }
});
$('#clear-area-btn').on('click', function () {
$('#area').val('');
});

</script>
@endsection
