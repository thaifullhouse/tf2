<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/condo/list"><i class="fa fa-long-arrow-left"></i> {{ trans('manager/condo.back') }}</a>
        </li>
    </ul>
</div>

<ul class="nav nav-tabs">
    <li role="presentation" <?php echo $tab == 'info' ? 'class="active"' : ''; ?> >
        <a href="/manager/condo/info/{{ $condo->id }}">{{ trans('manager/condo.info-tab') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'details' ? 'class="active"' : ''; ?>>
        <a href="/manager/condo/details/{{ $condo->id }}">{{ trans('manager/condo.details-tab') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'media' ? 'class="active"' : ''; ?>>
        <a href="/manager/condo/media/{{ $condo->id }}">{{ trans('manager/condo.media-tab') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'places' ? 'class="active"' : ''; ?>>
        <a href="/manager/condo/places/{{ $condo->id }}">{{ trans('manager/condo.places-tab') }}</a>
    </li>
    <li role="presentation" <?php echo $tab == 'listing' ? 'class="active"' : ''; ?>>
        <a href="/manager/condo/listing/{{ $condo->id }}">{{ trans('manager/condo.listing-tab') }}</a>
    </li>
</ul>