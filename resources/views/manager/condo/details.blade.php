@extends('layouts.manager')

@section('title', 'Page Title')

<?php
$tab = 'details';
?>

@section('content')

<h3>{{ trans('manager.condo.details-title') }}</h3>

<div class="manager-content">     

    @include('manager.condo.tab')

    <div class="form-container row">
        <div class="col-md-12">

            <h4>General info</h4>
            <form class="form-horizontal" action="/manager/condo/general-info/{{ $condo->id }}" method="post">
                <div class="form-group">
                    <label for="starting_sales_price" class="col-sm-2 control-label">Sales price start</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="starting_sales_price" value="{{ $details->starting_sales_price }}">
                    </div>

                    <label for="total_units" class="col-sm-2 control-label">Total units</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="total_units" value="{{ $details->total_units }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="office_hours" class="col-sm-2 control-label">Office hours</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="office_hours" value="{{ $details->office_hours }}">
                    </div>



                </div>
                <div class="form-group">
                    <label for="year_built" class="col-sm-2 control-label">Year built</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="year_built" value="{{ $details->year_built }}" placeholder="">
                    </div>

                    <label for="website" class="col-sm-2 control-label">Website</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="website" value="{{ $details->website }}" placeholder="">
                    </div>

                </div>
                <div class="form-group">
                    <label for="number_of_tower" class="col-sm-2 control-label">Number of tower</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="number_of_tower" value="{{ $details->number_of_tower }}">
                    </div>

                    <label for="district" class="col-sm-2 control-label">District</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="district" value="{{ $details->district }}">
                    </div>

                </div>
                <div class="form-group">
                    <label for="total_floors" class="col-sm-2 control-label">Total floors</label>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" name="total_floors" value="{{ $details->total_floors }}">
                    </div>

                    <label for="contact_person" class="col-sm-2 control-label">Contact</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="contact_person" value="{{ $details->contact_person }}">
                    </div>

                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>
            </form>
            <h4>Room types</h4>
            <form class="form-horizontal" action="/manager/condo/add-room/{{ $condo->id }}" method="post" id="room-types-form">
                <div id="room-type-container">
                    <?php
                    $rooms = $condo->getRoomTypes();

                    $i = 1;

                    foreach ($rooms as $room) {
                        ?>
                        <div class="form-group room-row-{{ $i }}">
                            <label for="inputEmail3" class="col-sm-2 control-label">Type # {{ $i }}</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control room_name" id="room_name_{{ $room->id }}" name="name" value="{{ $room->name }}" placeholder="Name" onkeyup="showUpdate('#update-button-{{ $i }}')">
                            </div>
                            <div class="col-sm-2">
                                <input type="text" class="form-control room_size" id="room_size_{{ $room->id }}" name="size" value="{{ $room->size }}" placeholder="Size (sqm)" onkeyup="showUpdate('#update-button-{{ $i }}')">
                            </div>
                            <div class="col-sm-2">
                                <button type="button" id="update-button-{{ $i }}" class="btn btn-room text-primary" onclick="updateRoom({{ $room->id }})" style="display: none;"><i class="fa fa-save"></i></button>
                                <button type="button" class="btn btn-room text-danger" onclick="deleteRoomType({{ $room->id }})"><i class="fa fa-trash"></i></button>
                            </div>

                        </div>
                        <?php
                        $i++;
                    }
                    ?>

                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">New room</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="room_name" name="name" value="" placeholder="Name">
                    </div>
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="room_size" name="size" value="" placeholder="Size (sqm)">
                    </div>
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" onclick="addRoomType()"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </form>
            <h4>Facilities</h4>
            <form class="form-horizontal" action="/manager/condo/update-facilities/{{ $condo->id }}" method="post">

                <?php
                if (count($facilities)) {
                    foreach ($facilities as $facility) {
                        ?>
                        <div class="checkbox col-md-4">
                            <label>
                                <input type="checkbox" name="facilities[]" value="{{ $facility->id }}" <?php echo in_array($facility->id, $condo_facilities) ? 'checked' : ''; ?> > {{ $facility->name_en }}
                            </label>
                        </div>
                        <?php
                    }
                }
                ?>

                <div class="form-group">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>

            </form>

            <h4>Others</h4>

            <form class="form-horizontal" action="/manager/condo/update-others/{{ $condo->id }}" method="post">
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="address">Address TH</label>
                        <input type="text" class="form-control" name="address_th" value="{{ $details->address_th }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="address">Address EN</label>
                        <input type="text" class="form-control" name="address_en" value="{{ $details->address_en }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="details">Details TH</label>
                        <textarea class="form-control" rows="7" name="details_th">{{ $details->details_th }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12">
                        <label for="details">Details EN</label>
                        <textarea class="form-control" rows="7" name="details_en">{{ $details->details_en }}</textarea>
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-sm-12">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary pull-right">Update</button>
                    </div>
                </div>
            </form>

        </div>
    </div>

</div>

@endsection

@section('scripts')

<script>

var addRoomType = function() {
    var formData = {
        name : $('#room_name').val(),
        size: $('#room_size').val()
    };

    $.post( "/manager/condo/add-room/{{ $condo->id }}", formData, function(response) {
        $("#room-type-container").html(response);
        $('#room_name').val('');
        $('#room_size').val('');
    });
};

var deleteRoomType = function(roomId) {
    $.get( "/manager/condo/delete-room/{{ $condo->id }}/" + roomId, function(response) {
        $("#room-type-container").html(response);
    });
};

var updateRoom = function(roomId){
    var formData = {
        name : $('#room_name_' + roomId).val(),
        size: $('#room_size_' + roomId).val(),
        id : roomId
    };

    $.post( "/manager/condo/update-room/{{ $condo->id }}/" + roomId, formData, function(response) {
        $("#room-type-container").html(response);
    });
};

var showUpdate = function(id)
{
    $(id).show();
};

</script>

@endsection
