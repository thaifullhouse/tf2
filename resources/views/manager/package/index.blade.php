@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ trans('manager.package.list-title') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/create-featured">{{ trans('manager.package.create-featured') }}</a>
        </li>
        <li role="presentation">
            <a href="/manager/package/create-exclusive">{{ trans('manager.package.create-exclusive') }}</a>
        </li>
    </ul>
</div>
<table class="table table-list">
    <thead>
        <tr>
            <th class="small-col">{{ trans('manager.package.col-type') }}</th>
            <th>{{ trans('manager.package.col-name') }}</th>            
            <th class="small-col text-center">{{ trans('manager.package.col-listing') }}</th>
            <th class="small-col text-center">{{ trans('manager.package.col-discount') }}</th>
            <th class="small-col text-right">{{ trans('manager.package.col-price') }}</th>
            <th class="small-col text-center">{{ trans('manager.package.col-free-listing') }}</th>
            <th class="small-col text-center">{{ trans('manager.package.col-period') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <?php
    if (count($packages)) {
        foreach ($packages as $package) {
            $discount = (int) (100 * $package->discounted_price / $package->normal_price);
            ?>
            <tr>
                <td>{{ strtoupper($package->type) }}</td>
                <td>
                    <a href="/manager/package/edit-{{ $package->type }}/{{ $package->id }}">{{ $package->name }}</a>
                </td>   
                <td class="text-center">
                    {{ number_format($package->listing_count) }}
                </td>
                <td class="text-center">
                    <?php
                        if($discount > 0) {
                            echo (100 - $discount), '%';
                        }
                        else {
                            echo '-';
                        }
                    ?>
                </td>
                <td class="text-right">
                    <?php
                    if ($package->discounted_price > 0) {
                        echo number_format($package->discounted_price);
                    } else {
                        echo number_format($package->normal_price);
                    }
                    ?>
                </td>
                <td class="text-center">
                    {{ number_format($package->free_listing_count) }}
                </td>
                <td class="text-center">
                    {{ $package->period }} 
                </td>
                <td class="icon-col">
                    <a href="/manager/package/edit-{{ $package->type }}/{{ $package->id }}"><i class="fa fa-pencil"></i></a>
                </td> 
            </tr>
            <?php
        }
    }
    ?>
</table>
@endsection