@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ trans('manager.package.delete-title') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/list">{{ trans('manager.package.back-to-list') }}</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <h3>{{ trans('manager.package.details-title') }}</h3>
        <table class="table model-details">
            <tr>
                <td>{{ trans('manager.package.name') }}</td>
                <td>{{ $package->name }}</td>
            </tr>
            <tr>
                <td>{{ trans('manager.package.type') }}</td>
                <td>{{ $package->type }}</td>
            </tr>
            <tr>
                <td>{{ trans('manager.package.listing-count') }}</td>
                <td>{{ $package->listing_count }}</td>
            </tr>
            <tr>
                <td>{{ trans('manager.package.free-listing-count') }}</td>
                <td>{{ $package->free_listing_count }}</td>
            </tr>
            <tr>
                <td>{{ trans('manager.package.normal-price') }}</td>
                <td>{{ number_format($package->normal_price) }}</td>
            </tr>
            <tr>
                <td>{{ trans('manager.package.discounted-price') }}</td>
                <td>{{ number_format($package->discounted_price) }}</td>
            </tr>
        </table>
        <div class="alert alert-danger">
            {{ trans('manager.package.details-confirmation-question') }}
        </div>
        <div>
            <a href="/manager/package/list" class="btn btn-default">{{ trans('form.cancel') }}</a>
            <a href="/manager/package/do-delete/{{ $package->id }}" class="btn btn-danger pull-right">{{ trans('form.delete') }}</a>
        </div>
    </div>
</div>

@endsection