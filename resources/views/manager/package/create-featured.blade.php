@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ trans('manager.package.create-featured-title') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/list">{{ trans('manager.package.back-to-list') }}</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-container">
            <form method="post" action="/manager/package/save-featured">
                <div class="form-group">
                    <label for="name">{{ trans('manager.package.name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="listing_count">{{ trans('manager.package.listing-count') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="listing_count" value="{{ old('listing_count') }}">
                </div>
                <div class="form-group">
                    <label for="normal_price">{{ trans('manager.package.normal-price') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="normal_price" value="{{ old('normal_price') }}">
                </div>
                <div class="form-group">
                    <label for="discounted_price">{{ trans('manager.package.discounted-price') }}</label>
                    <input type="number" class="form-control" name="discounted_price" value="{{ old('discounted_price') }}">
                </div>
                <div class="form-group">
                    <label for="period">{{ trans('manager.package.period') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="period" value="{{ old('period') }}">
                </div>
                <p>
                    <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
                </p>
                <button type="submit" class="btn btn-primary">{{ trans('form.create') }}</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <div class="col-md-6">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

    </div>
</div>

@endsection