@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')
<h3>{{ trans('manager.package.update-package-title') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation">
            <a href="/manager/package/list">{{ trans('manager.package.back-to-list') }}</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-container">
            <form method="post" action="/manager/package/update-exclusive/{{ $package->id }}">
                <div class="form-group">
                    <label for="name">{{ trans('manager.package.name') }} <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" value="{{ old('name', $package->name) }}">
                </div>
                <div class="form-group">
                    <label for="listing_count">{{ trans('manager.package.listing-count') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="listing_count" value="{{ old('listing_count', $package->listing_count) }}">
                </div>
                <div class="form-group">
                    <label for="free_listing_count">{{ trans('manager.package.free-listing-count') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="free_listing_count" value="{{ old('free_listing_count', $package->free_listing_count) }}">
                </div>
                <div class="form-group">
                    <label for="normal_price">{{ trans('manager.package.normal-price') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="normal_price" value="{{ old('normal_price', $package->normal_price) }}">
                </div>
                <div class="form-group">
                    <label for="discounted_price">{{ trans('manager.package.discounted-price') }}</label>
                    <input type="number" class="form-control" name="discounted_price" value="{{ old('discounted_price', $package->discounted_price) }}">
                </div>
                <div class="form-group">
                    <label for="period">{{ trans('manager.package.period') }} <span class="text-danger">*</span></label>
                    <input type="number" class="form-control" name="period" value="{{ old('period', $package->period) }}">
                </div>
                <p>
                    <span class="text-danger">*</span> {{ trans('form.required-fields-msg') }}
                </p>
                <button type="submit" class="btn btn-primary">{{ trans('form.update') }}</button>
                {{ csrf_field() }}
            </form>
        </div>
    </div>
    <div class="col-md-6">
        <?php
        if ($status === 'saved') {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ trans('manager.package.saved') }}
            </div>
            <?php
        }
        ?>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="danger-zone">
            <p>
                {{ trans('manager.package.delete-message') }}
            </p>
            <a href="/manager/package/delete/{{ $package->id }}" class="btn btn-danger">
                {{ trans('manager.package.delete-link') }}
            </a>
        </div>

    </div>
</div>

@endsection