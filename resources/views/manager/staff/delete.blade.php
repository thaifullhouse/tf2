<div class="alert alert-danger">
    <p>
        Delete <strong>{{ $user->realname }}</strong>
    </p>
    <p>
        <a href="/manager/staff/remove/{{ $user->id }}" class="btn btn-danger">{{ trans('form.delete') }}</a>
    </p>
</div>