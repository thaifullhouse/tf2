<h4>{{ trans('manager/staff.update-title') }}</h4>
<form id="update-staff-form" action="/manager/staff/update/{{ $staff->staff_id }}">
    <div class="form-group">
        <label for="name">{{ trans('manager/staff.name-label') }}</label>
        <input type="text" class="form-control" id="name" name="name" value="{{ $user->realname }}">
    </div>
    <div class="form-group">
        <label for="email">{{ trans('manager/staff.email-label') }}</label>
        <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" readonly>
    </div>
    <div class="form-group">
        <label for="email">{{ trans('manager/staff.role-label') }}</label>
        <select class="form-control" id="role" name="role">
            <?php
                $roles = \App\Models\User\Staff::getRoles();
                foreach($roles as $role => $name) {
                    ?>
                    <option value="{{ $role }}" <?php echo $staff->role == $role ? 'selected' : ''; ?> >{{ $name }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
</form>