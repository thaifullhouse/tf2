<h4>{{ trans('manager/staff.create-title') }}</h4>
<form id="create-staff-form" action="/manager/staff/create">
    <div class="form-group">
        <label for="name">{{ trans('manager/staff.name-label') }}</label>
        <input type="text" class="form-control" id="name" name="name" value="">
    </div>
    <div class="form-group">
        <label for="email">{{ trans('manager/staff.email-label') }}</label>
        <input type="text" class="form-control" id="email" name="email" value="">
    </div>
    <div class="form-group">
        <label for="email">{{ trans('manager/staff.role-label') }}</label>
        <select class="form-control" id="role" name="role">
            <?php
                $roles = \App\Models\User\Staff::getRoles();
                foreach($roles as $role => $name) {
                    ?>
                    <option value="{{ $role }}">{{ $name }}</option>
                    <?php
                }
            ?>
        </select>
    </div>
    <div class="alert alert-danger error-container" style="display: none;">
        {{ trans('manager/staff.user-exists') }}
    </div>
</form>