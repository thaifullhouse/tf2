<form id="listing-update-form" class="listing-update-form">
    <h4>#{{ $property->id }} : {{ $property->property_name }}</h4>
    <div class="form-group">
        <label for="listing_class">{{ trans('manager-listing.class-label') }}</label>
        <select class="form-control" name="listing_class">
            <option value="">-</option>
            <option value="standard" <?php echo $property->listing_class == 'standard' ? 'selected' : ''; ?> >
                {{ trans('manager-listing.class-standard') }}
            </option>
            <option value="featured" <?php echo $property->listing_class == 'featured' ? 'selected' : ''; ?> >
                {{ trans('manager-listing.class-featured') }}
            </option>
            <option value="exclusive" <?php echo $property->listing_class == 'exclusive' ? 'selected' : ''; ?> >
                {{ trans('manager-listing.class-exclusive') }}
            </option>
        </select>
    </div>
    <div class="form-group">
        <label for="property_status">{{ trans('manager-listing.status-label') }}</label>
        <select class="form-control" name="property_status">
            <option value="">-</option>
            <option value="present" <?php echo $property->status == 'present' ? 'selected' : '' ; ?>>{{ trans('manager-listing.status-present') }}</option>
            <option value="expired" <?php echo $property->status == 'expired'  ? 'selected' : '' ; ?>>{{ trans('manager-listing.status-expired') }}</option>
        </select>
    </div>
    <div class="form-group">
        <label for="publication_date">{{ trans('manager-listing.start-label') }}</label>
        <div class="input-group">
            <input type="text" id="from" name="publication_date" value="{{ date('d/m/Y', strtotime($property->publication_date)) }}" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
        </div>
    </div>
    <div class="form-group">
        <label for="expiration_date">{{ trans('manager-listing.end-label') }}</label>
        <div class="input-group">
            <input type="text" id="to" name="expiration_date" value="{{ date('d/m/Y', strtotime($property->expiration_date)) }}" class="form-control">
            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
            </span>
        </div>
    </div>
    {{ csrf_field() }}
    <input type="hidden" id="pid" name="id" value="{{ $property->id }}"/>
</form>