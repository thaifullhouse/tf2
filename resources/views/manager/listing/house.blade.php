@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<?php $active = 'house'; ?>

<h3>{{ trans('manager.listing.list-title') }}</h3>
<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right input">                
            <input type="text" name="q" value="" class="form-control search"/>
        </li>
        <li role="presentation" class="pull-right">
            <span class="input-label">{{ trans('manager/condo.search-filter') }}</span>
        </li>
    </ul>
</div>

<table class="table table-filter">
    <tr>
        <td class="filter-label">{{ trans('manager/project.filter-type') }}</td>
        <td class="filter-all">
            <input type="radio" name="type" value="all" class="filter" <?php echo array_get($filters, 'type') == 'all' ? 'checked' : ''; ?> > 
            {{ trans('manager/project.filter-all') }}
        </td>
        <td class="filter">
            <input type="radio" name="type" value="rent" 
                   class="filter" <?php echo array_get($filters, 'type') == 'rent' ? 'checked' : ''; ?> > 
            {{ trans('manager/project.filter-type-rent') }}
        </td>
        <td class="filter">
            <input type="radio" name="type" value="sale" 
                   class="filter" <?php echo array_get($filters, 'type') == 'sale' ? 'checked' : ''; ?> >  
            {{ trans('manager/project.filter-type-sale') }}
        </td>
        <td class="filter"></td>
        <td class="filter"></td>
        <td></td>
    </tr>
    <tr>
        <td class="filter-label">
            {{ trans('manager/project.filter-class') }}
        </td>
        <td>
            <input type="radio" name="class" value="all" 
                   class="filter" <?php echo array_get($filters, 'class') == 'all' ? 'checked' : ''; ?> >
            {{ trans('manager/project.filter-all') }}
        </td>
        <td>
            <input type="radio" name="class" value="standard" 
                   class="filter" <?php echo array_get($filters, 'class') == 'standard' ? 'checked' : ''; ?> >
            {{ trans('manager/project.filter-class-standard') }}
        </td>
        <td>
            <input type="radio" name="class" value="featured" 
                   class="filter" <?php echo array_get($filters, 'class') == 'featured' ? 'checked' : ''; ?> > 
            {{ trans('manager/project.filter-class-featured') }}
        </td>
        <td>
            <input type="radio" name="class" value="exclusive" 
                   class="filter" <?php echo array_get($filters, 'class') == 'exclusive' ? 'checked' : ''; ?> > 
            {{ trans('manager/project.filter-class-exclusive') }}
        </td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td class="filter-label">
            {{ trans('manager/project.filter-status') }}
        </td>
        <td>
            <input type="radio" name="status" value="all" 
                   class="filter" <?php echo array_get($filters, 'status') == 'all' ? 'checked' : ''; ?> > 
            {{ trans('manager/project.filter-all') }}
        </td>
        <td>
            <input type="radio" name="status" value="expired" 
                   class="filter" <?php echo array_get($filters, 'status') == 'expired' ? 'checked' : ''; ?> >  
            {{ trans('manager/project.filter-status-expired') }}
        </td>
        <td>
            <input type="radio" name="status" value="present" 
                   class="filter" <?php echo array_get($filters, 'status') == 'present' ? 'checked' : ''; ?> >  
            {{ trans('manager/project.filter-status-present') }}
        </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>


<table class="table table-list">
    <thead>
        <tr>
            <th>{{ trans('manager/project.id-col') }}</th>
            <th>{{ trans('manager/project.type-col') }}</th>
            <th>{{ trans('manager/project.property-col') }}</th>
            <th>{{ trans('manager/project.price-col') }}</th>
            <th>{{ trans('manager/project.province-col') }}</th>
            <th>{{ trans('manager/project.class-col') }}</th>
            <th>{{ trans('manager/project.start-col') }}</th>
            <th>{{ trans('manager/project.status-col') }}</th>
            <th class="icon-col">{{ trans('manager/project.active-col') }}</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($properties as $property) {
            $details = $property->getDetails();
            ?>
            <tr>
                <td>{{ $property->id }}</td><!-- id -->
                <td>{{ trans('property.listing_type.' . $property->listing_type) }}</td><!-- type -->
                <td>{{ trans('property.type.' . $property->property_type) }}</td><!-- property -->
                <td class="text-right">{{ number_format($details->listing_price) }}</td><!-- price -->
                <td>{{ $property->getProvince() }}</td><!-- province -->
                <td>{{ ucfirst($property->listing_class) }}</td><!-- class -->
                <td>{{ $property->getPublicationPeriode() }}</td><!-- start -->
                <td>{{ $property->status }}</td><!-- status -->
                <td class="text-center">
                    <input type="checkbox" name="published" value="YES" <?php echo $property->published ? 'checked' : ''; ?> 
                           class="listing-visibility" data-id="{{ $property->id }}"/>
                </td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-white edit-property" data-id="{{ $property->id }}">
                        <i class="fa fa-pencil"></i>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>



<div id="listing-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger pull-left" id="listing-delete">{{ trans('form.delete') }}</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ trans('form.close') }}</button>
                <button type="button" class="btn btn-primary" id="listing-save">{{ trans('form.save') }}</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{ $properties->appends($filters)->links() }}

@endsection

@section('scripts')
<script>

    var updatefilter = function () {

        var ftype = $('.filter[name=type]:checked').val();
        var fclass = $('.filter[name=class]:checked').val();
        var fstatus = $('.filter[name=status]:checked').val();

        window.location = '/manager/house/list?type=' + ftype + '&property=' + fproperty + '&class=' + fclass + '&status=' + fstatus;

    };

    $(document).ready(function () {
        
        $('.filter').on('click', function () {

            console.log(this);

            updatefilter();
        });

        $('.edit-property').on('click', function () {
            var pid = $(this).data('id');

            $('.modal-body').load('/manager/listing/form/' + pid, function () {

                var dateFormat = "dd/mm/yy";
                var from = $("#from")
                        .datepicker({
                            minDate : new Date(),
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 2,
                            dateFormat: dateFormat
                        })
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        });

                var to = $("#to")
                        .datepicker({
                            minDate : new Date(),
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 2,
                            dateFormat: dateFormat
                        })
                        .on("change", function () {
                            from.datepicker("option", "maxDate", getDate(this));
                        });

                function getDate(element) {
                    var date;
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }

                    return date;
                }

                $('#listing-modal').modal();
            });

        });

        //

        $('#listing-save').on('click', function () {
            var form = document.getElementById('listing-update-form');
            var data = new FormData(form);
            $.ajax({
                url: '/manager/listing/update',
                data: data,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    window.location.reload();
                }
            });
        });

        $('#listing-delete').on('click', function () {
            var pid = $('#pid').val();
            $.get('/manager/listing/delete/' + pid, function(){
                window.location.reload();
            });
        });
        
        $('.listing-visibility').on('click', function(){
            var pid = $(this).data('id');
            $.get('/manager/listing/toggle/' + pid);
        });
    });

</script>


@endsection