<?php
$langs = \App\Helpers\Language::getActives();
?>


<form id="translation-form">
    <h3>{{ trans('__section.' . $translation->section) }}</h3>
    <div class="well">
        {{ $translation->description }}
    </div>
    <?php
    foreach ($langs as $code => $lang) {
        ?>
        <div class="form-group">
            <label for="{{ $code }}">{{ $lang }}</label>
            <input type="text" class="form-control" name="{{ $code }}" value="{{ $translation->translation[$code] }}">
        </div>
        <?php
    }
    ?>
    <input type="hidden" name="id" value="{{ $translation->id }}"/>
</form>