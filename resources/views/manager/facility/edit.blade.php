@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>Update : {{ $facility->name_en }}</h3>

<div class="toolbar">
    <ul class="nav nav-pills">
        <li role="presentation" class="pull-right">
            <a href="/manager/facility/list"><i class="fa fa-long-arrow-left"></i> {{ trans('manager.facility.back') }}</a>
        </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="clearfix">
            <form method="post" action="/manager/facility/update/{{ $facility->id }}">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">
                    <label for="name_th">Name TH</label>
                    <input type="text" class="form-control" name="name_th" value="{{ old('name_th', $facility->name_th) }}" placeholder="">
                </div>
                <div class="form-group">
                    <label for="name_en">Name EN</label>
                    <input type="text" class="form-control" name="name_en" value="{{ old('name_en', $facility->name_en) }}" placeholder="">
                </div>

                {{ csrf_field() }}

                <button type="submit" class="btn btn-primary pull-right">Update</button>
            </form>
        </div>

        <div class="danger-zone">
            <p>
                {{ trans('manager.facility.delete-message') }}
            </p>
            <a href="/manager/facility/delete/{{ $facility->id }}" class="btn btn-danger">
                {{ trans('manager.facility.delete-link') }}
            </a>
        </div>
    </div>
    <div class="col-md-offset-1 col-md-4">
        <h3>Current icon</h3>
        <div class="facility-icon-preview">
            <img src="{{ $facility->getThumbUrl('100x100', true) }}" class="img-responsive"/>
        </div>
        <div class="row">
            <div class="col-md-12 img-preview-container">

            </div>
        </div>
        <div class="row upload-progress-container">
            <div class="col-md-10">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">

                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <span id="progress-label"></span>
            </div>
        </div>
        <div class="btn-group">
            <label title="Upload image file" for="facility_icon" class="btn btn-default">
                <input type="file" accept="image/*" name="image" id="facility_icon" class="hide">
                Upload an icon
            </label>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<script>
    $(document).ready(function () {

        var $inputImage = $("#facility_icon");

        var formData = new FormData();

        formData.append('facility_id', <?php echo $facility->id; ?>);

        var sendImages = function () {
            console.log(formData);

            $('.upload-progress-container').show();

            var xhr = $.ajax({
                url: '/manager/facility/upload',
                data: formData,
                type: 'POST',
                contentType: false,
                processData: false,
                xhr: function () {
                    var mxhr = $.ajaxSettings.xhr();
                    if (mxhr.upload) {
                        mxhr.upload.addEventListener('progress', progress, false);
                    }
                    return mxhr;
                },
                complete: function (xhr) {
                    $('.progress-bar').addClass('progress-bar-success');
                    $.notify("Uploaded successfully", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
                    setTimeout(function () {
                        window.location.reload(true);
                    }, 1500);
                }
            });
        };

        function progress(e) {

            if (e.lengthComputable) {
                var max = e.total;
                var current = e.loaded;
                var percent = (current * 100) / max;
                $('.progress-bar').css('width', percent + '%');
                $('#progress-label').html(percent.toFixed(2) + '%');

                if (percent >= 100) {
                    $('#progress-label').html(percent.toFixed(2) + '%');
                }
            }
        }

        var currentFileIndex = 0;
        var totalFileNumber = 0;

        if (window.FileReader) {
            $inputImage.on('change', function () {
                var fileReader = new FileReader(),
                        files = this.files,
                        file;

                if (!files.length) {
                    return;
                }

                readNextFile(files, fileReader);

                $inputImage.prop('disabled', true);


            });
        } else {
            $inputImage.addClass("hide");
        }

        var readNextFile = function (files, reader) {

            file = files[currentFileIndex];

            if (/^image\/\w+$/.test(file.type)) {

                formData.append('image', file);

                reader.readAsDataURL(file);
                reader.onload = function () {

                    var div = document.createElement("div");
                    $(div).addClass('img-preview');
                    var img = document.createElement("img");
                    $(img).prop('src', reader.result);
                    $(img).addClass('img-responsive');
                    $(div).append(img)
                    $('.img-preview-container').append(div);

                    currentFileIndex++;
                    console.log(currentFileIndex, totalFileNumber);

                    sendImages();
                };

            } else {

            }
        };
    });
</script>

@endsection