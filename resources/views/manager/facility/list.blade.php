@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.facility.list-title') }}</h3>

<div class="manager-content">
    <div clas="row">
        <div class="col-md-8">
            <table class="table table-list">
                <thead>
                    <tr>
                        <td width="25"></td>
                        <td width="56"></td>
                        <td>Name TH</td>
                        <td>Name EN</td>
                        <td class="tiny-col"></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($facilities as $facility) {
                        ?>
                        <tr class="facility-items" data-id="{{ $facility->id }}">
                            <td width="25" class="text-center sorting-handler" style="vertical-align: middle;">
                                <i class="fa fa-arrows-v text-muted"></a>
                            </td>
                            <td class="text-center"><img src="{{ $facility->getThumbUrl('48x48', true) }}" class="img-responsive"/></td>
                            <td contenteditable data-id="{{ $facility->id }}" data-domain="facility" data-field="name_th" style="vertical-align: middle;">{{ $facility->name_th }}</td>
                            <td contenteditable data-id="{{ $facility->id }}" data-domain="facility" data-field="name_en" style="vertical-align: middle;">{{ $facility->name_en }}</td>
                            <td style="vertical-align: middle;" class="text-center"><a href="/manager/facility/edit/{{ $facility->id }}">Edit</a></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-4">
            <h3>Facility manager</h3>
            <form method="post" action="/manager/facility/create">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul class="list-unstyled">
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="form-group">
                    <label for="name_th">Name TH</label>
                    <input type="text" class="form-control" name="name_th" value="{{ old('name_th') }}">
                </div>
                <div class="form-group">
                    <label for="name_en">Name EN</label>
                    <input type="text" class="form-control" name="name_en" value="{{ old('name_en') }}">
                </div>
                {{ csrf_field() }}
                <button type="submit" class="btn btn-primary pull-right">Create</button>
            </form>
        </div>
    </div>

</div>

@endsection

@section('scripts')
<script src="/js/content.js"></script>
<script src="/js/jquery-ui/jquery-ui.min.js"></script>
<script>
$(document).ready(function () {
    $("tbody").sortable({
        stop: function (event, ui) {
            var data = [];

            $.each($(".facility-items"), function (index, value) {
                data.push($(value).data('id'));

            });
            console.log(data);

            $.post('/manager/facility/order', {ranks: data}, function (data) {

            });
        }
    });
    $("tbody").disableSelection();
});
</script>
@endsection