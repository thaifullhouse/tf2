@extends('layouts.manager')

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.order.list-title') }}</h3>

<div class="manager-owners">

    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ trans('manager.order.id-col') }}</th>
                <th>{{ trans('manager.order.user-col') }}</th>
                <th>{{ trans('manager.order.package-col') }}</th>
                <th class="text-center">{{ trans('manager.order.listing-unit-col') }}</th>
                <th class="text-right">{{ trans('manager.order.price-login-col') }}</th>
                <th class="text-center">{{ trans('manager.order.status-col') }}</th>
                <th class="text-center">{{ trans('manager.order.payment-col') }}</th>
                <th class="icon-col text-center">{{ trans('manager.order.edit-col') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($purchases as $purchase) {
                $user = $purchase->getUser();
                $paysbuy = $purchase->getPaybuyPayment();
                ?>
                <tr>
                    <td>{{ $purchase->invoice_id }}</td><!-- invoice_id -->
                    <td>{{ $user->getProfile()->getPublicID() }}</td>
                    <td>{{ $purchase->package_name }}</td>
                    <td class="text-center">{{ $purchase->listing_count }}</td>
                    <td class="text-right">{{ number_format($purchase->getAmount(), 2) }}</td>
                    <td class="text-center">{{ $purchase->paid ? 'Paid' : 'Unpaid' }}</td>
                    <td class="text-center">
                        <?php
                        if ($purchase->payment_method == 'paysbuy') {
                            if ($paysbuy != null) {
                                echo $paysbuy->getPaymentMethod();
                            }
                            else {
                                echo ucfirst($purchase->payment_method);
                            }                            
                        } else if ($purchase->payment_method == 'bank'){           
                            
                            if ($purchase->bank) {
                                echo \App\Models\Bank::getBankName($purchase->bank);
                            }
                            else {
                                echo ucfirst($purchase->payment_method);
                            }
                        }
                        ?>
                    </td>
                    <td class="text-center">
                        <a href="/manager/order/details/{{ $purchase->id }}"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>

    {{ $purchases->links() }}

</div>
@endsection
