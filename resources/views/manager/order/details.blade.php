@extends('layouts.manager')

<?php
$active = 'order';

$user = $purchase->getUser();
?>

@section('title', 'Page Title')

@section('content')

<h3>{{ trans('manager.order.list-title') }} : #{{ $user->getProfile()->getPublicID() }}</h3>

<table class="table table-list">
    <thead>
        <tr>
            <th>{{ trans('manager.order.id-col') }}</th>
            <th>{{ trans('manager.order.package-col') }}</th>
            <th class="text-center">{{ trans('manager.order.listing-unit-col') }}</th>
            <th class="text-right">{{ trans('manager.order.price-login-col') }}</th>
            <th class="text-center">{{ trans('manager.order.status-col') }}</th>
            <th class="text-center">{{ trans('manager.order.payment-col') }}</th>
            <th class="icon-col text-center">{{ trans('manager.order.edit-col') }}</th>
        </tr>
    </thead>
    <tbody>

        <tr>
            <td>{{ $purchase->invoice_id }}</td><!-- invoice_id -->
            <td>{{ $purchase->package_name }}</td>
            <td class="text-center">{{ $purchase->listing_count }}</td>
            <td class="text-right">{{ number_format($purchase->getAmount(), 2) }}</td>
            <td class="text-center">{{ $purchase->paid ? 'Paid' : 'Unpaid' }}</td>
            <td class="text-center">{{ ucfirst($purchase->payment_method) }}</td>
            <td class="text-center">

            </td>
        </tr>
    </tbody>
</table>
<div class="row order-customer-setting">
    <div class="col-md-6">
        <h5 class="text-center"><i class="fa fa-circle"></i> {{ trans('manager.order.card-info-title') }}</h5>
        <?php
        if ($status == 'account_updated') {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ trans('manager.order.account-updated') }}</p>
            </div>
            <?php
        }
        ?>
        <div class="customer-account">
            <form class="form-horizontal" action="/manager/order/account/{{ $purchase->id }}" method="post">
                <!--<div class="form-group">
                    <label for="country" class="col-sm-4 control-label">{{ trans('manager.order.setting-country') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="country" name="country" value="{{ $account->country }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-4 control-label">{{ trans('manager.order.setting-address') }}</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" id="address" name="address" rows="5">{{ $account->address }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-4 control-label">{{ trans('manager.order.setting-city') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="city" name="city" value="{{ $account->city }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="city" class="col-sm-4 control-label">{{ trans('manager.order.setting-state-zip') }}</label>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="state" name="state" value="{{ $account->state }}">
                    </div>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="zipcode" name="zipcode" value="{{ $account->zipcode }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="firstname" class="col-sm-4 control-label">{{ trans('manager.order.setting-firstname') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="firstname" name="firstname" value="{{ $account->firstname }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="lastname" class="col-sm-4 control-label">{{ trans('manager.order.setting-lastname') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="lastname" name="lastname" value="{{ $account->lastname }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cardnum" class="col-sm-4 control-label">{{ trans('manager.order.setting-cardnum') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="cardnum" name="cardnum" value="{{ $account->cardnum }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="expiry" class="col-sm-4 control-label">{{ trans('manager.order.setting-expiry') }}</label>
                    <div class="col-sm-4">
                        <select class="form-control" id="expire_month" name="expire_month">
                            <?php
                            for ($m = 1; $m <= 12; $m++) {
                                $month = sprintf("%02d", $m);
                                ?>
                                <option value="{{ $month }}" <?php echo $month == $account->expire_month ? 'selected' : ''; ?> >{{ $month }}</option>
                                <?php
                            }
                            ?>
                        </select>

                    </div>
                    <div class="col-sm-4">
                        <select class="form-control" id="expire_year" name="expire_year">
                            <?php
                            $y0 = date('Y');
                            $yn = $y0 + 15;
                            for ($y = $y0; $y <= $yn; $y++) {
                                $year = sprintf("%04d", $y);
                                ?>
                                <option value="{{ $year }}" <?php echo $year == $account->expire_year ? 'selected' : ''; ?> >{{ $year }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="securitynum" class="col-sm-4 control-label">{{ trans('manager.order.setting-securitynum') }}</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="securitynum" name="securitynum" value="{{ $account->securitynum }}">
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-dark">{{ trans('manager.order.setting-edit') }}</button>
                    </div>
                </div>
-->

            </form>
        </div>
    </div> 
    <div class="col-md-6">
        <h5 class="text-center"><i class="fa fa-circle"></i> {{ trans('manager.order.bank-info-title') }}</h5>
        <?php
        if ($status == 'order_updated') {
            ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <p>{{ trans('manager.order.order-updated') }}</p>
            </div>
            <?php
        }
        ?>
        <div class="customer-account">
            <form class="form-horizontal" action="/manager/order/update/{{ $purchase->id }}" method="post">
                <div class="form-group">
                    <label for="bank" class="col-sm-4 control-label">{{ trans('manager.order.setting-bank') }}</label>
                    <div class="col-sm-8">
                        <select class="form-control" id="bank" name="bank">
                            <?php
                            foreach ($banks as $bank) {
                                ?>
                                <option value="{{ $bank->bank }}" <?php echo $purchase->bank == $bank->bank ? 'selected' : ''; ?> >{{ $bank->name }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="last_amount" class="col-sm-4 control-label">{{ trans('manager.order.setting-last_amount') }}</label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="last_amount" name="last_amount" value="{{ ($purchase->updated_amount / 100) }}">
                    </div>
                </div>
                {{ csrf_field() }}
                <div class="form-group">
                    <div class="col-sm-offset-4 col-sm-8">
                        <button type="submit" class="btn btn-dark">{{ trans('manager.order.setting-edit') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<h4>{{ trans('manager.order.history-title') }}</h4>
<?php
if (count($purchases)) {
    ?>
    <table class="table table-list">
        <thead>
            <tr>
                <th>{{ trans('manager.order.id-col') }}</th>
                <th>{{ trans('manager.order.package-col') }}</th>
                <th class="text-center">{{ trans('manager.order.listing-unit-col') }}</th>
                <th class="text-right">{{ trans('manager.order.price-login-col') }}</th>
                <th class="text-center">{{ trans('manager.order.status-col') }}</th>
                <th class="text-center">{{ trans('manager.order.payment-col') }}</th>
                <th class="icon-col text-center">{{ trans('manager.order.edit-col') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($purchases as $purchase) {
                $user = $purchase->getUser();
                ?>
                <tr>
                    <td>{{ $purchase->invoice_id }}</td>
                    <td>{{ $purchase->package_name }}</td>
                    <td class="text-center">{{ $purchase->listing_count }}</td>
                    <td class="text-right">{{ number_format($purchase->getAmount(), 2) }}</td>
                    <td class="text-center">{{ $purchase->paid ? 'Paid' : 'Unpaid' }}</td>
                    <td class="text-center">{{ ucfirst($purchase->payment_method) }}</td>
                    <td class="text-center">
                        <a href="/manager/order/details/{{ $purchase->id }}"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    {{ $purchases->links() }}
    <?php
}
?>


@endsection