<div class="list-group manager-sidebar">
    
    <a href="/admin/dashboard" class="list-group-item <?php echo $menu=='dashboard' ? 'active' : ''; ?>">Admin Dashboard</a>
    
    <h5>User Management</h5>
    <a href="/admin/user/list" class="list-group-item <?php echo $menu=='users' ? 'active' : ''; ?>">Users</a>
</div>