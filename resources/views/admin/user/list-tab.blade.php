<?php
$types = App\Models\User\Type::all();

$tab = session('filter') ? session('filter') : 'visitor';
?>
<ul class="nav nav-tabs">
<?php
foreach ($types as $type) {
    $count = $type->countUser();
    ?>
        <li role="presentation" <?php echo $tab == $type->code ? 'class="active"' : ''; ?> >
            <a href="/admin/user/list?filter={{ $type->code }}">{{ $type->name }} ({{ $count }})</a>
        </li>
    <?php
}
?>
</ul>