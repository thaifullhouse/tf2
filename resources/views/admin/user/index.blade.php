@extends('layouts.admin')

<?php
$menu = 'users';
?>

@section('content')

<h3>List of users</h3>

@include('admin.user.list-tab')

<table class="table table-list">
    <thead>
        <tr>
            <th class="tiny-col">ID</th>
            <th>Email</th>
            <th class="small-col text-center">Active</th>
            <th class="small-col text-center">Last login</th>
            <th class="icon-col"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($users as $user) {
            ?>
            <tr>
                <td class="text-center">{{ $user->id }}</td>
                <td>{{ $user->email }}</td>
                <td class="text-center">
                    <input type="checkbox" name="active" value="YES" <?php echo $user->active ? 'checked' : ''; ?> >
                </td>
                <td class="text-center">{{ date('d/m/Y', strtotime($user->last_login)) }}</td>
                <td class="text-center">
                    <button type="button" class="btn btn-xs btn-white" data-toggle="modal" data-target="#user-setting-modal" data-id="{{ $user->id }}">
                        <i class="fa fa-cog"></i>
                    </button>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

{{ $users->links() }}

<div id="user-setting-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-medium" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>

    $(function () {
        $('#user-setting-modal').on('show.bs.modal', function (event) {
            //console.log(event.relatedTarget);
            $('.modal-content').load('/admin/user/setting/' + $(event.relatedTarget).data('id'));
        });
    });
    
    var saveUserSettting = function() {
        var form = document.getElementById('user-setting-form');
        var data = new FormData(form);
        
        $.ajax({
                url: '/admin/user/update',
                type: "POST",
                data: data,
                processData: false, // tell jQuery not to process the data
                contentType: false   // tell jQuery not to set contentType
            }).done(function () {
                window.location.reload();
            });
    };
    
</script>
@endsection