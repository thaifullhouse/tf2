<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">User setting</h4>
</div>
<div class="modal-body">
    <form id="user-setting-form">
        <div class="form-group">
            <label for="email">Email </label>
            <input type="email" class="form-control" name="email" value="{{ $user->email }}" readonly>
        </div>
        <div class="form-group">
            <label for="exampleInputFile">Member type</label>
            <select name="type_id" class="form-control">
                <?php
                $types = App\Models\User\Type::all();
                foreach ($types as $type) {
                    ?>
                    <option value="{{ $type->id }}" <?php echo $type->id == $user->type_id ? 'selected' : ''; ?> >{{ $type->name }}</option>
                    <?php
                }
                ?>`
            </select>
        </div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="active" value="YES" <?php echo $user->active ? 'checked' : ''; ?> > Active
            </label>
        </div>
        <input type="hidden" name="id" value="{{ $user->id }}"/>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary" onclick="saveUserSettting();">Save changes</button>
</div>