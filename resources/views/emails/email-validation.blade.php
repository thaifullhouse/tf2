<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head></head>
    <body>
        <div style="max-width: 700px;">
            <div class="header" style="border-top:35px solid #16ADC5;font-family:'Source Sans Pro',sans-serif;overflow:hidden;padding:10px 25px 0 0">
                <img src="http://tfh.ladargroup.com/img/logo-small.png" style="float:left">
                <hr style="border-color:#16ADC5;border-style:solid;margin-top:56px">
            </div>
            <div class="container" style="font-family:'Source Sans Pro',sans-serif;padding:25px">
                <h1 style="margin-bottom:15px">Hi !</h1>
                <p style="color:#7e7e7e;float:left;font-family:'Source Sans Pro',sans-serif;width:100%">
                    We're so excited you registered with Thaifullhouse.<br/>
                    To verify your email address, please click the [Verify your email address] button bellow.
                </p>
                <p class="main-action" style="color:#7e7e7e;float:left;font-family:'Source Sans Pro',sans-serif;margin:35px 0;width:100%">
                    <a href="{{ url('/auth/email-verify?token='.$user->email_validation_token.'&id='.$user->id) }}" style="background-color:#62c9d9;color:#000;font-weight:700;padding:10px 25px;text-decoration:none">Verify your email address</a></p>
                <p style="color:#7e7e7e;float:left;font-family:'Source Sans Pro',sans-serif;width:100%">
                    If you did'nt attempt to verify your email address with Thaifullhouse, please delete this email
                </p>
                <p class="signature" style="color:#7e7e7e;float:left;font-family:'Source Sans Pro',sans-serif;margin-bottom:20px;margin-top:50px;width:100%">- Thai Full House Team</p>
            </div>
            <div class="footer" style="background-color:#f1f1f1;float:left;font-family:'Source Sans Pro',sans-serif;font-size:13px;overflow:hidden;width:100%">
                <table style="margin-top:25px;width:100%">
                    <tbody>
                        <tr>
                            <td width="40%" style="font-family:'Source Sans Pro',sans-serif">
                                <img src="http://tfh.ladargroup.com/img/logo-small-gc.png" style="float:left;">
                            </td>
                            <td class="address" style="font-family:'Source Sans Pro',sans-serif">
                                <h3 style="color:#6d6d6d;font-size:14px">Wonderpons Co.,Ltd</h3>
                                <p style="color:#7e7e7e;font-family:'Source Sans Pro',sans-serif;font-size:13px">1126/1 Unit 1401 Vanit Building 1, New Petchaburi Road,<br>Patchathevi, Bangkok 10400 Thailand<br><br>Tel. 02-655-3180, 083-244-0827 Fax 02-655-6206<br>E-mail: info@wonderpons.com Wechat: vilivseoul</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="copyright" style="color:#7e7e7e;font-family:'Source Sans Pro',sans-serif;margin:30px 0;text-align:center">Copyright © 2016 Thai Full House</p>
            </div>
        </div>
    </body>
</html>
