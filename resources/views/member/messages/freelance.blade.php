@extends('layouts.member')

@section('content')

<h4>{{ trans('member.messages.title') }}</h4>

<div class="clearfix"></div>
<!--<div class="row listing-status-filter">
    <div class="col-md-2 status-cell">
        {{ trans('member.messages.status') }}
    </div>
    <div class="col-md-2">
        <input type="radio"> {{ trans('member.messages.inbox') }}
    </div>
    <div class="col-md-2">
        <input type="radio"> {{ trans('member.messages.deleted') }}
    </div>
</div>-->

<div class="row listing-option-filter">
    <div class="col-md-2 status-cell">
        {{ trans('member.messages.option') }}
    </div>
    <div class="col-md-2">
        <input type="radio"> {{ trans('member.messages.all') }}
    </div>
    <div class="col-md-2">
        <input type="radio"> {{ trans('member.messages.read') }}
    </div>
    <div class="col-md-2">
        <input type="radio"> {{ trans('member.messages.unread') }}
    </div>
    <div class="col-md-4">
        <form>
            <div class="form-group">
                <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Search ...">
            </div>
        </form>
    </div>
</div>

<div class="listing-table-container">
    <table class="table listing-table">
        <thead>
            <tr>
                <th style="width: 25px;"></th>
                <th style="width: 75px;">{{ trans('member.messages.col-date') }}</th>
                <th>{{ trans('member.messages.col-topic') }}</th>							
                <th style="width: 200px;">{{ trans('member.messages.col-name') }}</th>
                <th style="width: 200px;">{{ __t('member_account.email-col', 'Email') }}</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($messages as $message) {
                $time = strtotime($message->created_at);
                $date = date('d/M/Y', $time);      
                ?>
                <tr class="even">
                    <td style="width: 25px;"><input type="checkbox"/></td>
                    <td style="width: 75px;">{{ $date }}</td>
                    <td>{{ $message->message_body }}</td>
                    <td style="width: 200px;">{{ $message->name }}</td>
                    <td>{{ $message->email }}</td>
                </tr>
                <?php
            }
            ?>
        </tbody>
    </table>
    
    {{ $messages->links() }}
</div>

@endsection
