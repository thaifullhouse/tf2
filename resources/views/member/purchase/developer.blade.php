@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<h3>{{ trans('member.purchase.title') }}</h3>

<div class="box">
    
    <a href="/member/package/buy-exclusive" class="btn btn-blue pull-right right-action">{{ trans('member.purchase.buy-new') }}</a>
    <div class="clearfix"></div>

    <div class="listing-table-container">
        <table class="table table-list">
            <thead>
                <tr>
                    <th class="small-col">{{ trans('member.purchase.col-id') }}</th>
                    <th>{{ trans('member.purchase.col-type') }}</th>
                    <th>{{ trans('member.purchase.col-package') }}</th>
                    <th>{{ trans('member.purchase.col-period') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($purchases as $key => $purchase) {
                    $package = $purchase->getPackage();
                    ?>
                    <tr>
                        <td>{{ $purchase->invoice_id }}</td>
                        <td>{{ $package->type }}</td>
                        <td>{{ $purchase->package_name }}</td>
                        <td>{{ $package->period }}</td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
        {{ $purchases->links() }}
    </div>
</div>


@endsection