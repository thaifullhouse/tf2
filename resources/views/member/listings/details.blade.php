@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row posting-container">
        
        <div class="col-md-12">
            <h2 class="top-title">Advertise with <em>Thai full house</em></h2>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="listing-crumb">
                <span class="active">{{ __t('member_posting.package.free-listing', 'Free listing') }}</span>
                <span><a href="/member/package/buy-exclusive">{{ __t('member_posting.package.upgrade', 'Upgrade') }}</a></span>
            </div>

            <div class="bs-wizard-container">
                <div class="row bs-wizard">

                    <div class="col-md-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-1', 'Step 1') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-select-property', 'Select property') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step active"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-2', 'Step 2') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-post-details', 'Post details') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-3', 'Step 3') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-preview', 'Preview') }}</div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h2 class="top-title">Advertise with <em>Thai full house</em></h2>
        </div>
        <div class="col-md-8 col-md-offset-2">
            

            @include('member.listings.property-media')

            <div class="property-details">
                <h3>Details</h3>
                <form method="post" action="/listing/details/{{ $property->id }}" id="property-details-form">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title (ภาษาไทย)</label>
                                <input type="text" class="form-control" id="title_th" name="title_th" value="{{ $details->title_th }}" placeholder="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="title">Title (English)</label>
                                <input type="text" class="form-control" id="title_en" name="title_en" value="{{ $details->title_en }}" placeholder="">
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered">
                        <tr>
                            <td width="20%">Deposite *</td>
                            <td width="30%">
                                <div class="form-group">
                                    <input type="text" name="deposite" value="{{ old('deposite', $details->deposite) }}" class="form-control" placeholder="">
                                </div>
                            </td>


                            <td width="20%">Bedrooms</td>
                            <td width="30%">
                                <div class="form-group">
                                    <select name="bedrooms" class="form-control">
                                        <option value="0">Number of bedrooms</option>
                                        <?php
                                        for ($bedrooms = 1; $bedrooms < 10; $bedrooms++) {
                                            ?>
                                            <option value="{{ $bedrooms }}" <?php echo $details->bedroom_num == $bedrooms ? 'selected' : ''; ?> >{{ $bedrooms }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">Rent *</td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="listing_price" value="{{ old('listing_price', $details->listing_price) }}" class="form-control">
                                </div>
                            </td>
                            <td>Bathrooms</td>
                            <td>
                                <div class="form-group">
                                    <select name="bathrooms" class="form-control">
                                        <option value="0">Number of bathrooms</option>
                                        <?php
                                        for ($bathrooms = 1; $bathrooms < 10; $bathrooms++) {
                                            ?>
                                            <option value="{{ $bathrooms }}" <?php echo $details->bathroom_num == $bathrooms ? 'selected' : ''; ?> >{{ $bathrooms }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Floors</td>
                            <td>
                                <div class="form-group">
                                    <select name="floors" class="form-control">
                                        <option value="0">Number of floors</option>
                                        <?php
                                        for ($floors = 1; $floors < 100; $floors++) {
                                            ?>
                                            <option value="{{ $floors }}" <?php echo $details->floor_num == $floors ? 'selected' : ''; ?> >{{ $floors }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                            <td>Size / sqm</td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="size" value="{{ old('size', $details->total_size) }}" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Year built</td>
                            <td>
                                <div class="form-group">
                                    <select name="year" class="form-control">
                                        <option value="0">Building year construction</option>
                                        <?php
                                        $start = date('Y');
                                        $end = $start - 30;

                                        for ($year = $start; $year > $end; $year--) {
                                            ?>
                                            <option value="{{ $year }}" <?php echo $details->year_built == $year ? 'selected' : ''; ?> >{{ $year }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                            <td>Tower</td>
                            <td>
                                <div class="form-group">
                                    <select name="tower" class="form-control">
                                        <option value="0">Number of towers</option>
                                        <?php
                                        for ($tower = 1; $tower < 10; $tower++) {
                                            ?>
                                            <option value="{{ $tower }}" <?php echo $details->tower_num == $tower ? 'selected' : ''; ?> >{{ $tower }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Key features</td>
                            <td colspan="3">
                                <div class="row facility-list">
                                    <?php
                                    if (count($features)) {
                                        foreach ($features as $feature) {

                                            if (in_array($feature->id, $selected_features)) {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item features active" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                                             class="img-responsive"
                                                             title="{{ $feature->getTranslatedField('name') }}" />
                                                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="feature_{{ $feature->id }}"
                                                               class="hidden" name="features[]" value="{{ $feature->id }}" checked=""/>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item features" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                                                             class="img-responsive"
                                                             title="{{ $feature->getTranslatedField('name') }}" />
                                                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="feature_{{ $feature->id }}"
                                                               class="hidden" name="features[]" value="{{ $feature->id }}"/>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Facilities *</td>
                            <td colspan="3">
                                <div class="row facility-list">
                                    <?php
                                    if (count($facilities)) {
                                        foreach ($facilities as $facility) {

                                            if (in_array($facility->id, $selected_facilities)) {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item facilities active" data-ref="#facility_{{ $facility->id }}" data-id="{{ $facility->id }}">
                                                        <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                                             class="img-responsive"
                                                             title="{{ $facility->getTranslatedField('name') }}"/>
                                                        <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="facility_{{ $facility->id }}"
                                                               class="hidden" name="facilities[]" value="{{ $facility->id }}" checked/>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item facilities" data-ref="#facility_{{ $facility->id }}" data-id="{{ $facility->id }}">
                                                        <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                                                             class="img-responsive"
                                                             title="{{ $facility->getTranslatedField('name') }}"/>
                                                        <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="facility_{{ $facility->id }}"
                                                               class="hidden" name="facilities[]" value="{{ $facility->id }}"/>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Optional features</td>
                            <td colspan="3">
                                <div class="row facility-list">
                                    <?php
                                    if (count($optional_features)) {
                                        foreach ($optional_features as $feature) {

                                            if (in_array($feature->id, $selected_optional_features)) {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item options_features active" data-ref="#options_feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->getTranslatedField('name')) }}"
                                                             class="img-responsive"
                                                             title="{{ $feature->getTranslatedField('name') }}"/>
                                                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="options_feature_{{ $feature->id }}"
                                                               class="hidden" name="options_features[]" value="{{ $feature->id }}" checked/>
                                                    </div>
                                                </div>
                                                <?php
                                            } else {
                                                ?>
                                                <div class="col-md-2">
                                                    <div class=" text-center facility-item options_features" data-ref="#options_feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                                                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->getTranslatedField('name')) }}"
                                                             class="img-responsive"
                                                             title="{{ $feature->getTranslatedField('name') }}"/>
                                                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                                                        <input type="checkbox" id="options_feature_{{ $feature->id }}"
                                                               class="hidden" name="options_features[]" value="{{ $feature->id }}"/>
                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>

                        <tr>
                            <td>Availability date</td>
                            <td>
                                <input type="text" id="availability_date" name="availability_date" value="" class="form-control single-date">
                            </td>
                            <td>Min rental period</td>
                            <td>
                                <select name="minimal_period" class="form-control">
                                    <option value="0">Select a period</option>
                                    <?php
                                    for ($period = 1; $period <= 12; $period++) {
                                        ?>
                                        <option value="{{ $period }}" <?php echo $details->minimal_rental_period == $period ? 'selected' : ''; ?> >{{ $period }} months</option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>

                        <tr class="row-data">
                            <td>Details</td>
                            <td colspan="3">
                                <label>ภาษาไทย</label>
                                <textarea class="form-control" rows="3" name="details_th" style="margin-bottom: 25px;">{{ $details->other_details_th }}</textarea>
                                <label>English</label>
                                <textarea class="form-control" rows="3" name="details_en">{{ $details->other_details_en }}</textarea>
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Address</td>
                            <td colspan="3">
                                <label>ภาษาไทย</label>
                                <textarea class="form-control" rows="3" name="address_th" style="margin-bottom: 25px;">{{ $details->address_th }}</textarea>
                                <label>English</label>
                                <textarea class="form-control" rows="3" name="address_en">{{ $details->address_en }}</textarea>
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Youtube link #1</td>
                            <td colspan="3">
                                <input type="text" id="ylink1" name="ylink1" value="{{ $details->ylink1 }}" class="form-control">
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Youtube link #2</td>
                            <td colspan="3">
                                <input type="text" id="ylink2" name="ylink2" value="{{ $details->ylink2 }}" class="form-control">
                            </td>
                        </tr>
                    </table>
                    <div class="upload-actions">
                        {{ csrf_field() }}
                        <button class="btn btn-success pull-right" type="button" id="submit_property_details">Submit details</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="property-preview" tabindex="-1" role="dialog" aria-labelledby="property-label">
    <div class="modal-dialog modal-preview" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection