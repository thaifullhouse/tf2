<?php
if (count($images)) {
    ?>
    <div class="row property-media">
        <div class="col-md-12">
            <h3>Current media</h3>
        </div>
        <div id="property-media-container">
            <?php
            foreach ($images as $image) {
                ?>
                <div class="col-md-2">
                    <div class="thumb-container">
                        <a class="fancybox" rel="group" href="{{ $image->getUrl() }}">
                            <img src="{{ $image->getThumbUrl('98x115', true) }}" class="img-responsive"/>
                        </a>
                        <span class="text-danger media-delete" onclick="deleteMedia({{ $image->id }}, {{ $property->id }})"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>

    </div>
    <?php
} else {
    if ($status == 'upload_image') {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger">
                    Please some image of your property before continuing
                </div>
            </div>
        </div>
        <?php
    }
}
?>
<form method="post">

    <h3>Add new images</h3>
    <div class="row img-preview-container">

    </div>
    <div class="row upload-progress-container">
        <div class="col-md-10">
            <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">

                </div>
            </div>
        </div>
        <div class="col-md-2 text-center">
            <span id="progress-label"></span>
        </div>
    </div>
    <div class="upload-actions">
        <div class="btn-group">
            <label title="Upload image file" for="prop_image" class="btn btn-primary">
                <input type="file" accept="image/*" name="image" id="prop_image" class="hide" multiple>
                Add an image
            </label>
        </div>
    </div>
</form>
