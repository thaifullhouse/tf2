@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>{{ trans('member.listing.title') }} ({{ $currentUser->countProperty() }})</h4>
    <a href="/member/posting/property-type" class="btn btn-blue pull-right right-action">{{ trans('member.listing.create-new') }}</a>
    <div class="clearfix"></div>
<!--    <div class="row listing-status-filter">
        <div class="col-md-2 status-cell">
            {{ trans('member.listing.status') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="status" value="present" onclick="applyFilter(this, 'status')" {{ $filter_status == 'present' ? 'checked' : '' }}> {{ trans('member.listing.status-present') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="status" value="draft" onclick="applyFilter(this, 'status')" {{ $filter_status == 'draft' ? 'checked' : '' }}> {{ trans('member.listing.status-draft') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="status" value="expired" onclick="applyFilter(this, 'status')" {{ $filter_status == 'expired' ? 'checked' : '' }}> {{ trans('member.listing.status-expired') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="status" value="exclusive" onclick="applyFilter(this, 'status')" {{ $filter_status == 'exclusive' ? 'checked' : '' }}> {{ trans('member.listing.status-exclusive') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="status" value="featured" onclick="applyFilter(this, 'status')" {{ $filter_status == 'featured' ? 'checked' : '' }}> {{ trans('member.listing.status-featured') }}
        </div>
    </div>-->

    <div class="row listing-option-filter">
        <div class="col-md-2 status-cell">
            {{ trans('member.listing.option') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="option" value="all" onclick="applyFilter(this, 'listing')" {{ $filter_listing == 'all' ? 'checked' : '' }}> {{ trans('member.listing.option-all') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="option" value="rent" onclick="applyFilter(this, 'listing')" {{ $filter_listing == 'rent' ? 'checked' : '' }}> {{ trans('member.listing.option-rent') }}
        </div>
        <div class="col-md-2">
            <input type="radio" name="option" value="sale" onclick="applyFilter(this, 'listing')" {{ $filter_listing == 'sale' ? 'checked'  : ''}}> {{ trans('member.listing.option-sale') }}
        </div>
    </div>


    <div class="listing-table-container">
        <table class="table listing-table">
            <thead>
                <tr>
                    <th style="width: 50px;">ID</th>
                    <th>{{ trans('member.listing.col-image') }}</th>
                    <th style="width: 50px;">{{ trans('member.listing.col-class') }}</th>
                    <th style="width: 50px;">{{ trans('member.listing.col-type') }}</th>
                    <th style="width: 150px;">{{ trans('member.listing.col-area') }}</th>
                    <th style="width: 50px;" class="text-right">{{ trans('member.listing.col-size') }}</th>
                    <th style="width: 75px;" class="text-right">{{ trans('member.listing.col-price') }}</th>
                    <th style="width: 200px;">{{ trans('member.listing.col-start') }}</th>
                    <th style="width: 50px;">{{ trans('member.listing.col-status') }}</th>
                    <th style="width: 50px;">{{ trans('member.listing.col-active') }}</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($properties as $key => $property) {
                    $details = $property->getDetails();
                    ?>
                    <tr class="odd">
                        <td>{{ $property->id }}</td>
                        <td>
                            <?php
                            $img = $property->getListingThumb();
                            if ($img != null) {
                                ?>
                                <img src="{{ $img->getThumbUrl('64x64', true) }}" class="img-responsive"/>
                                <?php
                            } else {
                                ?>
                                <img src="" class="img-responsive"/>
                                <?php
                            }
                            ?>
                        </td>
                        <td>{{ $property->getListingClass() }}</td>
                        <td>{{ $property->listing_type == 'rent' ? 'RENT' : 'SALE' }}</td>
                        <td>{{ $details->getArea() }}</td>
                        <td class="text-right">{{ $details->total_size }}</td>
                        <td class="text-right">{{ number_format($details->listing_price) }}</td>
                        <td>{{ $property->getPublicationPeriode() }}</td>
                        <td>{{ ucfirst($property->getStatus()) }}</td>
                        <td class="text-center"><input type="checkbox" {{ $property->published ? 'checked' : '' }} onclick="toggleStatus({{ $property->id }})"/></td>
                    </tr>
                    <tr class="even">
                        <td style="width: 50px;"></td>
                        <td></td>
                        <td colspan="8" class="text-right">
                            <span class="label label-default">{{ trans('member.listing.view-count') }}</span>
                            <a href="#" class="label label-warning">{{ trans('member.listing.upgrade-exclusive') }}</a>
                            <a href="#" class="label label-success">{{ trans('member.listing.update-featured') }}</a>
                            <a href="/member/listing/details?id={{ $property->id }}" class="label label-primary">{{ trans('member.listing.edit') }}</a>
                        </td>
                    </tr>
                    <?php
                }
                ?>

            </tbody>
        </table>
        {{ $properties->links() }}
    </div>
</div>


@endsection