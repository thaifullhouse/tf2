@extends('layouts.default')

@section('title', 'Page Title')

@section('breadcrumb')
<li><a rhef="/">{{ trans('nav.breadcrumb.home') }}</a></li>
<li class="active">{{ trans('nav.breadcrumb.buy-package') }}</li>

@endsection

@section('content')
<div class="container">
    <form id="payment-form" method="post" action="/member/package/buy-exclusive">
        <div class="buy-package-box package-box medium-container">
            <h1>{{ trans('member.package.buy-title') }}</h1>

            <div class="small-crumb">
                <span><a href="/posting/property-type">{{ trans('member.package.free-listing') }}</a></span>
                <span class="active">{{ trans('member.package.upgrade') }}</span>
            </div>

            <div class="ad-container">
                <div class="package-selector">
                    <span><a href="/member/package/buy-featured">{{ trans('member.package.featured') }}</a></span>
                    <span class="active">{{ trans('member.package.exclusive') }}</span>                
                </div>
                <div class="poster">
                    <img src="/img/exclu-pub.png" class="img-responsive"/>
                </div>
            </div>

            <div class="package-stepper">
                <div class="header">
                    <span class="step">{{ trans('member.package.step') }}</span>
                </div>            

                <div class="step-container">
                    <div class="step step-1">
                        <span><img src="/img/online_payment.jpg" class="img-responsive"/></span>
                    </div>
                    <div class="step step-2">
                        <span><img src="/img/screen-with-a-presentation.png" class="img-responsive"/></span>
                    </div>
                </div>
                <div class="footer">
                    <span class="arrow"><i class="fa fa-angle-right"></i></span>
                </div>    

            </div>

            <div class="package-list">
                <?php
                foreach ($packages as $package) {
                    $percent = 100 - (int) (100 * $package->discounted_price / $package->normal_price);
                    $current_price = 0;
                    ?>
                    <div class="package-container exclusive">
                        <div class="listing-container">
                            {{ $package->listing_count }} {{ trans('member.package.listing') }}
                            <?php
                            if ($package->free_listing_count > 0) {
                                ?>
                                <span class="free-listing-count">
                                    + Free {{ $package->free_listing_count }} listing
                                </span>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="price-container">
                            <?php
                            if ($package->discounted_price > 0) {
                                $current_price = $package->discounted_price;
                                ?>
                                <span class="old-price">{{ number_format($package->normal_price) }}</span>
                                <span class="actual-price">{{ number_format($package->discounted_price) }}</span>
                                <span class="percent-box">{{ $percent }}&percnt;<br/><i class="fa fa-arrow-down" aria-hidden="true"></i></span>
                                <?php
                            } else {
                                $current_price = $package->normal_price;
                                ?>
                                <span class="actual-price">{{ number_format($package->normal_price) }}</span>

                                <?php
                            }
                            ?>
                            <p class="currency">
                                BATH
                            </p>
                            <p class="per-month">
                                {{ trans('member.package.per-month') }}
                            </p>
                            <p class="select-package">
                                <label for="package-{{ $package->id }}" data-ref="ico-{{ $package->id }}" class="package-checkbox" data-package="{{ $package->id }}">
                                    <i id="ico-{{ $package->id }}" class="ico-choice fa fa-circle" aria-hidden="true"></i>
                                </label>
                                <input type="radio" id="package-{{ $package->id }}" name="package" value="{{ $package->id }}" class="hidden"/>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>

            <div class="package-price-details">
                <table class="table" cellspacing="5">
                    <tr>
                        <td><div class="package-name"></div></td>
                        <td><div class="package-price"></div></td>
                    </tr>
                    <tr>
                        <td><div>+ VAT (7&percnt;)</div></td>
                        <td><div class="package-vat"></div></td>
                    </tr>
                    <tr>
                        <td><div>Total Amount</div></td>
                        <td><div class="total-amount"></div></td>
                    </tr>
                </table>
            </div>

            <div class="package-payment-option">
                <div class="radio">
                    <label>
                        <input type="radio" class="poption" name="poption" value="bank">
                        {{ trans('member.package.payment-bank-transfer') }}
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" class="poption" name="poption" value="card">
                        {{ trans('member.package.payment-bank-card') }}
                    </label>
                </div>
            </div>

            <div class="package-bank-accounts">
                <table class="table">
                    <?php
                    foreach ($banks as $bank) {
                        ?>
                        <tr>
                            <td>
                                <input type="radio" name="bank" value="{{ $bank->bank }}">
                            </td>
                            <td>
                                <div class="bank {{ $bank->bank }}">
                                    <strong>{{ $bank->getBankNameByCode() }}</strong><br/>
                                    Name: {{ $bank->name }}<br/>
                                    Bank Account No. : {{ $bank->branch }}<br/>
                                    Branch : {{ $bank->account_no }}
                                    <img src="/img/bank-{{ $bank->bank }}.png">
                                </div>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>

            </div>
<!--            <div class="package-paysbuy">               
                <button type="button" class="btn-paysbuy">
                    <img src="/img/paysbuylogo.png"/>
                </button>             

            </div>-->

            <p class="payment-warning">
                {{ trans('member.package.payment-limit-warning') }}
            </p>

            <p class="step-title">
                <span>
                    {{ trans('member.package.step-2-title') }}
                </span>
            </p>

            <div class="package-method-list">
                <p>
                    1. {{ trans('member.package.receipt-method-line-text') }}
                </p>
                <p>
                    <img src="/img/tfh-line.png">
                </p>
                <p>
                    2. {!! trans('member.package.receipt-method-email-text') !!}
                </p>
            </div>
            <div class="package-action text-right">
                <a class="btn btn-blue">{{ trans('member.package.payment-skip') }}</a>
                {{ csrf_field() }}
                <button id="submit-payment" type="button" class="btn btn-blue" >{{ trans('member.package.payment-next') }}</button>
            </div>

        </div>
    </form>
</div>

<div class="modal fade" id="payment-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="modal-body">
                <h4 class="text-center">
                    {{ trans('member.package.buy.success-message') }}
                </h4>
                <p class="text-center">{{ trans('member.package.buy.order-id') }} : <span id="order-id"></span></p>
                <p class="text-center">{{ trans('member.package.buy.confirm-message') }}</p>
                <p class="text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="payment-processing" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="modal-body">
                <h4 class="text-center">
                    {{ trans('member.package.buy.processing-title') }}
                </h4>
                <p class="text-center">{{ trans('member.package.buy.processing-message') }}</p>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

@endsection

@section('scripts')
<script>

    var packages = <?php echo json_encode($packages->all()); ?>;
    var packageId = 0;
    var paymentMode = '';

    var getPackage = function (pid) {

        var package = null;

        for (var i = 0; i < packages.length; i++) {
            if (packages[i].id === pid) {
                package = packages[i];
            }
        }

        return package;
    };

    var setPackage = function (pid) {
        var package = getPackage(pid);
        packageId = pid;

        var price = parseFloat(package.normal_price) + 0;
        var discount = parseFloat(package.discounted_price) + 0;

        price = discount > 0 ? discount : price;
        var vat = 0.07 * price;
        var total = price + vat + 0;

        $('.package-name').html(package.name);
        $('.package-price').html(numberWithCommas(price.toFixed(2)) + ' Baht');
        $('.package-vat').html(numberWithCommas(vat.toFixed(2)) + ' Baht');
        $('.total-amount').html(numberWithCommas(total.toFixed(2)) + ' Baht');
    };

    $(document).ready(function () {

        $('.poption').on('click', function () {

            console.log(this);

            if (this.value === 'bank') {
                paymentMode = 'bank';
                $('.package-bank-accounts').show();
                $('.package-paysbuy').hide();
            } else {
                paymentMode = 'card';
                $('.package-bank-accounts').hide();
                $('.package-paysbuy').show();
            }

        });

        $('.package-checkbox').on('click', function () {
            var ref = $(this).data('ref');
            var pid = $(this).data('package');
            setPackage(pid);
            $('.ico-choice').removeClass('fa-dot-circle-o').addClass('fa-circle');
            $('#' + ref).removeClass('fa-circle').addClass('fa-dot-circle-o');
        });

        $('#submit-payment').on('click', function () {

            if (packageId > 0 && paymentMode !== '') {

                if (paymentMode === 'bank') {
                    var form = document.getElementById('payment-form');
                    var formData = new FormData(form);

                    $.ajax({
                        url: '/member/package/buy',
                        data: formData,
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (data) {
                            console.log(data);
                            $('#order-id').html(data.orderId);
                            $('#payment-modal').modal();
                        }
                    });
                } else {
                    $('#payment-processing').modal();

                    $.get('/payment/token/' + packageId, function (data) {

                        var doc = $.parseXML(data);
                        var $xml = $(doc);
                        var code = $xml.text().substring(0, 2);

                        if (code === '00') {
                            var refid = $xml.text().substring(2);
                            window.location = 'http://demo.paysbuy.com/paynow.aspx?refid=' + refid;
                        } else {
                            alert('An error occured');
                        }
                        console.log($xml.text());
                    });
                }
            } else {
                alert('Please select a package and bank');
            }

        });

        $('#payment-modal').on('hidden.bs.modal', function () {
            window.location = '/member/purchase';
        });

        $('.btn-paysbuy').on('click', function () {

            if (packageId > 0 && paymentMode !== '') {




            } else {

            }



        });
    });

</script>
@endsection