@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>{{ trans('member.notification.title') }}</h4>
    <div class="clearfix"></div>

    <div class="listing-table-container">
        <table class="table listing-table">
            <thead>
                <tr>
                    <th>{{ trans('member.notification.col-type') }}</th>
                    <th>{{ trans('member.notification.col-date') }}</th>
                    <th>{{ trans('member.notification.col-news') }}</th>
                </tr>
            </thead>
            <tbody>
                

            </tbody>
        </table>
    </div>
</div>

@endsection