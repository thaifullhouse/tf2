<div class="row facility-list">
    <?php
    if (count($facilities)) {
        foreach ($facilities as $facility) {

            if (in_array($facility->id, $selected_facilities)) {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item facilities active" data-ref="#facility_{{ $facility->id }}" data-id="{{ $facility->id }}">
                        <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                             class="img-responsive"
                             title="{{ $facility->getTranslatedField('name') }}"/>
                        <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                        <input type="checkbox" id="facility_{{ $facility->id }}"
                               class="hidden" name="facilities[]" value="{{ $facility->id }}" checked/>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item facilities" data-ref="#facility_{{ $facility->id }}" data-id="{{ $facility->id }}">
                        <img src="{{ $facility->getThumbUrl('100x100', true, $facility->name_th) }}"
                             class="img-responsive"
                             title="{{ $facility->getTranslatedField('name') }}"/>
                        <span class="text-success">{{ $facility->getTranslatedField('name') }}</span>
                        <input type="checkbox" id="facility_{{ $facility->id }}"
                               class="hidden" name="facilities[]" value="{{ $facility->id }}"/>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
        }
    }
    ?>
</div>