<div class="row facility-list">
    <?php
    if (count($features)) {
        foreach ($features as $feature) {

            if (in_array($feature->id, $selected_features)) {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item features active" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                             class="img-responsive"
                             title="{{ $feature->getTranslatedField('name') }}" />
                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                        <input type="checkbox" id="feature_{{ $feature->id }}"
                               class="hidden" name="features[]" value="{{ $feature->id }}" checked=""/>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="col-md-2">
                    <div class=" text-center facility-item features" data-ref="#feature_{{ $feature->id }}" data-id="{{ $feature->id }}">
                        <img src="{{ $feature->getThumbUrl('100x100', true, $feature->name_th) }}"
                             class="img-responsive"
                             title="{{ $feature->getTranslatedField('name') }}" />
                        <span class="text-success">{{ $feature->getTranslatedField('name') }}</span>
                        <input type="checkbox" id="feature_{{ $feature->id }}"
                               class="hidden" name="features[]" value="{{ $feature->id }}"/>
                    </div>
                </div>
                <?php
            }
            ?>

            <?php
        }
    }
    ?>
</div>