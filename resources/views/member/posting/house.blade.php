@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row posting-container">

        <div class="col-md-12">
            <h2 class="top-title">
                <?php echo __t('posting.apartment-title', 'Advertise with <em>Thai full house</em><br/><small>For apartment</small>'); ?>
            </h2>
        </div>

        <div class="col-md-8 col-md-offset-2">            

            <div class="bs-wizard-container">
                <div class="row bs-wizard">

                    <div class="col-md-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-1', 'Step 1') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-select-property', 'Select property') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step active"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-2', 'Step 2') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-post-details', 'Post details') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-3', 'Step 3') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-preview', 'Preview') }}</div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">

            @include('member.listings.property-media')

            <div class="property-details">

                <h3>Details</h3>

                <form method="post" action="/member/listing/details/{{ $property->id }}" id="property-details-form">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <div class="row">
                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <?php echo __trfields('title', $details); ?>
                            </div>
                        </div>
                    </div>

                    <table class="table table-bordered">
                        <tr>
                            <td width="18%">Deposite *</td>
                            <td width="32%">
                                <div class="form-group">
                                    <input type="text" name="deposite" value="{{ old('deposite', $details->deposite) }}" class="form-control" placeholder="">
                                </div>
                            </td>
                            <td width="18%"><?php echo __t('property.size', 'Size'); ?></td>
                            <td width="32%">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" name="size" value="{{ old('size', $details->total_size) }}" class="form-control">
                                        <span class="input-group-addon" id="basic-addon2">sqm</span>
                                    </div>
                                </div>
                            </td>                            
                        </tr>
                        <tr>
                            <?php
                            if ($property->listing_type == 'rent') {
                                ?>
                                <td>Rent *</td>
                                <?php
                            } else {
                                ?>
                                <td>Sale price *</td>
                                <?php
                            }
                            ?>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="listing_price" value="{{ old('listing_price', $details->listing_price) }}" class="form-control">
                                </div>
                            </td>
                            <td>Bedrooms</td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="bedrooms" value="{{ old('size', $details->bedroom_num) }}" class="form-control">
                                </div>
                            </td>                            
                        </tr>
                        <tr>
                            <td><?php echo __t('property.psf', 'PSF'); ?></td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" name="psf" value="{{ old('size', $property->psf) }}" class="form-control">
                                        <span class="input-group-addon" id="basic-addon2">Baht/sqm</span>
                                    </div>
                                </div>
                            </td>
                            <td>Bathrooms</td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="bathrooms" value="{{ old('size', $details->bathroom_num) }}" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Floors</td>
                            <td>
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="number" name="floors" value="{{ old('size', $details->floor_num) }}" class="form-control">
                                        <span class="input-group-addon" id="basic-addon2">Floor</span>
                                    </div>
                                </div>
                            </td>
                            <td>Year built</td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="year" value="{{ old('size', $details->year_built) }}" class="form-control">
                                </div>
                            </td>
                        </tr>
                        <tr>                            
                            <td><?php echo __t('property.land_size', 'Land Size'); ?></td>
                            <td>
                                <div class="form-group">
                                    <input type="number" name="land_size" value="{{ old('size', $details->land_size) }}" class="form-control">
                                </div>
                            </td>
                            <td><?php echo __t('property.developer', 'Developer'); ?></td>
                            <td>
                                <div class="form-group">
                                    <select name="developer_id" class="form-control">
                                        <option value="0"><?php echo __t('property.select-developer', 'Select a developer'); ?></option>
                                        <?php
                                        foreach ($developers as $developer) {
                                            ?>
                                            <option value="{{ $developer->user_id }}" <?php echo $property->developer_id == $developer->user_id ? 'selected' : ''; ?> >{{ $developer->getName() }}</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Key features</td>
                            <td colspan="3">
                                @include('member.posting.features')
                            </td>
                        </tr>
                        <tr>
                            <td>Facilities *</td>
                            <td colspan="3">
                                @include('member.posting.facilities')
                            </td>
                        </tr>

                        <?php
                        if ($property->listing_type == 'rent') {
                            ?>
                            <tr>
                                <td>Optional features</td>
                                <td colspan="3">
                                    @include('member.posting.optional-features')
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr>
                            <td>
                                {{ __t('property.nearby-transportation', 'Nearby Transportation') }}
                            </td>
                            <td colspan="3">
                                @include('member.posting.nearby-transportation')
                            </td>
                        </tr>

                        <tr>
                            <td>Availability date</td>
                            <td colspan="2">
                                <input type="text" id="availability_date" name="availability_date" value="<?php echo __pgdate2hr($details->availability_date); ?>" class="form-control single-date">
                            </td>
                            <td></td>
                        </tr>

                        <?php
                        if ($property->listing_type == 'rent') {
                            ?>
                            <tr>
                                <td>Min rental period</td>
                                <td colspan="2">
                                    <select name="minimal_period" class="form-control">
                                        <option value="0">Select a period</option>
                                        <?php
                                        for ($period = 1; $period <= 12; $period++) {
                                            ?>
                                            <option value="{{ $period }}" <?php echo $details->minimal_rental_period == $period ? 'selected' : ''; ?> >{{ $period }} months</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr class="row-data">
                            <td>Property name</td>
                            <td colspan="3">
                                <?php echo __trfields('name', $property, false); ?>
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Details</td>
                            <td colspan="3">
                                <?php echo __trfields('other_details', $details, true); ?>
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Address</td>
                            <td colspan="3">
                                <?php echo __trfields('address', $details, true); ?>
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Youtube link #1</td>
                            <td colspan="3">
                                <input type="text" id="ylink1" name="ylink1" value="{{ $details->ylink1 }}" class="form-control">
                            </td>
                        </tr>
                        <tr class="row-data">
                            <td>Youtube link #2</td>
                            <td colspan="3">
                                <input type="text" id="ylink2" name="ylink2" value="{{ $details->ylink2 }}" class="form-control">
                            </td>
                        </tr>
                    </table>
                    <div class="upload-actions text-center">
                        <p>
                            <input type="checkbox" id="agree_tac" name="agree_tac" value="yes"/> 
                            {{ __t('member.posting-agree-term', 'Agree with the terms and conditions of ThaiFullHouse.com') }}
                        </p>
                        {{ csrf_field() }}
                        <input type="hidden" id="property_id" name="property_id" value="{{ $property->id }}"/>
                        <button class="btn btn-success" type="submit" id="submit_property_details" disabled>Submit details</button>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="property-preview" tabindex="-1" role="dialog" aria-labelledby="property-label">
    <div class="modal-dialog modal-preview" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

@endsection

@section('scripts')
<link href="/js/chosen/chosen.min.css" rel="stylesheet">
<script src="/js/chosen/chosen.jquery.min.js"></script>
<script src="/js/member/property-details.js"></script>
@endsection