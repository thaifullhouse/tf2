@extends('layouts.default')

@section('content')
<div class="container">
    <div class="row posting-container">
        <div class="col-md-12">
            <h2 class="top-title">Advertise with <em>Thai full house</em></h2>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="listing-crumb">
                <span class="active">{{ __t('member_posting.package.free-listing', 'Free listing') }}</span>
                <span><a href="/member/package/buy-exclusive">{{ __t('member_posting.package.upgrade', 'Upgrade') }}</a></span>
            </div>

            <div class="bs-wizard-container">
                <div class="row bs-wizard">

                    <div class="col-md-4 bs-wizard-step active">
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-1', 'Step 1') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-select-property', 'Select property') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-2', 'Step 2') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-post-details', 'Post details') }}</div>
                    </div>

                    <div class="col-md-4 bs-wizard-step"><!-- complete -->
                        <div class="text-center bs-wizard-stepnum">{{ __t('member_posting.step-3', 'Step 3') }}</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">{{ __t('member_posting.wizard-preview', 'Preview') }}</div>
                    </div>
                    
                </div>
            </div>

            

            <form class="form-horizontal highlighted" action="/member/listing/create" method="post">
                <div class="form-group">
                    <h5 for="" class="col-md-12 posting-label">{{ __t('member_posting.listing-type.label', 'Want to') }} <span class="required">*</span></h5>
                    <div class="col-md-4">
                        <div class="radio">
                            <label>
                                <input type="radio" name="listing_type" value="rent">
                                <span>{{ __t('member_posting.listing-type.rent', 'RENT') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="radio">
                            <label>
                                <input type="radio" name="listing_type" value="sale">
                                <span>{{ __t('member_posting.listing-type.sale', 'SALE') }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="listing_type_error">
                        {{ __t('member_posting.listing-type.error', 'Please select a listing type') }}
                    </div>
                </div>
                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.property-type.label', 'Property type') }} <span class="required">*</span></h5>
                    <div class="clearfix"></div>
                    <div class="property-types">
                        <button class="btn" type="button" data-type="apartment">
                            <img src="/img/property-types/apartment.png"/><br/>
                            {{ __t('member_posting.property-type.apartment', 'Apartment') }}
                        </button>
                        <button class="btn" type="button" data-type="condo">
                            <img src="/img/property-types/condo.png"/><br/>
                            {{ __t('member_posting.property-type.condominium', 'Condominium') }}
                        </button>
                        <button class="btn" type="button" data-type="detachedhouse">
                            <img src="/img/property-types/detachedhouse.png"/><br/>
                            {{ __t('member_posting.property-type.detached-house', 'Single House') }}
                        </button>
                        <button class="btn" type="button" data-type="townhouse">
                            <img src="/img/property-types/townhouse.png"/><br/>
                            {{ __t('member_posting.property-type.townhouse', 'Townhouse') }}
                        </button>
                        <input type="hidden" id="property_type" name="property_type" value=""/>
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="property_type_error">
                        {{ __t('member_posting.property-type.error', 'Please select a property type') }}
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.property-name.label', 'Property name') }}</h5>
                    <div class="col-sm-9 property-name">
                        <input type="text" class="form-control" id="property_name" name="property_name" value="" placeholder="">
                        <input type="hidden" id="condo_id" name="condo_id" value=""/>
                        <span class="text-danger required-condo-name">
                            <br/>
                            <i class="fa fa-warning"></i> {{ __t('member_posting.property-name.required', 'Property name is required for condominium') }}
                        </span>                        
                    </div>
                    <div class="col-sm-3">
                        <i class="fa fa-refresh fa-spin" style="margin-top: 9px; display: none;"></i>
                        <button type="button" style="display: none;" class="btn btn-danger clear-property-name"><i class="fa fa-times"></i></button>
                    </div>
                    <input type="hidden" class="form-control" id="other_property_name" name="other_property_name" value="">
<!--                    <div class="col-sm-9 other_property_name">
                        <input type="text" class="form-control" id="other_property_name" name="other_property_name" value="" placeholder="Type the name of your the condo"> 
                        <span class="text-disabled">If your condo is not listed in our directory, you can put the name yourself</span>
                    </div>-->
<!--                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="property_name_error">
                        Please input a property name
                    </div>-->
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.province.label', 'Province') }} <span class="required">*</span></h5>
                    <div class="col-sm-9">
                        <select class="form-control" id="province_id" name="province_id">
                            <option value="0">{{ __t('member_posting.select-province', 'Select a province') }}</option>
                            <?php
                            foreach ($provinces as $id => $province) {
                                $province = json_decode($province, true);
                                ?>
                                <option value="{{ $id }}">{{ __trget($province) }}</option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="province_error">
                        {{ __t('member_posting.province.error', 'Please select a province') }}
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.district.label', 'District') }} <span class="required">*</span></h5>
                    <div class="col-sm-9">
                        <select class="form-control" id="district_id" name="district_id" disabled>                        
                            <option value="0">{{ __t('member_posting.select-province', 'Select a province') }}</option>
                        </select>
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="district_error">
                        {{ __t('member_posting.district.error', 'Please select a district') }}
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.area.label', 'Area') }}</h5>
                    <div class="col-sm-9">
                        <select class="form-control" id="area_id" name="area_id" disabled>
                            <option value="0">{{ __t('member_posting.select-district', 'Please select a district') }}</option>
                        </select>
                    </div>
                    <div class="col-md-offset-3 col-md-9 text-danger posting_error" id="area_error">
                        {{ __t('member_posting.area.error', 'Please select a area') }}
                    </div>
                </div> 

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.street-number.label', 'Street Number') }}</h5>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="street_number" name="street_number" value=""  placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.street-name.label', 'Street Name') }}</h5>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="street_name" name="street_name" value=""  placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.postale-code.label', 'Postal code') }}</h5>
                    <div class="col-sm-4">
                        <input type="text" class="form-control" id="postale_code" name="postale_code" value="" placeholder="">
                    </div>
                </div>

                <div class="form-group">
                    <h5 class="col-md-12 posting-label">{{ __t('member_posting.map.label', 'Map') }}</h5>
                    <div class="col-md-12 text-center">
                        <button type="button" class="btn btn-default btn-cyan"><i class="fa fa-map-pin"></i> {{ __t('member_posting.mark-location.label', 'Mark Location On Map') }}</button>
                    </div>
                    <input type="hidden" id="lat" name="lat" value=""/>
                    <input type="hidden" id="lng" name="lng" value=""/>
                </div>

                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="map-picker" style="width: 100%; height: 400px;"></div>
                    </div>
                </div>

                <div class="col-md-12 text-danger posting_error" id="all_error">
                    {{ __t('member_posting.error.text', 'Some error occured, please check your data') }}
                </div>
                {{ csrf_field() }}
                
                <p class="text-center posting-nav">
                    <button class="btn btn-primary listing-next" id="next" type="button">{{ __t('member_posting.button.next', 'NEXT') }} <i class="fa fa-angle-double-right"></i></button>
                </p>                

            </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')

<link href="/css/bootstrap-nav-wizard.min.css" rel="stylesheet">

<script src="/js/lp/locationpicker.jquery.js"></script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?key={{ config('google.browser_key') }}'></script>

<script src="/js/province.js"></script>
<script src="/js/jquery.autocomplete.js"></script>
<script src="/js/member/posting.js"></script>

@endsection