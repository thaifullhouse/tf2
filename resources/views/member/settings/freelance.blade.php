@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>Setting profile</h4>
    <table class="table">
        <tr>
            <td>{{ trans('member.setting.personal-name') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="name" value="{{ $currentProfile->name }}" data-name="name" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button></td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.citizen_id') }} </td>
            <td class="setting-data">
                <input type="text" name="citizen_id" value="{{ $currentProfile->citizen_id }}" data-name="citizen_id" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.phone') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $currentProfile->phone }}" class="small setting-data-item" data-name="phone"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        
        <tr>
            <td>{{ trans('member.setting.address') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <textarea rows="5" name="address" data-name="address" class="setting-data-item">{{ $currentProfile->address }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.birthday') }} </td>
            <td class="setting-data">
                <input type="text" id="birthday" name="birthday" value="{{ $currentProfile->getHRBirthday() }}" class="birthday small" data-name="birthday"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.languages-ability') }} </td>
            <td class="setting-data">
                <input type="text" name="languages" value="{{ $currentProfile->languages }}" data-name="languages" class="setting-data-item"/></td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.gender') }} </td>
            <td class="setting-data">
                <input type="text" name="gender" value="{{ $currentProfile->gender }}" data-name="gender" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.nationality') }} </td>
            <td class="setting-data">
                <input type="text" name="nationality" value="{{ $currentProfile->nationality }}" data-name="nationality" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.introduce-yourself') }}</td>
            <td class="setting-data">
                <textarea rows="5" name="introduction" data-name="introduction" class="setting-data-item">{{ $currentProfile->introduction }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
    </table>
</div>

@endsection

@section('scripts')
<script src="/js/member/profile.js"></script>
@endsection