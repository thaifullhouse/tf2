@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>Setting profile</h4>
    <table class="table">
        <tr>
            <td>{{ trans('member.setting.developer-name') }}<span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="business_name" value="{{ $currentProfile->company_name }}" data-name="company_name" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button></td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.phone') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $currentProfile->phone }}" class="small setting-data-item" data-name="phone"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>        
        <tr>
            <td>{{ trans('member.setting.website') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $currentProfile->website }}" class="small setting-data-item" data-name="website"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.address') }} <span class="text-danger">*</span></td>
            <td class="setting-data">
                <textarea rows="5" name="address" data-name="address" class="setting-data-item">{{ $currentProfile->address }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.languages-ability') }} </td>
            <td class="setting-data">
                <input type="text" name="languages" value="{{ $currentProfile->languages }}" data-name="languages" class="setting-data-item"/></td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.pinperty-url') }} </td>
            <td class="setting-data">
                <input type="text" name="pinperty_url" value="{{ $currentProfile->pinperty_url }}" data-name="pinperty_url" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.twitter') }} </td>
            <td class="setting-data">
                <input type="text" name="twitter_id" value="{{ $currentProfile->twitter_id }}" data-name="twitter_id" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>{{ trans('member.setting.facebook_link') }} </td>
            <td class="setting-data">
                <input type="text" name="facebook_link" value="{{ $currentProfile->facebook_link }}" data-name="facebook_link" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Introduce yourself</td>
            <td class="setting-data">
                <textarea rows="5" name="introduction" data-name="introduction" class="setting-data-item">{{ $currentProfile->introduction }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
    </table>
</div>

@endsection

@section('scripts')
<script src="/js/member/profile.js"></script>
@endsection