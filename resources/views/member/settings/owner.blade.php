@extends('layouts.member')

@section('title', 'Page Title')

@section('breadcrumb')

<li class="active">{{ trans('nav.breadcrumb.home') }}</li>

@endsection

@section('content')

<div class="box">
    <h4>Setting profile</h4>
    <table class="table setting-table">
        <tr>
            <td>Name <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="contact_name" value="{{ $currentProfile->contact_name }}" data-name="contact_name" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Phone <span class="text-danger">*</span></td>
            <td class="setting-data">
                <input type="text" name="phone" value="{{ $currentProfile->phone }}" class="small setting-data-item" data-name="phone"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Passport or citizen ID number </td>
            <td class="setting-data">
                <input type="text" name="citizen_id" value="{{ $currentProfile->citizen_id }}" data-name="citizen_id" class="setting-data-item"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Address <span class="text-danger">*</span></td>
            <td class="setting-data">
                <textarea rows="5" name="address" data-name="address" class="setting-data-item">{{ $currentProfile->address }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Birthday </td>
            <td class="setting-data">
                <input type="text" id="birthday" name="birthday" value="{{ $currentProfile->getHRBirthday() }}" class="birthday small" data-name="birthday"/>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Language ability </td>
            <td class="setting-data">
                <?php
                
                $supported = explode(',', $currentProfile->languages);
                
                $languages = config('countries.languages');
                
                ?>
                <select id="languages" name="languages" class="setting-data-item languages" data-name="languages" multiple style="width: 100%">
                    <?php
                        foreach($languages as $code => $lang) {
                            ?>
                            <option value="{{ $code }}" <?php echo in_array($code, $supported) ? 'selected' : ''; ?> >
                                {{ $lang }}
                            </option>
                            <?php
                        }
                    ?>
                </select>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default" onclick="updateLanguages('#languages');">Update</button>
            </td>
        </tr>
        <tr>
            <td>Gender </td>
            <td class="setting-data">
                <select name="gender" class="setting-data-item data-select" data-name="gender">
                    <option value="">Select a gender</option>
                    <option value="female" <?php echo $currentProfile->gender == 'female' ? 'selected' : ''; ?> > Female</option>
                    <option value="male" <?php echo $currentProfile->gender == 'male' ? 'selected' : ''; ?> > Male</option>
                </select>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Nationality </td>
            
            <td class="setting-data">                
                <select name="nationality" class="setting-data-item data-select" data-name="nationality">
                    <option value="">Select a nationality</option>
                    <?php        
                    
                        $countries = config('countries.list');
                    
                        foreach($countries as $country) {
                            ?>
                            <option value="{{ $country['code'] }}" <?php echo $currentProfile->nationality == $country['code'] ? 'selected' : ''; ?> > {{ $country['name'] }}</option>
                            <?php
                        }                    
                    ?>
                </select>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
        <tr>
            <td>Introduce yourself</td>
            <td class="setting-data">
                <textarea rows="5" name="introduction" data-name="introduction" class="setting-data-item">{{ $currentProfile->introduction }}</textarea>
            </td>
            <td class="action">
                <button type="button" class="btn btn-default">Update</button>
            </td>
        </tr>
    </table>
</div>

@endsection

@section('scripts')
<link href="/js/chosen/chosen.min.css" rel="stylesheet">
<script src="/js/chosen/chosen.jquery.js"></script>
<script src="/js/member/profile.js"></script>
@endsection