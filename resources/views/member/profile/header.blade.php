<?Php
$user = Auth::user();
?>


<div class="member-profile-header">
    <div class="account-cover">
        <img id="cover-img" src="{{ $currentProfile->getCoverPicture() }}" class="img-responsive"/>
        <label class="upload-trigger" for="cover">
            <input type="file" accept="image/*" name="cover" id="cover" data-id="{{ $currentUser->id }}" class="hide">
            <i class="fa fa-camera"></i>
        </label>
        <span class="cover-loading"><img src="/img/transfer.gif" class="img-responsive"/></span>
    </div>
    <div class="member-header">
        <div class="profile-picture">
            <img id="profile-img" src="{{ $currentProfile->getProfilePicture() }}" class="img-responsive"/>
            <label class="upload-trigger" for="profile">
                <input type="file" accept="image/*" name="profile" id="profile" data-id="{{ $currentUser->id }}" class="hide">
                <i class="fa fa-camera"></i>
            </label>
        </div>
        <h4><i class="fa fa-home"></i> {{ $currentProfile->getName() }} <small>(Agent ID: {{ $currentProfile->getPublicID() }})</small></h4>
        <?php
        if ($user->customer_type == 'freelance') {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ trans('member.setting.contact-name') }}</strong> : {{ $currentProfile->contact_name }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ trans('member.setting.phone') }}</strong> : {{ $currentProfile->phone }}</td> 
                    <td><strong>{{ trans('member.setting.email') }}</strong> : {{ $currentUser->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ trans('member.setting.address') }}</strong> : {{ $currentProfile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        } else if ($user->customer_type == 'realestate') {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ trans('member.setting.contact-name') }}</strong> : {{ $currentProfile->contact_name }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ trans('member.setting.phone') }}</strong> : {{ $currentProfile->phone }}</td> 
                    <td><strong>{{ trans('member.setting.email') }}</strong> : {{ $currentUser->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ trans('member.setting.address') }}</strong> : {{ $currentProfile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        } else if ($user->customer_type == 'developer') {
            ?>
            <table class="table">
                <tr>
                    <td><strong>{{ trans('member.setting.business-name') }}</strong> : {{ $currentProfile->business_name }}</td> 
                    <td><strong>{{ trans('member.setting.website') }}</strong> : {{ $currentUser->website }}</td> 
                </tr>
                <tr>
                    <td><strong>{{ trans('member.setting.phone') }}</strong> : {{ $currentProfile->phone }}</td> 
                    <td><strong>{{ trans('member.setting.email') }}</strong> : {{ $currentUser->email }}</td> 
                </tr>
            </table>
            <?php
        }
        else {
            ?>
            <table class="table">
                <tr>
                    <td colspan="2">
                        <strong>{{ trans('member.setting.personal-name') }}</strong> : {{ $currentProfile->contact_name }}
                    </td>
                </tr>
                <tr>
                    <td><strong>{{ trans('member.setting.phone') }}</strong> : {{ $currentProfile->phone }}</td> 
                    <td><strong>{{ trans('member.setting.email') }}</strong> : {{ $currentUser->email }}</td> 
                </tr>
                <tr>
                    <td colspan="2"><strong>{{ trans('member.setting.address') }}</strong> : {{ $currentProfile->getTextAddress() }}</td>
                </tr>
            </table>
            <?php
        }
        ?>

    </div>
</div>
