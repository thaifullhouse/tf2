<div class="list-group manager-sidebar">
    <a href="/member/settings" class="list-group-item <?php echo $active == 'profile' ? 'active' : ''; ?>">{{ trans('member.sidebar.profile') }} </a>
    <a href="/member/listing" class="list-group-item <?php echo $active == 'listing' ? 'active' : ''; ?>">{{ trans('member.sidebar.listing') }} ({{ $currentUser->countProperty() }})</a>
    <a href="/member/purchase" class="list-group-item <?php echo $active == 'purchase' ? 'active' : ''; ?>">{{ trans('member.sidebar.purchase') }} </a>
    <a href="/member/message" class="list-group-item <?php echo $active == 'message' ? 'active' : ''; ?>">{{ trans('member.sidebar.message') }} ({{ $currentUser->countNewMessage() }})</a>
    <a href="/member/notice" class="list-group-item <?php echo $active == 'notice' ? 'active' : ''; ?>">{{ trans('member.sidebar.notice') }} </a>
    <a href="/auth/logout" class="list-group-item <?php echo $active == 'logout' ? 'active' : ''; ?>">{{ trans('auth.logout') }} </a>
</div>
<div class="summary">
    <h4>{{ trans('member.summary.title') }}</h4>
    <h5>{{ trans('member.summary.total-listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countProperty() }}</span> {{ trans('member.summary.listing') }}
    </p>
    <h5>{{ trans('member.summary.exclusive-listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countExclusiveProperty() }}</span> {{ trans('member.summary.listing') }}
    </p>
    <h5>{{ trans('member.summary.featured-listing') }}</h5>
    <p>
        <span class="count">{{ $currentUser->countFeaturedProperty() }}</span> {{ trans('member.summary.listing') }}
    </p>
    <h5>{{ trans('member.summary.visit') }}</h5>
    <p>
        <span class="count">{{ $currentUser->getTotalVisit() }}</span> {{ trans('member.summary.times') }}
    </p>
</div>