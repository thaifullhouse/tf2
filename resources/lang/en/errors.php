<?php

return [
    'forbidden-access-title' => 'Access forbidden',
    'forbidden-access-text' => 'Sorry, you don\'t have permission to access this page',
    'not-found-title' => 'Page not found',
    'not-found-text' => 'Sorry, the page you are accessing is not found or haved moved to another place'
];

