<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Sidebar Lines
    |--------------------------------------------------------------------------
    */

    'sidebar.dashboard' => 'Dashboard',
    'sidebar.staff' => 'Staff accounts',
    'sidebar.condo_list' => 'Condo list',
    'sidebar.new_condo_list' => 'New condo',
    'sidebar.property_features' => 'Property features',
    'sidebar.optional_features' => 'Optional features',
    'sidebar.condo_facilities' => 'Condo facilitites',
    'sidebar.provinces' => 'Provinces',
    'sidebar.points' => 'Point of Interest',
    'sidebar.website_settings' => 'Website settings',    
    'sidebar.member-management' => 'Member Management',
    'sidebar.visitors' => 'General members',
    'sidebar.owners' => 'Owners',
    'sidebar.agents' => 'Agents',
    'sidebar.developers' => 'Land Developers',
    'sidebar.property-management' => 'Property Management',    
    'sidebar.geographic-data' => 'Geographic Data',    
    'sidebar.other-data' => 'Other Data',
    'sidebar.package' => 'Package Management',
    'sidebar.new-project' => 'New projects',
    'sidebar.orders' => 'Orders',
    'sidebar.customer-data' => 'Customer Data',
    'sidebar.listings' => 'Listings',
    'sidebar.banners' => 'Banners',
    'sidebar.condo_project' => 'Condo Projects',
    'sidebar.apartment_project' => 'Apartment Projects',
    'sidebar.house_project' => 'House Projects',
    'sidebar.townhouse_project' => 'Townhouse Projects',
    'sidebar.apartment_list' => 'Apartment list',
    'sidebar.house_list' => 'House list',
    'sidebar.townhouse_list' => 'Townhouse list',
    
    /*
    |--------------------------------------------------------------------------
    | Packages Lines
    |--------------------------------------------------------------------------
    */
    
    'package.list-title' => 'List of packages',
    'package.create-featured' => 'Create FEATURED',
    'package.create-exclusive' => 'Create EXCLUSIVE',
    'package.back-to-list' => 'Back to package list',
    
    'package.col-name' => 'Name',
    'package.col-type' => 'Type',
    'package.col-price' => 'Price',
    'package.col-listing' => 'Listing Unit',
    'package.col-free-listing' => 'Free listings',
    'package.col-discount' => 'Discount',
    'package.col-period' => 'Period (days)',
    
    'package.create-featured-title' => 'Create a new Featured package',
    'package.create-exclusive-title' => 'Create a new Exclusive package',
    'package.update-package-title' => 'Update package',
    'package.name' => 'Name',
    'package.listing-count' => 'Listing Unit',
    'package.free-listing-count' => 'Free listings',
    'package.normal-price' => 'Normal Price',
    'package.discounted-price' => 'Discounted price',
    'package.saved' => 'Package saved successfully',
    'package.delete-title' => 'Delete package',
    'package.details-title' => 'Package details',
    'package.details-confirmation-question' => 'Do you really want to delete this package ?',
    'package.type' => 'Type',
    'package.period' => 'Period (in days)',
    'package.delete-message' => 'You can delete this package if you don\'t to sell it anymore.',
    'package.delete-link' => 'Delete this package',
    /*
    |--------------------------------------------------------------------------
    | Points Lines
    |--------------------------------------------------------------------------
    */
    
    'points.title' => 'Manage Point of Interests',
    'points.manage-types' => 'Manage PoI types',
    
    'points.type_title' => 'Manage PoI types',
    'points.create_type' => 'Add new type',
    'points.save-poi' => 'SAVE',
    'points.name_en' => 'Name (EN)',
    'points.name_th' => 'Name (TH)',
    'points.name-error' => 'Please fill names (EN and TH)',
    'points.marker-error' => 'Please place a POI on the map (move the map then click on the location of the new PoI)',
    'points.action-created' => 'PoI created',
    'points.action-updated' => 'PoI updated',
    'points.create-poi' => 'To create a new PoI, fill the names (EN and TH), then click SAVE',    
      
    
    /*
    |--------------------------------------------------------------------------
    | Banner Lines
    |--------------------------------------------------------------------------
    */
    
    'banner.list-title' => 'Banner Management',
    
    /*
    |--------------------------------------------------------------------------
    | Facilities Lines
    |--------------------------------------------------------------------------
    */
    
    'facility.list-title' => 'Condo facilities',
    'facility.delete-message' => 'Permanetly delete this facility',
    'facility.delete-link' => 'Delete',
    
    /*
    |--------------------------------------------------------------------------
    | Facilities Lines
    |--------------------------------------------------------------------------
    */
    
    'feature.list-title' => 'Property features list',
    'feature.delete-message' => 'Permanetly delete this feature',
    'feature.delete-link' => 'Delete',
    
    /*
    |--------------------------------------------------------------------------
    | Listing Lines
    |--------------------------------------------------------------------------
    */
    
    'listing.list-title' => 'Listing Management',
    
    /*
    |--------------------------------------------------------------------------
    | New Condo Lines
    |--------------------------------------------------------------------------
    */
    
    'new-condo.list-title' => 'New condominium',
    'new-condo.name-col' => 'Name',
    'new-condo.area-col' => 'Area', 
    
    /*
    |--------------------------------------------------------------------------
    | Member Lines
    |--------------------------------------------------------------------------
    */
    
    'visitor.list-title' => 'General members',
    'visitor.email-col' => 'E-mail',
    'visitor.lang-col' => 'Language',
    'visitor.visit-col' => 'Visits',
    'visitor.comment-col' => 'Comments',
    'visitor.last-login-col' => 'Last login',
    'visitor.active-col' => 'Active',
    
    /*
    |--------------------------------------------------------------------------
    | Owner Lines
    |--------------------------------------------------------------------------
    */
    
    'owner.list-title' => 'Owner list',
    'owner.id-col' => 'ID',
    'owner.name-col' => 'Name',
    'owner.listing-col' => 'Listing',
    'owner.visit-col' => 'Visits',
    'owner.last-login-col' => 'Last login',
    'owner.active-col' => 'Active',
    
    /*
    |--------------------------------------------------------------------------
    | Agent Lines
    |--------------------------------------------------------------------------
    */
    'agent.list-title' => 'List of agents',
    'agent.freelance-tab' => 'Freelance',
    'agent.realestate-tab' => 'Real Estate',
    'agent.id-col' => 'ID',
    'agent.name-col' => 'Name',
    'agent.listing-col' => 'Listing',
    'agent.visit-col' => 'Visits',
    'agent.company-col' => 'Company',
    'agent.level-col' => 'Level',
    'agent.last-login-col' => 'Last login',
    'agent.active-col' => 'Active',
    
    /*
    |--------------------------------------------------------------------------
    | Land developer Lines
    |--------------------------------------------------------------------------
    */
    
    'developer.list-title' => 'Land Developer List',
    'developer.id-col' => 'ID',
    'developer.name-col' => 'Developer',
    'developer.listing-col' => 'Listings',
    'developer.banner-col' => 'Banner',
    'developer.visit-col' => 'Visits',
    'developer.last-login-col' => 'Last login',
    'developer.active-col' => 'Active',
    
    /*
    |--------------------------------------------------------------------------
    | Order Lines
    |--------------------------------------------------------------------------
    */
    
    'order.list-title' => 'Order Management',
    'order.history-title' => 'Order history',
    'order.id-col' => 'Order ID',
    'order.user-col' => 'User ID',
    'order.package-col' => 'Package Name',
    'order.listing-unit-col' => 'Listing Unit',
    'order.price-login-col' => 'Price',
    'order.status-col' => 'Status',
    'order.payment-col' => 'Payment',
    'order.edit-col' => 'Edit',
    'order.card-info-title' => 'Credit Card info',
    'order.bank-info-title' => 'Bank Transfer info',
    'order.setting-country' => 'Country',
    'order.setting-address' => 'Street Address',
    'order.setting-city' => 'City',
    'order.setting-state-zip' => 'State / Zip code',
    'order.setting-firstname' => 'Firstname',
    'order.setting-lastname' => 'Lastname',
    'order.setting-cardnum' => 'Card Number',
    'order.setting-expiry' => 'Expiry Month / Year',
    'order.setting-securitynum' => 'Security Number',
    'order.setting-edit' => 'Edit',
    'order.setting-bank' => 'Chosen Bank',
    'order.setting-last_amount' => 'Latest Amount',
    'order.account-updated' => 'User account was updated successfully',
    'order.order-updated' => 'User order was updated successfully',
    
    /*
    |--------------------------------------------------------------------------
    | Staff Lines
    |--------------------------------------------------------------------------
    */
    
    'staff.list-title' => 'Staff list',
    
    /*
    |--------------------------------------------------------------------------
    | Setting Lines
    |--------------------------------------------------------------------------
    */
    'setting.list-title' => 'Website setting',
    'setting.basic-tab' => 'Basic setting',
    'setting.contact-tab' => 'Contact',
    'setting.bank-tab' => 'Bank accounts',
    'setting.add-bank' => 'Add a bank account',
    'setting.back-bank' => 'Back to bank account list',
    'setting.bank.bank-label' => 'Bank',
    'setting.bank.bank-select' => 'Select a bank',
    'setting.bank.name-label' => 'Name',
    'setting.bank.account-no-label' => 'Account No.',
    'setting.bank.branch-label' => 'Branch',
    'setting.bank.add-button' => 'Add Bank',
    'setting.bank.update-button' => 'Update Bank',
    'setting.bank.delete-link' => 'Delete this bank',
    'setting.bank.delete-message' => 'Deleting a bank account does not delete the previous payment made by customers',
    'setting.facebook-link-label' => 'Facebook link',
    'setting.instagram-link-label' => 'Instagram link',
    'setting.twitter-link-label' => 'Twitter link',
    'setting.google-link-label' => 'Google link',
    'setting.social-network-title' => 'Social Network'
];
