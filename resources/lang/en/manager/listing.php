<?php

return [
    'class-label' => 'Listing class',
    'class-standard' => 'Standard',
    'class-featured' => 'Featured',
    'class-exclusive' => 'Exclusive',
    
    'status-label' => 'Status',
    'status-present' => 'Present',
    'status-expired' => 'Expired',
        
    'start-label' => 'Start date',
    'end-label' => 'End date'
];