<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Banner Lines
      |--------------------------------------------------------------------------
     */

    'list-title' => 'Banner Management',
    'create' => 'Add new banner',
    'name-label' => 'Banner Name',
    'developer-label' => 'Developer',
    'image-label' => 'Upload Image',
    'link-label' => 'Attached Link',
    'effect-label' => 'Banner Effect',
    'location-label' => 'Location',
    'start-label' => 'Start Date',
    'end-label' => 'End Date',
    'status-label' => 'Payment Status',
    
    'name-col' => 'Banner Name',
    'developers-col' => 'Developers',
    'start-col' => 'Start Date',
    'end-col' => 'End date',
    'location-col' => 'Location',
    'view-col' => 'View',
    
    'location-home' => 'Homepage',
    
    'location-map-sale' => 'Map Search - For Sale',
    'location-map-rent' => 'Map Search - For Rent',
    'location-map-project' => 'Map Search - New Project',
    'location-map-condo' => 'Map Search - Condo community',
    
    'location-list-sale' => 'List Search - For Sale',
    'location-list-rent' => 'List Search - For Rent',
    'location-list-project' => 'List Search - New Project',
    'location-list-condo' => 'List Search - Condo community',
    
    'developer-select' => 'Select a developer',
    'effect-select' => 'Select an effect',
    'location-select' => 'Select a location',
    'status-select' => 'Select a status',
    'create-error-text' => '<strong>An error occured</strong>, please check that all fields marked with * are filled'
];
