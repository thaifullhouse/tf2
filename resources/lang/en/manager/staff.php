<?php

return [
    'create' => 'Create a staff',
    'create-title' => 'Create a staff account',
    'update-title' => 'Update staff account',
    'name-label' => 'Name',
    'email-label' => 'Email',
    'role-label' => 'Role',
    'role-manager' => 'Manager',
    'role-editor' => 'Content Editor',
    'role-accountant' => 'Accountant',
    'role-translator' => 'Translator',
    
    'name-col' => 'Name',
    'role-col' => 'Role',
    'user-exists' => 'This email is already used by another user'
];