<?php

    /*
    |--------------------------------------------------------------------------
    | Condo Lines
    |--------------------------------------------------------------------------
    */

return [
    'back' => 'Back to condo list',
    'list-title' => 'Condominium list',
    'create' => 'Create a new Condo',
    'search-filter' => 'Search',
    'name-col' => 'Name',
    'area-col' => 'Area',
    'info-tab' => 'Location',
    'details-tab' => 'Details',
    'media-tab' => 'Media',
    'details-title' => 'Condo details',
    'places-tab' => 'Nearby Places',
    'listing-tab' => 'Listings',
    'photo-col' => 'Photo'
];
