<?php

return [
    
    /*
    |--------------------------------------------------------------------------
    | Project Lines
    |--------------------------------------------------------------------------
    */
    
    'list-title' => 'New project list',
    'create-condo-title' => 'Condo project',
    'create-aparment-title' => 'Apartment project',
    'create-house-title' => 'House project',
    'create-townhouse-title' => 'Townhouse project',
    
    'back-to-list' => 'Back to project list',
    
    'area-label' => 'Aread',
    'create' => 'Add new project',
    'id-col' => 'ID',
    'name-col' => 'Name',
    'area-col' => 'Area',
    'photo-col' => 'Photo',
    'type-col' => 'Type',
    'price-col' => 'Price',
    'province-col' => 'Province',
    'class-col' => 'Listing Class',
    'start-col' => 'Start date',
    'end-col' => 'End date',
    'status-col' => 'Status',
    'property-col' => 'Property',
    'active-col' => 'Active',
    'create-title' => 'Add a new project',
    
    'info-tab' => 'Location',
    'details-tab' => 'Settings',
    'review-tab' => 'Review',
    'places-tab' => 'Nearby places',
    'media-tab' => 'Media',
    
    'filter-type' => 'Type',
    'filter-property' => 'Property Type',
    'filter-class' => 'Listing Class',
    'filter-status' => 'Status',
    
    'filter-all' => 'All',
    'filter-type-rent' => 'Rent',
    'filter-type-sale' => 'Sale',
    
    'filter-property-condo' => 'Condo',
    'filter-property-townhouse' => 'Townhouse',
    'filter-property-house' => 'House',
    'filter-property-apartment' => 'Apartment',
    
    'filter-class-standard' => 'Standard',
    'filter-class-featured' => 'Featured',
    'filter-class-exclusive' => 'Exclusive',
    
    'filter-status-expired' => 'Expired',
    'filter-status-present' => 'Present',
    
    'details-title' => 'Condo details',
    
    'review.how-to-get-there-title' => 'How to get there',
    'review.facility-landscape-title' => 'Facility &amp; landscape',
    'review.floor-plan-title' => 'Floor plan',
    'review.video-title' => 'Project Video',
    'review.overview-title' => 'Overview',
    'review.add-image' => 'Add image',
    'review.details-title' => 'Details',
    
    //
    
    'name-en-label' => 'Name EN',
    'name-th-label' => 'Name TH',
    'latitude-label' => 'Latitude',
    'longitude-label' => 'Longitude',
    'postcode-label' => 'Postcode',
    'submit-btn' => 'Submit',
    'clear-btn' => 'Clear'
    
];