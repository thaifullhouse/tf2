<?php

return [
    'required-fields-msg' => ' are required fields',
    'create' => 'CREATE',
    'update' => 'UPDATE',
    'cancel' => 'CANCEL',
    'delete' => 'DELETE',
    'close' => 'CLOSE',
    'save' => 'SAVE'
];