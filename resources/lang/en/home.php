<?php

return [
    'hero.search.title' => 'Search faster',
    'hero.search.text' => 'we provide a searching map services to reach your dream house faster',
    
    'hero.free.title' => 'free posting',
    'hero.free.text' => 'You can post a listing for free on Thaifullhouse website',
    
    'hero.info.title' => 'Full information',
    'hero.info.text' => 'Our website provide all needed information to you for the better decision',
    
    'hero.agents.title' => 'Best agents',
    'hero.agents.text' => 'We provide you the information of proffesional agents, trustable and ready to be your best partner',
    
    'hero.title' => 'How is Thaifullhouse different ?',
    
    'home.premium-listing' => 'PREMIUM <em>PROJECT</em>',
    'starting-price' => 'Starting price'
];
