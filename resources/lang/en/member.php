<?php

return [
    
    'sidebar.profile' => 'Settings',
    'sidebar.listing' => 'Listings',
    'sidebar.purchase' => 'Purchases',
    'sidebar.message' => 'Messages',
    'sidebar.notice' => 'Notice',
    
    /*
    |---------------------------------------------------------------------------
    | Sidebar Language Lines
    |---------------------------------------------------------------------------
    */

    'summary.title' => 'Summary',
    'summary.total-listing' => 'Total listings',
    'summary.exclusive-listing' => 'Exclusive listings',
    'summary.featured-listing' => 'Featured listings',
    'summary.visit' => 'Visit your profile',
    'summary.listing' => 'listings',
    'summary.times' => 'times',
    
    /*
    |---------------------------------------------------------------------------
    | Listing Lines
    |---------------------------------------------------------------------------
    */
    
    'listing.title' => 'My listings',
    'listing.status' => 'Status',
    'listing.create-new' => 'Create new',
    'listing.status-present' => 'Present',
    'listing.status-draft' => 'Draft',
    'listing.status-expired' => 'Expired',
    'listing.status-exclusive' => 'Exclusive',
    'listing.status-featured' => 'Featured',
    'listing.option' => 'Option',
    'listing.option-all' => 'All',
    'listing.option-rent' => 'Rent',
    'listing.option-sale' => 'Sale',    
    'listing.col-image' => 'Image',
    'listing.col-class' => 'Class',
    'listing.col-type' => 'Type',
    'listing.col-area' => 'Area',
    'listing.col-size' => 'Sqm',
    'listing.col-price' => 'Price',
    'listing.col-start' => 'Start/End',
    'listing.col-status' => 'Status',
    'listing.col-active' => 'Active',
    
    /*
    |---------------------------------------------------------------------------
    | Purchase Lines
    |---------------------------------------------------------------------------
    */
    
    'purchase.title' => 'My Purchase',
    'purchase.col-type' => 'Type',
    'purchase.col-package' => 'Package',
    'purchase.col-period' => 'Period',
    'purchase.buy-new' => 'Buy new',
    'purchase.col-id' => 'ID',
    
    /*
    |---------------------------------------------------------------------------
    | Message Lines
    |---------------------------------------------------------------------------
    */
    'messages.title' => 'My Messages',
    'messages.status' => 'Status',
    'messages.inbox' => 'Inbox',
    'messages.deleted' => 'Deleted',
    'messages.option' => 'Option',
    'messages.all' => 'All',
    'messages.read' => 'Read',
    'messages.unread' => 'Unread',    
    'messages.col-date' => 'Date',
    'messages.col-interest' => 'Interest',
    'messages.col-topic' => 'Topic',
    'messages.col-name' => 'Name',
    
    /*
    |---------------------------------------------------------------------------
    | Message Lines
    |---------------------------------------------------------------------------
    */
    
    'notification.title' => 'Notices',
    'notification.col-type' => 'Type',
    'notification.col-date' => 'Date',
    'notification.col-news' => 'News',
    
    /*
    |---------------------------------------------------------------------------
    | Naviguation Language Lines
    |---------------------------------------------------------------------------
    */

    'setting.business-name' => 'Name',
    'setting.website' => 'Website',
    'setting.phone' => 'Phone',
    'setting.email' => 'Email',
    'setting.developer-name' => 'Land developer name',
    'setting.address' => 'Address',
    'setting.languages-ability' => 'Language ability',
    'setting.pinperty-url' => 'Pinpterty URL',
    'setting.twitter' => 'Twitter',
    'setting.facebook_link' => 'Facebook',
    
    /*
    |---------------------------------------------------------------------------
    | Package
    |---------------------------------------------------------------------------
    */
    
    'package.buy-title' => 'Advertise with ThaiFullHouse',
    'package.free-listing' => 'Free listing',
    'package.upgrade' => 'Upgrade',
    'package.step' => 'STEP',
    'package.exclusive' => 'Exlusive listing',
    'package.featured' => 'Featured listing',
    'package.listing' => 'Listing',
    'package.per-month' => 'Per month',
    'package.payment-bank-transfer' => 'Bank transfer',
    'package.payment-bank-card' => 'Credit card',
    'package.payment-limit-warning' => '* The user must',
    'package.step-2-title' => '2 methods to send a receipt',
    'package.receipt-method-line-text' => 'You can SCAN our Line QR Code and send your receipt picture with your agent or owner ID',
    'package.receipt-method-email-text' => 'You can send the receipt picture with your agent or owner ID to <em>info@thaifullhouse.com</em>',
    'package.payment-skip' => 'SKIP',
    'package.payment-next' => 'NEXT',
    'package.buy.success-message' => 'THANK YOU FOR YOU ORDER',
    'package.buy.order-id' => 'Order ID',
    'package.buy.confirm-message' => 'Please confirm your payment within 24 hours',
    'package.buy.processing-title' => 'Getting payment data',
    'package.buy.processing-message' => 'We are redirecting you to our payment gateway, please do not close this window',
    
    'posting.wizard-select-property' => 'Select property',
    'posting.wizard-post-details' => 'Post details',
    'posting.wizard-preview' => 'Preview'
    
    ];