<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Naviguation Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'appname' => 'Thai Full House',

    'menu.buy' => 'BUY',
    'menu.rent' => 'RENT',
    'menu.new-project' => 'NEW PROJECT',
    'menu.find-agent' => 'FIND AN AGENT',
    'menu.condo-directory' => 'Condo directory',
    'menu.login-register' => 'login/register',

    'menu.sign-facebook' => 'Sign in with Facebook',
    'menu.sign-twitter' => 'Sign in with Twitter',
    'menu.sign-google' => 'Sign in with Google+',

    'menu.join-facebook' => 'Join with Facebook',
    'menu.join-twitter' => 'Join with Twitter',
    'menu.join-google' => 'Join with Google+',

    'menu.email' => 'Email',
    'menu.password' => 'Password',
    'menu.remember-me' => 'Remember me',
    'menu.sign-in' => 'Sign in',
    'menu.forgot-password' => 'Forgot password ?',
    'menu.dont-have-account' => 'Don\'t have an account ?',

    'breadcrumb.home' => 'Home',
    'breadcrumb.search' => 'Search',
    'breadcrumb.search-bts' => 'BTS',
    'breadcrumb.search-mrt' => 'MRT',
    'breadcrumb.search-arl' => 'ARL',
    'breadcrumb.free-listing' => 'free listing',

];
