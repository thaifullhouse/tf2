<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Naviguation Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'sidebar.dashboard' => 'แผงควบคุม',
    'sidebar.staff' => 'Staff accounts',
    'sidebar.condo_list' => 'Condo list',
    'sidebar.new_condo_list' => 'New condo',
    'sidebar.property_features' => 'Property features',
    'sidebar.optional_features' => 'Optional features',
    'sidebar.condo_facilities' => 'Condo facilitites',
    'sidebar.provinces' => 'Provinces',
    'sidebar.points' => 'Point of Interest',
    'sidebar.website_settings' => 'Website settings',
    
    'points.title' => 'Manage Point of Interests',
    'points.manage-types' => 'Manage PoI types',
    
    'points.type_title' => 'Manage PoI types',
    'points.create_type' => 'Add new type',
    'points.save-poi' => 'SAVE',
    'points.name_en' => 'Name (EN)',
    'points.name_th' => 'Name (TH)',
    'points.name-error' => 'Please fill names (EN and TH)',
    'points.marker-error' => 'Please place a POI on the map (move the map then click on the location of the new PoI)',
    'points.action-created' => 'PoI created',
    'points.action-updated' => 'PoI updated',
    'points.create-poi' => 'To create a new PoI, fill the names (EN and TH), then click SAVE'
];
