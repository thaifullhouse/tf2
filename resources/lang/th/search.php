<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Naviguation Language Lines
    |--------------------------------------------------------------------------
    |
    */

    'listing.rent' => 'RENT',
    'listing.sale' => 'SALE',
    'listing.type' => 'Listing Type',
    'any-price' => 'Any price',
    'beds' => 'Beds',
    'home-type' => 'Home type',
    'more' => 'More',
    'saved' => 'Saved home (:count)',
    'save' => 'Save',
    'search-by' => 'Seach by',
    'listing.for-rent' => 'For rent',
    'listing.for-sale' => 'For sale',

    'listing.apartment' => 'Apartment',
    'listing.condo' => 'Condominium',
    'listing.detached-house' => 'Detached House',
    'listing.townhouse' => 'Townhouse',

    'listing.condo-dir' => 'Condo directory',
    'listing.new-project' => 'New project',

    'listing.bathrooms' => 'Bathrooms',
    'listing.size' => 'Size',
    'listing.min-size' => 'Min size',
    'listing.max-size' => 'Max size',
    'listing.year-built' => 'Year built',
    'listing.min-year-built' => 'Min year',
    'listing.max-year-built' => 'Max year',
    'listing.apply-more-filter' => 'Apply',
    'listing.min-price' => 'Min price',
    'listing.max-price' => 'Max price',

    'sorter.home-for-you' => 'Homes for you',
    'sorter.newest' => 'Newest',
    'sorter.cheapest' => 'Cheapest',
    'sorter.more' => 'More',

    'sorter.bedrooms' => 'Bedrooms',
    'sorter.bathrooms' => 'Bathrooms',
    'sorter.size' => 'Size',
    'sorter.year-built' => 'Year built',
    'sorter.short-rentat' => 'Short rental',

    'results.updating' => 'Updating results'
];
