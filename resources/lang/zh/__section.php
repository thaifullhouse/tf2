<?php

return [
    'topmenu' => 'Top menu',
    'manager' => 'Manager',
    'manager_translation' => 'Translation management',
    'visitor_breadcrumb' => 'Front > Breadcrumb',
    'visitor_registration' => 'Front > Registration',
    'manager_agent' => 'Agent management',
    'manager_condo' => 'Condo management',
    'manager_location' => 'Location management',
    'manager_province' => 'Province management',
    'manager_project' => 'New project management',
    'member_posting' => 'Member posting',
    'alert' => 'Alert & notification text',
    'form' => 'Text for forms'
];