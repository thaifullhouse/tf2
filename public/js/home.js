var hl = hl || '';
var searchOptions = searchOptions || {};

var pid = 0;

$(document).ready(function () {
    $('#search').autocomplete({
        source: "/rest/search-autocomplete",
        minLength: 2,
        classes: {
            "ui-autocomplete": "home-search"
        },
        select: function (event, ui) {
            var lat = parseFloat(ui.item.lat);
            var lng = parseFloat(ui.item.lng);

            //console.log(ui.item);

            window.location = '/search?q=' + ui.item.value
                    + '&rel=' + ui.item.domain
                    + '&lat=' + lat
                    + '&lng=' + lng
                    + '&id=' + ui.item.id
                    + '&lt=' + searchOptions.lt;
                    + '&np=' + searchOptions.np;
                    + '&pt=' + searchOptions.pt;
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $('<li class="poi-item">').addClass('prop').addClass(item.type).data('lat', item.lat).data('lng', item.lng).data('zl', item.zoom_level)
                .attr("data-value", item.id)
                .append('<span class="icon ' + item.type + '"></span><span class="name">' + item.value + '</span><span class="poi-type text-muted">' + item.type_label + '</span>' +
                        '<span class="more">' + item.more + '</span>')
                .appendTo(ul);
    };

    $('#property-preview').on('show.bs.modal', function (e) {
        $('#property-preview .modal-body').load('/property-preview/' + pid, function () {
            var width = 0;
            $('.media-slider .media-group').each(function (index, value) {
                width = width + $(value).width() + 2.0;                
            });
            $('.media-slider').width(width);

            //showStreetView(parseFloat(currentPropertyData.lat), parseFloat(currentPropertyData.lng));
        });
    });
    
    $('.premium-item').on('click', function(){
       
        pid = $(this).data('id');
        
        $('#property-preview .modal-body').html('');
        $('#property-preview').modal('show');
        
    });
});




var panorama;

var showStreetView = function (lat, lng) {
    panorama = null;
    panorama = new google.maps.StreetViewPanorama(
            document.getElementById('street-view'),
            {
                position: {lat: lat, lng: lng},
                pov: {heading: 165, pitch: 0},
                zoom: 1
            });
    console.log('showStreetView');
};