var SearchSuggestion = function (element) {
    this.element = document.getElementById(element);
    this.element.addEventListener('keyup', this.find.bind(this), false);
    this.term = '';
    this.xhr = new XMLHttpRequest();
    this.xhr.addEventListener("load", this.onLoad.bind(this));
    this.xhr.addEventListener("error", this.onError.bind(this));
    this.xhr.addEventListener("timeout", this.onTimeout.bind(this));
    this.defer_next = false;
    this.running = false;

};

SearchSuggestion.prototype.find = function () {

    this.term = this.element.value.trim();

    if (this.term.length > 2) {
        if (this.running === false) {
            this.xhr.open("GET", "/rest/search-suggestion?term=" + this.term);
            this.xhr.send();
        } else {
            this.defer_next = true;
        }
    }
};

SearchSuggestion.prototype.onLoad = function () {

    console.log(this.xhr);
    
    this.running = false;

    if (this.defer_next === true) {
        this.find();
    } 
};

SearchSuggestion.prototype.onError = function () {
    this.running = false;
};

SearchSuggestion.prototype.onTimeout = function () {
    this.running = false;
};