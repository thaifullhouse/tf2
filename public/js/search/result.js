var propertyMarkers = [];
var markerCluster;
var markerInfos = [];
var currentInfo;

var loadProperties = function (list, favorites) {
    
    $('.search-results').html('');

    if (propertyMarkers.length) {
        for (var i = 0; i < propertyMarkers.length; i++) {
            var marker = propertyMarkers[i];
            marker.setMap(null);
        }
        propertyMarkers = [];
        markerInfos = [];
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    /**/

    if (list.length > 0) {


        for (var i = 0; i < list.length; i++) {
            var pdata = list[i];
            var prop = new TFHProperty(pdata);
            $('.search-results').append(prop.getTemplate(favorites));
            //console.log('append + ' + i);

            var markerIcon;

            if (pdata.listing_type === 'rent') {
                markerIcon = '/img/pin-blue.png';
            } else {
                markerIcon = '/img/pin-red.png';
            }

            var marker = new google.maps.Marker({
                position: {lat: parseFloat(pdata.lat), lng: parseFloat(pdata.lng)}, icon: {
                    url: markerIcon,
                    size: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(20, 0)
                }
            });

            marker.data = pdata;

            var tpl = $('#marker-info').html();
            var elt = $(tpl);

            elt.find('.price').html('‎฿ ' + numberWithCommas(pdata.details.listing_price));
            elt.find('.beds').html(pdata.details.bedroom_num);
            elt.find('.baths').html(pdata.details.bathroom_num);
            elt.find('.size').html(pdata.details.total_size);
            elt.find('.marker-img img').prop('src', prop.getMainImage());

            var infowindow = new google.maps.InfoWindow({
                content: elt[0].outerHTML
            });

            marker.infoIndex = propertyMarkers.length;

            marker.addListener('mouseover', function () {
                var info = markerInfos[this.infoIndex];
                if (currentInfo) {
                    currentInfo.close();
                }
                info.open(map, this);
                currentInfo = info;
            });

            marker.addListener('click', function () {
                console.log(this.data);
                showPropertyPreview(this.data);
            });

            propertyMarkers.push(marker);
            markerInfos.push(infowindow);
        }

        $('.favorite-btn').on('click', function (event) {
            
            event.stopPropagation();
            
            var elt = $(this);

            var pid = elt.data('id');

            if (pid) {
                $.get('/rest/user/favorite-property/' + pid, function (data, status, xhr) {
                    if (data.status === 'OK') {
                        if (data.result === 'ADDED') {
                            elt.find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
                        } else {
                            elt.find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
                        }
                    } else {

                    }
                });
            }
        });

        if (markerCluster) {
            markerCluster.addMarkers(propertyMarkers);
        } else {
            var clusterStyles = [
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                }
            ];

            markerCluster = new MarkerClusterer(map, propertyMarkers,
                    {
                        imagePath: '/img/m',
                        styles: clusterStyles
                    });
        }
        $('.property-count').html(list.length + ' properties');

        if (sortAttr) {
            applySort();
        }
    } else {
        //console.log('list.length = 0');
        $('.property-count').html('No property found');
    }
};