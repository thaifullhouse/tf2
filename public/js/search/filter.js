
/**********************************
 *  Init filters
 **********************************/

var SearchFilters = function (search, request) {
    this.search = search;
    this.options = {
        listing_types: [],
        project_type: [],
        price_min: 0,
        price_max: '',
        bedrooms: 1,
        home_types: [],
        bathrooms: '',
        size_min: 0,
        size_max: '',
        year_min: 0,
        year_max: ''
    };

    this.request = request;

    this.initFilters();
};

SearchFilters.prototype.initFilters = function () {

    this.initListingTypes();
    this.initPropertyTypes();
    this.addFilterListeners();
};

SearchFilters.prototype.initListingTypes = function () {

    if (this.request.pr && this.request.pr === 'undefined') {
        $('#lt-rent').prop('checked', false);
        $('#lt-sale').prop('checked', false);

        if (this.request.pr === 'co') {
            $('#pr-co').prop('checked', true);
        }

        if (this.request.pr === 'np') {
            $('#pr-np').prop('checked', true);
        }
    } else {
        if (this.request.lt && this.request.lt.length > 0) {
            var options = this.request.lt.split(',');
            if (options.length > 0) {
                if (options.indexOf('rent') >= 0) {
                    $('#lt-rent').prop('checked', true);
                }
                if (options.indexOf('sale') >= 0) {
                    $('#lt-sale').prop('checked', true);
                }
            } else {
                $('#lt-rent').prop('checked', true);
                $('#lt-sale').prop('checked', true);
            }
        }
    }
};

SearchFilters.prototype.initPropertyTypes = function () {
    if (this.request.pt && this.request.pt.length > 0) {
        var options = this.request.pt.split(',');

        if (options.length > 0) {
            if (options.indexOf('ap') >= 0) {
                $('#pt-ap').prop('checked', true);
            }
            if (options.indexOf('co') >= 0) {
                $('#pt-co').prop('checked', true);
            }
            if (options.indexOf('dh') >= 0) {
                $('#pt-dh').prop('checked', true);
            }
            if (options.indexOf('th') >= 0) {
                $('#pt-th').prop('checked', true);
            }
        } else {
            $('#pt-ap').prop('checked', true);
            $('#pt-co').prop('checked', true);
            $('#pt-dh').prop('checked', true);
            $('#pt-th').prop('checked', true);
        }
    }
};

SearchFilters.prototype.addFilterListeners = function () {

    /**********************************
     *  Update listing types
     **********************************/

    var listingTypeListener = function () {
        $('.pr').prop('checked', false);
        this.updateListingTypes();
        this.apply();
    };

    $('.lt').on('click', listingTypeListener.bind(this));

    /**********************************
     *  Update property types
     **********************************/

    var propertyTypeListener = function () {
        $('.lt').prop('checked', false);
        this.options.project_type = $('.pr:checked').val();
        this.apply();
    };

    $('.pr').on('click', propertyTypeListener.bind(this));

    /**********************************
     *  Update price range
     **********************************/

    var minPriceListener = function () {
        this.options.price_min = $(this).data('price');
        $('#price_min').val($(this).text());
        $('.min-prices').hide();
        $('.max-prices').show();
        event.stopPropagation();
    };

    $('.min-price').on('click', minPriceListener);


    /**********************************
     *  Update price range
     **********************************/

    var maxPriceListener = function () {
        this.options.price_max = $(this).data('price');
        $('#price_max').val($(this).text());
        $('.min-prices').show();
        $('.max-prices').hide();
        this.apply();
    };

    $('.max-price').on('click', maxPriceListener);

    /**********************************
     *  Update bedroom
     **********************************/

    var bedUpdateListener = function (event) {
        console.log(event);
        this.options.bedrooms = $(event.currentTarget).data('filter');
        this.apply();
        $('.bed-item').removeClass('active');
        $(event.currentTarget).addClass('active');
        
        
    };

    $('.bed-item').on('click', bedUpdateListener.bind(this));

    /**********************************
     *  Update home type
     **********************************/

    var homeTypeListener = function () {
        this.updateHomeTypes();
        this.apply();
    };
    $('.pt').on('click', homeTypeListener.bind(this));
};



/**********************************
 *  Init home type
 **********************************/

SearchFilters.prototype.updateTransFilter = function (filter, data) {
    this.options.trans = filter;
    $('#current-trans').prop('src', data);
    this.apply();
};

SearchFilters.prototype.updateListingTypes = function () {
    var lt = $('.lt:checked');
    this.options.listing_types = [];
    if (lt.length > 0) {
        for (var i = 0; i < lt.length; i++)
        {
            this.options.listing_types.push($(lt[i]).val());
        }
    }
};

SearchFilters.prototype.updateHomeTypes = function () {
    var pt = $('.pt:checked');
    this.options.home_types = [];
    if (pt.length > 0) {
        for (var i = 0; i < pt.length; i++)
        {
            this.options.home_types.push($(pt[i]).val());
        }
    }
};

SearchFilters.prototype.saveMoreFilter = function () {
    this.bathrooms = $('#bathroom').val();
    this.size_range = [$('#size_min').val(), $('#size_max').val()];
    this.year_range = [$('#year_min').val(), $('#year_max').val()];
};

SearchFilters.prototype.apply = function () {
    //
    var bounds = this.search.map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    url = 'nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6);
    url = url + '&pt=' + this.options.home_types.join(',') + '&lt=' + this.options.listing_types.join(',');
    url = url + '&pmin=' + this.options.price_min + '&pmax=' + this.options.price_max;
    url = url + '&bed=' + this.options.bedrooms + '&bath=' + this.options.bathrooms;
    url = url + '&smin=' + this.options.size_min + '&smax=' + this.options.size_max;
    url = url + '&ymin=' + this.options.year_min + '&ymax=' + this.options.year_max;
    url = url + '&pr=' + this.options.project_type;
    url = url + '&z=' + this.search.map.getZoom();
    
    if (currentId) {
        url = url + '&id=' + currentId;
    }
    

    window.history.pushState('page2', $('title').text(), '/search?' + url);

    this.search.sendSearchQuery(url);
};

/**********************************
 *  Auto complete
 **********************************/



var getImageUrl = function (type) {
    var url = '';
    switch (type) {
        case 'bts':
            url = '/img/maps/selected/bts.png';
            break;
        case 'mrt':
            url = '/img/maps/selected/mrt.png';
            break;
        case 'apl':
            url = '/img/maps/selected/apl.png';
            break;
        case 'bank':
            url = '/img/maps/selected/bank.png';
            break;
        case 'dept-store':
            url = '/img/maps/selected/departmentstore.png';
            break;
        case 'hospital':
            url = '/img/maps/selected/hospital-building.png';
            break;
        case 'school':
            url = '/img/maps/selected/school.png';
            break;
        default:
            url = '/img/maps/selected/bts.png';
            break;
    }

    return url;
};