
var priceClass = '.sale-prices';

var f_lt = '';
var f_bed = 0;
var f_np = '';
var f_bat = 0;

var price_min = 0;
var price_max = 0;

var min_price_label = null;
var max_price_label = null;

var map;

var reloadOnIdle = true;

var currentPage = 1;


var mapContainer = {
    listingWidth: 670,
    mapHeight: 0,
    propertyListingWidth: 0,
    propertyMapWidth: 0,
    docWidth: 0,
    currentMapCenter: 0,
    haveBigMap: false
};

var searchResult;

var updateDimensions = function () {

    var oldWidth = $('.col-list').outerWidth();

    var mapOffset = $('#map').offset();
    mapContainer.mapHeight = $(window).height() - mapOffset.top;
    $('#map').height(mapContainer.mapHeight);
    $('.col-list').height(mapContainer.mapHeight).outerWidth(mapContainer.propertyListingWidth);
    $('.property-listing').height(mapContainer.mapHeight - 20);
    if (mapContainer.docWidth !== $('.search-result-container').width()) {
        updateSectionDimension();
    }

    if (mapContainer.propertyListingWidth > 335) {
        $('.cheap-filter').show();
    } else {
        $('.cheap-filter').hide();
    }

    if (mapContainer.propertyListingWidth < oldWidth) {
        $('.more-maps .more').hide();
        $('.more-maps .less').show();
    } else {
        $('.more-maps .more').show();
        $('.more-maps .less').hide();
    }
};

var updateSectionDimension = function () {

    mapContainer.docWidth = $('.search-result-container').width();
    mapContainer.propertyListingWidth = mapContainer.listingWidth;
    mapContainer.propertyMapWidth = mapContainer.docWidth;
    if (mapContainer.docWidth > 1280) {
        // 2-column listing
        mapContainer.propertyListingWidth = mapContainer.listingWidth;
        mapContainer.haveBigMap = false;

    } else if (mapContainer.docWidth > 1024) {
        // 1-column listing
        mapContainer.propertyListingWidth = mapContainer.listingWidth / 2.0 + 5.0;
        mapContainer.haveBigMap = true;

    } else {
        // map only
        mapContainer.propertyListingWidth = 0.0;
        mapContainer.haveBigMap = true;
    }

    mapContainer.propertyMapWidth = mapContainer.docWidth - mapContainer.propertyListingWidth - 1;

    updateDimensions();
};

var initAutoComplete = function () {
    $('#q').autocomplete({
        source: "/rest/search-autocomplete",
        minLength: 2,
        classes: {
            "ui-autocomplete": "map-search"
        },
        select: selectSuggested,
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        //console.log(item);
        return $('<li class="poi-item">').addClass('prop').addClass(item.type).data('lat', item.lat).data('lng', item.lng).data('zl', item.zoom_level)
                .attr("data-value", item.id)
                .append('<span class="icon ' + item.type + '"></span><span class="name">' + item.value + '</span><span class="poi-type text-muted">' + item.type_label + '</span>' +
                        '<span class="more">' + item.more + '</span>')
                .appendTo(ul);
    };
};

var selectSuggested = function (event, ui) {

    console.log(ui.item);

    currentId = ui.item.id;

    domain = ui.item.domain;

    currentPoiData = ui.item;

    var lat = parseFloat(ui.item.lat);
    var lng = parseFloat(ui.item.lng);
    var zl = parseFloat(ui.item.zoom_level);
    map.panTo({lat: lat, lng: lng});
    //map.setZoom(zl);

    if (ui.item.domain === 'poi') {
        var poimg = getImageUrl(ui.item.type);

        if (poimg) {
            var image = {
                url: poimg,
                size: new google.maps.Size(32, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 37)
            };

            var marker = new google.maps.Marker({
                position: {lat: lat, lng: lng},
                draggable: true,
                title: ui.item.item,
                icon: image,
                map: map
            });
        }

    } else {
        // Display 
        //openMakerInfoById(ui.item.id);

        if (ui.item.domain === 'condo') {
            $('#pt-cd').prop('checked', true);
        }

        applySearchCriteria();
    }
};

var updateSavedSearch = function () {
    $.get('/rest/user/saved-search', function (data) {
        console.log(data);

        $('.seach-list').html('');

        if (data.searches.length > 0) {

            $('.saved-count').html('(' + data.searches.length + ')');

            var html = '<ul class="list-unstyled">';
            for (var i = 0; i < data.searches.length; i++) {
                search = data.searches[i];
                html = html + '<li><a href="/search?' + search.url + '">' + search.label + '</a></li>';
            }
            html = html + '</ul>';
            $('.seach-list').html(html);

        } else {
            var html = '<ul class="list-unstyled"><li>No saved search</li></ul>';
            $('.seach-list').html(html);
            $('.saved-count').html('');
        }
    });
};

$(function () {

    //

    updateSavedSearch();

    updateDimensions();

    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat0, lng: lng0},
        zoom: zl0
    });

    map.addListener('bounds_changed', mapBoundsListener = function () {
        reloadOnIdle = true;
    });

    map.addListener('idle', mapIdleListener = function () {
        if (reloadOnIdle) {
            currentPage = 1;
            applySearchCriteria();
        }
        updateDimensions();
    });

    initAutoComplete();

    $('.btn-map-search').on('click', function () {
        currentPage = 1;
        applySearchCriteria();
    });

    updatePriceRangeVisibility();

    $('.max-prices').hide();

    $('.lt').on('click', function () {
        // Update price range  
        updatePriceRangeVisibility();

        // disable project type        
        $('.pr').prop('checked', false);
    });

    $('.pt.co').on('click', function () {

        f_np = $(this).val();
        f_lt = '';

        $('.lt').prop('checked', false);

        if ($(this).prop('checked')) {
            $('.pt.nm').prop('disabled', true);
        } else {
            $('.pt.nm').prop('disabled', false);
        }

    });

    $('.min-price').on('click', function (event) {
        event.stopPropagation();
        $('.min-prices').hide();
        $('.max-prices').show();

        price_min = parseInt($(this).data('value'));
        var label = $(this).data('label');

        if (price_min > 0) {
            $('#price_min').val(price_min);
            min_price_label = label;
        } else {
            $('#price_min').val($(this).text().trim());
            min_price_label = null;
        }

        updatePriceRangeLabel();

        // check max price value

        if (price_max > 0 && price_max < price_min) {
            // User need to select a new max price
            price_max = 0;
            $('#price_max').val('');
        }

        $('.max-price').removeClass('inactive');

        $('.max-price').each(function (idx, elt) {
            var value = parseInt($(elt).data('value'));

            if (value > 0 && value <= price_min) {
                $(elt).addClass('inactive');
            }
        });

    });

    $('.max-price').on('click', function (event) {

        if ($(this).hasClass('inactive')) {
            event.stopPropagation();
        } else {

            $('.min-prices').show();
            $('.max-prices').hide();

            price_max = parseInt($(this).data('value'));
            var label = $(this).data('label');

            if (price_max > 0) {
                $('#price_max').val(price_max);
                max_price_label = label;
            } else {
                $('#price_max').val($(this).text().trim());
                max_price_label = null;
            }

            updatePriceRangeLabel();
        }
    });

    //

    $('.bed-item').on('click', function () {
        $('.bed-item').removeClass('active');
        f_bed = $(this).data('filter');
        $(this).addClass('active');
        $('.bed-count').html('(' + f_bed + ')');
    });

    $('.bath-item').on('click', function () {
        $('.bath-item').removeClass('active');
        f_bat = $(this).data('filter');
        $(this).addClass('active');
        $('.bath-count').html('(' + f_bat + ')');
    });

    // Save current search

    $('.save').on('click', function () {

        if (!userIsOnline) {
            alert(onOfflineMessage);
        } else {
            var url = generateSearchUrl();

            $.post('/rest/search/save', {url: url}, function () {
                $('#saved-message').modal('show');
                updateSavedSearch();
            });
        }
    });

    $('#property-preview').on('show.bs.modal', function (e) {
        
        console.log(currentPropertyData);
        
        var url = '';
        
        if (currentPropertyData.ltype === 'condo') {
            url = '/condo/preview/' + currentPropertyData.id;
        } else {
            url = '/property-preview/' + currentPropertyData.id;
        }
        
        $('#property-preview .modal-body').load(url, function () {
            var width = 0;
            $('.media-slider .media-group').each(function (index, value) {
                width = width + $(value).width() + 2.0;
            });
            $('.media-slider').width(width);

            showStreetView(parseFloat(currentPropertyData.lat), parseFloat(currentPropertyData.lng));
        });
    });

    $(".property-listing").mCustomScrollbar({scrollbarPosition: 'outside', theme: "dark-2"});

    $('.more-maps').on('click', function () {

        mapContainer.currentMapCenter = map.getCenter();
        if (mapContainer.haveBigMap) {
            mapContainer.haveBigMap = false;
            mapContainer.propertyListingWidth = mapContainer.propertyListingWidth + (mapContainer.listingWidth / 2.0);
        } else {
            mapContainer.haveBigMap = true;
            mapContainer.propertyListingWidth = mapContainer.propertyListingWidth - (mapContainer.listingWidth / 2.0);
            mapContainer.propertyListingWidth = Math.max(mapContainer.propertyListingWidth, 0);
        }
        var docWidth = $('.col-list').width();
        mapContainer.propertyMapWidth = docWidth - mapContainer.propertyListingWidth - 1;

        updateDimensions();

        /*applyUpdates();
         
         */

        google.maps.event.trigger(map, 'resize');

        map.setCenter(mapContainer.currentMapCenter);

    });

});

/**********************************
 *  Update price range values
 **********************************/

var updatePriceRangeVisibility = function () {
    $('.sale-prices').hide();
    $('.rent-prices').hide();

    f_np = '';

    if ($('#lt-sale').prop('checked') && $('#lt-rent').prop('checked')) {
        priceClass = '.sale-prices';
        $('.sale-prices').show();
        f_lt = 'rent,sale';

    } else if ($('#lt-sale').prop('checked')) {
        priceClass = '.sale-prices';
        $('.sale-prices').show();
        f_lt = 'sale';
    } else if ($('#lt-rent').prop('checked')) {
        priceClass = '.rent-prices';
        $('.rent-prices').show();
        f_lt = 'rent';
    } else {
        f_lt = '';
        $('.sale-prices').show();
    }
};

var updatePriceRangeLabel = function () {

    if (min_price_label === null && max_price_label === null) {
        $('.price-range-label').html('Any price');
    } else if (min_price_label !== null && max_price_label === null) {
        $('.price-range-label').html('Min ' + min_price_label);
    } else if (min_price_label === null && max_price_label !== null) {
        $('.price-range-label').html('Max ' + max_price_label);
    } else {
        $('.price-range-label').html(min_price_label + ' - ' + max_price_label);
    }
};

var generateSearchUrl = function () {

    var url = '?q=' + $('#q').val();

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();

    url = url + '&nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6);

    f_np = $('#np').prop('checked') ? 'yes' : '';

    url = url + '&lt=' + f_lt;
    url = url + '&np=' + f_np;
    url = url + '&pmin=' + price_min + '&pmax=' + price_max;

    var tmp = [];

    $('.pt:checked').each(function (idx, elt) {
        tmp.push($(elt).val());
    });

    url = url + '&pt=' + tmp.join(',');
    url = url + '&bed=' + f_bed;
    url = url + '&bath=' + f_bat;

    url = url + '&smin=' + $('#size_min').val();
    url = url + '&smax=' + $('#size_max').val();

    url = url + '&ymin=' + $('#year_min').val();
    url = url + '&ymax=' + $('#year_max').val();

    if (currentPage > 1) {
        url = url + '&page=' + currentPage;
    }

    if (currentId) {
        url = url + '&id=' + currentId;
    }

    if (domain) {
        url = url + '&rel=' + domain;
    }

    return url;

};

var currentUrl;

var applySearchCriteria = function () {
    updateMapResult();
    updateListResult();
};

var updateMapResult = function () {

    currentUrl = generateSearchUrl();

    $.get('/rest/search/pro/map' + currentUrl, function (searchResult) {
        loadPropertiesOnMap(searchResult/*data.properties*/);
    });
};

var updateListResult = function () {

    currentUrl = generateSearchUrl();

    $('.loading-results').show();

    window.history.pushState('page2', $('title').text(), '/search' + currentUrl);

    $.get('/rest/search/pro/list' + currentUrl, function (searchResult) {

        $('.loading-results').hide();

        reloadOnIdle = false;

        $('.property-count').html(searchResult.properties.total + ' properties');

        if (searchResult.properties.last_page > 1) {
            $('.search-results-pagination').pagination({
                items: searchResult.properties.total,
                itemsOnPage: searchResult.properties.per_page,
                currentPage: searchResult.properties.current_page,
                hrefTextPrefix: '#',
                displayedPages: 3,
                onPageClick: function (pageNum, event) {
                    currentPage = pageNum;
                    updateListResult();
                },
                selectOnClick: false
            });
        } else {
            $('.search-results-pagination').html('');
        }

        loadPropertiesOnList(searchResult/*searchResult.properties.data, searchResult.favorites*/);

    });

};

var propertyMarkers = [];
var markerCluster = null;
//var markerInfos = [];
var currentInfo = null;
var currentInfoXhr;
var currentMarker = null;

var loadPropertiesOnMap = function (searchResult) {
    
    if (propertyMarkers.length) {

        for (var i = 0; i < propertyMarkers.length; i++) {
            var marker = propertyMarkers[i];
            marker.setMap(null);
        }
        propertyMarkers = [];
        markerInfos = [];
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    currentMarker = null;

    if (searchResult.properties.length > 0) {
        for (var i = 0; i < searchResult.properties.length; i++) {
            var pdata = searchResult.properties[i];

            var markerIcon;

            if (pdata.np === true) {
                if (pdata.lclass === 'featured') {
                    markerIcon = '/img/maps/pin/pin-sale-featured.png';
                } else if (pdata.lclass === 'exclusive') {
                    markerIcon = '/img/maps/pin/pin-sale-exclusive.png';
                } else {
                    markerIcon = '/img/maps/pin/pin-sale-standard.png';
                }
            } else {
                if (pdata.lclass === 'featured') {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-featured.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-featured.png';
                    }
                } else if (pdata.lclass === 'exclusive') {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-exclusive.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-exclusive.png';
                    }
                } else {
                    if (pdata.ltype === 'rent') {
                        markerIcon = '/img/maps/pin/pin-rent-standard.png';
                    } else {
                        markerIcon = '/img/maps/pin/pin-sale-standard.png';
                    }
                }

            }



            var marker = new google.maps.Marker({
                position: {lat: parseFloat(pdata.lat), lng: parseFloat(pdata.lng)}, icon: {
                    url: markerIcon,
                    size: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(20, 0)
                }
            });

            marker.data = pdata;
            marker.data.type = searchResult.type;

            //var infowindow = 

            marker.infoIndex = propertyMarkers.length;

            marker.addListener('mouseover', function () {
                openMakerInfo(this);
            });

            marker.addListener('click', function () {
                console.log(this.data);
                showPropertyPreview(this.data);
                currentId = pdata.id;
            });



            if (domain !== 'poi' && currentId > 0 && pdata.id === currentId) {
                console.log('matching currentMarker');
                currentMarker = marker;
            }

            propertyMarkers.push(marker);
            //markerInfos.push(infowindow);
        }

        console.log('currentId', currentId);

        if (markerCluster) {
            markerCluster.addMarkers(propertyMarkers);
        } else {
            var clusterStyles = [
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                }
            ];

            markerCluster = new MarkerClusterer(map, propertyMarkers,
                    {
                        imagePath: '/img/m',
                        styles: clusterStyles
                    });
        }
    }

    if (currentMarker !== null) {
        console.log('currentMarker', currentMarker);
        openMakerInfo(currentMarker);
    } else if (currentId > 0) {
        //openMakerInfoById(currentId);
    }

    if (currentPoiData !== null) {

        console.log('currentPoiData', currentPoiData);

        if (currentPoiData.domain === 'poi') {
            var poimg = getImageUrl(currentPoiData.type);

            if (poimg) {
                var image = {
                    url: poimg,
                    size: new google.maps.Size(32, 37),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(16, 37)
                };

                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(currentPoiData.lat), lng: parseFloat(currentPoiData.lng)},
                    draggable: true,
                    title: currentPoiData.title,
                    icon: image,
                    map: map
                });
            }
        } else {
            openMakerInfoById(currentPoiData.id);
        }
    }
};

var currentMarker;

var openMakerInfoById = function (id) {
    var marker;
    console.log('openMakerInfoById');
    currentMarker = null;
    for (var i = 0; i < propertyMarkers.length; i++) {
        var marker = propertyMarkers[i];

        if (marker.data.id === id) {
            currentMarker = marker;
            console.log('marker', marker);
            openMakerInfo(marker);
        }
    }

    if (currentMarker === null) {
        console.log('marker not found');
        applySearchCriteria();
    }
};

var openMakerInfo = function (marker) {

    console.log('openMakerInfo');

    if (currentInfo) {
        currentInfo.close();
    }
    if (currentInfoXhr) {
        currentInfoXhr.abort();
    }

    var option = {
        disableAutoPan: false
        , maxWidth: 0
        , pixelOffset: new google.maps.Size(-125, -80)
        , zIndex: null
        , closeBoxURL: ""
        , infoBoxClearance: new google.maps.Size(1, 1)
        , isHidden: false
        , pane: "floatPane"
        , enableEventPropagation: false
    };

    currentInfo = new InfoBox(option);



    //currentInfo = new google.maps.InfoWindow();

    var rand = Math.random() * (1000000) + 1000000;

    currentInfoXhr = $.get('/rest/property/map-info/' + marker.data.id + '?type=' + marker.data.type + '&t=' + rand, markerInfoLoader.bind(marker));
};

var markerInfoLoader = function (data) {

    currentInfo.setContent(data);

    if (this.map !== null) {
        currentInfo.open(this.map, this);
        currentInfoXhr = null;
    } else {
        console.log('markerInfoLoader no map');
        currentInfo.setPosition({lat: parseFloat(this.data.lat), lng: parseFloat(this.data.lng)});
        currentInfo.open(map);
    }
};

var loadPropertiesOnList = function (searchResult) {
    
    $('.search-results').html('');

    if (searchResult.properties.data.length > 0) {

        $('.no-search-results').hide();

        for (var i = 0; i < searchResult.properties.data.length; i++) {
            var pdata = searchResult.properties.data[i];
            pdata.rank = i;
            var prop = new TFHProperty(pdata, searchResult.type);
            $('.search-results').append(prop.getTemplate(searchResult.favorites));
        }

        $('.favorite-btn').on('click', function (event) {

            event.stopPropagation();

            var elt = $(this);

            var pid = elt.data('id');

            if (pid) {
                $.get('/rest/user/favorite-property/' + pid + '?t=' + searchResult.type, function (data, status, xhr) {
                    if (data.status === 'OK') {
                        if (data.result === 'ADDED') {
                            elt.find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
                        } else {
                            elt.find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
                        }
                    } else {

                    }
                });
            }
        });

        if (sortAttr) {
            applySort();
        }

    } else {
        $('.no-search-results').show();
        $('.property-count').html('No property found');
    }
};

var sortAttr;
var sortAsc = true;

var sortby = function (attr, target) {

    if (sortAttr === attr) {
        sortAsc = !sortAsc;
    } else {
        sortAsc = true
    }
    sortAttr = attr;
    applySort();

    $('.nav-filter li').removeClass('active');
    $('#sort-' + target).addClass('active');
};

var applySort = function () {

    var sorted = $('.search-results div.property-item').sort(function (a, b) {
        var contentA = parseInt($(a).data(sortAttr));
        var contentB = parseInt($(b).data(sortAttr));

        if (sortAsc) {
            return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
        } else {
            return (contentA > contentB) ? -1 : (contentA < contentB) ? 1 : 0;
        }

    });
    $('.search-results').html('');
    $('.search-results').append(sorted);
    //console.log('applySort');
};

var currentPropertyData;

var showPropertyPreview = function (data) {

    currentPropertyData = data;

    $('#property-preview .modal-body').html('');
    $('#property-preview').modal('show');

    /*$('#property-title').html(data.property_name);*/
};

var previewMap;
var previewMarker;
var previewPoiMarker;

var showStreetView = function (lat, lng) {
    previewMap = null;
    previewMap = new google.maps.Map(
            document.getElementById('property-map'),
            {
                center: {lat: lat, lng: lng},
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

    previewMarker = new google.maps.Marker({
        position: {lat: lat, lng: lng},
        draggable: true,
        icon: '/img/maps/pin/pin-sale-standard.png',
        map: previewMap
    });

    $('.poi-list').on('change', function () {

        var optionSelected = $("option:selected", this);

        var options = $(optionSelected[0]).data();
        
        console.log(options);

        if (options.lat) {
            var poimg = getImageUrl(options.type);
            var location = {lat: parseFloat(options.lat), lng: parseFloat(options.lng)};

            var image = {
                url: poimg,
                size: new google.maps.Size(32, 37),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(16, 37)
            };

            if (previewPoiMarker) {
                previewPoiMarker.setMap(null);
                previewPoiMarker = null;
            }

            previewPoiMarker = new google.maps.Marker({
                position: location,
                draggable: true,
                icon: image,
                map: previewMap
            });
            
            var bounds = new google.maps.LatLngBounds();
            bounds.extend({lat: lat, lng: lng});
            bounds.extend(location);

            previewMap.fitBounds(bounds);
        }
        else {
            previewMap.panTo({lat: lat, lng: lng});
        }


    });
    
//    $('.share').socialShare({
//			social: 'facebook,twitter,google',
//			whenSelect: true,
//			selectContainer: '.share',
//			blur: true
//		});
    
//    $('.map-preview').on('click', function () {
//        
//        $('#property-preview').modal('close');
//
//        $('#big-map').on('shown.bs.modal', function (e) {
//            if (bigMap === null) {
//                var location = {lat: lat, lng: lng};
//                bigMap = new google.maps.Map(
//                        document.getElementById('big-map-container'),
//                        {
//                            center: location,
//                            zoom: 15
//                        });
//
//                var markerIcon = '/img/maps/pin/pin-sale-standard.png';
//
//                var marker = new google.maps.Marker({
//                    position: location,
//                    map: bigMap,
//                    icon: {
//                        url: markerIcon,
//                        size: new google.maps.Size(40, 40)
//                    }
//                });
//            } else {
//
//            }
//        });
//
//        $('#big-map').modal('show');
//    });
};

var getImageUrl = function (type) {
    
    var url = null;
    
    switch (type) {
        case 'bts':
            url = '/img/maps/selected/bts.png';
            break;
        case 'mrt':
            url = '/img/maps/selected/mrt.png';
            break;
        case 'apl':
            url = '/img/maps/selected/apl.png';
            break;
        case 'bank':
            url = '/img/maps/selected/bank.png';
            break;
        case 'dept-store':
            url = '/img/maps/selected/departmentstore.png';
            break;
        case 'hospital':
            url = '/img/maps/selected/hospital-building.png';
            break;
        case 'school':
            url = '/img/maps/selected/school.png';
            break;
        default:
            //url = '/img/maps/selected/bts.png';
            break;
    }

    return url;
};