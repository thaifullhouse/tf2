

var TfhMapSearch = function () {

    this.reloadOnIdle = true;
    this.reloadMap = true;
    this.haveBigMap = false;
    this.mapContainer = {
        listingWidth: 670,
        mapHeight: 0,
        propertyListingWidth: 0,
        propertyMapWidth: 0,
        docWidth: 0
    };

    this.propertyMarkers = [];
    this.markerCluster = null;
    this.markerInfos = [];
};

/**********************************
 *  Init map
 **********************************/

TfhMapSearch.prototype.init = function (lat, lng, zl) {

    this.updateDimensions();

    this.map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: lat, lng: lng},
        zoom: zl
    });
    this.map.addListener('bounds_changed', this.mapBoundsListener.bind(this));
    this.map.addListener('idle', this.mapIdleListener.bind(this));
    
    this.initAutoComplete();

};

TfhMapSearch.prototype.initFilters = function (request) {
    //this.filters = new SearchFilters(this, request);
};

/**********************************
 *  Listen to map bound changes
 **********************************/

TfhMapSearch.prototype.mapBoundsListener = function () {
    this.reloadOnIdle = true;
};

/**********************************
 *  Action to trigger when map is idle
 **********************************/

TfhMapSearch.prototype.mapIdleListener = function () {
    if (this.reloadOnIdle) {
        //this.filters.apply();
    }
    this.updateDimensions();
};

/**********************************
 *  Update page sections dimension
 **********************************/

TfhMapSearch.prototype.updateSectionDimension = function () {

    console.log('updateSectionDimension');
    this.mapContainer.docWidth = $('.search-result-container').width();
    this.mapContainer.propertyListingWidth = this.mapContainer.listingWidth;
    this.mapContainer.propertyMapWidth = this.mapContainer.docWidth;
    if (this.mapContainer.docWidth > 1280) {
        // 2-column listing
        this.mapContainer.propertyListingWidth = this.mapContainer.listingWidth;
    } else if (this.mapContainer.docWidth > 1024) {
        // 1-column listing
        this.mapContainer.propertyListingWidth = this.mapContainer.listingWidth / 2.0 + 5.0;
    } else {
        // map only
        this.mapContainer.propertyListingWidth = 0.0;
    }

    this.mapContainer.propertyMapWidth = this.mapContainer.docWidth - this.mapContainer.propertyListingWidth - 1;
    this.updateDimensions();
};

/**********************************
 *  Update map dimension
 **********************************/

TfhMapSearch.prototype.updateDimensions = function () {

    var mapOffset = $('#map').offset();
    this.mapContainer.mapHeight = $(window).height() - mapOffset.top;
    $('#map').height(this.mapContainer.mapHeight);
    $('.col-list').height(this.mapContainer.mapHeight).outerWidth(this.mapContainer.propertyListingWidth);
    $('.property-listing').height(this.mapContainer.mapHeight - 20);
    if (this.mapContainer.docWidth !== $('.search-result-container').width()) {
        this.updateSectionDimension();
    }

    if (this.mapContainer.propertyListingWidth > 335) {
        $('.cheap-filter').show();
    } else {
        $('.cheap-filter').hide();
    }
};

/**********************************
 *  Apply search filters
 **********************************/

TfhMapSearch.prototype.sendSearchQuery = function (url) {

    //var service = 'http://tfh.ladargroup.com/rest/search/property?' + url;

    var service = '/rest/search/property?' + url;

    $('.loading-results').show();



    var xhr = $.get(service);

    var propertyLoader = function (data) {
        if (data.status === 'OK') {
            this.loadProperties(data.properties, data.favorites);
        }
    };

    xhr.done(propertyLoader.bind(this));

    xhr.always(function () {
        $('.loading-results').hide();
    });
};

TfhMapSearch.prototype.loadProperties = function (list, favorites) {
    $('.search-results').html('');

    if (this.propertyMarkers.length) {
        for (var i = 0; i < this.propertyMarkers.length; i++) {
            var marker = this.propertyMarkers[i];
            marker.setMap(null);
        }
        this.propertyMarkers = [];
        this.markerInfos = [];
    }

    if (this.markerCluster) {
        this.markerCluster.clearMarkers();
    }

    /**/

    if (list.length > 0) {


        for (var i = 0; i < list.length; i++) {
            var pdata = list[i];
            var prop = new TFHProperty(pdata);
            $('.search-results').append(prop.getTemplate(favorites));
            //console.log('append + ' + i);

            var markerIcon;

            if (pdata.listing_type === 'rent') {
                markerIcon = '/img/pin-blue.png';
            } else {
                markerIcon = '/img/pin-red.png';
            }

            var marker = new google.maps.Marker({
                position: {lat: parseFloat(pdata.lat), lng: parseFloat(pdata.lng)}, icon: {
                    url: markerIcon,
                    size: new google.maps.Size(40, 40),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(20, 0)
                }
            });

            marker.data = pdata;

            var tpl = $('#marker-info').html();
            var elt = $(tpl);

            elt.find('.price').html('‎฿ ' + numberWithCommas(pdata.details.listing_price));
            elt.find('.beds').html(pdata.details.bedroom_num);
            elt.find('.baths').html(pdata.details.bathroom_num);
            elt.find('.size').html(pdata.details.total_size);
            elt.find('.marker-img img').prop('src', prop.getMainImage());

            var infowindow = new google.maps.InfoWindow({
                content: elt[0].outerHTML
            });

            marker.infoIndex = this.propertyMarkers.length;

            marker.addListener('mouseover', function () {
                var info = markerInfos[this.infoIndex];
                if (this.currentInfo) {
                    this.currentInfo.close();
                }
                info.open(this.map, this);
                this.currentInfo = info;
            });

            marker.addListener('click', function () {
                console.log(this.data);
                showPropertyPreview(this.data);
            });

            this.propertyMarkers.push(marker);
            this.markerInfos.push(infowindow);
        }

        $('.favorite-btn').on('click', function (event) {

            event.stopPropagation();

            var elt = $(this);

            var pid = elt.data('id');

            if (pid) {
                $.get('/rest/user/favorite-property/' + pid, function (data, status, xhr) {
                    if (data.status === 'OK') {
                        if (data.result === 'ADDED') {
                            elt.find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
                        } else {
                            elt.find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
                        }
                    } else {

                    }
                });
            }
        });

        if (this.markerCluster) {
            this.markerCluster.addMarkers(propertyMarkers);
        } else {
            var clusterStyles = [
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                },
                {
                    textColor: 'white',
                    url: '/img/m1.png',
                    height: 40,
                    width: 40
                }
            ];

            this.markerCluster = new MarkerClusterer(this.map, propertyMarkers,
                    {
                        imagePath: '/img/m',
                        styles: clusterStyles
                    });
        }
        $('.property-count').html(list.length + ' properties');

        if (this.sortAttr) {
            applySort();
        }
    } else {
        //console.log('list.length = 0');
        $('.property-count').html('No property found');
    }
};

TfhMapSearch.prototype.initAutoComplete = function () {
    $('#q').autocomplete({
        source: "/rest/search-autocomplete",
        minLength: 2,
        classes: {
            "ui-autocomplete": "map-search"
        },
        select: this.selectSuggested.bind(this),
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $('<li class="poi-item">').addClass('prop').addClass(item.type).data('lat', item.lat).data('lng', item.lng).data('zl', item.zoom_level)
                .attr("data-value", item.id)
                .append('<span class="icon ' + item.type + '"></span><span class="name">' + item.value + '</span><span class="poi-type text-muted">' + item.type_label + '</span>' +
                        '<span class="more">' + item.more + '</span>')
                .appendTo(ul);
    };
};

TfhMapSearch.prototype.selectSuggested = function (event, ui) {

    console.log(ui.item);

    var lat = parseFloat(ui.item.lat);
    var lng = parseFloat(ui.item.lng);
    var zl = parseFloat(ui.item.zoom_level);
    this.map.panTo({lat: lat, lng: lng});
    this.map.setZoom(zl);

    if (ui.item.domain === 'poi') {
        var image = {
            url: getImageUrl(ui.item.type),
            size: new google.maps.Size(32, 37),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(16, 37)
        };

        var marker = new google.maps.Marker({
            position: {lat: lat, lng: lng},
            draggable: true,
            title: ui.item.item,
            icon: image,
            map: this.map
        });
    } else {
        // Display 
    }

    var url = '/search?rel=' + ui.item.domain + '&lat=' + lat + '&lng=' + lng + '&pid=' + ui.item.id + '&pr=' + ui.item.pr;

    window.location = url;

};


/**********************************
 *  On map resize
 **********************************/

/**********************************
 *  Point of interest marker
 **********************************/
var presentPoiMarker = function () {

    if (currentPoiData !== null) {
        map.panTo({lat: currentPoiData.lat, lng: currentPoiData.lng});
        map.setZoom(currentPoiData.zl);
        var image = {
            url: getImageUrl(currentPoiData.type),
            size: new google.maps.Size(32, 37),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(16, 37)
        };
        /*if (currentPoi && currentPoi.setMap) {
         currentPoi.setMap(null);
         currentPoi = null;
         }*/

        var marker = new google.maps.Marker({
            position: {lat: currentPoiData.lat, lng: currentPoiData.lng},
            draggable: true,
            title: currentPoiData.title,
            icon: image,
            map: map
        });
        console.log(marker);
    }
};
/**********************************
 *  Property marker
 **********************************/
var presentProMarker = function () {

};

$(function () {
    var mapSearch = new TfhMapSearch();
    mapSearch.init(lat0, lng0, zl0);
    mapSearch.initFilters(searchCriteria);
    window.onresize = mapSearch.updateSectionDimension;
});