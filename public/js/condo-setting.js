var condo_id = condo_id || 0;

var addRoomType = function () {
    var formData = {
        name: $('#room_name').val(),
        size: $('#room_size').val()
    };
    $.post("/manager/condo/add-room/" + condo_id, formData, function (response) {
        $("#room-type-container").html(response);
        $('#room_name').val('');
        $('#room_size').val('');
    });
};

var deleteRoomType = function (roomId) {
    $.get("/manager/condo/delete-room/" + condo_id + "/" + roomId, function (response) {
        $("#room-type-container").html(response);
    });
};

var updateRoom = function (roomId) {
    var formData = {
        name: $('#room_name_' + roomId).val(),
        size: $('#room_size_' + roomId).val(),
        id: roomId
    };
    $.post("/manager/condo/update-room/" + condo_id + "/" + roomId, formData, function (response) {
        $("#room-type-container").html(response);
    });
};

var showUpdate = function (id)
{
    $(id).show();
};