var panorama;
var latlng = latlng || {};
var google = google || {};

var showStreetView = function (lat, lng) {
    panorama = null;
    panorama = new google.maps.Map(
            document.getElementById('property-map'),
            {
                center: {lat: lat, lng: lng},
                zoom: 15
            });
    console.log('showStreetView');
};

var propertyShare = function (pid) {
    
};



var bigMap = null;

$(document).ready(function () {
    
    $('.social-share').socialShare({
        social: 'facebook,google,twitter'
    });

    showStreetView(parseFloat(latlng.lat), parseFloat(latlng.lng));

    var width = 0;

    $('.media-slider .media-group').each(function (index, value) {
        width = width + $(value).width() + 2.0;
    });
    $('.media-slider').width(width);

    console.log('width', width);

    /*
     $('.contact-box').affix({
     offset: 15
     });
     */

    $('.map-preview').on('click', function () {
        console.log('map-preview');

        $('#big-map').on('shown.bs.modal', function (e) {
            if (bigMap === null) {
                var location = {lat: parseFloat(latlng.lat), lng: parseFloat(latlng.lng)};
                bigMap = new google.maps.Map(
                        document.getElementById('big-map-container'),
                        {
                            center: location,
                            zoom: 15
                        });

                var markerIcon = '/img/pin-blue.png';

//                if (pdata.listing_type === 'rent') {
//                    markerIcon = '/img/pin-blue.png';
//                } else {
//                    markerIcon = '/img/pin-red.png';
//                }

                var marker = new google.maps.Marker({
                    position: location,
                    map: bigMap,
                    icon: {
                        url: markerIcon,
                        size: new google.maps.Size(40, 40)
                    }
                });
            } else {

            }
        });

        $('#big-map').modal('show');
    });
});
