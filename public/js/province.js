var enableAndLoadDistricts = function (provinceId, callback)
{
    $('#district_id').prop('disabled', false);

    $.getJSON('/rest/location/district/' + provinceId, function (data, status, xhr) {
        if (data.length > 0) {
            $('#district_id').html('<option value="0">Select a district</option>');

            for (var i in data) {
                $('#district_id').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            }
        } else {
            $('#district_id').html('<option value="0">Sorry, no district available</option>');
        }

        if (callback) {
            callback.call(this);
        }
    });
};

var disableAndClearDistricts = function ()
{
    $('#district_id').prop('disabled', true);
    $('#district_id').html('<option value="0">Please select a province</option>');
    disableAndClearArea();
};

var enableAndLoadArea = function (district_id, callback)
{
    $('#area_id').prop('disabled', false);

    $.getJSON('/rest/location/area/' + district_id, function (data, status, xhr) {
        if (data.length > 0) {
            $('#area_id').html('<option value="0">Select an area</option>');

            for (var i in data) {
                $('#area_id').append('<option value="' + data[i].id + '">' + data[i].name + '</option>');
            }
        } else {
            $('#area_id').html('<option value="0">Sorry, no area available</option>');
        }
        if (callback) {
            callback.call(this);
        }
    });
};

var disableAndClearArea = function ()
{
    $('#area_id').prop('disabled', true);
    $('#area_id').html('<option value="0">Please select a district</option>');
};


$(document).ready(function () {
    //-----

    $('#province_id').on('change', function () {
        var province_id = $(this).val();

        disableAndClearDistricts();

        if (parseInt(province_id) > 0) {
            $('#province_error').hide();
            enableAndLoadDistricts(province_id);
        } else {
            $('#province_error').show();
        }
    });

    // --------

    $('#district_id').on('change', function () {
        var district_id = $(this).val();

        disableAndClearArea();

        if (parseInt(district_id) > 0) {
            enableAndLoadArea(district_id);
            $('#district_error').hide();
        } else {
            $('#district_error').show();
        }
    });


});