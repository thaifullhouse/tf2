/* Search filters */

var searchCriteria = searchCriteria || [];
var currentPid = currentPid || null;

var google = google || {};
var lat0 = lat0 || 0;
var lng0 = lng0 || 0;
var zl0 = zl0 || 0;

var currentPoi = currentPoi || {};
var currentPoiData = currentPoiData || {};

var currentProperty;
/* Transportation */

var currentPropertyData;

var showPropertyPreview = function (data) {

    currentPropertyData = data;

    $('#property-preview .modal-body').html('');
    $('#property-preview').modal('show');

    console.log(data);
};

var propertyToggleFavorite = function(pid){
    $.get('/rest/user/favorite-property/' + pid, function(data, status, xhr){
        if (data.status === 'OK') {
            if (data.result === 'ADDED') {
                $('.favorite').find('.fa').removeClass('fa-heart-o').addClass('fa-heart').addClass('text-danger');
            }
            else {
                $('.favorite').find('.fa').removeClass('fa-heart').removeClass('text-danger').addClass('fa-heart-o');
            }
        }
        else {
            
        }
    });
};

var showFullProperty = function () {
    if (currentPropertyData) {
        window.location = currentPropertyData.dlink;
    }
};

var sortAttr;

var sortby = function (attr) {
    sortAttr = attr;
    applySort();
};

var applySort = function () {

    var sorted = $('.search-results div.property-item').sort(function (a, b) {
        var contentA = parseInt($(a).data(sortAttr));
        var contentB = parseInt($(b).data(sortAttr));
        return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    });
    /*$('.search-results').html('');*/
    $('.search-results').append(sorted);
    //console.log('applySort');
};



$(document).ready(function () {
    
    

    $('#property-preview').on('show.bs.modal', function (e) {
        $('#property-preview .modal-body').load(currentPropertyData.plink, function () {
            var width = 0;
            $('.media-slider .media-group').each(function (index, value) {
                width = width + $(value).width() + 2.0;
            });
            $('.media-slider').width(width);

            $('.contact-box').affix({
                offset: 15
            });

            showStreetView(parseFloat(currentPropertyData.lat), parseFloat(currentPropertyData.lng));
        });
    });

    updateListingTypes();
    updateHomeTypes();

    updateSectionDimension();
    initMap();

    $('.more-maps').on('click', function () {

        currentMapCenter = map.getCenter();
        if (haveBigMap) {
            haveBigMap = false;
            propertyListingWidth = propertyListingWidth + (listingWidth / 2.0);
        } else {
            haveBigMap = true;
            propertyListingWidth = propertyListingWidth - (listingWidth / 2.0);
            propertyListingWidth = Math.max(propertyListingWidth, 0);
        }
        var docWidth = $('.col-list').width();
        propertyMapWidth = docWidth - propertyListingWidth - 1;

        applyUpdates();

        google.maps.event.trigger(map, 'resize');

        map.setCenter(currentMapCenter);

    });


    $('.nav-filter li').on('click', function () {
        $('.nav-filter li').removeClass('active');
        $(this).addClass('active');
    });

    $('.max-prices').hide();

    $(".property-listing").mCustomScrollbar({scrollbarPosition: 'outside', theme: "dark-2"});

    var hl = hl || '';

    

});



var panorama;

var showStreetView = function (lat, lng) {
    panorama = null;
    panorama = new google.maps.StreetViewPanorama(
            document.getElementById('street-view'),
            {
                position: {lat: lat, lng: lng},
                pov: {heading: 165, pitch: 0},
                zoom: 1
            });
    console.log('showStreetView');
};