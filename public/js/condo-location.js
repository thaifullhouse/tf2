var lat = lat || 0;
var lng = lng || 0;

var updateArea = function (addressComponents)
{
    console.log(addressComponents);
    area = addressComponents.streetName + ', ' + addressComponents.district;
    $('#address').val(area);
    $('#postcode').val(addressComponents.postalCode);
};

$('#map-picker').locationpicker({

    location: {latitude: lat, longitude: lng},
    radius: 0,
    enableAutocomplete: true,
    autocompleteSetting: {
        componentRestrictions: {country: "th"}
    },
    inputBinding: {
        latitudeInput: $('#lat'),
        longitudeInput: $('#lng'),
        locationNameInput: $('#area')
    },
    onchanged: function (currentLocation, radius, isMarkerDropped) {
        var addressComponents = $(this).locationpicker('map').location.addressComponents;
        updateArea(addressComponents);
    },
    oninitialized: function (component) {
        var addressComponents = $(component).locationpicker('map').location.addressComponents;
        updateArea(addressComponents);
    }
});

$('#clear-area-btn').on('click', function () {
    $('#area').val('');
});