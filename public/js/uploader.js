$(document).ready(function () {

    $('.upload-trigger').on('click', function () {
        var target = $(this).data('target');
        $(target).trigger('click');
    });

    $('.image-input').on('change', function () {
        var action = $(this).data('action');
        var progress = $(this).data('progress');
        var data = new FormData();

        if (this.files.length > 0) {
            data.append('image', this.files[0]);

            $.ajax({
                url: action,
                type: "POST",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // tell jQuery not to process the data
                contentType: false,
                xhr: function () {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {                            
                            if (progress) {
                                var percentComplete = 100 * (evt.loaded / evt.total);
                                $(progress).width(percentComplete + '%');
                            }
                        }
                    }, false);

                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {                            
                            if (progress) {
                                var percentComplete = 100 * (evt.loaded / evt.total);
                                $(progress).width(percentComplete + '%');
                            }                            
                        }
                    }, false);

                    return xhr;
                }
            }).done(function (data) {
                window.location.reload();
            });
        }

    });

    $('.image-multiple').on('change', function () {
        var action = $(this).data('action');
        var progress = $(this).data('progress');
        var data = new FormData();

        if (this.files.length > 0) {

            for (var i = 0; i < this.files.length; i++) {
                data.append('images[]', this.files[i]);
            }

            $.ajax({
                url: action,
                type: "POST",
                data: data,
                enctype: 'multipart/form-data',
                processData: false, // tell jQuery not to process the data
                contentType: false,
                xhr: function () {
                    var xhr = new XMLHttpRequest();
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {                            
                            if (progress) {
                                var percentComplete = 100 * (evt.loaded / evt.total);
                                $(progress).width(percentComplete + '%');
                            }
                        }
                    }, false);

                    xhr.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {                            
                            if (progress) {
                                var percentComplete = 100 * (evt.loaded / evt.total);
                                $(progress).width(percentComplete + '%');
                            }                            
                        }
                    }, false);

                    return xhr;
                }
            }).done(function (data) {
                window.location.reload();
            });
        }

    });

});