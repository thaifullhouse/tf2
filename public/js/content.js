$('[contenteditable]').on('focus', function() {
    var $this = $(this);
    $this.data('before', $this.text());
});

$('[contenteditable]').on('blur', function() {
    var $this = $(this);
    if ($this.data('before') !== $this.text()) {
        console.log('changed', $this.data('before'));
        $this.data('after', $this.text());
        
        $.post('/rest/content/update', $this.data(), function(){
            $.notify("Updated", 'success', {elementPosition : 'bottom center', autoHideDelay : 2000});
        });
    }    
});

$('.setting-data-item').on('focus', function() {
    console.log('focus');
    var $this = $(this);
    $this.data('before', $this.val());
});

$('.setting-data-item').on('blur', function() {
    updateSetting(this);
});

var updateSetting = function(elt){
    console.log(elt);
    var $this = $(elt);
    if ($(elt).data('before') !== $(elt).val()) {
        console.log('changed', $(elt).data('before'));
        $(elt).data('after', $(elt).val());
        
        $.post('/rest/content/setting', $(elt).data(), function(){
            if ($.notify) {
                $.notify("Updated", 'success', {elementPosition : 'bottom center', autoHideDelay : 2000});
            }            
        });
    }    
};