"user strict";

var google = google || {};
var map, reloadPointsOnIdle;
var allpois = [];
var filters = [];

var poiMarkers = [];
var markerCluster;

/*
 * Manager points
 */

var initPointsMap = function () {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 13.756705, lng: 100.505188},
        zoom: 15
    });

    map.addListener('bounds_changed', function () {
        reloadPointsOnIdle = true;
    });

    map.addListener('idle', function () {
        $('#zl').val(map.getZoom());
        if (reloadPointsOnIdle) {
            // reload points
            updateFilterValues();
            loadCurrentPois();
        }
    });

    google.maps.event.addListener(map, 'click', function (event) {
        placeNewPoi(event.latLng);
    });
};

var newMarker, currentMarker;

var placeNewPoi = function (latlng) {
    if (newMarker) {
        newMarker.setMap(null);
    }
    newMarker = new google.maps.Marker({
        position: latlng,
        draggable: true,
        map: map
    });
    showNewPoiDialog.call(newMarker);
    newMarker.addListener('click', showNewPoiDialog.bind(newMarker));
    newMarker.addListener('dragend', updateLocation.bind(newMarker));

    $('#marker-error').hide();
};

var showNewPoiDialog = function () {
    console.log('click', this);
    var location = parseFloat(this.position.lat()).toFixed(6) + ',' + parseFloat(this.position.lng()).toFixed(6);
    $('#location').val(location);

    if (this.data) {
        $('#name_th').val(this.data.name.th);
        $('#name_en').val(this.data.name_en);
        $('#poi-id').val(this.data.id);
        $('#type').val(this.data.type);
        if (newMarker) {
            newMarker.setMap(null);
            newMarker = null;
        }
        currentMarker = this;
        $('#create-poi').hide();
    } else {
        $('#name_th').val('');
        $('#name_en').val('');
        $('#poi-id').val('');
        $('#create-poi').show();
        currentMarker = null;
    }
};

var updateLocation = function () {
    var location = parseFloat(this.position.lat()).toFixed(6) + ',' + parseFloat(this.position.lng()).toFixed(6);
    $('#location').val(location);

    console.log('dragend', this);

    if (this.data) {
        $('#name_th').val(this.data.name_th);
        $('#name_en').val(this.data.name_en);
        $('#poi-id').val(this.data.id);
        $('#type').val(this.data.type);
        if (newMarker) {
            newMarker.setMap(null);
            newMarker = null;
        }
        currentMarker = this;
        savePoi();
    } else {
        currentMarker = null;
    }
};

var savePoi = function () {

    console.log('asdasd');

    $('.poi-editor .alert').hide();

    if (newMarker || currentMarker) {
        var name_en = $('#name_en').val().trim();
        var name_th = $('#name_th').val().trim();

        if (name_en === '' || name_th === '') {
            $('#name-error').show();
        } else {
            $.post('/manager/point/create', {
                'location': $('#location').val(),
                'name_th': name_th,
                'name_en': name_en,
                'type': $('#type').val(),
                'zl': $('#zl').val(),
                'id': $('#poi-id').val()
            }, function (data, status, xhr) {
                $('.poi-message').hide();
                $('#create-poi').hide();
                if (data.action === 'updated') {
                    $('.poi-message.action-updated').show();
                } else {
                    $('.poi-message.action-created').show();
                }
                clearForm();
                loadCurrentPois();
            });
        }
    } else {
        $('#marker-error').show();
    }

};

var loadCurrentPois = function () {

    if (poiMarkers.length) {
        for (var i = 0; i < poiMarkers.length; i++) {
            var marker = poiMarkers[i];
            marker.setMap(null);
        }
        poiMarkers = [];
        markerInfos = [];
    }

    if (markerCluster) {
        markerCluster.clearMarkers();
    }

    if (newMarker) {
        newMarker.setMap(null);
        newMarker = null;
    }

    var bounds = map.getBounds();
    var ne = bounds.getNorthEast();
    var sw = bounds.getSouthWest();
    url = 'nl=' + sw.lat().toFixed(6) + ',' + sw.lng().toFixed(6) + '&fr=' + ne.lat().toFixed(6) + ',' + ne.lng().toFixed(6) + '&filter=' + filters.join(',');

    $.get('/manager/point/entries?' + url, function (data, status, xhr) {
        removeAll();
        console.log(data.entries);
        if (data.entries.length > 0) {
            for (var i = 0; i < data.entries.length; i++) {
                var entry = data.entries[i];
                var image = {
                    url: getImageUrl(entry.type),
                    // This marker is 20 pixels wide by 32 pixels high.
                    size: new google.maps.Size(32, 37),
                    // The origin for this image is (0, 0).
                    origin: new google.maps.Point(0, 0),
                    // The anchor for this image is the base of the flagpole at (0, 32).
                    anchor: new google.maps.Point(16, 37)
                };
                var marker = new google.maps.Marker({
                    position: {lat: parseFloat(entry.lat), lng: parseFloat(entry.lng)},
                    draggable: true,
                    title: entry.name_en + ' | ' + entry.name_th,
                    icon: image/*,
                     map: map*/
                });
                marker.data = entry;

                marker.addListener('click', showNewPoiDialog.bind(marker));
                marker.addListener('dragend', updateLocation.bind(marker));

                allpois.push(marker);

                poiMarkers.push(marker);
            }

            if (markerCluster) {
                markerCluster.addMarkers(poiMarkers);
            } else {
                var clusterStyles = [
                    {
                        textColor: 'white',
                        url: '/img/m1.png',
                        height: 40,
                        width: 40
                    },
                    {
                        textColor: 'white',
                        url: '/img/m1.png',
                        height: 40,
                        width: 40
                    },
                    {
                        textColor: 'white',
                        url: '/img/m1.png',
                        height: 40,
                        width: 40
                    }
                ];

                markerCluster = new MarkerClusterer(map, poiMarkers,
                        {
                            imagePath: '/img/m',
                            styles: clusterStyles
                        });
            }
        }
    });
};

var removeAll = function () {
    if (allpois.length > 0) {
        for (var i = 0; i < allpois.length; i++) {
            var marker = allpois[i];
            if (marker.setMap) {
                marker.setMap(null);
            }
        }
        allpois = [];
    }
};

var getImageUrl = function (type) {
    var url = '';
    switch (type) {
        case 'bts':
            url = '/img/maps/selected/bts.png';
            break;
        case 'mrt':
            url = '/img/maps/selected/mrt.png';
            break;
        case 'apl':
            url = '/img/maps/selected/apl.png';
            break;
        case 'bank':
            url = '/img/maps/selected/bank.png';
            break;
        case 'dept-store':
            url = '/img/maps/selected/departmentstore.png';
            break;
        case 'hospital':
            url = '/img/maps/selected/hospital-building.png';
            break;
        case 'school':
            url = '/img/maps/selected/school.png';
            break;
        default:
            url = '/img/maps/selected/bts.png';
            break;
    }

    return url;
};

var clearForm = function () {
    $('#name_th').val('');
    $('#name_en').val('');
    $('#poi-id').val('');
    $('#location').val('');
};

var updateFilterValues = function () {
    var selected = $('.poi-filter:checked');
    filters = [];
    if (selected.length > 0) {
        for (var i = 0; i < selected.length; i++) {
            var filter = selected[i];
            filters.push($(filter).val());
        }
    }
};

$(function () {
    initPointsMap();

    $('.poi-filter').on('click', function () {
        updateFilterValues();
        loadCurrentPois();
    });

    $('.select-all').on('click', function () {
        $('.poi-filter').prop('checked', true);
        updateFilterValues();
        loadCurrentPois();
    });
});