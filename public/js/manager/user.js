$(function () {

    $('.user-status').on('click', function (event) {
        var checked = $(this).is(':checked');

        var userId = $(this).data('id');

        $.post('/manager/user/status', {id: userId, active: checked}, function () {
            if ($.notify) {
                $.notify("Updated", 'success', {elementPosition: 'bottom center', autoHideDelay: 1000});
            }
        });

    });

});