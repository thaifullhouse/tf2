$(function(){
    
    $('#image-tab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    });
    $('#image-tab a:first').tab('show');
    
    $('.banner-image').on('change', function () {
        var file = this;
        sendFile(file);
    });
    
});


var sendFile = function (input) {

    var data = new FormData();
    
    var progress = $(input).data('progress');

    if (input.files.length > 0) {
        
        data.append('image', input.files[0]);
        var options = $(input).data();
        
        for(var key in options) {
            data.append(key, options[key]);
        }
        
        $.ajax({
            url: '/manager/banner/upload',
            type: "POST",
            data: data,
            enctype: 'multipart/form-data',
            processData: false, // tell jQuery not to process the data
            contentType: false,
            xhr: function () {
                var xhr = new XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        if (progress) {
                            var percentComplete = 100 * (evt.loaded / evt.total);
                            $(progress).width(percentComplete + '%');
                        }
                    }
                }, false);

                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        if (progress) {
                            var percentComplete = 100 * (evt.loaded / evt.total);
                            $(progress).width(percentComplete + '%');
                        }
                    }
                }, false);

                return xhr;
            }
        }).done(function (data) {
            window.location.reload();
        });
    }



};