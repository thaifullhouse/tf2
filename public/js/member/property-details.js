var currentFileIndex = 0;
var totalFileNumber = 0;
var formData = new FormData();

$(function () {
    
    $(".bts").chosen({max_selected_options: 4});
    $(".mrt").chosen({max_selected_options: 4});
    $(".apl").chosen({max_selected_options: 4});
    
    $('#agree_tac').on('click', function(){
        
    });

    $('.facility-item').on('click', function () {
        console.log('fsdfsdf');
        $(this).toggleClass('active')

        var ref = $(this).data('ref');

        if ($(this).hasClass('active')) {
            $(ref).prop("checked", true);
        } else {
            $(ref).prop("checked", false);
        }
    });
    
    formData.set('property_id', $('#property_id').val());
    
    var $inputImage = $("#prop_image");
    if (window.FileReader) {
        $inputImage.on('change', function () {
            console.log(this);
            var fileReader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            currentFileIndex = 0;
            totalFileNumber = files.length;

            readNextFile(files, fileReader);
        });
    } else {
        $inputImage.addClass("hide");
        console.log('FileReader not supported');
    }

    var sendImages = function () {

        console.log(formData);

        $('.upload-progress-container').show();

        var xhr = $.ajax({
            url: '/member/listing/upload',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            xhr: function () {
                var mxhr = $.ajaxSettings.xhr();
                if (mxhr.upload) {
                    mxhr.upload.addEventListener('progress', progress, false);
                }
                return mxhr;
            },
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');

                $.notify("Uploa ded successfully", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
                setTimeout(function () {
                    //window.location.reload(true);
                }, 2000);
            }
        });
    };

    function progress(e) {

        if (e.lengthComputable) {
            var max = e.total;
            var current = e.loaded;
            var percent = (current * 100) / max;
            $('.progress-bar').css('width', percent + '%');
            $('#progress-label').html(percent.toFixed(2) + '%');

            if (percent >= 100) {
                $('#progress-label').html(percent.toFixed(2) + '%');
            }
        }
    }

    var readNextFile = function (files, reader) {

        file = files[currentFileIndex];

        if (/^image\/\w+$/.test(file.type)) {

            formData.append('images[]', file);

            reader.readAsDataURL(file);
            reader.onload = function () {

                var div = document.createElement("div");
                $(div).addClass('col-md-3').addClass('img-preview');
                var img = document.createElement("img");
                $(img).prop('src', reader.result);
                $(img).addClass('img-responsive');
                $(div).append(img)
                $('.img-preview-container').append(div);

                currentFileIndex++;

                if (currentFileIndex < totalFileNumber) {
                    readNextFile(files, reader);
                }

                if (currentFileIndex == totalFileNumber) {
                    sendImages();
                }
            };

        } else {

        }
    };


});

var deleteMedia = function (mediaId, propertyId) {
    $.get('/member/listing/delete-media?mid=' + mediaId + '&pid=' + propertyId, function (response) {
        $('#property-media-container').html(response);
    });
};