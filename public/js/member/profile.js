$(document).ready(function () {

    $(".languages").chosen({});

    $(".birthday").datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true,
        beforeShow: function (input, inst) {
            $('#birthday').data('before', $('#birthday').val());
        },
        onClose: function (dateText, inst) {
            var data = {
                'before': $('#birthday').data('before'),
                'after': dateText,
                'name': 'birthday'
            };
            $.post('/member/settings/update', data, function () {
                if ($.notify) {
                    $.notify("Updated", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
                }
            });
        }
    });

    /* Upload cover picture */

    var formData = new FormData();

    var sendCover = function () {

        console.log(formData);

        $('.cover-loading').show();

        var xhr = $.ajax({
            url: '/member/settings/upload-cover',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');
                console.log('finished');

                $('.cover-loading').hide();

                $.notify("Uploaded successfully", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            }
        });
    };

    var sendProfile = function () {

        console.log(formData);

        //$('.cover-loading').show();

        var xhr = $.ajax({
            url: '/member/settings/upload-profile',
            data: formData,
            type: 'POST',
            contentType: false,
            processData: false,
            complete: function (xhr) {
                $('.progress-bar').addClass('progress-bar-success');
                console.log('finished');
                //$('.cover-loading').hide();
                $.notify("Uploaded successfully", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
                setTimeout(function () {
                    window.location.reload(true);
                }, 2000);
            }
        });
    };

    var $coverImage = $("#cover");
    var $profileImage = $("#profile");
    if (window.FileReader) {
        $coverImage.on('change', function () {
            formData = new FormData();
            var reader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {

                formData.set('image', file);

                reader.readAsDataURL(file);
                reader.onload = function () {
                    $('#cover-img').prop('src', reader.result);
                    sendCover();
                };

            }
        });

        $profileImage.on('change', function () {
            formData = new FormData();
            var reader = new FileReader(),
                    files = this.files,
                    file;

            if (!files.length) {
                return;
            }

            file = files[0];

            if (/^image\/\w+$/.test(file.type)) {

                formData.set('image', file);

                reader.readAsDataURL(file);
                reader.onload = function () {
                    $('#profile-img').prop('src', reader.result);
                    sendProfile();
                };

            }
        });

    } else {
        $profileImage.addClass("hide");
    }

    $('.setting-data-item').on('focus', function () {
        console.log('focus');
        var $this = $(this);
        $this.data('before', $this.val());
    });

    $('.setting-data-item').on('blur', function () {
        updateSetting(this);
    });

    $('select.data-select').on('change', function () {
        updateSetting(this);
    });
});

var updateSetting = function (elt) {
    console.log(elt);
    var $this = $(elt);
    if ($(elt).data('before') !== $(elt).val()) {
        console.log('changed', $(elt).data('before'));
        $(elt).data('after', $(elt).val());

        console.log($(elt).data());

        $.post('/member/settings/update', $(elt).data(), function () {
            if ($.notify) {
                $.notify("Updated", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
            }
        });
    }
};

var updateLanguages = function (elt) {
    console.log(elt);
    var $this = $(elt);

    var current = $this.val().join(',');

    if ($this.data('before') !== current) {
        console.log('changed', $this.data('before'));
        $this.data('after', current);

        var data = $this.data();

        console.log();

        $.post('/member/settings/update', {name: data.name, after: data.after}, function () {
            if ($.notify) {
                $.notify("Updated", 'success', {elementPosition: 'bottom center', autoHideDelay: 2000});
            }
        });
    }
};


