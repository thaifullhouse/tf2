
var currentHL = currentHL || null;
/*
 var initAutoComplete = function () {
 $("#property_name").autocomplete({
 serviceUrl: '/rest/condo/names',
 select: function (event, ui) {
 //var terms = split(this.value);                
 $("#property_name").val(ui.item.label);
 $('#condo_id').val(ui.item.value);
 $('.other_property_name').hide();
 updateLocations(ui.item);
 return false;
 },
 response: function (event, ui) {
 if (ui.content.length === 0) {
 $('#condo_id').val('');
 }
 $('.property-name').removeClass('running');
 },
 search: function () {
 $('.property-name').addClass('running');
 },
 minLength: 1
 }).data("ui-autocomplete")._renderItem = function (ul, item) {
 
 console.log(item);
 
 var name = $.parseJSON(item.name);
 var current = name[currentHL].length > 0 ? name[currentHL] : name['th'];
 //console.log(item);
 return $('<li>sdasda' + current + '</li>').appendTo(ul);
 };
 };
 */

var initAutoComplete = function () {

    $("#property_name").devbridgeAutocomplete({
        serviceUrl: '/rest/condo/names',
        paramName: 'term',
        dataType: 'json',
        transformResult: function (response) {
            return {
                suggestions: $.map(response.suggestions, function (item) {
                    var name = $.parseJSON(item.name);
                    var current = name[currentHL].length > 0 ? name[currentHL] : name['th'];
                    return {value: current, data: item.id, custom: item};
                })
            };
        },
        onSearchStart: function () {
            $('.fa-spin').show();
            $('.clear-property-name').hide();
        },
        onSearchComplete: function (query, suggestions) {
            if (suggestions.length === 0) {
                $('#condo_id').val('');
            }
            $('.fa-spin').hide();
        },
        onSelect: function (suggestion) {
            console.log(suggestion);
            console.log(suggestion.custom);
            $('#condo_id').val(suggestion.data);
            $('.other_property_name').hide();
            updateLocations(suggestion.custom);
            $('.clear-property-name').show();
        }
    });

};

$(document).ready(function () {

    $('#required-condo-name').hide();

    initAutoComplete();

    /**/
    
    $('.clear-property-name').on('click', function(){
        
        resetLocations();
        $('.clear-property-name').hide();
        
    });

    $('.property-types button').on('click', function () {
        $('.property-types button').removeClass('btn-selected');
        $(this).addClass('btn-selected');

        var type = $(this).data('type');

        $('#property_type').val(type);

        if (type === 'condo') {
            initAutoComplete();
            $('.required-condo-name').show();
        } else {
            resetAutoComplete();
            resetLocations();
            $('.required-condo-name').hide();
        }
    });



    $('#show_property_name').on('click', function () {
        //$('.other_property_name').show();
    });

    $('#map-picker').locationpicker({
        location: {latitude: 13.734856, longitude: 100.543396},
        radius: 0,
        enableAutocomplete: true,
        autocompleteSetting: {
            componentRestrictions: {country: "th"}
        },
        onchanged: function (currentLocation, radius, isMarkerDropped) {
            var addressComponents = $(this).locationpicker('map').location.addressComponents;
            console.log(addressComponents);
            console.log(currentLocation);
            updateInputIfEmpty('#street_number', addressComponents.streetNumber);
            updateInputIfEmpty('#street_name', addressComponents.streetName);
            updateInputIfEmpty('#postale_code', addressComponents.postalCode);

            $('#lat').val(currentLocation.latitude);
            $('#lng').val(currentLocation.longitude);
        }
    });



    $('#next').on('click', function () {

        // Check listing_type

        var canBeSubmited = true;

        var listing_type = $('input[name=listing_type]:checked').val();

        if (listing_type == undefined) {
            $('#listing_type_error').show();
            canBeSubmited = false;
        } else {
            $('#listing_type_error').hide();

        }

        // Check property_type

        var propertyType = $('#property_type').val();

        if (propertyType == '') {
            $('#property_type_error').show();
            canBeSubmited = false;
        } else {
            $('#property_type_error').hide();
        }

        // Check property_name

        var propertyName = $('#property_name').val();
        var propertyId = parseInt($('#condo_id').val());

        if (propertyType === 'condo' && (propertyName === '' || propertyId === 0)) {
            $('#property_name_error').show();
            canBeSubmited = false;
        } else {
            $('#property_name_error').hide();
        }

        // Check province

        var provinceId = parseInt($('#province_id').val());

        if (provinceId > 0) {
            $('#province_error').hide();
        } else {
            $('#province_error').show();
            canBeSubmited = false;
        }

        // Check district

        var districtId = parseInt($('#district_id').val());

        if (districtId > 0) {
            $('#district_error').hide();
        } else {
            $('#district_error').show();
            canBeSubmited = false;
        }

        // Check area

//        var areaId = parseInt($('#area_id').val());
//
//        if (areaId > 0) {
//            $('#area_error').hide();
//        } else {
//            $('#area_error').show();
//            canBeSubmited = false;
//        }

        if (canBeSubmited) {
            $('form').submit();
        } else {
            $('#all_error').show();
        }
    });
});

var updateInputIfEmpty = function (input, value)
{
    if ($(input).val() === '') {
        $(input).val(value);
    }
};

var updateLocations = function (condo) {

    console.log(condo);

    resetLocations();

    $('.custom-location').hide();

    var province_id = parseInt(condo.province_id);
    var district_id = parseInt(condo.district_id);
    var area_id = parseInt(condo.area_id);

    if (condo.lat && condo.lng) {
        var $lpicker = $('#map-picker').locationpicker({
            location: {latitude: condo.lat, longitude: condo.lng}
        });
        var map = $('#map-picker').locationpicker('map');
        map.marker.draggable = false;
    }

    if (province_id > 0) {
        $('#province_id').val(province_id);
        $('#province_id').prop('disabled', true);
        enableAndLoadDistricts(province_id, function () {
            if (district_id > 0) {
                $('#district_id').val(district_id);
            }
        });
    }

    if (district_id > 0) {
        $('#district_id').val(district_id);
        $('#district_id').prop('disabled', true);
        enableAndLoadArea(district_id, function () {
            if (area_id > 0) {
                $('#area_id').val(area_id);
                $('#area_id').prop('disabled', true);
            }
        });
    }
};

var resetLocations = function () {
    $('#province_id').prop('disabled', false);
    $('#district_id').prop('disabled', false);
    $('#area_id').prop('disabled', false);


    var map = $('#map-picker').locationpicker('map');
    map.marker.draggable = true;
};

var resetAutoComplete = function () {
    $("#property_name").devbridgeAutocomplete('disable');
};