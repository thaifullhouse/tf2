<?php

namespace App\Emails;

use Mail;

class AccountCreated
{
    public function send($user)
    {
        Mail::send('emails.account-created', ['user' => $user], function ($m) use ($user) {
                $m->from('no-reply@thaifullhouse.com', 'Thai Full House');

                $m->to($user->email)->subject('Congratulations');
            });
    }
}
