<?php

namespace App\Emails;

use Mail;

class EmailValidation
{
    public function send($user)
    {
        Mail::send('emails.email-validation', ['user' => $user], function ($m) use ($user) {
                $m->from('no-reply@thaifullhouse.com', 'Thai Full House');

                $m->to($user->email, $user->name)->subject('Email validation');
            });
    }
}
