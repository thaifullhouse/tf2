<?php

namespace App\Emails;

use Mail;

class ResetPasswordLink
{
    public function send($user)
    {
        Mail::send('emails.reset-password', ['user' => $user], function ($m) use ($user) {
                $m->from('no-reply@thaifullhouse.com', 'Thai Full House');

                $m->to($user->email, $user->name)->subject('Password reset');
            });
    }
}
