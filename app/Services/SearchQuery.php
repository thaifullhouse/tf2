<?php

namespace App\Services;

use Illuminate\Http\Request;

class SearchQuery {
    
    protected $options;
    
    public function __construct(Request $request) {
        $this->options = [];
        
        $all = $request->all();
        
        $this->prepateLocations($all);
        $this->prepareListingType($all);
        $this->preparePropertyType($all);
    }
    
    public function getLocations() {
        return $this->options['locations'];
    }
    
    public function addListingType($type) {
        $types = array_get($this->options, 'lt');
        $types[] = $type;
        $this->options['lt'] = array_unique($types);
    }
    
    public function addPropertyType($type) {
        $types = array_get($this->options, 'pt');
        $types[] = $type;
        $this->options['pt'] = array_unique($types);
    }
    
    protected function prepareListingType($params) {
        $tmp = explode(',', array_get($params, 'lt'));
        $this->options['lt'] = array_filter($tmp);
    }
    
    protected function preparePropertyType($params) {
        $tmp = explode(',', array_get($params, 'pt'));
        $this->options['pt'] = array_filter($tmp);
    }
    
    protected function prepateLocations($params) {
        
        $nl = array_get($params, 'nl');
        $fr = array_get($params, 'fr');
        
        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);
        
        $this->options['locations'] = [$lng_min, $lat_min, $lng_max, $lat_max];
    }
    
}