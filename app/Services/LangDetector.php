<?php

namespace App\Services;
/**
 * Detect user language from input text
 */
class LangDetector {
    
    public static function findBestLang($word) {
        
        $lang = '';
        
        if (preg_match('/\p{Thai}/u', $word) === 1) {
            $lang = 'th';
        }
        else {
            $lang = 'en';
        }
        
        return $lang;
    }
    
}

