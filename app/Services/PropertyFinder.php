<?php

namespace App\Services;

use Illuminate\Http\Request;
use DB;

class PropertyFinder {

    protected $request;
    protected $query;
    protected $map_center;

    public function __construct(Request $request) {

        $this->request = $request;
    }

    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForMap($exclude = []) {

        // remove details data from map result to minimize data transfer
        $this->prepareSearchQuery($exclude, false);        

        return $this->query->get();
    }

    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForListing($exclude = []) {

        $this->prepareSearchQuery($exclude, true);

        return $this->query->paginate(20)->appends($this->request->all());
    }

    protected function prepareSearchQuery($exclude, $getDetails = false) {

        $this->query = DB::table('properties')->select(
                'properties.id'
                , 'properties.property_type As ptype'
                , 'properties.listing_type AS ltype'
                , 'properties.listing_class AS lclass'
                , 'properties.new_project AS np'
                , DB::raw('ST_X(ST_TRANSFORM(properties.location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(properties.location::geometry, 4326)) AS lat'));

        $this->query->leftJoin('property_details', 'property_details.property_id', 'properties.id');

        if ($getDetails) {
            $this->addDetailedData();
        }

        if (count($exclude) > 0) {
            $this->query->whereNotIn('properties.id', $exclude);
        }
        
        $this->query->where('properties.poster_id', '>', 0);
        $this->query->where('properties.published', true);
        $this->query->where('properties.publication_date', '<', date('Y-m-d'));
        $this->query->where(function($query) {
            $query->orWhere('properties.expiration_date', '>', date('Y-m-d'));
            $query->orWhereRaw('properties.expiration_date IS NULL');
        });

        $this->applyAllFitlers();
        
        if ($this->map_center) {
            $this->query->whereRaw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$this->map_center->lng} {$this->map_center->lat})')) < " . 20000);
        } else {
            $this->query->limit(1000);
        }
    }

    protected function addDetailedData() {
        
        $lang = \App::getLocale();
        
        $this->query->addSelect(DB::raw("properties.name"));
        $this->query->addSelect('property_details.listing_price');
        $this->query->addSelect('property_details.bedroom_num');
        $this->query->addSelect('property_details.bathroom_num');
        $this->query->addSelect('property_details.total_size');
        $this->query->addSelect('property_details.year_built');
        $this->query->addSelect('property_details.minimal_rental_period');
    }

    protected function applyAllFitlers() {

        $this->addListingTypeFilters();
        $this->addPropertyTypeFilter();
        $this->addPriceRangeFilter();
        $this->addBedroomFitler();
        $this->addBathroomFilter();
        $this->addSizeRangeFitler();
        $this->addYearBuiltFilter();
        $this->addLocationFitler();
        $this->addNewProjectFitler();
    }

    //

    protected function addListingTypeFilters() {

        $ltypes = $this->translateListingType(
                explode(',', $this->request->input('lt'))
        );

        if (is_array($ltypes) && count($ltypes) > 0) {
            $this->query->whereIn('properties.listing_type', $ltypes);
        }
    }

    protected function addPropertyTypeFilter() {

        $ptypes = $this->translatePropertyType(
                explode(',', $this->request->input('pt'))
        );

        if (is_array($ptypes) && count($ptypes) > 0) {
            $this->query->whereIn('properties.property_type', $ptypes);
        }
    }

    public function addPriceRangeFilter() {

        $price_min = (int) $this->request->input('pmin');
        $price_max = (int) $this->request->input('pmax');

        if (is_int($price_min) && $price_min > 0) {
            $this->query->where('property_details.listing_price', '>=', $price_min);
        }
        if (is_int($price_max) && $price_max > 0) {
            $this->query->where('property_details.listing_price', '<=', $price_max);
        }
    }

    public function addBedroomFitler() {

        $bedroom = $this->request->input('bed');

        if ($bedroom == 'studio') {
            $this->query->where('property_details.bedroom_num', '=', 0);
        } else if (preg_match('/\+/', $bedroom)) {
            $bedroom = str_replace('+', '', $bedroom) + 0;

            if (is_int($bedroom)) {
                $this->query->where('property_details.bedroom_num', '>=', $bedroom);
            }
        } else if (!empty($bedroom)) {
            $bedroom = $bedroom + 0;

            if (is_int($bedroom)) {
                $this->query->where('property_details.bedroom_num', '=', $bedroom);
            }
        }
    }

    public function addBathroomFilter() {

        $bathroom = $this->request->input('bath');
        $comp = '=';

        if (preg_match('/\+/', $bathroom)) {
            $bathroom = str_replace('+', '', $bathroom) + 0;
            $comp = '>=';
        } else {
            $bathroom = $bathroom + 0;
            $comp = '=';
        }

        if (is_int($bathroom) && $bathroom > 0) {
            $this->query->where('property_details.bathroom_num', $comp, $bathroom);
        }
    }

    public function addSizeRangeFitler() {
        $size_min = (int) $this->request->input('smin');
        $size_max = (int) $this->request->input('smax');

        if (is_int($size_min) && $size_min > 0) {
            $this->query->where('property_details.total_size', '>=', $size_min);
        }
        if (is_int($size_max) && $size_max > 0) {
            $this->query->where('property_details.total_size', '<=', $size_max);
        }
    }

    public function addYearBuiltFilter() {
        $year_min = (int) $this->request->input('ymin');
        $year_max = (int) $this->request->input('ymax');

        if (is_int($year_min) && $year_min > 0) {
            $this->query->where('property_details.year_built', '>=', $year_min);
        }
        if (is_int($year_max) && $year_max > 0) {
            $this->query->where('property_details.year_built', '<=', $year_max);
        }
    }

    public function addLocationFitler() {

        $nl = $this->request->input('nl');
        $fr = $this->request->input('fr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);

        if (is_numeric($lng_min) && is_numeric($lat_min) && is_numeric($lng_max) && is_numeric($lat_max)) {

            $this->map_center = new \stdClass;
            $this->map_center->lng = ($lng_min + $lng_max) / 2.0;
            $this->map_center->lat = ($lat_min + $lat_max) / 2.0;

            $this->query->whereRaw(DB::raw("properties.location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)"));
        }
    }

    public function addNewProjectFitler() {

        $np = $this->request->input('np');

        if ($np == 'yes') {
            $this->query->where('properties.new_project', true);
        }
        else {
            $this->query->where('properties.new_project', false);
        }
    }

    protected function translatePropertyType($values) {
        $types = [];
        foreach ($values as $val) {
            $val = trim($val);
            switch ($val) {
                case 'ap':
                    $types[] = 'apartment';
                    break;
                case 'co':
                    $types[] = 'condo';
                    break;
                case 'dh':
                    $types[] = 'detachedhouse';
                    break;
                case 'th':
                    $types[] = 'townhouse';
                    break;
                default:
                    break;
            }
        }

        return $types;
    }

    protected function translateListingType($values) {
        $types = [];
        foreach ($values as $val) {
            switch ($val) {
                case 'sale':
                    $types[] = 'sale';
                    break;
                case 'rent':
                    $types[] = 'rent';
                    break;
                default:
                    break;
            }
        }

        return $types;
    }

}
