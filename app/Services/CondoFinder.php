<?php

namespace App\Services;

use Illuminate\Http\Request;
use DB;

class CondoFinder {
    
    protected $request;
    protected $query;
    protected $map_center;
    
    public function __construct(Request $request) {

        $this->request = $request;
    }
    
    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForMap($exclude = []) {

        // remove details data from map result to minimize data transfer
        $this->prepareSearchQuery();        

        return $this->query->get();
    }

    /**
     * 
     * @param type $exclude
     * @return type
     */
    public function findForListing() {

        $this->prepareSearchQuery(true);

        return $this->query->paginate(20)->appends($this->request->all());
    }
    
    protected function prepareSearchQuery($getDetails = false) {

        $this->query = DB::table('condo_list')->select(
                'condo_list.id'
                , DB::raw("'condo' AS ptype")
                , DB::raw("'condo' AS ltype")
                , DB::raw('condo_list.new_project AS np')
                , DB::raw('ST_X(ST_TRANSFORM(condo_list.location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(condo_list.location::geometry, 4326)) AS lat'));
        
        $this->query->leftJoin('condo_profile_data', 'condo_profile_data.condo_id', 'condo_list.id');        
        
        $this->query->where('condo_list.active', true);
        $this->query->where('condo_list.user_submited', false);
        
        $this->addLocationFitler();
        $this->addNewProjectFitler();
        
        if ($getDetails) {
            $this->addDetails();
        }
        
        if ($this->map_center) {
            $this->query->whereRaw("ST_Distance(condo_list.location, ST_GeogFromText('SRID=4326;POINT({$this->map_center->lng} {$this->map_center->lat})')) < " . 20000);
        } else {
            $this->query->limit(1000);
        }
    }
    
    protected function addDetails() {
        $this->query->addSelect('condo_list.name');
        $this->query->addSelect('condo_list.location_name');
        $this->query->addSelect('condo_profile_data.starting_sales_price');
        $this->query->addSelect('condo_profile_data.address');
    }
    
    protected function addLocationFitler() {

        $nl = $this->request->input('nl');
        $fr = $this->request->input('fr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);

        if (is_numeric($lng_min) && is_numeric($lat_min) && is_numeric($lng_max) && is_numeric($lat_max)) {

            $this->map_center = new \stdClass;
            $this->map_center->lng = ($lng_min + $lng_max) / 2.0;
            $this->map_center->lat = ($lat_min + $lat_max) / 2.0;

            $this->query->whereRaw(DB::raw("condo_list.location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)"));
        }
    }
    
    protected function addNewProjectFitler() {

        $np = $this->request->input('np');

        if ($np == 'yes') {
            $this->query->where('new_project', true);
        }
        else {
            $this->query->where('new_project', false);
        }
    }
}