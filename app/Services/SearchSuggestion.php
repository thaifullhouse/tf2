<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Helpers\Language;
use DB;

class SearchSuggestion {

    protected $request;
    protected $langs;
    protected $terms;

    public function __construct(Request $request) {
        $this->request = $request;
        $this->langs = Language::getActiveCodes();
    }

    public function find() {
        $this->sanitize();

        if (count($this->terms)) {
            return array_merge(
                    $this->getCondoDirectory()
                    , $this->getProperties()
                    , $this->getPointOfInterests()
            );
        } else {
            return [];
        }
    }

    protected function sanitize() {
        $term = trim($this->request->input('term'));
        $terms = preg_split('/(\s+)/', $term);
        $this->terms = array_filter($terms);
    }

    protected function getPointOfInterests() {

        $query = DB::table('search_pois')
                ->select(
                'id'
                , 'name'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , 'type');

        $terms = $this->terms;

        foreach ($this->langs as $lang) {
            $query->orWhere(function($query) use ($terms, $lang) {
                foreach ($terms as $_term) {
                    $query->where('name->' . $lang, '~*', $_term);
                }
            });
        }

        return $query->limit(50)->get()->all();
    }

    protected function getProperties() {
        $query = DB::table('properties')->select(
                'id'
                , 'name'
                , DB::raw('property_type AS type')
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
        );

        $terms = $this->terms;

        $query->where(function($_query1) use ($terms) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                $_query1->orWhere(function($_query2) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $_query2->where('name->' . $lang, '~*', $_term);
                    }
                });
            }
        });

        $now = date('Y-m-d H:i:s');

        $query->where('published', true);
        $query->where('publication_date', '<', $now);
        $query->where(function($query) use ($now) {
            $query->orWhere('expiration_date', '>', $now);
            $query->orWhereRaw('expiration_date IS NULL');
        });
        $query->whereRaw(DB::raw('deleted_at IS NULL'));

        // Properties

        return $query->limit(50)->get()->all();
    }

    protected function getCondoDirectory() {

        $query = DB::table('condo_list')
                ->select(
                'id'
                , 'name'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , DB::raw("'condo_dir'::varchar AS type")
        );

        $query->where('active', true)->where('user_submited', false);

        $terms = $this->terms;

        $query->where(function($_query1) use ($terms) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                $_query1->orWhere(function($_query2) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $_query2->where('name->' . $lang, '~*', $_term);
                    }
                });
            }
        });

        return $query->limit(50)->get()->all();
    }

}
