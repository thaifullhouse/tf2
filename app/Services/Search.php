<?php

namespace App\Services;

use Illuminate\Http\Request;
use DB;
use App\Models\Properties\Details;
use App\Models\Property;

//use Auth;

class Search {

    protected $exclude_ids;

    public function __construct() {
        $this->exclude_ids = [];
    }

    public function exclude($list) {
        $this->exclude_ids = array_merge($this->exclude_ids, $list);
    }

    public function find(Request $request, $limit = 0) {

        $pr = $request->input('pr');

        $queries = [];

        $properties = [];

        if ($pr === 'co') {
            $properties = $this->findCondos($request, $limit, $queries);
        } else if ($pr === 'np') {
            $properties = array_merge(
                    $this->findCondos($request, $limit, $queries)
                    , $this->findProperties($request, $limit, $queries)
            );
        } else {
            $properties = $this->findProperties($request, $limit, $queries);
        }

        return [
            'data' => $properties,
            'query' => $queries
        ];
    }

    protected function findProperties(Request $request, $limit, &$queries) {

        $nl = $request->input('nl');
        $fr = $request->input('fr');

        $np = $request->input('pr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);

        $query = DB::table('properties')->select(
                'properties.id'
                , 'properties.property_type'
                , 'properties.listing_type'
                , 'properties.property_name'
                , DB::raw('ST_X(ST_TRANSFORM(properties.location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(properties.location::geometry, 4326)) AS lat'));

        $query->leftJoin('property_details', 'property_details.property_id', 'properties.id');

        //----------------------------------------------------------------------
        // Listing filters
        //----------------------------------------------------------------------

        $price_min = (int) $request->input('pmin');
        $price_max = (int) $request->input('pmax');

        //$price_where_clause = '';

        if ($price_min > 0) {
            $query->where('property_details.listing_price', '>=', $price_min);
        }
        if ($price_max > 0) {
            $query->where('property_details.listing_price', '<=', $price_max);
        }

        $bedroomNumberLimit = 4;

        //----------------------------------------------------------------------
        // Bedroom filters
        //----------------------------------------------------------------------

        $bedroom = $request->input('bed');

        if ($bedroom == 'studio') {
            $query->where('property_details.bedroom_num', '=', 0);
        } else if (preg_match('/\+/', $bedroom)) {
            $bedroom = str_replace('+', '', $bedroom) + 0;
            $query->where('property_details.bedroom_num', '>', $bedroom - 1);
        }
        else {
            $bedroom = $bedroom + 0;
            $query->where('property_details.bedroom_num', '=', $bedroom);
        }

        //----------------------------------------------------------------------
        // Bathroom filters
        //----------------------------------------------------------------------

        $bathroomNumberLimit = 4;

        $bathroom = $request->input('bath') + 0;

        if ($bathroom > 0 && $bathroom < $bathroomNumberLimit) {
            $query->where('property_details.bathroom_num', '=', $bathroom);
        } else if ($bathroom > 0 && $bathroom > ($bathroomNumberLimit - 1)) {
            $query->where('property_details.bathroom_num', '>', $bathroom - 1);
        }

        //----------------------------------------------------------------------
        // Bathroom filters
        //----------------------------------------------------------------------

        $size_min = (int) $request->input('smin');
        $size_max = (int) $request->input('smax');

        if ($size_min > 0) {
            $query->where('property_details.total_size', '>=', $size_min);
        }
        if ($size_max > 0) {
            $query->where('property_details.total_size', '<=', $size_max);
        }

        //----------------------------------------------------------------------
        // Year built filters
        //----------------------------------------------------------------------

        $year_min = (int) $request->input('ymin');
        $year_max = (int) $request->input('ymax');

        if ($year_min > 0) {
            $query->where('property_details.year_built', '>=', $year_min);
        }
        if ($year_max > 0) {
            $query->where('property_details.year_built', '<=', $year_max);
        }

        // Get requested property types

        $ptypes = $this->translatePropertyType(
                explode(',', $request->input('pt'))
        );

        $results = [];

        //----------------------------------------------------------------------
        // Load normal properties
        //----------------------------------------------------------------------

        $ltypes = $this->translateListingType(
                explode(',', $request->input('lt'))
        );

        if (count($ltypes) > 0) {
            $query->whereIn('properties.listing_type', $ltypes);
        }

        // Property type clause

        if ($ptypes) {
            $query->whereIn('properties.property_type', $ptypes);
        }

        $query->whereRaw(DB::raw("properties.location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)"));



        if (count($this->exclude_ids)) {
            $query->whereNotIn('properties.id', $this->exclude_ids);
        }

        if ($np == 'np') {
            $query->where('properties.new_project', true);
        } else {
            $query->where('properties.new_project', false);
        }

        $query->where('properties.published', true);
        $query->where('properties.expiration_date', '<', date('Y-m-d'));

       

        $queries[] = $query->toSql();

        $properties = $query->paginate(20);

        foreach ($properties as $property) {
            $images = DB::select('SELECT filepath FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$property->id, 'image']);
            $thumbs = [];
            if (count($images)) {
                foreach ($images as $k => $image) {
                    $images[$k] = url('/thumb/320x300/' . $image->filepath);
                    $thumbs[$k] = url('/thumb/250x250/' . $image->filepath);
                }
            } else {
                $images = ['http://placehold.it/350x550?text=No+image+yet'];
            }

            $property->images = $images;
            $property->thumbs = $thumbs;
            $property->dlink = url('property/details/' . $property->id);
            $property->plink = url('property-preview/' . $property->id);

            $details = null;

            try {
                $details = Details::find($property->id);
            } catch (ModelNotFoundException $ex) {
                //$details = new PropertyDetails;
            }
            $property->id = $property->id; //'p' . $property->id;
            $property->details = $details;
            $results[] = $property;
        }

        return $results;
    }

    protected function findCondos(Request $request, $limit, &$queries) {

        $nl = $request->input('nl');
        $fr = $request->input('fr');

        $np = $request->input('pr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);

        $query = DB::table('condo_list')->select('*'
                        , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                        , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat'))
                ->where('active', true)
                ->whereRaw(DB::raw("location::geometry && ST_MakeEnvelope($lng_min, $lat_min, $lng_max, $lat_max, 4326)"));

        if ($np == 'np') {
            $query->where('new_project', true);
        } else {
            $query->where('new_project', false);
        }

        $queries[] = $query->toSql();

        $list = $query->get();



        /* DB::select('SELECT *,
          ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
          ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat
          FROM condo_list WHERE active = true
          AND location::geometry && ST_MakeEnvelope(?, ?, ?, ?, 4326)', [$lng_min, $lat_min, $lng_max, $lat_max]); */

        $results = [];

        foreach ($list as $row) {

            $images = DB::select('SELECT filepath FROM condo_have_media WHERE condo_id = ? AND media_type = ?', [$row->id, 'image']);
            if (count($images)) {
                foreach ($images as $k => $image) {
                    $images[$k] = url('/image/' . $image->filepath);
                }
            } else {
                $images = ['http://placehold.it/350x550?text=No+image+yet'];
            }

            // Get listing price

            $properties = DB::select('SELECT pd.* FROM properties_belongs_to_condos AS pbc'
                            . ' LEFT JOIN property_details AS pd ON pd.property_id = pbc.property_id'
                            . ' WHERE pbc.condo_id = ? ORDER BY pd.listing_price DESC', [$row->id]);

            $listing_price = '';

            if (count($properties)) {
                $property = $properties[0];
                $listing_price = $property->listing_price + 0;
            }

            $names = json_decode($row->name, true);
            $lang = \App::getLocale();

            $results[] = [
                'id' => $row->id,
                'property_type' => 'condo',
                'listing_type' => 'rent',
                'property_name' => array_get($names, $lang),
                'lat' => $row->lat,
                'lng' => $row->lng,
                'images' => $images,
                'details' => [
                    'listing_price' => $listing_price,
                ],
                'dlink' => url('condo/details/' . $row->id),
                'plink' => url('condo/preview/' . $row->id)
            ];
        }

        return $results;
    }

    public function findSimilar(Property $property, $count = 4, $distance = 10000) {
        $details = Details::findOrFail($property->id);

        $query = 'SELECT p.id, p.property_type, p.listing_type, p.property_name,
                        ST_X(ST_TRANSFORM(p.location::geometry, 4326)) AS lng,
                        ST_Y(ST_TRANSFORM(p.location::geometry, 4326)) AS lat,
                        abs(pd.listing_price - ?) AS price_diff,
                        ST_Distance(p.location, ST_GeomFromText(?,4326)) AS distance
                        FROM properties AS p
                        LEFT JOIN property_details AS pd ON pd.property_id = p.id 
                        WHERE p.id <> ? AND p.property_type = ? AND p.listing_type = ? AND pd.property_id > 0
                        AND (p.district_id = ? OR ST_Distance(p.location, ST_GeomFromText(?,4326)) < ?)
                        ORDER BY distance ASC, price_diff ASC  LIMIT ?';

        $latlng = $property->getLatLng();

        $properties = DB::select($query, [
                    $details->listing_price,
                    'POINT(' . $latlng->lng . ' ' . $latlng->lat . ')',
                    $property->id,
                    $property->property_type,
                    $property->listing_type,
                    $property->district_id,
                    'POINT(' . $latlng->lng . ' ' . $latlng->lat . ')',
                    $distance, $count
        ]);

        if (count($properties)) {
            foreach ($properties as $k => $_property) {
                $properties[$k]->details = Details::findOrFail($_property->id);

                $images = DB::select('SELECT filepath FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$_property->id, 'image']);
                $thumbs = [];
                if (count($images)) {
                    foreach ($images as $j => $image) {
                        $thumbs[$j] = url('/thumb/250x250/' . $image->filepath);
                    }
                } else {
                    $images = ['http://placehold.it/350x550?text=No+image+yet'];
                }

                $properties[$k]->thumbs = $thumbs;
            }
        }

        return $properties;
    }

    protected function translatePropertyType($values) {
        $types = [];
        foreach ($values as $val) {
            $val = trim($val);
            switch ($val) {
                case 'ap':
                    $types[] = 'apartment';
                    break;
                case 'co':
                    $types[] = 'condo';
                    break;
                case 'dh':
                    $types[] = 'detachedhouse';
                    break;
                case 'th':
                    $types[] = 'townhouse';
                    break;
                default:
                    break;
            }
        }

        return $types;
    }

    protected function translateListingType($values) {
        $types = [];
        foreach ($values as $val) {
            switch ($val) {
                case 'sale':
                    $types[] = 'sale';
                    break;
                case 'rent':
                    $types[] = 'rent';
                    break;
                default:
                    break;
            }
        }

        return $types;
    }
    
    protected function getPropertyStats() {
        
        // Listing type
        
        $lt_stat = DB::table('select listing_type, count(id) from properties where listing_type IS NOT NULL and published = true group by listing_type');
    }

}
