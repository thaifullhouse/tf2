<?php

namespace App\Services;

use App\Models\Property;
use App\Models\Properties\Details;
use DB;

class HomeProperties {

    public function geListingByClass($type, $class, $count) {

        $query = DB::table('properties')
                ->leftJoin('property_details', 'property_details.property_id', 'properties.id')
                ->select('properties.id' 
                        , 'properties.name' 
                        , 'property_details.listing_price'
                        , 'properties.street_name'
                        , 'properties.listing_type');

        if ($type) {
            $query->where('properties.listing_type', $type);
        }

        $query->where('properties.poster_id', '>', 0);
        $query->where('properties.published', true);
        $query->where('properties.publication_date', '<', date('Y-m-d'));
        $query->where('properties.listing_class', $class);
        $query->where(function($query) {
            $query->orWhere('properties.expiration_date', '>', date('Y-m-d'));
            $query->orWhereRaw('properties.expiration_date IS NULL');
        });

        $properties = $query->limit($count)->get();

        if (count($properties) > 0) {

            foreach ($properties as $k => $property) {
                $images = DB::select('SELECT filepath FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$property->id, 'image']);
                $thumbs = [];

                $names = json_decode($property->name);

                $properties[$k]->address = 'Property address';
                $properties[$k]->name = __trget($names);
                $properties[$k]->details = Details::find($property->id);

                if (count($images)) {
                    $image = $images[0];
                    $properties[$k]->main = url('/thumb/618x370/' . $image->filepath);
                    $properties[$k]->thumb = url('/thumb/120x120/' . $image->filepath);
                    
                    foreach ($images as $j => $image) {
                    $thumbs[$j] = url('/thumb/309x250/' . $image->filepath);
                }
                } else {
                    $images = [];
                    $properties[$k]->main = 'http://placehold.it/618x370?text=No+image+yet';
                    $properties[$k]->thumb = 'http://placehold.it/120x120?text=No+image+yet';
                }                
                $properties[$k]->thumbs = $thumbs;
            }
        }

        return $properties;
    }

}
