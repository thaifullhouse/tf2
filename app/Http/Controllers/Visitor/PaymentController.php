<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use GuzzleHttp\Psr7;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ServerException;
use Illuminate\Http\Request;
use App\Models\Package;

class PaymentController extends Controller {

    public function getPaysbuyToken(Request $request, $pid) {

        // Create new order  

        $package = Package::find($pid);
        $purchase = $package->createPurchase($request);

        $data = [
            'psbID' => '1440357735',
            'username' => 'wonderpons_merchant@paysbuy.com',
            'secureCode' => 'e134428b19f197c9cc39149d15ec66d8',
            'inv' => $purchase->invoice_id,
            'itm' => $package->name,
            'amt' => $purchase->getAmount(),
            'paypal_amt' => '',
            'curr_type' => 'TH',
            'com' => '',
            'method' => '2',
            'language' => 'T',
            'resp_front_url' => url('/member/purchase'),
            'resp_back_url' => url('/api/payment/callback'),
            'opt_fix_redirect' => '',
            'opt_fix_method' => '',
            'opt_name' => '',
            'opt_email' => '',
            'opt_mobile' => '',
            'opt_address' => '',
            'opt_detail' => '',
            'opt_param' => '' 
            
        ];

        $client = new HttpClient();

        try {
            $response = $client->request('POST', 'https://demo.paysbuy.com/api_paynow/api_paynow.asmx/api_paynow_authentication_v3', ['form_params' => $data]);
            return $response->getBody();
        } catch (ServerException $ex) {
            return Psr7\str($ex->getResponse());
        }
    }
    
    public function getThanks() {
        return view('visitor.payment.thanks');
    }

    public function getFeatured() {

        return view('visitor.payment.featured');
    }

    public function getExclusive() {

        return view('visitor.payment.exclusive');
    }

}
