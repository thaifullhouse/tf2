<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;


class ErrorController extends Controller {
    
    /**
     * 
     * @return type
     */
    public function pageNotFound() {        
        return view('errors.notfound');        
    }
    
    /**
     * 
     * @return type
     */
    public function accessForbidden() {
        return view('errors.access');      
    }
    
}