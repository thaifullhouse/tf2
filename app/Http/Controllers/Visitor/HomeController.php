<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
//use App\Models\Property;
//use App\Models\Properties\Details;
use App\Services\HomeProperties;
//use DB;

class HomeController extends Controller {

    public function index() {
        
        $service = new HomeProperties;

        $premium = $service->geListingByClass(null, 'featured', 9);   

//        foreach ($premium as $k => $property) {
//            $premium[$k]->details = Details::find($property->id);
//
//            $images = DB::select('SELECT filepath FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$property->id, 'image']);
//            $thumbs = [];
//            if (count($images)) {
//                foreach ($images as $j => $image) {
//                    $thumbs[$j] = url('/thumb/309x250/' . $image->filepath);
//                }
//            } else {
//                $images = ['http://placehold.it/350x550?text=No+image+yet'];
//            }
//
//            $premium[$k]->thumbs = $thumbs;
//        }        
        
        
        
        // For rent
        
        $rentals = $service->geListingByClass('rent', 'exclusive', 9);       
        
        // For sales
        
        $sales = $service->geListingByClass('sale', 'exclusive', 9);

        return view('visitor.home.index', ['premium' => $premium, 'rentals' => $rentals, 'sales' => $sales]);
    }

}
