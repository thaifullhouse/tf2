<?php

namespace App\Http\Controllers\Visitor;

use Image;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use App\Helpers\ImageHelper;
use DB;

/**
 * Default controller
 */
class ImageController extends Controller {

    public function index($file) {
        $path = storage_path('app/public/' . $file);

        return $this->send_file($path);
    }

    public function getThumb($size, $file, $default = false) {

        $path = storage_path('app/public/' . $file);

        if (file_exists($path)) {
            $thumb = storage_path('app/public/' . $size . '-' . $file);
            if (!file_exists($thumb)) {
                $img = ImageHelper::createThumbFromFile($size, $file);

                if ($img != null) {
                    $img->encode('png');
                    $img->save($thumb);
                    $response = Response::make($img);
                    //$response->header('Content-Length', $img->filesize());
                    $response->header('Content-Type', 'image/png');
                    return $response;
                } else {

                    if ($default === false) {
                        return response('Image Not found', 404);
                    } else {
                        return $this->send_file($default);
                    }
                }
            } else {
                return $this->send_file($thumb);
            }
        } else {
            if ($default === false) {
                return response('Image Not found', 404);
            } else {
                return $this->send_file($default);
            }
        }
    }

    protected function send_file($file) {
        if (is_readable($file)) {
            $img = Image::make($file);
            $response = Response::make($img->encode('png'));
            //$response->header('Content-Length', $img->filesize());
            $response->header('Content-Type', 'image/png');
            return $response;
        } else {
            return response('Image Not found', 404);
        }
    }

    public function propertyThumb($id) {
        $file = DB::table('properties_have_media')->where('property_id', $id)
                ->value('filepath');

        if ($file) {
            return $this->getThumb('300x300', $file, public_path('/img/default-property.png'));
        } else {
            return $this->send_file(public_path('/img/default-property.png'));
        }
    }

}
