<?php

namespace App\Http\Controllers\Visitor;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\Registration\CreateOwnerRequest;
use App\Http\Requests\Registration\CreateAgentRequest;
use App\Http\Requests\Registration\CreateRealCompanyRequest;
use App\Http\Requests\Registration\CreateRealDeveloperRequest;
use App\Models\User\Type as UserType;
use App\Models\User;
use App\Models\User\Profile as UserProfile;
use App\Models\Address;
use Hash;
use App\Emails\EmailValidation;

class RegistrationController extends BaseController {

    public function getType() {
        return view('visitor.register.type');
    }

    public function postType(Request $request) {

        $utype = $request->input('utp');

        if (Auth::check()) {

            $user = Auth::user();

            if ($user->isVisitor()) {
                $usertype = UserType::where('code', 'member')->first();
                $user->type_id = $usertype->id;
                $user->save();
            }

            return redirect('/member/posting/property-type');
        } else {
            return redirect('/auth/register/' . $utype);
        }
    }

    public function showOwner() {

        if (Auth::check()) {
            return '/?reason=already-registered';
        }

        return view('visitor.register.owner');
    }

    public function postOwner(CreateOwnerRequest $request) {
        $user = $this->createUserFromRequest($request);
        $this->createProfileForUser($user, $request, UserProfile::OWNER, true);

        $continue_free_listing = session('continue_free_listing');

        Auth::login($user, true);

        if ($continue_free_listing) {
            return redirect('/member/posting/property-type');
        } else {
            return redirect('/member/account');
        }
    }

    public function showAgent() {

        if (Auth::check()) {
            return '/?reason=already-registered';
        }

        return view('visitor.register.freelance');
    }

    public function postAgent(CreateAgentRequest $request) {
        $user = $this->createUserFromRequest($request);
        $this->createProfileForUser($user, $request, UserProfile::FREELANCE_AGENT);

        $continue_free_listing = session('continue_free_listing');

        Auth::login($user, true);

        if ($continue_free_listing) {
            return redirect('/member/posting/property-type');
        } else {
            return redirect('/member/account');
        }
    }

    public function showRealEstate() {

        if (Auth::check()) {
            return '/?reason=already-registered';
        }

        return view('visitor.register.realestate');
    }

    public function postRealEstate(CreateRealCompanyRequest $request) {
        $user = $this->createUserFromRequest($request);
        $this->createProfileForUser($user, $request, UserProfile::COMPANY_AGENT);

        $continue_free_listing = session('continue_free_listing');

        Auth::login($user, true);

        if ($continue_free_listing) {
            return redirect('/member/posting/property-type');
        } else {
            return redirect('/member/account');
        }
    }

    public function showDeveloper() {

        if (Auth::check()) {
            return '/?reason=already-registered';
        }

        return view('visitor.register.developer');
    }

    public function postDeveloper(CreateRealDeveloperRequest $request) {
        $user = $this->createUserFromRequest($request);
        $this->createProfileForUser($user, $request, UserProfile::LAND_DEVELOPER);

        $continue_free_listing = session('continue_free_listing');

        Auth::login($user, true);

        if ($continue_free_listing) {
            return redirect('/member/posting/property-type');
        } else {
            return redirect('/member/account');
        }
    }

    protected function createUserFromRequest(Request $request) {

        $type = UserType::where('code', 'member')->first();

        $user = new User;
        $user->type_id = $type->id;
        $user->email = $request->input('email');
        $user->email_validated = false;
        $user->email_validation_token = str_random(20);
        $user->password = Hash::make($request->input('password'));
        $user->prefered_lang = App::getLocale();
        $user->save();

        $mail = new EmailValidation();
        $mail->send($user);

        return $user;
    }

    protected function createProfileForUser(User $user, Request $request, $type, $save_address = false) {

        //$province_id = $request->input('province_id');
        //$district_id = $request->input('district_id');

        $profile = new UserProfile;
        $profile->user_id = $user->id;
        $profile->company_name = $request->input('company_name');
        $profile->company_registration = $request->input('company_registration');
        $profile->firstname = $request->input('firstname');
        $profile->lastname = $request->input('lastname');
        $profile->phone = $request->input('phone');
        $profile->mobile = $request->input('mobile');
        $profile->line_id = $request->input('line_id');
        $profile->citizen_id = $request->input('citizen_id');
        $profile->member_type = $type;


        // Save file             

        /* if ($request->hasFile('image')) {

          $file = $request->file('image');

          $validator = Validator::make(['file' => $file], ['file' => 'required|mimes:png,gif,jpeg']);

          if ($validator->passes()) {
          $destinationPath = storage_path('app/public');
          $extention = $file->getClientOriginalExtension();
          $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
          $uploaded = $file->move($destinationPath, $filename);

          if ($uploaded) {
          $profile->citizen_id_attachment = $filename;
          }
          }
          } */

        if ($request->hasFile('photo_file')) {

            $file = $request->file('photo_file');

            $destinationPath = storage_path('app/public');
            $extention = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
            $uploaded = $file->move($destinationPath, $filename);

            if ($uploaded) {
                $profile->profile_picture_path = $filename;
            }
        }

        $profile->save();

        // Save address for [owner] only

        if ($save_address) {
            $address = new Address;
            $address->user_id = $user->id;
            $address->street_no = $request->input('street_no');
            $address->street_name = $request->input('street_name');
            $address->area = $request->input('area');
            $address->district = $request->input('district');
            $address->province = $request->input('province');
            $address->postal_code = $request->input('postal_code');
            $address->save();
        }


        return $profile;
    }

    /* public function getType() {

      $status = session('user_type_status');

      return view('visitor.register.type', ['status' => $status]);
      }

      public function getDetails(Request $request) {

      $type = $request->input('utp');

      session(['utp' => $type]);

      return view('visitor.register.' . $type);
      } */
}
