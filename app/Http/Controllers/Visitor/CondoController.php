<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Condo;
use App\Models\Properties\Facility;
use App\Models\Property;
use DB;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CondoController extends Controller {

    public function showCondoDetails(Request $request, $id) {

        try {
            //$condo = Condo::findOrFail($id);

            return view('visitor.condo.details');
        } catch (ModelNotFoundException $ex) {
            return redirect('/page-not-found');
        }
    }

    public function showCondoPreview(Request $request, $id) {

        $condo = Condo::findOrFail($id);
        $details = $condo->getDetails();

        $selected_facilities = $details->getFacilityIds();

        $facilities = Facility::all();

        $ids = DB::table('properties_belongs_to_condos')
                ->where('condo_id', $condo->id)
                ->pluck('property_id');

        $properties = Property::whereIn('id', $ids)->get();

        return view('visitor.condo.preview', [
            'details' => $details
            , 'condo' => $condo
            , 'facilities' => $facilities
            , 'selected_facilities' => $selected_facilities
            , 'properties' => $properties]);

        //return view('visitor.preview.condo');
    }

}
