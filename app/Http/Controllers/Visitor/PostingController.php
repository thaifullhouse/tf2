<?php

namespace App\Http\Controllers\Visitor;

//use Illuminate\Http\Request;
use Auth;
use DB;

class PostingController extends BaseController {

    public function getPropertyType() {

        // Check if user is already logged-in, if not redirect to registration page

        if (!Auth::check()) {
            session(['continue_free_listing' => true]);
            return redirect('/auth/register/type');
        }
        
        // Check if member type is visitor        
                
        $user = Auth::user();
        
        if($user->isVisitor()) {
            return redirect('/auth/register/type');
        }
        
        if($user->isManager()) {
            return redirect('/manager/dashboard');
        }

        $lang = $this->getCurrentLang();

        $provinces = DB::table('provinces')->pluck('name_' . $lang, 'id');

        return view('member.posting.type', ['provinces' => $provinces]);
    }

}
