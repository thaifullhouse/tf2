<?php

namespace App\Http\Controllers\Visitor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Models\User\Profile;
use App\Models\Properties\Details;
use App\Models\Properties\Facility;
use App\Models\Properties\Feature;
use App\Models\Properties\OptionalFeature;
use App\Services\Search;

class PropertyController extends Controller {

    /**
     *
     */
    public function getDetails(Request $request, $id) {
        $details = Details::findOrFail($id);
        $property = Property::findOrFail($id);

        $selected_facilities = $details->getFacilityIds();
        $selected_features = $details->getFeatureIds();
        $selected_optional_features = $details->getOptinoalFeatureIds();

        $facilities = Facility::all();
        $features = Feature::all();
        $optional_features = OptionalFeature::all();

        $searchService = new Search;

        $results = $searchService->findSimilar($property, 4);

        return view('visitor.property.details_' . $property->property_type, [
            'details' => $details
            , 'property' => $property
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'selected_optional_features' => $selected_optional_features
            , 'related' => $results]);
    }

    public function getDetails2(Request $request, $lt, $type) {

        $property = Property::where('listing_type', $lt)->where('poster_id', '>', 0)
                        ->where('property_type', $type)->firstOrFail();

        $details = Details::find($property->id);

        if ($details == null) {
            $details = new Details;
            $details->property_id = $property->id;
            $details->save();
        }


        $selected_facilities = $details->getFacilityIds();
        $selected_features = $details->getFeatureIds();
        $selected_optional_features = $details->getOptinoalFeatureIds();

        $facilities = Facility::all();
        $features = Feature::all();
        $optional_features = OptionalFeature::all();

        $searchService = new Search;

        $results = $searchService->findSimilar($property, 4);

        return view('visitor.property.' . $lt . '.' . $type, [
            'details' => $details
            , 'property' => $property
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'selected_optional_features' => $selected_optional_features
            , 'related' => $results]);
    }

    /**
     *
     */
    public function getPreview(Request $request, $id) {
        $property = Property::findOrFail($id);
        $details = $property->getDetails();

        $selected_facilities = $details->getFacilityIds();
        $selected_features = $details->getFeatureIds();
        $selected_optional_features = $details->getOptinoalFeatureIds();

        $facilities = Facility::all();
        $features = Feature::all();
        $optional_features = OptionalFeature::all();
        $poster = Profile::find($property->poster_id);

        return view('visitor.preview.' . $property->property_type, [
            'details' => $details
            , 'property' => $property
            , 'poster' => $poster
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'selected_optional_features' => $selected_optional_features]);
    }

    /**
     *
     */
    public function getReviews() {
        return view('visitor.property.reviews');
    }

}
