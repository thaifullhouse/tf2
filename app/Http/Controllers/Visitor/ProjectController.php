<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;


class ProjectController extends Controller {
    
    public function getDetails() {
        
        return view('visitor.project.details');
        
    }    
}