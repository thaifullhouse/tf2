<?php

namespace App\Http\Controllers\Visitor;

use Auth;
use App\Http\Controllers\Controller;
use Socialite;
use App\Models\User;
use App\Models\User\Type as UserType;
use App\Emails\EmailValidation;
use App\Emails\ResetPasswordLink;
use App\Services\UserProvider;
use Illuminate\Http\Request;
use Hash;

class AuthController extends Controller {

    public function login() {
        return view('visitor.auth.login');
    }

    public function validateUserEmail($id) {
        try {
            $user = User::findOrFail($id);

            return view('visitor.auth.validate', ['user' => $user]);
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }
    }

    public function verifyUserEmail(Request $request) {
        try {
            $id = $request->input('id');
            $token = $request->input('token');
            $user = User::findOrFail($id);

            $email_verified = false;

            if ($user->email_validated) {
                $email_verified = true;
            } else if ($user->email_validation_token == $token) {
                $email_verified = true;
                $user->email_validated = true;
                $user->email_validation_token = '';
                $user->active = true;
                $user->save();
            }

            return view('visitor.auth.verified', ['user' => $user, 'email_verified' => $email_verified]);
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        }
    }

    public function getRequestEmail($id) {

        try {
            $user = User::findOrFail($id);

            $mail = new EmailValidation();
            $mail->send($user);
            
        } catch (ModelNotFoundException $e) {
            return redirect('/');
        } catch (\Swift_TransportException $ex) {
            
        }

        return redirect('/auth/validate-email/' . $id);
    }
    
    public function getRequestPassword(Request $request)
    {
        return view('visitor.auth.request-password', [
            'password_reset_token' => $request->input('token'),
            'id' => $request->input('id')]);
    }
    
    public function createRequestPassword(Request $request)
    {
        $token = str_random(20);

        $user = User::where('email', $request->input('email'))->first();
        $user->password_reset_token = $token;
        $user->save();

        $email = new ResetPasswordLink;
        $email->send($user, $token);

        return view('visitor.auth.check_email_password', ['user' => $user]);
    }
    
    public function getUpdatePassword(Request $request)
    {
        return view('visitor.auth.update-password', [
            'password_reset_token' => array_get($_GET, 'token'),
            'id' => array_get($_GET, 'id')]);
    }

    public function postUpdatePassword(Request $request)
    {
        $user = User::find($request->input('id'));

        if ($user->password_reset_token == $request->input('token')) {
            $user->password = Hash::make($request->input('password'));
            $user->password_reset_token = null;
            $user->save();
        }
        return redirect('/auth/updated');
    }
    
    public function getUpdated() {
        return view('visitor.auth.updated');
    }

    /**
     * This method is used to redirect users properly after login.
     * Putting it in middleware will hide the logic, so better to put this here.
     */
    public function autoRedirect() {
        if (Auth::check()) {
            $user = Auth::user();
            $usertype = UserType::find($user->type_id);

            $redirect = '/';

            switch ($usertype->code) {
                case 'admin':
                    $redirect = '/admin/dashboard';
                    break;
                case 'manager':
                    $redirect = '/manager/dashboard';
                    break;
                case 'staff':
                    $redirect = '/staff/dashboard';
                    break;
                case 'member':
                    $redirect = '/member/settings';
                    break;
                default:

                    break;
            }
            return redirect($redirect);
        } else {
            return redirect('/');
        }
    }

    /**
     * Facebook auth
     */
    public function redirectToFacebook() {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleFacebookCallback() {
        $fbuser = Socialite::driver('facebook')->user();

        $user = UserProvider::findOrCreateFromFB($fbuser);

        Auth::login($user, false);

        return redirect('/');
    }

    /**
     * Google + auth
     */
    public function redirectToGoogle() {
        return Socialite::driver('google')->redirect();
    }

    public function handleGoogleCallback() {
        
        $user = Socialite::driver('google')->user();
        
        var_dump($user);
    }

    /**
     * Twitter auth
     */
    public function redirectToTwitter() {
        return Socialite::driver('twitter')->redirect();
    }

    public function handleTwitterCallback() {
        $user = Socialite::driver('twitter')->user();
        var_dump($user);
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }

}
