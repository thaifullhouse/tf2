<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;


class BaseController extends Controller {
    
    protected function getUniqueFileName()
    {
        return uniqid() . '-' . strtolower(base_convert(time(), 10, 16));
    }
    
    protected function getCurrentLang()
    {
        $langs = ['en', 'th'];
        $lang = session('lang');        
        return in_array($lang, $langs) ? $lang : 'th';
    }
}