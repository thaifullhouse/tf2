<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Poi;
use App\Models\Condo;
use App\Models\Property;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SearchController extends Controller {
    
    protected $all;

    /**
     * Search property
     */
    public function findProperty(Request $request) {

        $target = $request->input('tr');
        $rel = $request->input('rel');
        $poi = null;
        $mapCenter = null;
        $id = $request->input('id');
        
        $this->all = $request->all();

        if ($rel && $rel == 'poi') {
            try {
                $poi = Poi::findOrFail($id);
                $mapCenter = $poi->getLatLng();
                $mapCenter->zl = 15;
            } catch (ModelNotFoundException $ex) {
                
            }
        }

        if ($rel == 'condo') {
            $re['pr'] = 'co';
            if ($id > 0) {
                try {
                    $condo = Condo::find($id);
                    $location = $condo->get_location();

                    $mapCenter = new \stdClass();
                    $mapCenter->lat = $location['lat'];
                    $mapCenter->lng = $location['lng'];
                    $mapCenter->zl = 15;
                    
                    $this->updatePropertyType('cd');
                    
                } catch (ModelNotFoundException $ex) {
                    
                }
            }
        }

        if ($rel == 'property') {
            $re['pr'] = 'np';
            if ($id > 0) {
                try {
                    $property = Property::find($id);
                    $mapCenter = $property->getLatLng();
                    $mapCenter->zl = 15;
                    
                    $this->updateListingType($property->listing_type);
                    $this->updatePropertyType($property->property_type);
                    
                } catch (ModelNotFoundException $ex) {
                    
                }
            }
        }

        return view('visitor.search.search', ['target' => $target
                , 'mapCenter' => $mapCenter
                , 'all' => $this->all
                , 'poi' => $poi]);
    }

    public function showProjects() {
        return view('visitor.search_project');
    }
    
    protected function updateListingType($type) {
        
        $lt = array_get($this->all, 'lt');
        $lt = explode(',', $lt);
        $lt[] = $type;
        
        $lt = array_unique($lt);
        $this->all['lt'] = implode(',', array_filter($lt));
    }
    
    protected function updatePropertyType($type) {
        $types = [
            'apartment' => 'ap',
            'condo' => 'co',
            'detachedhouse' => 'dh',
            'townhouse' => 'th'
        ];
        
        
        $lt = array_get($this->all, 'pt');
        $lt = explode(',', $lt);
        $lt[] = array_get($types, $type);
        
        $lt = array_unique($lt);
        $this->all['pt'] = implode(',', array_filter($lt));
    }

}
