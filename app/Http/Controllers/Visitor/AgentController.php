<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User\Profile;
use DB;

class AgentController extends Controller {

    /**
     * Display profile details
     */
    public function getDetails() {
        
        //DB::table('user_profiles')->where('user_id', $id)->update(['visites' => DB::raw('visites + 1')]);
        
        return view('visitor.agent.details');
    }

    /**
     * Display profile details
     */
    public function getRealDetails() {
        return view('visitor.agent.real-details');
    }

    /**
     *  Search agent
     */
    public function search(Request $request) {

        $search = $request->all();

        $query = DB::table('user_profiles');
        $query->leftJoin('users', 'users.id', '=', 'user_profiles.user_id');

        $wheres = [];

        $typ = strtolower($request->input('typ', 'all'));
        
        $bindings = [];

        // Type

        if ($typ == Profile::FREELANCE_AGENT) {
            $query->whereRaw('user_profiles.member_type = ?');
            $bindings = [Profile::FREELANCE_AGENT];
            
        } else if ($typ == Profile::COMPANY_AGENT) {
            $query->whereRaw('user_profiles.member_type = ?');
            $bindings = [Profile::COMPANY_AGENT];
        } else {
            $query->where(function ($query) {
                $query->orWhereRaw('user_profiles.member_type = ?')
                        ->orWhereRaw('user_profiles.member_type = ?');
            });
            $bindings = [Profile::FREELANCE_AGENT, Profile::COMPANY_AGENT];
        }
        
        // Name

        $name = strtolower($request->input('nam', ''));
        $name = trim($name);

        if (!empty($name)) {
            $query->where(function ($query) use ($name) {
                $query->orWhereRaw('user_profiles.company_name ~* ?')
                        ->orWhereRaw('user_profiles.firstname ~* ?')
                        ->orWhereRaw('user_profiles.lastname ~* ?');
            });
            
            $bindings = array_merge($bindings, [$name, $name, $name]); 
        }
        
        

        // Location
        
        $location = strtolower($request->input('loc', ''));
        $location = trim($location);
        
        if (!empty($location)) {
            $query->whereRaw('user_profiles.address ~* ?');
            $bindings[] = $location;
        }
        
        // Verified agent only
        
        $query->where('user_profiles.agent_verified', true);
        $bindings[] = true; 
        
        //$query = $query->where('active', true);
        
        if (count($bindings)) {
            $query->setBindings($bindings);
        }
        
        $query->where('users.active', true);
        
        // Order 
        
        $query->orderBy('user_profiles.search_ranking', 'ASC');
        
        $agents = $query->paginate(9);

        return view('visitor.agent.search', [
            'search' => $search,
            'agents' => $agents
        ]);
    }

}
