<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;

class RealEstateController extends Controller {
    
    public function getDetails() {
        
        return view('visitor.realestate.details');
        
    }    
}