<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;

class TestController extends Controller {
	/**
	 * 
	 * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
	 */
	public function index() {
		return view ( 'visitor.test.index' );
	}
}