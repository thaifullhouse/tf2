<?php

namespace App\Http\Controllers\Visitor;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display profile details
     */
    public function getDetails()
    {
        return view('visitor.profile.details');
    }

    /**
     * Display profile reviews. This should probably be loaded from an Ajax request
     */
    public function getReviews()
    {
        return view('visitor.profile.reviews');
    }

    /**
     * Allow authentified visitor to send a message
     */
    public function sendMessage(Request $request)
    {

    }

    /**
     * Allow authentified visitor to post a review
     */
    public function postReview(Request $request)
    {

    }

}
