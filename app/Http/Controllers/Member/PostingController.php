<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Auth;
use DB;

class PostingController extends BaseController {

    public function getPropertyType() {

        // Check if user is already logged-in, if not redirect to registration page

        if (!Auth::check()) {
            session(['continue_free_listing' => true]);
            return redirect('/auth/register');
        }

        $provinces = DB::table('provinces')->pluck('name', 'id');

        return view('member.posting.type', 
                ['provinces' => $provinces]);
    }
    
    public function savePropertyDetails(Request $request) {
        
    }
    
    public function getPropertyDetails(Request $request) {
        
        $type = $request->input('tp');
        
        return view('member.posting.' . $type);
    }
    
    /**
     * Save Apartment details
     * @param Request $request
     */
    public function saveApartmentDetails(Request $request) {
        
    }
    
    /**
     * Save Condo details
     * @param Request $request
     */
    public function saveCondoDetails(Request $request) {
        
    }
    
    /**
     * Save House details
     * @param Request $request
     */
    public function saveHouseDetails(Request $request) {
        
    }
    
    /**
     * Save Townhouse details
     * @param Request $request
     */
    public function saveTownhouseDetails(Request $request) {
        
    }
    
    /**
     * Save Project details
     * @param Request $request
     */
    public function saveProjectDetails(Request $request) {
        
    }

}
