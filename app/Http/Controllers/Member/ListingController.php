<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Models\Property;
use App\Models\Properties\Facility;
use App\Models\Properties\Feature;
use App\Models\Properties\OptionalFeature;
use App\Models\Condo;
use App\Models\Poi;
use App\Models\User\Profile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use DB;
use Validator;
use App\Helpers\ImageHelper;

class ListingController extends BaseController {

    public function index() {

        $user = Auth::user();

        $filter_status = session('filter_status');
        $filter_listing = session('filter_listing');


        $properties = Property::where('poster_id', $user->id)->orderBy('id', 'desc')->paginate(10);

        return view('member.listings.' . $this->getCustomerType(), [
            'active' => 'listing'
            , 'properties' => $properties
            , 'filter_status' => $filter_status
            , 'filter_listing' => $filter_listing]);
    }

    public function create(Request $request) {

        $user = Auth::user();

        session(['listing_type' => $request->input('listing_type')]);

        $lang = \App::getLocale();

        $property = new Property;
        $property->poster_id = $user->id;
        $property->listing_type = $request->input('listing_type');
        $property->property_type = $request->input('property_type');
        $property->name = json_encode([$lang => $request->input('property_name')]);
        //$property->property_name = $request->input('property_name');
        $property->street_number = $request->input('street_number');
        $property->street_name = $request->input('street_name');
        $property->postale_code = $request->input('postale_code');

        $property->save();

        DB::table('user_profiles')->where('user_id', $user->id)->update(['has_paid' => true]);

        $condo_id = $request->input('condo_id');

        if ($condo_id > 0) {
            try {
                $condo = Condo::findOrFail($condo_id);
                $property->userCondoLocations($condo);
            } catch (ModelNotFoundException $ex) {
                
            }
        } else {

            $name = $request->input('property_name');

            $condo = new Condo;
            $condo->name = json_encode(['en' => $name, 'th' => $name]);
            $condo->postcode = $request->input('postale_code');
            $condo->user_submited = true;
            $condo->active = false;

            if ($request->input('province_id')) {
                $condo->province_id = $request->input('province_id');
                $property->province_id = $request->input('province_id');
            }

            if ($request->input('district_id')) {
                $condo->district_id = $request->input('district_id');
                $property->district_id = $request->input('district_id');
            }

            if ($request->input('area_id')) {
                $condo->area_id = $request->input('area_id');
                $property->area_id = $request->input('area_id');
            }

            $property->save();

            $condo->save();

            $condo_id = $condo->id;

            // Save lat, lng

            $lat = $request->input('lat');
            $lng = $request->input('lng');

            if ($lat != 0 || $lng != 0) {
                DB::update('UPDATE properties SET location = ST_GeomFromText(?, 4326) WHERE id = ?', ['POINT(' . implode(' ', [$lng, $lat]) . ' 0.0)', $property->id]);
                DB::update('UPDATE condo_list SET location = ST_GeomFromText(?, 4326) WHERE id = ?', ['POINT(' . implode(' ', [$lng, $lat]) . ' 0.0)', $condo_id]);
            }
        }

        DB::insert('INSERT INTO properties_belongs_to_condos (condo_id, property_id) VALUES (?, ?)', [$condo_id, $property->id]);

        return redirect('/member/listing/details/' . $property->id);
    }

    public function getDetails($id) {

        $property = Property::find($id);
        $details = $property->getDetails();

        $facilities = Facility::all();
        $features = Feature::all();
        $optional_features = OptionalFeature::all();

        $selected_facilities = $details->getFacilityIds();
        $selected_features = $details->getFeatureIds();
        $selected_optional_features = $details->getOptinoalFeatureIds();

        $developers = Profile::where('member_type', 'developer')->get();

        $bts_list = Poi::where('type', 'bts')->get();
        $mrt_list = Poi::where('type', 'mrt')->get();
        $apl_list = Poi::where('type', 'apl')->get();

        $selected_bts_list = $property->getNearbyTransIdByType('bts');
        $selected_mrt_list = $property->getNearbyTransIdByType('mrt');
        $selected_apl_list = $property->getNearbyTransIdByType('apl');

        $images = [];

        $media = DB::select('SELECT * FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$id, 'image']);

        if (count($media)) {
            foreach ($media as $row) {
                $img = new ImageHelper($row);
                $img->id = $row->id;
                $images[] = $img;
            }
        }
        
        $template = $property->property_type == 'detachedhouse' ? 'house' : $property->property_type;

        return view('member.posting.' . $template, [
            'details' => $details
            , 'property' => $property
            , 'facilities' => $facilities
            , 'features' => $features
            , 'optional_features' => $optional_features
            , 'selected_facilities' => $selected_facilities
            , 'selected_features' => $selected_features
            , 'selected_optional_features' => $selected_optional_features
            , 'images' => $images
            , 'status' => session('status')
            , 'developers' => $developers
            , 'bts_list' => $bts_list
            , 'mrt_list' => $mrt_list
            , 'apl_list' => $apl_list
            , 'selected_bts_list' => $selected_bts_list
            , 'selected_mrt_list' => $selected_mrt_list
            , 'selected_apl_list' => $selected_apl_list]);
    }

    public function postDetails(Request $request, $id) {

        $property = Property::find($id);
        
        $property->developer_id = $request->input('developer_id');
        $property->psf = $request->input('psf') + 0;
        $property->save();

        $availability_date = $request->input('availability_date');
        
        $details = $property->getDetails();

        $details->deposite = $request->input('deposite');
        $details->listing_price = (int) $request->input('listing_price');
        $details->floor_num = (int) $request->input('floors');
        $details->bedroom_num = (int) $request->input('bedrooms');
        $details->bathroom_num = (int) $request->input('bathrooms');
        $details->total_size = (int) $request->input('size');
        $details->year_built = (int) $request->input('year');
        $details->tower_num = (int) $request->input('tower');
        $details->land_size = (int) $request->input('land_size');

        $details->setJsonTranslatedField('other_details', $request->input('other_details', []));
        $details->setJsonTranslatedField('address', $request->input('address', []));
        $details->setJsonTranslatedField('title', $request->input('title', []));

        $details->ylink1 = $request->input('ylink1');
        $details->ylink2 = $request->input('ylink2');

        if ($property->listing_type == 'rent') {
            $details->minimal_rental_period = $request->input('minimal_period');
        }

        if ($availability_date) {
            list($date, $month, $year) = explode('/', $availability_date);
            $details->availability_date = $year . '-' . $month . '-' . $date;
        }
        $details->save();

        $features = $request->input('features');

        DB::delete('DELETE FROM properties_have_features WHERE property_id = ?', [$id]);

        if (count($features)) {

            $data = [];
            foreach ($features as $value) {
                $data[] = [
                    'property_id' => $id,
                    'feature_id' => $value
                ];
            }
            DB::table('properties_have_features')->insert($data);
        }

        $facilities = $request->input('facilities');

        DB::delete('DELETE FROM properties_have_facilities WHERE property_id = ?', [$id]);

        if (count($facilities)) {
            $data = [];
            foreach ($facilities as $value) {
                $data[] = [
                    'property_id' => $id,
                    'facility_id' => $value
                ];
            }
            DB::table('properties_have_facilities')->insert($data);
        }

        $options_features = $request->input('options_features');

        DB::delete('DELETE FROM properties_have_optional_features WHERE property_id = ?', [$id]);

        if (count($options_features)) {
            $data = [];
            foreach ($options_features as $value) {
                $data[] = [
                    'property_id' => $id,
                    'feature_id' => $value
                ];
            }
            DB::table('properties_have_optional_features')->insert($data);
        }

        // Save BTS, MRT, APL

        DB::delete('DELETE FROM property_nearby_transportations WHERE property_id = ?', [$id]);

        $data = [];

        $trains = $request->input('nearby_bts', []);

        foreach ($trains as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'bts'
            ];
        }

        $trains = $request->input('nearby_mrt', []);

        foreach ($trains as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'mrt'
            ];
        }

        $trains = $request->input('nearby_apl', []);

        foreach ($trains as $train) {
            $data[] = [
                'property_id' => $id,
                'trans_id' => $train,
                'type' => 'apl'
            ];
        }

        DB::table('property_nearby_transportations')->insert($data);

        return back();
    }
    
    public function postUpload(Request $request) {
        
        $property_id = $request->input('property_id');

        $files = $request->file('images');

        $errors = [];

        foreach ($files as $file) {

            $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'

            $validator = Validator::make(array('file' => $file), $rules);

            if ($validator->passes()) {
                $destinationPath = storage_path('app/public');
                $extention = $file->getClientOriginalExtension();
                $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
                
                $uploaded = $file->move($destinationPath, $filename);

                if ($uploaded) {
                    $errors[] = $destinationPath . '/' . $filename;
                    // Resize and add watermak
                    ImageHelper::resizeAndAddWatermark($destinationPath . '/' . $filename);

                    DB::insert('INSERT INTO properties_have_media (property_id, media_type, filepath, created_at, updated_at) VALUES (?, ?, ?, NOW(), NOW())', [$property_id, 'image', $filename]);
                }
                else {
                    $errors[] = ['not_uploaded' => $file];
                }
            }
            else {
                $errors[] = ['error' => $file];
            }
        }

        return response()->json(['errors' => $errors]);
    }

}
