<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\Models\Purchase;
//use App\Models\User\Account;

class PurchaseController extends BaseController {
    
    public function index() {
        
        $user = Auth::user();
        $account = $user->getPurchaseAccount();
        $purchases = Purchase::where('account_id', $account->id)->orderBy('updated_at', 'desc')->paginate(10);
        
        return view('member.purchase.' . $this->getCustomerType()
                , ['active' => 'purchase', 'purchases' => $purchases]);
    }
    
}