<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Auth;

class BaseController extends Controller
{
    protected function getUniqueFileName()
    {
        return uniqid() . '-' . strtolower(base_convert(time(), 10, 16));
    }
    
    protected function getCurrentLang()
    {
        $langs = ['en', 'th'];
        $lang = session('lang');        
        return in_array($lang, $langs) ? $lang : 'th';
    }
    
    public function getCustomerType() {
        
        $user = Auth::user();
        
        return $user->customer_type ? $user->customer_type : 'owner';
    }
}
