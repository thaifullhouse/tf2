<?php

namespace App\Http\Controllers\Member;

class ProfileController extends BaseController {
    
    public function index() {
        
        return view('member.profile.index', ['active' => 'profile']);
        
    }
    
}

