<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use Auth;
use Validator;
use DB;

class SettingController extends BaseController
{
    /**
     *
     */
    public function index()
    {
        return view('member.settings.' . $this->getCustomerType()
                , ['active' => 'settings']);
    }
    
    public function update(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $profile = $user->getProfile();

            if ($profile) {
                $field = $request->input('name');

                $before = $profile->$field;                
                $after = $request->input('after');

                if ($field == 'birthday') {
                    list($day, $month, $year) = explode('/', $after);
                    $after = $year . '-' . $month . '-' . $day;
                }

                if ($after != $before) {
                    $profile->$field = $after;
                    $profile->save();
                }
            }
        }
    }

    /**
     *
     */
    public function uploadCoverPicture(Request $request)
    {
        $file = $request->file('image');

    	$user = Auth::user();

        $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            $destinationPath = storage_path('app/public');
            $extention = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
            $uploaded = $file->move($destinationPath, $filename);

            if ($uploaded) {
                DB::update('UPDATE user_profiles SET cover_picture_path = ?, updated_at = NOW() WHERE user_id = ?', [$filename, $user->id]);
            }
        }
    }

    /**
     *
     */
    public function uploadProfilePicture(Request $request)
    {
        $file = $request->file('image');

        $user = Auth::user();

        $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            $destinationPath = storage_path('app/public');
            $extention = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
            $uploaded = $file->move($destinationPath, $filename);

            if ($uploaded) {
                DB::update('UPDATE user_profiles SET profile_picture_path = ?, updated_at = NOW() WHERE user_id = ?', [$filename, $user->id]);
            }
        }
    }
}
