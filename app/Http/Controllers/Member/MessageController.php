<?php

namespace App\Http\Controllers\Member;

use Auth;
use DB;
use App\Models\Message;

class MessageController extends BaseController
{
    /**
     * 
     * @return type
     */
    public function index() {
        
        $user = Auth::user();
        
        $messages = Message::where('receiver_id',  $user->id)->orderby('created_at', 'desc')->paginate(15);
        
        return view('member.messages.' . $this->getCustomerType()
                , ['active' => 'message', 'messages' => $messages]);
    }

    /**
     * 
     * @return type
     */
    public function getNotifications()
    {        
        $user = Auth::user();
        
        $notifications = DB::select('SELECT nm.* FROM notification_receivers AS nr '
                . 'LEFT JOIN notification_messages AS nm ON nm.id = nr.message_id '
                . 'WHERE nr.receiver_id = ?', [$user->id]);
        
        return view('member.notifications.' . $this->getCustomerType()
                , ['active' => 'notifications', 'notifications' => $notifications]);
    }
}
