<?php

namespace App\Http\Controllers\Member;

use App\Models\Package;
//use App\Models\Purchase;
use App\Models\Bank;
use App\Http\Requests\Package\BuyRequest as BuyPackageRequest;
//use Auth;

class PackageController extends BaseController {

    public function buyExclusive() {

        $packages = Package::where('type', 'exclusive')->orderBy('normal_price', 'asc')->get();
        $banks = Bank::all();

        return view('member.package.buy-exclusive', [
            'packages' => $packages,
            'banks' => $banks
        ]);
    }

    public function buyFeatured() {

        $packages = Package::where('type', 'featured')->orderBy('normal_price', 'asc')->get();
        $banks = Bank::all();

        return view('member.package.buy-featured', [
            'packages' => $packages,
            'banks' => $banks
        ]);
    }

    public function postBuy(BuyPackageRequest $request) {

        $package = Package::find($request->input('package'));

        $purchase = $package->createPurchase($request);

        if ($purchase) {
            $orderId = sprintf('%06d', $purchase->id);

            if ($package->type === 'exclusive') {
                $orderId = 'E-' . $orderId;
            } else {
                $orderId = 'F-' . $orderId;
            }

            return response()->json([
                'status' => 'OK',
                'id' => $purchase->id,
                'orderId' => $orderId
            ]);
        }
        else {
             return response()->json(['status' => 'ERROR']);
        }
    }

}
