<?php

namespace App\Http\Controllers\Rest;

use App\Http\Requests\CondoReviewCreate;
use App\Models\Condo\Review As CondoReview;
use App\Models\Condo;
use App\Models\Condo\Details;
use App\Helpers\Language;
use Illuminate\Http\Request;
use App\Models\Property;
use DB;

class CondoController extends BaseController {

    public function getDetails($id) {

        $faker = \Faker\Factory::create();

        $condo = Condo::find($id);
        $details = Details::find($id);

        $json = [
            'name' => $condo->getTranslatedField('name'),
            'salesPrice' => $details->starting_sales_price,
            'principlePrice' => ($details->starting_sales_price * 1.5),
            'ranking' => mt_rand(0, 10),
            'hours' => $details->office_hours,
            'built' => $details->year_built,
            'tower' => $details->number_of_tower,
            'floor' => $details->total_floors,
            'totalUnits' => $details->total_units,
            'totalFloors' => $details->total_floors,
            'roomType' => $details->room_types,
            'district' => $details->district,
            'parkingSpace' => mt_rand(20, 200),
            'contact' => $details->contact_person,
            'website' => $details->website,
            'images' => $this->getRandomImages(),
            'facilities' => [
                'gym' => true,
                'pool' => true,
                'sauna' => false,
                'playground' => false,
                'garden' => true,
                'readingRoom' => false,
                'elevetor' => true,
                'security' => true,
                'cctv' => false,
                'parking' => true,
                'supermarket' => false,
                'laundry' => false
            ],
            'details' => $condo->getTranslatedField('details'),
            'address' => $condo->getTranslatedField('address'),
            'location' => [
                'lat' => $faker->latitude(13.57, 13.82),
                'lng' => $faker->longitude(100.31, 100.88),
                'hospitals' => $this->getRandomPlaces(3),
                'banks' => $this->getRandomPlaces(3),
                'shops' => $this->getRandomPlaces(3),
                'schools' => $this->getRandomPlaces(3),
                'btsStations' => $this->getRandomPlaces(3),
                'mrtStations' => $this->getRandomPlaces(3),
                'aplStations' => $this->getRandomPlaces(3)
            ],
            'listings' => $this->getRandomListing(),
            'review' => [
                'howToGetThere' => [
                    'details' => $faker->text,
                    'images' => $this->getRandomImages(5)
                ],
                'roomType' => [
                    'details' => $faker->text,
                    'images' => $this->getRandomImages(5)
                ],
                'facilities' => [
                    'details' => $faker->text,
                    'images' => $this->getRandomImages(5)
                ],
                'floorPlan' => [
                    'details' => $faker->text,
                    'images' => $this->getRandomImages(5)
                ],
                'overview' => [
                    'details' => $faker->text
                ],
                'video' => [
                    'url' => 'https://www.youtube.com/embed/DiK1K665YB4'
                ]
            ],
            'comments' => [
                'itemsPerPage' => 10,
                'maxSize' => 6,
                'totalItems' => 10,
                'currentPage' => 1,
                'commentList' => $this->getRandomComments()
            ]
        ];

        return response()->json([
                    'status' => 'OK',
                    'condo' => $json
        ]);
    }

    protected function getRandomComments() {

        $faker = \Faker\Factory::create();

        $comments = [];

        for ($i = 0; $i < 10; $i++) {
            $comments[] = [
                'id' => ($i + 1),
                'likes' => mt_rand(0, 100),
                'dislikes' => mt_rand(0, 100),
                'email' => $faker->email,
                'created_at' => $faker->date('d.m.Y'),
                'review' => $faker->text,
                'ratings' => [
                    'facilities' => mt_rand(0, 10),
                    'design' => mt_rand(0, 10),
                    'management' => mt_rand(0, 10),
                    'security' => mt_rand(0, 10),
                    'amenities' => mt_rand(0, 10),
                    'accesibility' => mt_rand(0, 10)
                ],
                'images' => $this->getRandomImages(3)
            ];
        }

        return $comments;
    }

    protected function getRandomImages($count = 10) {

        $faker = \Faker\Factory::create();

        $images = [];

        for ($i = 0; $i < $count; $i++) {
            $images[] = $faker->imageUrl(300, 300);
        }
        return $images;
    }

    protected function getRandomPlaces($count = 3) {
        $places = [];

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < $count; $i++) {
            $places[] = [
                'name' => $faker->name,
                'lat' => $faker->latitude(13.57, 13.82),
                'lng' => $faker->longitude(100.31, 100.88),
            ];
        }

        return $places;
    }

    protected function getRandomListing() {

        $listings = [];

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 5; $i++) {

            $images = [];

            for ($j = 0; $j < 5; $j++) {
                $images[] = $faker->imageUrl(100, 100);
            }

            $type = mt_rand(0, 1000) > 500 ? 'rent' : 'sale';

            $listings[] = [
                'property_name' => $faker->name,
                'address_en' => $faker->address,
                'listing_type' => $type,
                'details' => [
                    'listing_price' => $faker->randomNumber()
                ],
                'images' => $images
            ];
        }

        return $listings;
    }

    public function getReviews(Request $request, $id) {

        $reviews = CondoReview::where('condo_id', $id)->orderBy('id', 'desc')->paginate(15);

        // Append ratings

        foreach ($reviews as $id => $review) {
            $ratings = DB::table('condo_review_scores')->where('review_id', $review->id)->pluck('score', 'domain');
            $review->ratings = $ratings;
            $reviews[$id] = $review;
        }

        return response()->json([
                    'status' => 'OK',
                    'reviews' => $reviews->all()
        ]);
    }

    public function postReview(CondoReviewCreate $request) {

        $review = CondoReview::createFromUserRequest($request);

        return response()->json([
                    'status' => 'OK',
                    'review' => $review
        ]);
    }

    /**
     * Review answer
     * @param Request $request
     */
    public function postComment(Request $request) {

        return response()->json([
                    'status' => 'OK'
        ]);
    }

    public function getProperties(Request $request, $id) {

        $ids = DB::table('properties_belongs_to_condos')->where('condo_id', $id)->pluck('property_id');

        $properties = Property::whereIn('id', $ids)->get();

        return response()->json([
                    'status' => 'OK',
                    'properties' => $properties->all()
        ]);
    }

    public function getCondomium(Request $request) {

        //$langs = Language::getActiveCodes();

        //$lang = $this->getCurrentLang();
        $terms = preg_split('/(\s+)/', $request->input('term'));

        /*$values = [];

        $where_clause = [];

        $locale = \App::getLocale();*/

        $query = DB::table('condo_list')
                ->select('id'
                        , 'name'
                        , 'area_id'
                        , 'province_id'
                        , 'district_id'
                        , 'location_name'
                        , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                        , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat'))
                ->where('active', true)
                ->where('user_submited', false)
                ->where('new_project', false);

        $query->where(function($query) use ($terms) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                foreach ($terms as $_term) {
                    $query->orWhere('name->' . $lang, '~*', $_term);
                }
            }
        });

        $condos = $query->get();

        /*foreach ($condos as $id => $_condo) {
            $name = json_decode($_condo->name, true);
            $_condo->label = array_get($name, $locale);
            $condo[$id] = $_condo;
        }*/

        return response()->json(['suggestions' => $condos]);
    }

}
