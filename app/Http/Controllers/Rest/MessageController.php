<?php

namespace App\Http\Controllers\Rest;

use App\Models\Message;
use App\Http\Requests\Message\Create as CreateMessageRequest;

class MessageController extends BaseController {

    public function send(CreateMessageRequest $request) {

        $message = new Message;
        $message->email = $request->input('email');
        $message->receiver_id = $request->input('id');
        $message->name = $request->input('name');
        $message->phone = $request->input('phone');
        $message->message_body = $request->input('message');
        $message->save();
        
        return response()->json(['status' => 'OK', 'message_id' => $message->id]);
    }
}
