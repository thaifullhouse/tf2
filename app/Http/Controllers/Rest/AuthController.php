<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use Validator;
use Auth;
use App\Emails\EmailValidation;
use App\Models\User;
use App\Models\User\Type as UserType;
use App\Models\User\Profile as UserProfile;
use Hash;

class AuthController extends BaseController {
    
    public function postLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'errors' => ['<li>Email or password incorrect</li>']]);
        } else {
            if ($this->_doLogin($request)) {
                // Login did succeed
                $user = Auth::user();

                if ($user->email_validated == true) {
                    return response()->json(['status' => 'OK', 'user' => $user->toArray()]);
                } else {
                    Auth::logout();
                    return response()->json(['status' => 'NOT_VALIDATED', 'user' => $user->toArray()]);
                }
            } else {
                return response()->json(['status' => 'ERROR', 'errors' => ['<li>Email or password incorrect</li>']]);
            }
        }
    }
    
    public function postRegistration(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => 'ERROR', 'errors' => $validator->errors()->all('<li>:message</li>')]);
        } else {
            $user = $this->_doRegister($request);

            //--------------------------------------
            // Send user password
            //--------------------------------------
            
            try {
                $mail = new EmailValidation();
                $mail->send($user);
            } catch (\Swift_TransportException $ex) {

            }

            return response()->json(['status' => 'OK', 'user' => $user->toArray()]);
        }
    }
    
    /**
     * Process login data.
     */
    protected function _doLogin($request)
    {
        $post = $request->all();
        return Auth::attempt(array_only($post, ['email', 'password']), true);
    }

    /**
     * Process registration data.
     */
    protected function _doRegister($request)
    {
        // Get member type

        $type = UserType::where('code', '=', 'visitor')->first();

        $user = new User();
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->type_id = $type->id;
        $user->email_validated = false;
        $user->email_validation_token = str_random(20);
        $user->save();
        
        // Create user profile
        
        $profile = new UserProfile;
        $profile->user_id = $user->id;
        $profile->member_type = UserProfile::VISITOR;
        $profile->save();

        return $user;
    }
}