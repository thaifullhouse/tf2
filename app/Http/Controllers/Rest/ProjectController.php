<?php

namespace App\Http\Controllers\Rest;

//use App\Models\Property;
use App\Models\Condo;

use Illuminate\Http\Request;

class ProjectController extends BaseController {   
    
    
    public function getDetails(Request $request, $id) {
        
        $condo = Condo::find($id);
        
        return response()->json([
            'status' => 'OK',
            'project' => $condo
        ]);
        
    }    
}