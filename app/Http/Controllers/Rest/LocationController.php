<?php

namespace App\Http\Controllers\Rest;

use DB;

class LocationController extends BaseController {
    
    public function getDistrict($id) {
        $data = DB::table('districts')->select('id', 'name')->where('province_id', $id)->get();
        
        $locale = \App::getLocale();
        
        foreach($data as $id => $row) {
            $name = json_decode($row->name, true);
            $row->name = array_get($name, $locale);
            $data[$id] = $row;
        }
        
        return response()->json($data);
    }
    
    public function getArea($id) {
        $data = DB::table('geo_area')->select('id', 'name')->where('district_id', $id)->get();
        
        $locale = \App::getLocale();
        
        foreach($data as $id => $row) {
            $name = json_decode($row->name, true);
            $row->name = array_get($name, $locale);
            $data[$id] = $row;
        }
        
        return response()->json($data);
    }
    
}