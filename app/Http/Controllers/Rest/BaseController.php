<?php

namespace App\Http\Controllers\Rest;

use App\Http\Controllers\Controller;
use Auth;

class BaseController extends Controller {
    
    protected $user;
    
    public function __construct() {
        if (Auth::check()) {
            $this->user = Auth::user();
        }
        else {
            $this->user = 0;
        }
    }    
}