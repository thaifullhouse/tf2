<?php

namespace App\Http\Controllers\Rest;

//use Illuminate\Http\Request;

use DB;

class PropertyController extends BaseController {

    public function __construct() {

        parent::__construct();
    }

    /* public function toggleFavorite(Request $request) {

      } */

    public function getImageMeta($id) {

        $file = null;

        $count = DB::table('properties_have_media')->where('property_id', $id)
                ->count();

        if ($count > 0) {
            $file = DB::table('properties_have_media')->where('property_id', $id)
                    ->value('filepath');

            $file = url('/thumb/350x350/' . $file);
        }

        return response()->json([
                    'main' => $file,
                    'count' => $count
        ]);
    }

    public function getMapInfo($id) {

        $data = DB::table('properties')
                ->select('property_details.listing_price'
                        , 'properties.name'
                        , 'property_details.bedroom_num'
                        , 'property_details.bathroom_num'
                        , 'property_details.total_size')
                ->leftJoin('property_details', 'property_details.property_id', 'properties.id')
                ->where('properties.id', $id)
                ->first();

        $image = DB::table('properties_have_media')->where('property_id', $id)
                ->value('filepath');

        if ($image) {
            $image = url('/thumb/100x100/' . $image);
        }
        else {
            $image = '/img/default-property.png';
        }
        
        $names = json_decode($data->name, true);
        
        $name = __trget($names);
        
        $data->name = $name;

        return view('visitor.property.map-info', ['data' => $data, 'image' => $image]);
    }

}
