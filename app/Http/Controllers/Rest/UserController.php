<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
use Auth;
use DB;

/**
 *
 */
class UserController extends BaseController {

    /**
     *
     */
    public function __construct() {

        parent::__construct();
    }

    /**
     *
     * @param Request $request
     * @param type $id
     */
    public function favoriteProperty(Request $request, $id) {

        $user = Auth::user();

        if (is_object($user)) {
            $type = $request->input('t');
            if ($type == 'condo') {
                $result = $user->toggleFavoriteCondo($id);
                return response()->json(['status' => 'OK', 'favorites' => $user->getFavoriteCondos(), 'result' => $result]);
            } else {
                $result = $user->toggleFavoriteProperty($id);
                return response()->json(['status' => 'OK', 'favorites' => $user->getFavoriteProperties(), 'result' => $result]);
            }
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

    /**
     *
     * @param Request $request
     * @param type $id
     */
    public function favoriteAgent(Request $request, $id) {

        $user = Auth::user();

        if (is_object($user)) {
            $user->toggleFavoriteAgent($id);
            return response()->json(['status' => 'OK', 'favorites' => $user->getFavoriteAgentsgetFavoriteProperties(), 'result' => $result]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

    public function getSavedSearch() {

        $searches = [];

        if (Auth::check()) {
            $user = Auth::user();

            $list = DB::table('user_saved_searches')
                    ->select('search_data', 'created_at')
                    ->where('user_id', $user->id)
                    ->orderby('created_at', 'desc')
                    ->limit(100)
                    ->get();

            foreach ($list as $search) {

                $label = [];

                $data = json_decode($search->search_data, true);
                $date = '<span class="date">' . date('d M', strtotime($search->created_at)) . '</span>';

                $label[] = trim(array_get($data, 'q'));


                $lt = array_filter(explode(',', array_get($data, 'lt')));

                $label = array_filter($label);

                if (count($lt)) {
                    $lt = array_map('ucfirst', $lt);
                    $label[] = implode(', ', $lt);
                }

                $searches[] = [
                    'label' => implode(', ', $label) . ' ' . $date,
                    'url' => http_build_query($data)
                ];
            }
        }

        return response()->json([
                    'status' => 'OK',
                    'searches' => $searches
        ]);
    }

}
