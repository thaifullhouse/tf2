<?php

namespace App\Http\Controllers\Rest;

use App\Models\User\Profile;
use App\Http\Requests\User\Review\Create as CreateAgentReview;
use App\Models\User\Review;
use App\Models\User;
use DB;


class AgentController extends BaseController {
    
    public function getDetails($id) {
        
        $profile = Profile::find($id);
        
        DB::table('user_profiles')->where('user_id', $id)->update(['visites' => DB::raw('visites + 1')]);
        
        return response()->json(['agent' => $profile]);        
    }
    
    public function getReviews($id) {
        
        $reviews = Review::where('agent_id', $id)
                ->orderBy('created_at', 'desc')
                ->paginate(10);        
        
        return response()->json($reviews);
        
    }
    
    public function postReview(CreateAgentReview $request) {
        
        $user = User::find($request->input('user'));
        
        $review = new Review();
        $review->agent_id = $request->input('id');
        $review->rating = $request->input('rating');
        $review->review = $request->input('review');
        $review->user_id = $user->id;
        
        $review->save();
        
        return response()->json(['status' => 'OK', 'id' => $review->id]);
        
    }
    
}