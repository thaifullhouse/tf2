<?php

namespace App\Http\Controllers\Rest;

use Illuminate\Http\Request;
//use App\Services\Search;
use App\Services\PropertyFinder;
use App\Services\CondoFinder;
use App\Services\SearchSuggestion;
use App\Helpers\Language;
use DB;
use Auth;

/**
 * Base Controller for Admin controllers
 */
class SearchController extends BaseController {

    public function __construct() {
        // @TODO Add Access control middleware here
    }

    public function findPropertiesForMap(Request $request) {

        $pt = explode(',', $request->input('pt'));

        $results = [];
        $type = '';

        if (in_array('cd', $pt)) {
            $type = 'condo';
            $finder = new CondoFinder($request);
            $results = $finder->findForMap();
        } else {
            $type = 'prop';
            $finder = new PropertyFinder($request);
            $results = $finder->findForMap();
        }

        return response()->json([
                    'status' => 'OK', 'type' => $type
                    , 'properties' => $results]
                        , 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function findPropertiesForListing(Request $request) {

        $pt = explode(',', $request->input('pt'));

        $favorites = [];
        $results = [];
        $type = '';

        if (in_array('cd', $pt)) {
            $type = 'condo';
            $finder = new CondoFinder($request);
            $results = $finder->findForListing();

            if (Auth::check()) {
                $user = Auth::user();
                $favorites = DB::table('user_favorite_condos')
                        ->where('user_id', $user->id)
                        ->pluck('condo_id');
            }
        } else {
            $type = 'prop';
            $finder = new PropertyFinder($request);
            $results = $finder->findForListing();

            if (Auth::check()) {
                $user = Auth::user();
                $favorites = DB::table('user_favorite_properties')
                        ->where('user_id', $user->id)
                        ->pluck('property_id');
            }
        }

        return response()->json([
                    'status' => 'OK', 'type' => $type,
                    'properties' => $results, 'favorites' => $favorites
                        ], 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }

    public function getProposition(Request $request) {

        // Point of interest

        $suggestions = array_merge(
                $this->getSuggestedPointOfInterest($request)
                , $this->getSuggestedProperties($request)
                , $this->getSuggestedCondo($request)
        );

        //$suggestions = $this->getSuggestedProperties($request);

        return response()->json($suggestions);
    }
    
    public function getSuggestion(Request $request) {
        
        $service = new SearchSuggestion($request);
        
        $suggestions = $service->find();
        
        return response()->json($suggestions);
        
    }

    protected function getSuggestedPointOfInterest(Request $request) {

        $langs = Language::getActiveCodes();

        $term = $request->input('term');

        $suggestions = [];

        $terms = preg_split('/(\s+)/', $term);

        // Point of interest

        $query = DB::table('search_pois')
                ->select(
                'id'
                , 'name'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , 'type');

        foreach ($langs as $lang) {
            $query->orWhere(function($query) use ($terms, $lang) {
                foreach ($terms as $_term) {
                    $query->where('name->' . $lang, '~*', $_term);
                }
            });
        }

        //var_dump($query->toSql());


        $list = $query->limit(50)->get();

        if (count($list) > 0) {

            $lang = \App::getLocale();

            foreach ($list as $point) {

                $names = json_decode($point->name, true);
                $name = trim(array_get($names, $lang));

                $point->type_label = empty($point->type) ? 'bts' : $point->type;
                $point->type = empty($point->type) ? 'bts' : $point->type;
                $point->more = 'More info';
                $point->domain = 'poi';
                $point->value = $name ? $name : array_get($names, 'th');
                $point->label = $point->value;
                $point->zoom_level = 15;
                $point->pr = '';
                unset($point->name);
                $suggestions[] = $point;
            }
        }

        return $suggestions;
    }

    protected function getSuggestedProperties(Request $request) {

        $term = $request->input('term');
        $suggestions = [];

        $terms = array_filter(preg_split('/(\s+)/', $term));

        $query = DB::table('properties')->select(
                'id'
                , DB::raw('name')
                , DB::raw('property_type AS type')
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , 'new_project'
        );

        $query->where(function($query) use ($terms) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                $query->orWhere(function($query) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $query->where('name->' . $lang, '~*', $_term);
                    }
                });
            }
        });

        $now = date('Y-m-d H:i:s');

        $query->where('published', true)
                ->where('publication_date', '<', $now)
                ->where(function($query) use ($now) {
                    $query->orWhere('expiration_date', '>', $now);
                    $query->orWhereRaw('expiration_date IS NULL');
                })
                ->whereRaw(DB::raw('deleted_at IS NULL'));

        // Properties

        $properties = $query->limit(50)->get();

        $lang = \App::getLocale();

        if (count($properties) > 0) {
            foreach ($properties as $property) {

                $names = json_decode($property->name, true);
                $name = trim(array_get($names, $lang));

                $property->type_label = $property->type;
                $property->more = 'More info';
                $property->zoom_level = 15;
                $property->domain = 'property';

                $property->value = $name ? $name : array_get($names, 'th');
                $property->label = $property->value;

                if ($property->new_project) {
                    $property->pr = 'np';
                } else {
                    $property->pr = '';
                }

                $suggestions[] = $property;
            }
        }

        return $suggestions;
    }

    protected function getSuggestedCondo(Request $request) {



        $term = $request->input('term');
        $suggestions = [];

        $terms = preg_split('/(\s+)/', $term);

        $query = DB::table('condo_list')
                ->select(
                'id'
                , 'name'
                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat')
                , 'new_project'
        );
        $query->where('active', true)->where('user_submited', false);

        $query->where(function($query) use ($terms) {
            $langs = Language::getActiveCodes();
            foreach ($langs as $lang) {
                $query->orWhere(function($query) use ($terms, $lang) {
                    foreach ($terms as $_term) {
                        $query->where('name->' . $lang, '~*', $_term);
                    }
                });
            }
        });

        $condos = $query->limit(50)->get();

        if (count($condos) > 0) {

            $lang = \App::getLocale();
            foreach ($condos as $condo) {

                $names = json_decode($condo->name, true);
                $name = trim(array_get($names, $lang));

                $condo->type_label = 'Condo';
                $condo->more = 'More info';
                $condo->zoom_level = 15;
                $condo->domain = 'condo';
                $condo->value = array_get($names, $lang);

                $condo->value = $name ? $name : array_get($names, 'th');
                $condo->label = $condo->value;

                if ($condo->new_project) {
                    $condo->pr = 'np';
                } else {
                    $condo->pr = '';
                }

                unset($condo->name);

                $suggestions[] = $condo;
            }
        }

        return $suggestions;
    }

    public function getSaved() {
        $saved = [];

        if (Auth::check()) {
            
            $user = Auth::user();

            $saved = DB::table('user_saved_searches')
                    ->where('user_id', $user->id)
                    ->orderBy('created_at', 'desc')
                    ->limit(20);
        }

        return $saved;
    }

    public function addSaved(Request $request) {

        if (Auth::check()) {
            $user = Auth::user();

            $url = parse_url($request->input('url'), PHP_URL_QUERY);

            $data = [];

            parse_str($url, $data);

            DB::table('user_saved_searches')->insert([
                'user_id' => $user->id,
                'search_data' => json_encode($data),
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }
    }

}
