<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Payment\Paysbuy;
use App\Models\Purchase;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PaymentController extends Controller {

    public function handlePaysbuyCallback(Request $request) {

        $result = $request->input('result');

        $invoice_id = substr($result, 2);
        
        $response = [];

        try {

            $purchase = Purchase::where('invoice_id', $invoice_id)->firstOrFail();

            $payment = new Paysbuy;
            $payment->purchase_id = $purchase->id;
            $payment->result = $request->input('result');
            $payment->apcode = $request->input('apCode');
            $payment->amount = round($request->input('amt') * 100);
            $payment->fee = round($request->input('fee') * 100);
            $payment->method = $request->input('method');
            $payment->save();

            $code = substr($result, 0, 2);

            if ($code === '00') {

                $purchase->paid = true;
                $purchase->paid_on = date('Y-m-d H:i:s');
                $purchase->payment_method = 'paysbuy';
                $purchase->save();
            }

            $response = ['status' => 'OK', 'payment' => $payment->id];
            
        } catch (ModelNotFoundException $ex) {
            $response = ['status' => 'OK', 'error' => $ex->getMessage()];
        }
        
        return response()->json($response);
    }

}
