<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function humanDateToPgdate($hdate) {
        
        $components = explode('/', $hdate);
        
        if (count($components) == 3) {
            list($day, $month, $year) = $components;      
            return $year . '-' . $month . '-' . $day;
        }
        else {
            return null;
        }
    }
}
