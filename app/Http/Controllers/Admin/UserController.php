<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User\Type as UserType;

class UserController extends BaseController {
 
    public function getList(Request $request) {
        
        $uf = $request->input('filter');
        
        if (!empty($uf)) {
            $filter = $uf;
            session(['filter' => $uf]);
        }
        else {
            $filter = session('filter', 'visitor');
        }
        
        $type = UserType::where('code', $filter)->first();
        
        $users = User::where('type_id', $type->id)
                ->orderBy('updated_at', 'desc')
                ->paginate(15);
        
        return view('admin.user.index', ['users' => $users]);
    }
    
    public function getModalSettingContent($id) {
        $user = User::find($id);
        return view('admin.user.modal-setting', ['user' => $user]);
    }
    
    public function update(Request $request) {
        $id = $request->input('id');
        $user = User::find($id);
        $user->type_id = $request->input('type_id');
        $user->save();
    }
    
}
