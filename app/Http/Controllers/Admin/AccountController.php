<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use App\Models\User\Account;

class AccountController extends BaseController {
 
    public function getList() {
        $accounts = Account::orderBy('created_at', 'desc')->paginate(20);
        
        return view('admin.account.list', ['accounts' => $accounts]);
    }
    
}