<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BaseController extends Controller {

    protected function getUniqueFileName() {
        return uniqid() . '-' . strtolower(base_convert(time(), 10, 16));
    }

    protected function getCurrentSorting(Request $request, $default = null) {

        $reset = $request->input('resort');

        if ($reset != 'true') {
            $sort0 = explode(',', $request->input('sortby'));

            // Try request

            if (count($sort0) == 2) {
                session(['sortby' => $sort0]);
                return $sort0;
            }

            // Try session

            $sort1 = session('sortby');

            if (count($sort1) == 2) {
                return $sort1;
            }
        } else {
            session(['sortby' => null]);
        }

        // Return default

        return $default;
    }

}
