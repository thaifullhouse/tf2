<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use DB;

class PointController extends BaseController {

    public function getEntryList(Request $request) {
        
        return view('manager.points.entries', ['active' => 'poi']);
        
    }
    
    public function getEntries(Request $request) {
        
        $nl = $request->input('nl');
        $fr = $request->input('fr');

        list($lat_min, $lng_min) = explode(',', $nl);
        list($lat_max, $lng_max) = explode(',', $fr);
        
        $filters = explode(',', $request->input('filter'));
        
        $entries = [];
        
        $query = '';
        
        if (count($filters) > 0) {
            $query = implode("', '", $filters);
            $query = "SELECT id, name,type, ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
                        ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM search_pois 
                        WHERE location::geometry && ST_MakeEnvelope(?, ?, ?, ?, 4326) AND (type IN ('" . $query . "') OR (type = '') IS NOT FALSE)";
      
            $entries = DB::select($query, [$lng_min, $lat_min, $lng_max, $lat_max]);
        }
        else {
            $entries = DB::select('SELECT id, name, type, ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
                        ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM search_pois 
                        WHERE location::geometry && ST_MakeEnvelope(?, ?, ?, ?, 4326)', [$lng_min, $lat_min, $lng_max, $lat_max]);
        }
        
         return response()->json([
             'entries' => $entries,
             'query' => $query
         ]);      
    }

    public function createNewEntry(Request $request)
    {
        
        list($lat, $lng) = explode(',', $request->input('location'));
        
        $id = $request->input('id');
        
        $action = '';
        
        if ($id > 0) {
            DB::update('UPDATE search_pois SET name = ?, location = ST_GeomFromText(?, 4326), zoom_level = ?, type = ? WHERE id = ?', [
                json_encode($request->input('name')),
                'POINT(' . $lng . ' ' . $lat . ' 0.0)',
                $request->input('zl'),
                $request->input('type'),
                $id
            ]);
            $action = 'updated';
        }
        else {
            DB::insert('INSERT INTO search_pois (name, location, zoom_level, type) VALUES (?, ?, ST_GeomFromText(?, 4326) , ?, ?)', [
                $request->input('name'),
                'POINT(' . $lng . ' ' . $lat . ' 0.0)',
                $request->input('zl'),
                $request->input('type')
            ]);
            $action = 'created';
        }
        
        return response()->json([
             'action' => $action
         ]); 
    }

    public function getTypeList(Request $request)
    {
        return view('manager.points.types', ['active' => 'points']);
    }

    public function  createNewType(Request $request)
    {

    }
}
