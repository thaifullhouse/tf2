<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Models\Property;
use DB;

class ListingController extends BaseController {

    protected function getFilteredList(Request $request, $property) {
        
        // Listing type : property.listing_type
        
        $filters = $this->getListFilters();
        
        $query = Property::where('property_type', $property);
        
        if ($filters['type'] != 'all') {
            $query = $query->where('listing_type', $filters['type']);
        }
        else {
            $query = $query->where('listing_type', '<>', 'all');
        }
        
        // Class : property.listing_class
        
        if ($filters['class'] != 'all') {
            $query = $query->where('listing_class', $filters['class']);
        }
        
        // Status : property.expiration_date
        
        $today = date('Y-m-d');
        
        if ($filters['status'] == 'present') {
            $query = $query->where('expiration_date', '>', $today);
        }
        if ($filters['status'] == 'expired') {
            $query = $query->where('expiration_date', '<=', $today);
        }
        
        $properties = $query->orderBy('updated_at', 'desc')->paginate(20);
        
        return $properties;
        
    }
    
    protected function getListFilters(Request $request) {
        return [
            'type' => $request->input('type', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all')
        ];
    }

    public function index(Request $request) {

        $filters = [
            'type' => $request->input('type', 'all'),
            'property' => $request->input('property', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all')
        ];

        $query = null;

        // Listing type : property.listing_type

        if ($filters['type'] != 'all') {
            $query = Property::where('listing_type', $filters['type']);
        } else {
            $query = Property::where('listing_type', '<>', 'all');
        }

        // Property type : property.property_type

        if ($filters['property'] != 'all') {
            $query = $query->where('property_type', $filters['property']);
        }

        // Class : property.listing_class

        if ($filters['class'] != 'all') {
            $query = $query->where('listing_class', $filters['class']);
        }

        // Status : property.expiration_date

        $today = date('Y-m-d');

        if ($filters['status'] == 'present') {
            $query = $query->where('expiration_date', '>', $today);
        }
        if ($filters['status'] == 'expired') {
            $query = $query->where('expiration_date', '<=', $today);
        }
        
        $query = $query->where('new_project', false);

        $properties = $query->orderBy('updated_at', 'desc')->paginate(20);

        return view('manager.listing.index', ['active' => 'listing', 'filters' => $filters, 'properties' => $properties]);
    }

    public function getForm($id) {
        $property = Property::find($id);
        return view('manager.listing.form', ['property' => $property]);
    }

    public function toggleVisibility($id) {
        $property = Property::find($id);
        $property->published = !$property->published;
        $property->save();
    }

    public function postUpdate(Request $request) {

        $id = $request->input('id');

        $property = Property::find($id);

        $property->listing_class = $request->input('listing_class');
        $property->status = $request->input('property_status');

        list($day, $month, $year) = explode('/', $request->input('publication_date'));
        $property->publication_date = $year . '-' . $month . '-' . $day;

        list($day, $month, $year) = explode('/', $request->input('expiration_date'));
        $property->expiration_date = $year . '-' . $month . '-' . $day;

        $property->save();
    }

    public function getDelete($id) {
        $property = Property::find($id);
        $property->delete();
    }
    
    public function apartmentList(Request $request) {

        $filters = [
            'type' => $request->input('type', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all')
        ];

        $properties =$this->getPropertyList($filters, 'apartment');

        return view('manager.listing.apartment', ['active' => 'listing', 'filters' => $filters, 'properties' => $properties]);
    }
    
    public function houseList(Request $request) {

        $filters = [
            'type' => $request->input('type', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all')
        ];

        $properties =$this->getPropertyList($filters, 'detachedhouse');

        return view('manager.listing.house', ['active' => 'listing', 'filters' => $filters, 'properties' => $properties]);
    }
    
    public function townhouseList(Request $request) {

        $filters = [
            'type' => $request->input('type', 'all'),
            'class' => $request->input('class', 'all'),
            'status' => $request->input('status', 'all')
        ];

        $properties =$this->getPropertyList($filters, 'townhouse');

        return view('manager.listing.townhouse', ['active' => 'listing', 'filters' => $filters, 'properties' => $properties]);
    }
    
    protected function getPropertyList($filters, $type) {
        $query = null;

        // Listing type : property.listing_type

        if ($filters['type'] != 'all') {
            $query = Property::where('listing_type', $filters['type']);
        } else {
            $query = Property::where('listing_type', '<>', 'all');
        }

        // Property type : property.property_type

        $query->where('property_type', $type);

        // Class : property.listing_class

        if ($filters['class'] != 'all') {
            $query->where('listing_class', $filters['class']);
        }

        // Status : property.expiration_date

        $today = date('Y-m-d');

        if ($filters['status'] == 'present') {
            $query = $query->where('expiration_date', '>', $today);
        }
        if ($filters['status'] == 'expired') {
            $query = $query->where('expiration_date', '<=', $today);
        }
        
        $query->where('new_project', false);

        return $query->orderBy('updated_at', 'desc')->paginate(20);
    }

}
