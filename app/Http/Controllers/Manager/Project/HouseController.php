<?php

namespace App\Http\Controllers\Manager\Project;

use App\Http\Controllers\Manager\BaseController;
use App\Models\Property;

class HouseController extends BaseController {
    
    public function getList() {
        $properties = Property::where('new_project', true)->where('property_type', 'detachedhouse')->paginate(20);
        
        return view('manager.project.house.list', ['properties' => $properties]);
    }
    
}