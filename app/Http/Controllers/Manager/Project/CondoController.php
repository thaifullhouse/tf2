<?php

namespace App\Http\Controllers\Manager\Project;

use App\Http\Controllers\Manager\BaseController;
use App\Models\Condo;

/**
 * 
 */
class CondoController extends BaseController {
    
    /**
     * Get the list of new Condo project
     * @return type
     */
    public function getList() {
        
        $condos = Condo::where('new_project', true)->paginate(20);
        
        return view('manager.project.condo.list', ['active' => 'project', 'condos' => $condos]);
    }
    
}