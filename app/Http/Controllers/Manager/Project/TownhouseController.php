<?php

namespace App\Http\Controllers\Manager\Project;

use App\Http\Controllers\Manager\BaseController;
use App\Models\Property;

class TownhouseController extends BaseController {
    
    public function getList() {
        $properties = Property::where('new_project', true)->where('property_type', 'townhouse')->paginate(20);
        
        return view('manager.project.townhouse.list', ['properties' => $properties]);
    }
    
}