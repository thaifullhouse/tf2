<?php

namespace App\Http\Controllers\Manager;

use DB;
use Illuminate\Http\Request;
use App\Models\User\Profile as UserProfile;

class CustomerController extends BaseController {
    
    /**
     * Get list of visitors
     * @return type
     */
    public function getVisitors() {
        
        $profiles = UserProfile::where('member_type', UserProfile::OWNER)->where('user_id', '>', 0)->paginate(20);
        
        return view('manager.customer.visitor'
                , ['active' => 'visitors', 'profiles' => $profiles]);
    }
    
    /**
     * Get the list of agents
     * @return type
     */
    public function getFreelanceAgents() {
        
        $profiles = UserProfile::where('member_type', UserProfile::FREELANCE_AGENT)->where('user_id', '>', 0)->paginate(20);
        
        return view('manager.customer.freelance'
                , ['active' => 'agents', 'profiles' => $profiles]);
    }
    
    /**
     * Get the list of agents
     * @return type
     */
    public function getRealestateAgents() {
        
        $profiles = UserProfile::where('member_type', UserProfile::COMPANY_AGENT)->where('user_id', '>', 0)->paginate(20);
        
        return view('manager.customer.realestate'
                , ['active' => 'agents', 'profiles' => $profiles]);
    }
    
    /**
     * Get the list of owners
     * @return type
     */
    public function getOwners() {
        
        $profiles = UserProfile::where('member_type', UserProfile::OWNER)->where('user_id', '>', 0)->paginate(20);
        
        return view('manager.customer.owner'
                , ['active' => 'owners', 'profiles' => $profiles]);
    }
    
    /**
     * Get the list of land developers
     * @return type
     */
    public function getDevelopers() {
        
        $profiles = UserProfile::where('member_type', UserProfile::LAND_DEVELOPER)->where('user_id', '>', 0)->paginate(20);
        
        return view('manager.customer.developer'
                , ['active' => 'developers', 'profiles' => $profiles]);
    }
    
    public function updateStatus(Request $request) {
        DB::table('users')
                ->where('id', $request->input('id'))
                ->update(['active' => $request->input('active')]);
        
        return response()->json(['status' => 'OK']);
    }
    
}