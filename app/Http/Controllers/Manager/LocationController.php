<?php

namespace App\Http\Controllers\Manager;

use App\Models\Geo\Province;
use App\Models\Geo\District;
use App\Models\Geo\Area;

class LocationController extends BaseController {

    /**
     * Get province list
     * @return type
     */
    public function getProvinceList() {

        $provinces = Province::all();
        return view('manager.location.province-list', ['active' => 'province', 'provinces' => $provinces]);
    }

    /**
     * Get province details
     * @return type
     */
    public function getProvinceDetails($id) {

        $province = Province::find($id);
        $districts = District::where('province_id', $id)->get();

        return view('manager.location.province-details', [
            'active' => 'province'
            , 'province' => $province
            , 'districts' => $districts]);
    }

    /**
     * Get district list
     * @return type
     */
    public function getDistrictList($id) {
        
        $province = Province::find($id);
        $districts = District::where('province_id', $id)->get();
        
        return view('manager.location.district-list', ['active' => 'province', 'province' => $province
            , 'districts' => $districts]);
    }

    /**
     * Get district details
     * @return type
     */
    public function getDistrictDetails($id) {
        return view('manager.location.district-details', ['active' => 'province']);
    }
    
    /**
     * Get district list
     * @return type
     */
    public function getAreaList($id) {
        
        $district = District::find($id);
        $areas = Area::where('district_id', $id)->get();
        $province = Province::find($district->province_id);
        
        return view('manager.location.area-list', ['active' => 'province', 'province' => $province
            , 'district' => $district, 'areas' => $areas]);
    }

}
