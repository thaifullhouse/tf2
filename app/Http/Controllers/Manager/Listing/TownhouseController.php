<?php

namespace App\Http\Controllers\Manager\Listing;

use Illuminate\Http\Request;
use App\Http\Controllers\Manager\BaseController;

class TownouseController extends BaseController {
    
    public function getList(Request $request) {

        $properties = $this->getFilteredList($request, 'townhouse');
        $filters = $this->getListFilters($request);

        return view('manager.listing.townhouse.list', [
            'active' => 'listing', 
            'filters' => $filters, 
            'properties' => $properties]);
    }    
}