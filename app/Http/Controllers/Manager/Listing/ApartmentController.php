<?php

namespace App\Http\Controllers\Manager\Listing;

use Illuminate\Http\Request;
use App\Http\Controllers\Manager\ListingController;

class ApartmentController extends ListingController {

    public function getList(Request $request) {

        $properties = $this->getFilteredList($request, 'apartment');
        $filters = $this->getListFilters($request);

        return view('manager.listing.apartment.list', [
            'active' => 'listing', 
            'filters' => $filters, 
            'properties' => $properties]);
    }

}
