<?php

namespace App\Http\Controllers\Manager\Listing;

use Illuminate\Http\Request;
use App\Http\Controllers\Manager\BaseController;

class HouseController extends BaseController {
    
    public function getList(Request $request) {

        $properties = $this->getFilteredList($request, 'house');
        $filters = $this->getListFilters($request);

        return view('manager.listing.house.list', [
            'active' => 'listing', 
            'filters' => $filters, 
            'properties' => $properties]);
    }
    
}