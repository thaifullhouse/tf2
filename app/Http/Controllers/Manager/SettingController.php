<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Http\Requests\Bank\CreateRequest as CreateBankRequest;
use App\Models\Bank;
use DB;
use App\Helpers\UploadHelper;
use App\Helpers\ImageHelper;

class SettingController extends BaseController {

    public function index() {

        $settings = DB::table('settings')->pluck('value', 'domain')->all();

        return view('manager.setting.index', ['active' => 'setting', 'settings' => $settings]);
    }

    public function update(Request $request) {

        $this->_update($request, 'facebook_link');
        $this->_update($request, 'instagram_link');
        $this->_update($request, 'twitter_link');
        $this->_update($request, 'google_link');

        return redirect('/manager/setting');
    }

    protected function _update($request, $domain) {
        $date = date('Y-m-d H:i:s');
        try {
            DB::insert('INSERT INTO settings (domain, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , [$domain, $request->input($domain), $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE domain = ?', [$request->input($domain), $date, $domain]);
        }
    }

    public function getContact() {

        $setting = DB::table('settings')->where('domain', 'default_message')->value('value');

        $footer = DB::table('settings')->where('domain', 'footer_contact')->value('value');

        return view('manager.setting.contact', ['active' => 'setting', 'default_message' => $setting, 'footer' => $footer]);
    }

    public function updateContact(Request $request) {

        $value = json_encode($request->input('details'));

        $date = date('Y-m-d H:i:s');
        try {
            DB::insert('INSERT INTO settings (domain, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , ['default_message', $value, $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE domain = ?', [$value, $date, 'default_message']);
        }

        return redirect('/manager/setting/contact');
    }

    public function updateFooter(Request $request) {

        $value = json_encode($request->input('footer'));

        $date = date('Y-m-d H:i:s');
        try {
            DB::insert('INSERT INTO settings (domain, value, created_at, updated_at) VALUES (?, ?, ?, ?)'
                    , ['footer_contact', $value, $date, $date]);
        } catch (QueryException $ex) {
            DB::update('UPDATE settings SET value = ?, updated_at = ? WHERE domain = ?', [$value, $date, 'footer_contact']);
        }

        return redirect('/manager/setting/contact');
    }

    /**
     * Get bank list
     * @return type
     */
    public function getBank() {
        $banks = Bank::orderBy('created_at', 'asc')->get();
        return view('manager.setting.bank'
                , ['active' => 'setting', 'banks' => $banks]);
    }

    /**
     * Get new bank form
     * @return type
     */
    public function getAddBank() {
        return view('manager.setting.add-bank'
                , ['active' => 'setting']);
    }

    /**
     * Save new bank
     * @param CreateBankRequest $request
     * @return type
     */
    public function addBank(CreateBankRequest $request) {

        $bank = new Bank;
        $bank->bank = $request->input('bank');
        $bank->name = $request->input('name');
        $bank->account_no = $request->input('account_no');
        $bank->branch = $request->input('branch');
        $bank->save();

        $request->session()->flash('status', 'bank_created');

        return redirect('/manager/setting/bank');
    }

    public function editBank($id) {
        $bank = Bank::find($id);
        return view('manager.setting.edit-bank'
                , ['active' => 'setting', 'bank' => $bank]);
    }

    public function updateBank(CreateBankRequest $request, $id) {

        $bank = Bank::find($id);
        $bank->bank = $request->input('bank');
        $bank->name = $request->input('name');
        $bank->account_no = $request->input('account_no');
        $bank->branch = $request->input('branch');
        $bank->save();

        $request->session()->flash('status', 'bank_created');

        return redirect('/manager/setting/bank');
    }

    public function deleteBank($id) {
        $bank = Bank::find($id);
        $bank->delete();
        return redirect('/manager/setting/bank');
    }

    public function getImages() {
        $condo_image = DB::table('settings')->where('domain', 'condo_image')->value('value');
        $featured_image = DB::table('settings')->where('domain', 'featured_image')->value('value');
        $exclusive_image = DB::table('settings')->where('domain', 'exclusive_image')->value('value');
        
        $home_image = DB::table('settings')->where('domain', 'home_image')->value('value');

        if (!$condo_image) {
            $this->initTranslatedImages('condo_image');
        }

        if (!$featured_image) {
            $this->initTranslatedImages('featured_image');
        }

        if (!$exclusive_image) {
            $this->initTranslatedImages('exclusive_image');
        }
        
        if (!$home_image) {
            $this->initHomeImages();
        }

        return view('manager.setting.images', [
            'active' => 'setting'
            , 'condo_image' => $condo_image
            , 'featured_image' => $featured_image
            , 'exclusive_image' => $exclusive_image
                , 'home_image' => $home_image
        ]);
    }

    public function uploadImage(Request $request) {

        $helper = new UploadHelper($request);

        $path = $helper->saveSingle('image');

        $lang = $request->input('lang');
        $domain = $request->input('domain');

        if ($path) {
            if ($lang) {
                $setting = DB::table('settings')->where('domain', $domain)->value('value');
                $values = json_decode($setting, true);
                $values[$lang] = $path;
                DB::table('settings')->where('domain', $domain)->update(['value' => json_encode($values)]);
            } else {
                
            }
            return response()->json(['status' => 'OK', 'path' => $path]);
        } else {
            return response()->json(['status' => 'ERROR']);
        }
    }

    public function removeTranslatedImage(Request $request) {
        $lang = $request->input('lang');
        $domain = $request->input('domain');

        $setting = DB::table('settings')->where('domain', $domain)->value('value');
        $values = json_decode($setting, true);

        $path = $values[$lang];

        ImageHelper::remove($path);

        $values[$lang] = '';
        DB::table('settings')->where('domain', $domain)->update(['value' => json_encode($values)]);

        return back();
    }

    protected function initTranslatedImages($domain) {
        $langs = \App\Helpers\Language::getActiveCodes();
        $values = array_fill_keys($langs, '');

        DB::table('settings')->insert([
            'domain' => $domain,
            'value' => json_encode($values)
        ]);
    }
    
    protected function initHomeImages() {
        DB::table('settings')->insert([
            'domain' => 'home_image',
            'value' => json_encode(array_fill_keys(['rent', 'sale', 'new_project', 'condo'], ''))
        ]);
    }

}
