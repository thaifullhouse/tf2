<?php

namespace App\Http\Controllers\Manager;

use DB;
use Illuminate\Http\Request;
use App\Models\Translation;

class TranslationController extends BaseController {

    /**
     * 
     */
    public function getList(Request $request) {

        $sections = DB::table('translations')->groupBy('section')->orderBy('section', 'asc')->pluck('section');

        $currentSection = $request->input('section');

        if (!empty($currentSection)) {
            session(['translationSection' => $currentSection]);
        } else {
            $currentSection = session('translationSection');
        }

        $translations = [];

        if (!empty($currentSection)) {
            $translations = DB::select('SELECT * FROM translations WHERE section = ? ORDER BY updated_at ASC', [$currentSection]);
        }

        return view('manager.translation.sections', ['sections' => $sections, 'translations' => $translations, 'currentSection' => $currentSection]);
    }

    /**
     * 
     * @param type $id
     * @return type
     */
    public function getEdit($id) {

        $translation = Translation::find($id);
        return view('manager.translation.edit', ['translation' => $translation]);
    }

    /**
     * 
     * @param Request $request
     */
    public function postUpdate(Request $request) {

        $langs = \App\Helpers\Language::getActives();

        $options = array_only($request->all(), array_keys($langs));

        $id = $request->input('id');

        $translation = Translation::find($id);
        $translation->translation = $options;
        $translation->save();

        
    }
    
    public function publishTranslation(Request $request) {
        
        $this->updateTranslationFile($request->input('section'));
        
        return back();
        
    }

    protected function updateTranslationFile($section) {

        $translations = Translation::where('section', $section)->get();

        $langs = array_keys(\App\Helpers\Language::getActives());

        $files = array_fill_keys($langs, []);

        foreach ($translations as $translation) {
            foreach ($langs as $lang) {
                if (!isset($files[$lang])) {
                    $files[$lang] = [];
                }                
                $options = $translation->translation;          
                
                $value = $options[$lang] ? $options[$lang] : $translation->label;
                
                $files[$lang][$translation->label] = $value;
            }
        }
        
        $path = resource_path('lang');
        
        foreach ($langs as $lang) {
            $langdir = $path . '/' . $lang;
            if (!file_exists($langdir)) {
                mkdir($langdir);
            }
            
            $langfile = $langdir .'/_generated_' . $section . '.php';
            
            if (!file_exists($langfile)) {
                touch($langfile);
            }
            
            file_put_contents($langfile, "<?php \n return " . var_export($files[$lang], true) . ';');
        }
    }

}
