<?php

namespace App\Http\Controllers\Manager;


class DashboardController extends BaseController {
    
    public function index() {
        return view('manager.dashboard.index', ['active' => 'dashboard']);
    }
    
}