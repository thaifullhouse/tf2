<?php

namespace App\Http\Controllers\Manager;

use App\Models\Properties\OptionalFeature as Feature;
use App\Http\Requests\FacilityCreateRequest;
use Validator;
use DB;
use Illuminate\Http\Request;

class OptionalFeatureController extends BaseController {
    
    public function getList() {
        $list = Feature::orderBy('rank', 'asc')->get();
        
        $last_id = session('last_id');
        
        return view('manager.optional-feature.list', 
                ['active' => 'optional-feature', 'list' => $list, 'last_id' => $last_id]);
    }

    public function postCreate(FacilityCreateRequest $request)
    {
        $feature = new Feature;
        $feature->name = json_encore($request->input('name'));
        $feature->save();
        
        session(['last_id' => $feature->id]);

        return redirect('/manager/optional-feature/edit/' . $feature->id);
    }

    public function getEdit($id)
    {
        $feature = Feature::find($id);
        return view('manager.optional-feature.edit', ['feature' => $feature]);
    }
    
    public function getDelete($id)
    {
        $feature = Feature::find($id);
        $feature->delete();
        
        return redirect('/manager/optional-feature/list');
    }

    public function postUpdate(FacilityCreateRequest $request, $id)
    {
        $feature = Feature::find($id);
        $feature->name = json_encode($request->input('name'));
        $feature->save();
        
        session(['last_id' => $feature->id]);

        return redirect('/manager/optional-feature/edit/' . $feature->id);
    }

    public function postUpload(Request $request)
    {
        $feature_id = $request->input('feature_id');

        $file = $request->file('image');

        $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'
            $validator = Validator::make(array('file' => $file), $rules);
            if ($validator->passes()) {
                $destinationPath = storage_path('app/public');
                $extention = $file->getClientOriginalExtension();
                $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
                $uploaded = $file->move($destinationPath, $filename);

                if ($uploaded) {
                    DB::update('UPDATE optional_features SET icon_path = ?, updated_at = NOW() WHERE id = ?',
                            [$filename, $feature_id]);
                }
            }
    }

    public function order(Request $request)
    {
        $ranks = $request->input('ranks');

        if (count($ranks)) {
            $index = 1;
            foreach($ranks as $rank) {
                DB::update('UPDATE optional_features SET rank = ? WHERE id = ?', [$index, $rank]);
                $index++;
            }
        }
    }
    
    public function getOptionalList() {
        return view('manager.option-feature.list', 
                ['active' => 'optional-feature']);
    }
    
}