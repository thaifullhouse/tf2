<?php

namespace App\Http\Controllers\Manager;

use App\Models\Properties\Facility;
use App\Http\Requests\FacilityCreateRequest;
use Illuminate\Http\Request;
use DB;
use Validator;

class FacilityController extends BaseController {
    
    public function getList() {
        
        $facilities = Facility::orderBy('rank', 'asc')->get();
        
        return view('manager.facility.list', 
                [  'active' => 'facility'
                 , 'facilities' => $facilities]);
    }
    
    public function postCreate(FacilityCreateRequest $request)
    {
        $facility = new Facility;
        $facility->name = json_encode($request->input('name'));
        $facility->save();
        
        session(['last_id' => $facility->id]);

        return redirect('/manager/facility/edit/' . $facility->id);
    }
    
    public function getEdit($id)
    {
        $facility = Facility::find($id);
        return view('manager.facility.edit', ['active' => 'facility', 'facility' => $facility]);
    }
    
     public function getDelete($id)
    {
        $facility = Facility::find($id);
        $facility->delete();
        return redirect('/manager/facility/list');
    }
    
    public function postUpdate(FacilityCreateRequest $request, $id)
    {
        $facility = Facility::find($id);
        $facility->name = json_encode($request->input('name'));
        $facility->save();
        
        session(['last_id' => $facility->id]);

        return redirect('/manager/facility/edit/' . $facility->id);
    }

    public function postUpload(Request $request)
    {
        $facility_id = $request->input('facility_id');

        $file = $request->file('image');

        $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'
        $validator = Validator::make(array('file' => $file), $rules);
        if ($validator->passes()) {
            $destinationPath = storage_path('app/public');
            $extention = $file->getClientOriginalExtension();
            $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
            $uploaded = $file->move($destinationPath, $filename);

            if ($uploaded) {
                DB::update('UPDATE condo_facilities SET icon_path = ?, updated_at = NOW() WHERE id = ?',
                        [$filename, $facility_id]);
            }
        }
    }

    public function order(Request $request)
    {
        $ranks = $request->input('ranks');

        if (count($ranks)) {
            $index = 1;
            foreach($ranks as $rank) {
                DB::update('UPDATE condo_facilities SET rank = ? WHERE id = ?', [$index, $rank]);
                $index++;
            }
        }
    }
    
}