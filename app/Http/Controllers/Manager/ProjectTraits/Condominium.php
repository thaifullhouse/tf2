<?php

namespace App\Http\Controllers\Manager\ProjectTraits;

use Illuminate\Http\Request;
use App\Http\Requests\CondoCreateRequest;
use App\Models\Properties\Facility;
use DB;
use App\Models\Condo;
use App\Models\Project\Review as ProjectReview;
use App\Helpers\ImageHelper;
use App\Helpers\UploadHelper;

trait Condominium {

    public function getCreateCondo() {
        return view('manager.project.condo.create', ['active' => 'project']);
    }

    public function postCreateCondo(CondoCreateRequest $request) {

        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        $date_start = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_start'));
        $date_end = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_end'));

        $id = DB::table('condo_list')->insertGetId([
            'name' => json_encode($request->input('name')),
            'postcode' => $request->input('postcode'),
            'location' => DB::raw("ST_GeomFromText('POINT({$location})', 4326)"),
            'location_name' => $request->input('address', ''),
            'user_submited' => false,
            'new_project' => true,
            'active' => true,
            'project_start' => $date_start,
            'project_end' => $date_end,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect('/manager/project/condo/details/' . $id);
    }

    public function getCondoInfo($id) {

        $condo = Condo::find($id);

        return view('manager.project.condo.info'
                , ['active' => 'project', 'condo' => $condo]);
    }

    public function getCondoDetails($id) {

        $condo = Condo::find($id);

        $details = $condo->getDetails();
        $facilities = Facility::all();
        $condo_facilities = $condo->getFacilitiesID();

        return view('manager.project.condo.setting'
                , [
            'active' => 'project',
            'condo' => $condo,
            'details' => $details,
            'facilities' => $facilities,
            'condo_facilities' => $condo_facilities
        ]);
    }

    public function getCondoMedia($id) {

        $condo = Condo::find($id);

        return view('manager.project.condo.media'
                , ['active' => 'project', 'condo' => $condo]);
    }

    public function getCondoReviews($id) {

        $condo = Condo::find($id);

        $review = $condo->getProjectReview();

        return view('manager.project.condo.review'
                , ['active' => 'project', 'condo' => $condo, 'review' => $review]);
    }

    public function getCondoPlaces($id) {

        $condo = Condo::find($id);

        return view('manager.project.condo.places'
                , ['active' => 'project', 'condo' => $condo]);
    }

    /**
     * Update condo info
     * @param type $id
     */
    public function postCondoInfo(CondoCreateRequest $request, $id) {

        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        DB::update('UPDATE condo_list SET name = ?, postcode = ?, location = ST_GeomFromText(?, 4326), location_name = ?, updated_at =  NOW() WHERE id = ?', [
            json_encode($request->input('name')),
            $request->input('postcode'),
            'POINT(' . $location . ')',
            $request->input('address'),
            $id
        ]);

        $condo = Condo::find($id);

        $province_id = $request->input('province_id');
        $district_id = $request->input('district_id');
        $area_id = $request->input('area_id');

        if ($province_id > 0) {
            $condo->province_id = $province_id;
        }

        if ($district_id > 0) {
            $condo->district_id = $district_id;
        }

        if ($area_id > 0) {
            $condo->area_id = $area_id;
        }

        $condo->save();

        return redirect('manager/project/condo/info/' . $id);
    }

    /**
     * 
     */
    public function postCondoDetails(Request $request, $id) {
        $condo = Condo::find($id);

        $details = $condo->getDetails();

        $details->starting_sales_price = $request->input('starting_sales_price');
        $details->total_units = $request->input('total_units') + 0;
        $details->office_hours = $request->input('office_hours');
        $details->room_types = $request->input('room_types');
        $details->year_built = $request->input('year_built') + 0;
        $details->website = $request->input('website');
        $details->number_of_tower = $request->input('number_of_tower') + 0;
        $details->district = $request->input('district');
        $details->total_floors = $request->input('total_floors') + 0;
        $details->contact_person = $request->input('contact_person');

        $details->save();

        return redirect('manager/project/condo/details/' . $condo->id);
    }

    public function postCondoFacilities(Request $request, $id) {
        // Delete facilities first

        DB::delete('DELETE FROM condo_have_facilities WHERE condo_id = ?', [$id]);

        $facitities = $request->get('facilities');

        if (count($facitities)) {
            foreach ($facitities as $facility_id) {
                DB::insert('INSERT INTO condo_have_facilities (condo_id, facility_id) VALUES (?, ?)', [$id, $facility_id]);
            }
        }

        return redirect('manager/project/condo/details/' . $id);
    }

    public function postCondoOthers(Request $request, $id) {

        $condo = Condo::find($id);
        $details = $condo->getDetails();

        $details->address = json_encode($request->input('address'));
        $details->details = json_encode($request->input('details'));
        $details->save();

        return redirect('manager/project/condo/details/' . $id);
    }

    public function uploadReviewImage(Request $request, $id, $domain) {

        $condo = Condo::find($id);

        $review = $condo->getProjectReview();

        $table = false;

        if ($domain == 'location') {
            $table = 'project_location_images';
        } else if ($domain == 'facility') {
            $table = 'project_facility_images';
        } else if ($domain == 'floorplan') {
            $table = 'project_floorplan_images';
        }

        if ($table) {

            $uploader = new UploadHelper($request);
            $files = $uploader->saveMultiple('images');

            foreach ($files as $filename) {
                DB::insert('INSERT INTO ' . $table . ' (review_id, filepath) VALUES (?, ?)', [
                    $review->id, $filename
                ]);
            }
        }
    }

    public function removeReviewImage($id, $domain, $condo) {

        $table = false;

        if ($domain == 'location') {
            $table = 'project_location_images';
        } else if ($domain == 'facility') {
            $table = 'project_facility_images';
        } else if ($domain == 'floorplan') {
            $table = 'project_floorplan_images';
        }

        if ($table) {
            $filepath = DB::table($table)->where('id', $id)->value('filepath');
            //$path = storage_path('app/public/' . $filepath);
            ImageHelper::remove($filepath);

            DB::delete('DELETE FROM ' . $table . ' WHERE id = ?', [$id]);
        }

        return redirect('manager/project/condo/review/' . $condo);
    }

    public function updateReviewDetails(Request $request, $id, $domain) {
        $review = ProjectReview::find($id);

        if ($domain == 'location') {
            $review->location_details = $request->input('location_details');
        } else if ($domain == 'facility') {
            $review->facility_details = $request->input('facility_details');
        } else if ($domain == 'floorplan') {
            $review->floor_plan_details = $request->input('floor_plan_details');
        } else if ($domain == 'video') {
            $review->video_link = $request->input('video_link');
        } else if ($domain == 'overview') {
            $review->overview = $request->input('overview');
        }
        $review->save();

        return redirect('manager/project/condo/review/' . $review->condo_id);
    }

}
