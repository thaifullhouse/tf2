<?php

namespace App\Http\Controllers\Manager\ProjectTraits;

use Illuminate\Http\Request;
use App\Models\Property;
use DB;
use Auth;

trait Apartment {

    public function getCreateApartment() {
        return view('manager.project.apartment.create', ['active' => 'project']);
    }

    public function postCreateApartment(Request $request) {
        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        $date_start = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_start'));
        $date_end = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_end'));
        
        $user = Auth::user();

        $id = DB::table('properties')->insertGetId([
            'poster_id' => $user->id,
            'name' => json_encode($request->input('name')),
            'postale_code' => $request->input('postcode'),
            'location' => DB::raw("ST_GeomFromText('POINT({$location})', 4326)"),
            'street_name' => $request->input('address', ''),
            'new_project' => true,
            'published' => false,
            'property_type' => 'apartment',
            'project_start' => $date_start,
            'project_end' => $date_end,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect('/manager/project/apartment/info/' . $id);
    }

    public function getApartmentInfo($id) {

        $property = Property::find($id);

        return view('manager.project.apartment.info'
                , ['active' => 'project', 'property' => $property]);
    }

    public function postApartmentInfo(Request $request, $id) {

        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        $date_start = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_start'));
        $date_end = preg_replace('/(\d+)\/(\d+)\/(\d+)/', '$3-$2-$1', $request->input('date_end'));

        DB::table('properties')->where('id', $id)->update([
            'name' => json_encode($request->input('name')),
            'postale_code' => $request->input('postcode'),
            'location' => DB::raw("ST_GeomFromText('POINT({$location})', 4326)"),
            'street_name' => $request->input('address', ''),
            'new_project' => true,
            'published' => false,
            'project_start' => $date_start,
            'project_end' => $date_end,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return redirect('/manager/project/apartment/info/' . $id);
    }
    
    public function getApartmentDetails($id) {
        
        $property = Property::find($id);
        
        return view('manager.project.apartment.setting'
                , [
            'active' => 'project',
            'property' => $property,
            'details' => [],
            'facilities' => [],
            'condo_facilities' => []
        ]);
        
    }
    
    public function getApartmentPlaces($id) {
        $property = Property::find($id);

        return view('manager.project.apartment.places'
                , ['active' => 'project', 'property' => $property]);
    }

    public function getApartmentReviews($id) {
        $property = Property::find($id);

        $review = $property->getProjectReview();

        return view('manager.project.apartment.review'
                , [
            'active' => 'project',
            'property' => $property,
            'review' => $review
        ]);
    }

}
