<?php

namespace App\Http\Controllers\Manager;

use Illuminate\Http\Request;
use App\Http\Requests\CondoCreateRequest;
use App\Models\Condo;
use App\Models\Properties\Facility;
//use App\Models\Property;
use App\Helpers\ImageHelper;
use DB;
use Validator;

class CondoController extends BaseController {

    public function getList() {

        $condos = Condo::where('new_project', false)->where('user_submited', false)->orderBy('updated_at', 'desc')->paginate(20);

        return view('manager.condo.list', ['active' => 'condo', 'condos' => $condos]);
    }

    public function getInfo($id) {
        $condo = Condo::find($id);
        $provinces = DB::table('provinces')->pluck('name', 'id');
        $districts = [];
        $areas = [];

        if ($condo->province_id > 0) {
            $districts = DB::table('districts')->where('province_id', $condo->province_id)->pluck('name', 'id');
        }

        if ($condo->district_id > 0) {
            $areas = DB::table('geo_area')->where('district_id', $condo->district_id)->pluck('name', 'id');
        }

        return view('manager.condo.info'
                , [
            'active' => 'condo',
            'condo' => $condo,
            'provinces' => $provinces,
            'districts' => $districts,
            'areas' => $areas]);
    }

    /**
     * Create a new condo
     */
    public function postCreate(CondoCreateRequest $request) {
        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        $id = DB::table('condo_list')->insertGetId([
            'name' => json_encode($request->input('name')),
            'postcode' => $request->input('postcode', ''),
            'location' => DB::raw("ST_GeomFromText('POINT({$location})', 4326)"),
            'location_name' => $request->input('address', ''),
            'user_submited' => false,
            'active' => false,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return redirect('manager/condo/details/' . $id);
    }

    public function postInfo(CondoCreateRequest $request) {
        $lng = $request->input('lng');
        $lat = $request->input('lat');

        $location = implode(' ', [$lng, $lat, '0.0']);

        $condodata = [
            'name' => json_encode($request->input('name')),
            'postcode' => $request->input('postcode', ''),
            'location' => DB::raw("ST_GeomFromText('POINT({$location})', 4326)"),
            'location_name' => $request->input('address'),
            'updated_at' => date('Y-m-d H:i:s')
        ];

        $province_id = $request->input('province_id');
        $district_id = $request->input('district_id');
        $area_id = $request->input('area_id');

        if ($province_id > 0) {
            $condodata['province_id'] = $province_id;
        }

        if ($district_id > 0) {
            $condodata['district_id'] = $district_id;
        }

        if ($area_id > 0) {
            $condodata['area_id'] = $area_id;
        }

        DB::table('condo_list')->where('id', $request->input('id'))->update($condodata);

        return redirect('manager/condo/info/' . $request->input('id'));
    }

    public function getDetails($id) {

        $condo = Condo::find($id);
        $details = $condo->getDetails();
        $facilities = Facility::all();
        $condo_facilities = $condo->getFacilitiesID();

        return view('manager.condo.details'
                , ['active' => 'condo',
            'condo' => $condo,
            'details' => $details,
            'facilities' => $facilities,
            'condo_facilities' => $condo_facilities]);
    }

    public function postGeneralInfo(Request $request, $id) {
        $condo = Condo::find($id);

        $details = $condo->getDetails();

        $details->starting_sales_price = $request->input('starting_sales_price');
        $details->total_units = $request->input('total_units') + 0;
        $details->office_hours = $request->input('office_hours');
        $details->room_types = $request->input('room_types');
        $details->year_built = $request->input('year_built') + 0;
        $details->website = $request->input('website');
        $details->number_of_tower = $request->input('number_of_tower') + 0;
        $details->district = $request->input('district');
        $details->total_floors = $request->input('total_floors') + 0;
        $details->contact_person = $request->input('contact_person');

        $details->save();

        return redirect('manager/condo/details/' . $condo->id);
    }

    public function postUpdateFacilities(Request $request, $id) {

        // Delete facilities first

        DB::delete('DELETE FROM condo_have_facilities WHERE condo_id = ?', [$id]);

        $facitities = $request->get('facilities');

        if (count($facitities)) {
            foreach ($facitities as $facility_id) {
                DB::insert('INSERT INTO condo_have_facilities (condo_id, facility_id) VALUES (?, ?)', [$id, $facility_id]);
            }
        }

        return redirect('manager/condo/details/' . $id);
    }

    public function postUpdateOthers(Request $request, $id) {

        $condo = Condo::find($id);
        $details = $condo->getDetails();
        
        $details->address = json_encode($request->input('address'));
        $details->details = json_encode($request->input('details'));
        
        $details->save();

        return redirect('manager/condo/details/' . $id);
    }

    public function getMedia($id) {
        
        $condo = Condo::find($id);

        $images = [];

        $media = DB::select('SELECT * FROM condo_have_media WHERE condo_id = ? AND media_type = ? ORDER BY rank ASC', [$id, 'image']);

        if (count($media)) {
            foreach ($media as $row) {
                $image = new ImageHelper($row);
                $image->id = $row->id;
                $images[] = $image;
            }
        }

        return view('manager.condo.media'
                , ['active' => 'condo', 'condo' => $condo, 'images' => $images]);
    }
    
    public function uploadMedia(Request $request) {
        $condo_id = $request->input('condo_id');

        $files = $request->file('images');

        foreach ($files as $file) {
            $rules = array('file' => 'required|mimes:png,gif,jpeg'); //'required,txt,pdf,doc'
            $validator = Validator::make(array('file' => $file), $rules);
            if ($validator->passes()) {
                $destinationPath = storage_path('app/public');
                $extention = $file->getClientOriginalExtension();
                $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
                $uploaded = $file->move($destinationPath, $filename);

                if ($uploaded) {
                    ImageHelper::resizeAndAddWatermark($destinationPath . '/' . $filename);
                    DB::insert('INSERT INTO condo_have_media (condo_id, media_type, filepath, created_at, updated_at) VALUES (?, ?, ?, NOW(), NOW())',
                            [$condo_id, 'image', $filename]);
                }
            }
        }
    }
    
    public function sortMedia(Request $request)
    {
        $ranks = $request->input('ranks');

        if (count($ranks)) {
            $index = 1;
            foreach($ranks as $rank) {
                DB::update('UPDATE condo_have_media SET rank = ? WHERE id = ?', [$index, $rank]);
                $index++;
            }
        }
    }
    
    public function deleteMedia($id) {
        
        $image = DB::table('condo_have_media')->where('id', $id)->first();
        
        if ($image) {
            ImageHelper::remove($image->filepath);
            DB::table('condo_have_media')->where('id', $id)->delete();
        }
        
        return back();
        
    }

    public function getPlaces($id) {
        $condo = Condo::find($id);
        
        $_places = DB::table('condo_have_nearby_places')->where('condo_id', $id)->get();
        
        $places = [];
        
        $ids = [];
        
        foreach($_places as $place) {
            $places[$place->type][] = $place;
            $ids[] = $place->place_id;
        }
        
        return view('manager.condo.places'
                , ['active' => 'condo', 'condo' => $condo, 'places' => $places, 'ids' => $ids]);
    }
    
    public function postPlaces(Request $request) {
        
        //DB::delete('DELETE FROM condo_have_nearby_places WHERE condo_id = ?', [$request->input('id')]);
        
        $places = $request->input('places');
        $type = $request->input('type');
        $condo_id = $request->input('id');
        
        $data = [];
        
        $now = date('Y-m-d H:i:s');
        
        foreach($places as $id => $name) {
            $data[] = [
                'condo_id' => $condo_id,
                'place_id' => $id,
                'name' => $name,
                'type' => $type,
                'created_at' => $now,
                'updated_at' => $now
            ];
        }
        
        DB::table('condo_have_nearby_places')->insert([$data]);
        
    }

    public function getListings($id) {
        $condo = Condo::find($id);
        
        $properties = DB::table('properties_belongs_to_condos')
                ->select('properties.*')
                ->leftJoin('properties', 'properties.id', 'properties_belongs_to_condos.property_id')
                ->where('properties_belongs_to_condos.condo_id', $id)
                ->orderBy('properties.created_at', 'desc')->paginate(15);
        
        return view('manager.condo.listing'
                , ['active' => 'condo', 'condo' => $condo, 'properties' => $properties]);
    }

    public function getNewList() {
        
        
        
        return view('manager.condo.new-list', ['active' => 'new-condo']);
    }

    public function postRoom(Request $request, $id) {
        $condo = Condo::find($id);
        $condo->addRoomType($request->input('name'), $request->input('size'));

        return view('manager.condo.rooms', ['condo' => $condo]);
    }

    public function deleteRoom(Request $request, $condo, $room) {
        $condo = Condo::find($condo);
        $condo->deleteRoomType($room);

        return view('manager.condo.rooms', ['condo' => $condo]);
    }

    public function updateRoom(Request $request, $condo, $room) {
        $condo = Condo::find($condo);
        $condo->updateRoomType($request);

        return view('manager.condo.rooms', ['condo' => $condo]);
    }

    public function getCreate() {
        return view('manager.condo.create', ['active' => 'condo']);
    }

}
