<?php

namespace App\Http\Controllers\Manager;

use App\Models\User;
use App\Models\User\Type as UserType;
use App\Models\User\Staff;
use Illuminate\Http\Request;
use App\Http\Requests\Customer\Staff\Create as CreateStaffRequest;
use App\Http\Requests\Customer\Staff\Update as UpdateStaffRequest;
use Hash;
use DB;

class StaffController extends BaseController {

    /**
     * Get list of staff
     * @return type
     */
    public function getList(Request $request) {

        $sortby = $this->getCurrentSorting($request, ['updated_at', 'desc']);

        $staffs = [];
        
        $count_per_page = 15;

        if ($sortby[0] == 'name') {
            $staffs = Staff::select(DB::raw('business_staffs.*'))
                    ->join('users', 'users.id', '=', 'business_staffs.staff_id')
                    ->orderBy('users.realname', $sortby[1])
                    ->paginate($count_per_page);
        } else {
            $staffs = Staff::orderBy($sortby[0], $sortby[1])
                    ->paginate($count_per_page);
        }

        return view('manager.staff.list', [
            'active' => 'staff',
            'staffs' => $staffs,
            'sortby' => $sortby
        ]);
    }

    public function getCreate() {
        return view('manager.staff.create');
    }

    public function postCreate(CreateStaffRequest $request) {

        $role = $request->input('role');

        $typecode = (($role == 'manager') ? 'manager' : 'staff');

        $type = UserType::where('code', $typecode)->first();

        $password = str_random();

        $user = new User;
        $user->type_id = $type->id;
        $user->email = $request->input('email');
        $user->realname = $request->input('name');
        $user->password = Hash::make($password);
        $user->save();

        DB::insert('INSERT INTO business_staffs (staff_id, role, created_at, updated_at) '
                . 'VALUES (?, ?, NOW(), NOW())'
                , [$user->id, $role]);

        // Send email verification

        return response()->json(['status' => 'OK', 'user' => $user->id]);
    }

    public function getUpdate($id) {

        $user = User::find($id);
        $staff = Staff::find($id);

        return view('manager.staff.update', ['user' => $user, 'staff' => $staff]);
    }

    public function postUpdate(UpdateStaffRequest $request, $id) {

        $role = $request->input('role');

        $user = User::find($id);
        $user->realname = $request->input('name');

        if ($role == 'manager') {
            $type = UserType::where('code', 'manager')->first();
            $user->type_id = $type->id;
        }

        $user->save();

        DB::update('UPDATE business_staffs SET role = ?, updated_at = NOW() WHERE staff_id = ?'
                , [$role, $id]);

        return response()->json(['status' => 'OK', 'user' => $user->id]);
    }

    public function getDelete($id) {        
        $user = User::find($id);        
        return view('manager.staff.delete', ['user' => $user]);
    }
    
    public function delete($id) {
        
        DB::delete('DELETE FROM business_staffs WHERE staff_id = ?', [$id]);
        
        $user = User::find($id);
        $user->delete();
        
        return redirect('/manager/staff/list');
    }

}
