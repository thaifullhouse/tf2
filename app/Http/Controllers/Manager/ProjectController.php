<?php

namespace App\Http\Controllers\Manager;

use App\Http\Controllers\Manager\ProjectTraits\Apartment as ApartmentTrait;
use App\Http\Controllers\Manager\ProjectTraits\Condominium as CondominiumTrait;
use App\Http\Controllers\Manager\ProjectTraits\House as HouseTrait;
use App\Http\Controllers\Manager\ProjectTraits\Townhouse as TownhouseTrait;
use App\Models\Condo;
use App\Models\Property;
use App\Models\Project\PropertyReview;
use App\Helpers\UploadHelper;
use Illuminate\Http\Request;
use App\Helpers\ImageHelper;
use DB;

class ProjectController extends BaseController {

    use ApartmentTrait;
    use CondominiumTrait;
    use HouseTrait;
    use TownhouseTrait;

    public function getList() {

        $condos = Condo::where('new_project', true)->paginate(20);

        return view('manager.project.list', ['active' => 'project', 'condos' => $condos]);
    }

    public function getCreate() {
        return view('manager.project.create', ['active' => 'project']);
    }

    public function uploadPropertyReviewImage(Request $request, $id, $domain) {

        $property = Property::find($id);

        $review = $property->getProjectReview();

        $table = false;

        if ($domain == 'location') {
            $table = 'property_project_location_images';
        } else if ($domain == 'facility') {
            $table = 'property_project_facility_images';
        } else if ($domain == 'floorplan') {
            $table = 'property_project_floorplan_images';
        }

        if ($table) {

            $uploader = new UploadHelper($request);
            $files = $uploader->saveMultiple('images');

            foreach ($files as $filename) {
                DB::insert('INSERT INTO ' . $table . ' (review_id, filepath) VALUES (?, ?)', [
                    $review->id, $filename
                ]);
            }
        }
    }

    public function removePropertyReviewImage($id, $domain, $pid) {

        $property = Property::find($pid);

        $table = false;

        if ($domain == 'location') {
            $table = 'property_project_location_images';
        } else if ($domain == 'facility') {
            $table = 'property_project_facility_images';
        } else if ($domain == 'floorplan') {
            $table = 'property_project_floorplan_images';
        }

        if ($table) {
            $filepath = DB::table($table)->where('id', $id)->value('filepath');
            //$path = storage_path('app/public/' . $filepath);
            ImageHelper::remove($filepath);

            DB::delete('DELETE FROM ' . $table . ' WHERE id = ?', [$id]);
        }

        $link = $property->property_type == 'detachedhouse' ? 'house' : $property->property_type;

        return redirect('manager/project/' . $link . '/review/' . $pid);
    }

    public function updatePropertyReviewDetails(Request $request, $id, $domain) {
        $review = PropertyReview::find($id);

        $property = Property::find($review->property_id);

        if ($domain == 'location') {
            $review->location_details = $request->input('location_details');
        } else if ($domain == 'facility') {
            $review->facility_details = $request->input('facility_details');
        } else if ($domain == 'floorplan') {
            $review->floor_plan_details = $request->input('floor_plan_details');
        } else if ($domain == 'video') {
            $review->video_link = $request->input('video_link');
        } else if ($domain == 'overview') {
            $review->overview = $request->input('overview');
        }
        $review->save();

        $link = $property->property_type == 'detachedhouse' ? 'house' : $property->property_type;

        return redirect('manager/project/' . $link . '/review/' . $review->property_id);
    }

}
