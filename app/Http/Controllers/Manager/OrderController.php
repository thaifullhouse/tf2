<?php

namespace App\Http\Controllers\Manager;

use App\Models\Purchase;
use Illuminate\Http\Request;
use App\Models\User\Account as UserAccount;
use App\Models\Bank;
use DB;

class OrderController extends BaseController {
    
    public function getList() {
        
        $purchases = Purchase::orderBy('updated_at', 'desc')->paginate(25);
        
        return view('manager.order.list', 
                [
                    'active' => 'order', 
                    'purchases' => $purchases
                ]);
    }
    
    public function getDetails($id) {
        
        $status = session('status');
        
        $purchase = Purchase::find($id);
        
        $purchases = Purchase::where('account_id', $purchase->account_id)
                ->orderBy('created_at', 'desc')
                ->paginate(15); 
        $account = UserAccount::find($purchase->account_id);
        $banks = Bank::orderBy('created_at', 'asc')->get();
        
        return view('manager.order.details', [
            'purchase' => $purchase,
            'purchases' => $purchases,
            'account' => $account,
            'banks' => $banks,
            'status' => $status
        ]);
        
    }
    
    public function updateAccount(Request $request, $id) {
        $purchase = Purchase::find($id);
        $account = UserAccount::find($purchase->account_id);
        
        $account->country = $request->input('country');
        $account->address = $request->input('address');
        $account->city = $request->input('city');
        $account->state = $request->input('state');
        $account->zipcode = $request->input('zipcode');
        $account->firstname = $request->input('firstname');
        $account->lastname = $request->input('lastname');
        $account->cardnum = $request->input('cardnum');
        $account->expire_month = $request->input('expire_month');
        $account->expire_year = $request->input('expire_year');
        $account->securitynum = $request->input('securitynum');
        $account->save();
        
        $request->session()->flash('status', 'account_updated');
        
        return redirect('/manager/order/details/' . $id);
    }
    
    public function updateOrder(Request $request, $id) {
        
        $purchase = Purchase::find($id);
        $purchase->bank = $request->input('bank');
        $purchase->updated_amount = round(((int)$request->input('last_amount')) * 100);
        $purchase->save();
        
        $request->session()->flash('status', 'order_updated');
        
        return redirect('/manager/order/details/' . $id);
        
    }
    
    public function getStat() {
        
        $last30days = DB::select('SELECT d.date, SUM(d.amount) AS total '
                . 'FROM (SELECT (total_amount::float/100) AS amount, created_at::date AS date FROM customer_purchases WHERE created_at::date > current_date - interval ? day) '
                . 'AS d GROUP BY d.date ORDER BY d.date ASC', [30]);
        
        
        $last6months = DB::select('SELECT d.month, SUM(d.amount) AS total '
                . 'FROM (SELECT (total_amount::float/100) AS amount, '
                . 'to_char(date(created_at::date,\'YYYY-MM\') AS month '
                . 'FROM customer_purchases WHERE created_at::date > current_date - interval ? month) '
                . 'AS d GROUP BY d.date ORDER BY d.date ASC', [6]);
        
        return view('manager.order.stat', 
                [
                    'active' => 'order', 
                    'last30days' => $last30days,
                    'last6months' => $last6months,
                ]);
    }
    
}