<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Models\User\Type as UserType;

class RedirectIfNotCustomer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return redirect('/access-forbidden');
        }

        $user = Auth::user();        
        $type = UserType::where('code', 'member')->first();        
        
        if ($user->type_id != $type->id) {
            return redirect('/access-forbidden');
        }
        
        $profile = $user->getProfile();
        View::share('currentUser', $user);
        View::share('currentProfile', $profile);

        return $next($request);
    }
}
