<?php

namespace App\Http\Middleware;

use Closure;
//use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = $request->user();
        
        if (!$user) {
            return redirect('/access-forbidden');
        }
        
        /*if (!$request->user()->hasRole($role)) {
            return redirect('/access-forbidden');
        }*/

        return $next($request);
    }
}
