<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\Models\User\Type as UserType;

class RedirectIfNotManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (!Auth::guard($guard)->check()) {
            return redirect('/auth/login');
        }
        
        $user = Auth::user();        
        $type = UserType::where('code', 'manager')->first();        
        
        if ($user->type_id != $type->id) {
            return redirect('/access-forbidden');
        }

        return $next($request);
    }
}
