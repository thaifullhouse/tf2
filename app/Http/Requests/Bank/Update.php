<?php

namespace App\Http\Requests\Bank;

use App\Http\Requests\Base\ManagerRequest;

class Update extends ManagerRequest {    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'bank' => 'required',
            'name' => 'required',
            'account_no' => 'required',
            'branch' => 'required'
        ];
    }

}
