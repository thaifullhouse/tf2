<?php

namespace App\Http\Requests\Condo;

use App\Http\Requests\Base\ManagerRequest;

class Create extends ManagerRequest {    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {        
        $rules = [
            'lat' => 'required',
            'lng' => 'required'
        ];

        $langs = \App\Helpers\Language::getActiveCodes();

        foreach ($langs as $lang) {
            $rules['name.' . $lang] = 'required';
        }
        return $rules;
    }

}
