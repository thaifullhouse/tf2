<?php

namespace App\Http\Requests\Banner;

use Auth;
use App\Models\User\Type as UserType;
use Illuminate\Foundation\Http\FormRequest;

class Create extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        if (Auth::check()) {
            /* Manager only */
            $manager = UserType::where('manager')->first();
            $user = Auth::user();
            return $user->type_id == $manager->id;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'developer_id' => 'required',
            'image' => 'required',
            'location' => 'required'
        ];
    }

}
