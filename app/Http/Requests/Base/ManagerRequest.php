<?php

namespace App\Http\Requests\Base;

use Auth;
use App\Models\User\Type as UserType;
use Illuminate\Foundation\Http\FormRequest;

class ManagerRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        if (Auth::check()) {
            $manager = UserType::where('code', 'manager')->first();
            $user = Auth::user();
            return $user->type_id == $manager->id;
        } else {
            return false;
        }
    }
}