<?php

namespace App\Http\Requests\Base;

use Auth;
use App\Models\User\Type as UserType;
use Illuminate\Foundation\Http\FormRequest;

class AdminRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        if (Auth::check()) {
            $admin = UserType::where('code',  'admin')->first();
            $user = Auth::user();
            return $user->type_id == $admin->id;
        } else {
            return false;
        }
    }
}