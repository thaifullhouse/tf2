<?php

namespace App\Http\Requests\Base;

use Auth;
use App\Models\User\Type as UserType;
use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {

        if (Auth::check()) {
            $member = UserType::where('code', 'member')->first();
            $user = Auth::user();
            return $user->type_id == $member->id;
        } else {
            return false;
        }
    }
}