<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FacilityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /* Manager and  admin only */
        /*$user = Auth::user();
        return $user->type_id < 3;*/
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        $rules = [];

        $langs = \App\Helpers\Language::getActiveCodes();

        foreach ($langs as $lang) {
            $rules['name.' . $lang] = 'required';
        }
        return $rules;
    }
}