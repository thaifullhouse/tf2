<?php

namespace App\Http\Requests\Bank;

use App\Http\Requests\Base\ManagerRequest;

class Create extends ManagerRequest { 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'name' => 'required',
            'developer_id' => 'required',
            'image' => 'required',
            'location' => 'required'
        ];
    }

}
