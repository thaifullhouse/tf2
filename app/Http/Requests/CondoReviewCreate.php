<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
//use Auth;

class CondoReviewCreate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Only authenticated user
        
        //return Auth::check();
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'review' => 'required',
            'condo_id' => 'required|exists:condo_list,id'
        ];
    }
}
