<?php

namespace App\Http\Requests\Package;

use App\Http\Requests\Base\ManagerRequest;

class Create extends ManagerRequest {    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'listing_count' => 'required',
            'normal_price' => 'required',
            'period' => 'required'
        ];
    }

}
