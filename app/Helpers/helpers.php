<?php
if (!function_exists('__pgdate2hr')) {

    function __pgdate2hr($date, $format = 'd/m/Y') {

        return date($format, strtotime($date));
    }

}

if (!function_exists('__asset')) {

    function __asset($file) {
        return $file . '?t=' . filemtime(public_path() . $file);
    }

}

if (!function_exists('__dbconf')) {

    function __dbconf($token, $translate = false) {

        $conf = DB::table('settings')
                ->where('domain', $token)
                ->value('value');

        if ($translate) {
            $locale = App::getLocale();

            $translations = json_decode($conf, true);

            $conf = trim(array_get($translations, $locale));

            if (empty($conf)) {
                $conf = trim(array_get($translations, 'th'));
            }
        }

        return $conf;
    }

}


if (!function_exists('__trget')) {

    function __trget($data, $lang = null, $default_lang = 'th') {

        if ($lang == null) {
            $lang = App::getLocale();
        }

        $tr = trim(array_get($data, $lang));
        return $tr ? $tr : array_get($data, $default_lang);
    }

}

if (!function_exists('__condo_image')) {

    function __trimg($domain, $lang = null, $default_lang = 'th') {

        if ($lang == null) {
            $lang = App::getLocale();
        }

        $setting = DB::table('settings')->where('domain', $domain)->value('value');

        if ($setting) {
            $data = json_decode($setting, true);
            $img = array_get($data, $lang);
            return $img ? $img : array_get($data, $default_lang);
        } else {
            return null;
        }
    }

}

if (!function_exists('__knum')) {

    function __knum($value) {

        $result = 0;

        if ($value > 999 && $value <= 999999) {
            $result = floor($value / 1000) . 'K';
        } elseif ($value > 999999) {
            $result = floor($value / 1000000) . 'M';
        } else {
            $result = $value;
        }

        return $result;
    }

}

if (!function_exists('__t')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function __t($token, $default, $description = '') {

        if (preg_match('/(\w+)(\.)(.+)/', $token, $matches)) {

            $locale = App::getLocale();

            $translation = trans('_generated_' . $token);

            if ($translation != '_generated_' . $token) {
                return empty($translation) ? $default : $translation;
            }

            $text = DB::table('translations')
                    ->where('section', $matches[1])
                    ->where('label', $matches[3])
                    ->value('translation');

            if (empty($text)) {

                $langs = array_keys(\App\Helpers\Language::getActives());

                $now = date('Y-m-d H:i:s');

                $description = empty($description) ? $default : $description;

                DB::table('translations')
                        ->insert([
                            'section' => $matches[1],
                            'label' => $matches[3],
                            'translation' => json_encode(array_fill_keys($langs, '')),
                            'description' => $description,
                            'created_at' => $now,
                            'updated_at' => $now
                ]);

                return empty($default) ? $translation : $default;
            } else {

                return $default ? $default : trans($token);
            }
        } else {
            return $default ? $default : trans($token);
        }
    }

}

if (!function_exists('sortable_col')) {

    /**
     * description
     *
     * @param
     * @return
     */
    function sortable_col($text, $link, $col, $default, $current) {

        if ($current[0] == $col) {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $current[0]; ?>,<?php echo $current[1] == 'asc' ? 'desc' : 'asc'; ?>"
               class="sortable">
                   <?php
                   echo $text;
                   if ($current[1] == 'asc') {
                       ?>
                    <i class="fa fa-angle-up"></i>
                    <?php
                } else {
                    ?>
                    <i class="fa fa-angle-down"></i>
                    <?php
                }
                ?>
            </a>            
            <?php
        } else {
            ?>
            <a href="<?php echo $link; ?>?sortby=<?php echo $col; ?>,<?php echo $default; ?>">
                <?php echo $text; ?>
            </a>
            <?php
        }
    }

}


if (!function_exists('__banner')) {

    function __banner($location) {

        $now = date('Y-m-d H:i:s');
        $banner = DB::table('banners')
                ->where('location', $location)
                ->where('date_start', '<=', $now)
                ->where('date_end', '>=', $now)
                ->orderby('updated_at', 'asc')
                ->first();

        if ($banner) {
            DB::table('banners')->where('id', $banner->id)->update(['print' => DB::raw('print + 1'), 'updated_at' => date('Y-m-d H:i:s')]);

            $lang = App::getLocale();

            $images = json_decode($banner->images, true);

            $image = array_get($images, $lang);

            $image = $image ? $image : array_get($images, 'th');

            if ($image) {
                ?>
                <div class="banner <?php echo $location; ?>">
                    <a href="<?php echo $banner->related_link; ?>" target="_blank">
                        <img class="img-responsive" src="/image/<?php echo $image; ?>"/>
                    </a>                    
                </div>
                <?php
            }
        }
    }

}

if (!function_exists('__trfields')) {

    function __trfields($name, $object, $textarea = false, $row = 3) {
        $langs = \App\Helpers\Language::getActives();

        foreach ($langs as $code => $lang) {
            
            $value = $object->getJsonTranslatedField($name, $code);
            
            if ($textarea == true) {
                ?>
                <p class="input-lang input-lang-<?php echo $code; ?>">
                    <label><?php echo $lang; ?></label>
                    <textarea class="form-control" rows="<?php echo $row; ?>" name="<?php echo $name, '[', $code, ']'; ?>"><?php echo $value; ?></textarea>
                </p>
                <?php
            } else {
                ?>
                <p class="input-lang input-lang-<?php echo $code; ?>">
                    <label><?php echo $lang; ?></label>
                    <input class="form-control" name="<?php echo $name, '[', $code, ']'; ?>" value="<?php echo $value; ?>"/>
                </p>
                <?php
            }            
        }
        ?>
        <p class="show-more-lang">
            <span class="btn-link show-more-lang" data-target="<?php echo $name; ?>">Show more languages</span>
        </p>
        <?php
    }
}