<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use App\Helpers\Language;

class FakeLocation extends Command {
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:location';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function handle() {
        
        $this->createProvinces();
        
        $ids = Db::table('provinces')->pluck('id');
        
        foreach($ids as $id) {
            $this->createDistrict($id);
        }
        
        $ids = Db::table('districts')->pluck('id');
        
        foreach($ids as $id) {
            $this->createArea($id);
        }
        
    }
    
    protected function createProvinces($count = 20) {
        
        $data = [];
        $now = date('Y-m-d H:i:s');
        
        for($i = 0;  $i < $count; $i++) {            
            $names = $this->getNames();
            $data[] = [
                'name' => json_encode($names),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        
        DB::table('provinces')->insert($data);
    }
    
    protected function createDistrict($parent_id, $count = 20) {
        $data = [];
        $now = date('Y-m-d H:i:s');
        
        for($i = 0;  $i < $count; $i++) {            
            $names = $this->getNames();
            $data[] = [
                'name' => json_encode($names),
                'province_id' => $parent_id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        
        DB::table('districts')->insert($data);
    }
    
    protected function createArea($parent_id, $count = 20) {
        $data = [];
        $now = date('Y-m-d H:i:s');
        
        for($i = 0;  $i < $count; $i++) {            
            $names = $this->getNames();
            $data[] = [
                'name' => json_encode($names),
                'district_id' => $parent_id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        
        DB::table('geo_area')->insert($data);
    }
    
    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();
        
        foreach($langs as $lang) {
            $names[$lang] = $faker->city;
        }
        
        return $names;
    }
    
}