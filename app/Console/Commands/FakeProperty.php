<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Language;
use DB;

class FakeProperty extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:property';
    protected $provinces;
    protected $districts;
    protected $areas;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    public function handle() {

        $this->line('Loading geo data');

        $this->prepateGeoData();

        $this->line('Loading profile data');
        
        $this->createPropertiesForType('owner');
        $this->createPropertiesForType('agent');
        $this->createPropertiesForType('company');        
    }
    
    protected function createPropertiesForType($type) {
        
        $this->line('Create data property for type : ' . $type);
        
        $users = DB::table('user_profiles')->where('member_type', $type)->pluck('user_id');
        
        $bar = $this->output->createProgressBar(count($users));
        
        foreach($users as $id) {
            $this->createProperty($id, 'apartment', 25);
            $this->createProperty($id, 'condo', 25);
            $this->createProperty($id, 'detachedhouse', 25);
            $this->createProperty($id, 'townhouse', 25);
            $bar->advance();
        }
        
        $bar->finish();
        $this->line('');
    }

    protected function createProperty($user_id, $type, $count = 10) {

        $last_id = DB::table('properties')->orderBy('id', 'desc')->value('id');

        $faker = \Faker\Factory::create();

        $data = [];

        $now = date('Y-m-d H:i:s');

        for ($i = 0; $i < $count; $i++) {

            $lng = $faker->longitude(100.243783, 100.763573);
            $lat = $faker->longitude(13.603501, 13.911629);

            $location = "ST_GeomFromText('POINT($lng $lat 0.0)', 4326)";

            $_data = [
                'poster_id' => $user_id,
                'property_type' => $type,
                'listing_type' => $faker->randomElement(['rent', 'sale']),
                'property_name' => $faker->streetAddress,
                'street_number' => $faker->buildingNumber,
                'street_name' => $faker->streetName,
                'postale_code' => $faker->postcode,
                'location' => DB::raw($location),
                'created_at' => $now,
                'updated_at' => $now,
                'published' => true,
                'publication_date' => '2016-01-01',
                'listing_class' => $faker->randomElement(['standard', 'exclusive', 'featured']),
                'expiration_date' => '2017-12-31',                
                'new_project' => $faker->randomElement([true, false]),
                'name' => $this->getNames()
            ];

            if ($_data['new_project']) {
                //$_data[] = 
            }
            
            $data[] = $_data;
        }

        DB::table('properties')->insert($data);

        $properties = DB::table('properties')->select('id', 'listing_type', 'property_type')->where('id', '>', $last_id)->get();

        // insert facilities, features, media, details
        
        foreach($properties as $property) {
            $this->createDetails($property);
        }
    }

    protected function createDetails($property) {

        $faker = \Faker\Factory::create();

        $price = 0;

        if ($property->listing_type == 'sale') {
            $price = $faker->numberBetween(500000, 10000000);
        } else {
            $price = $faker->numberBetween(2000, 100000);
        }

        $now = date('Y-m-d H:i:s');

        $data = [
            'property_id' => $property->id,
            'deposite' => ($price * 2),
            'listing_price' => $price,
            'floor_num' => $faker->numberBetween(2, 30),
            'bedroom_num' => $faker->numberBetween(0, 5),
            'bathroom_num' => $faker->numberBetween(0, 3),
            'total_size' => $faker->numberBetween(15, 100),
            'year_built' => $faker->numberBetween(2000, 2016),
            'availability_date' => '2015-01-01',
            'created_at' => $now,
            'updated_at' => $now,
            'address' => $this->getAddress(),
            'title' => $this->getTitle(),
            'other_details' => $this->getDetails()
        ];

        if ($property->listing_type == 'rent') {
            $data['minimal_rental_period'] = $faker->numberBetween(6, 12);
        }

        if ($property->property_type == 'condo' || $property->property_type == 'apartment') {
            $data['tower_num'] = $faker->numberBetween(0, 3);
        }

        DB::table('property_details')->insert($data);
    }
    
    protected function getAddress() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->streetAddress;
        }

        return json_encode($names);
    }
    
    protected function getTitle() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->streetName;
        }

        return json_encode($names);
    }
    
    protected function getDetails() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->text();
        }

        return json_encode($names);
    }

    protected function getNames() {
        $faker = \Faker\Factory::create();
        $names = [];
        $langs = Language::getActiveCodes();

        foreach ($langs as $lang) {
            $names[$lang] = $faker->city;
        }

        return json_encode($names);
    }

    protected function prepateGeoData() {

        $this->provinces = DB::table('provinces')->pluck('id');

        $districts = DB::table('districts')->get();

        foreach ($districts as $district) {
            $this->districts[$district->province_id][] = $district->id;
        }

        $area = DB::table('geo_area')->get();

        foreach ($area as $areum) {
            $this->areas[$areum->district_id] = $areum->id;
        }
    }

    protected function getGeodata() {

        $faker = \Faker\Factory::create();

        $province_id = $faker->randomElement($this->provinces);

        $district_id = $faker->randomElement($this->districts[$province_id]);
        $area_id = $faker->randomElement($this->areas[$district_id]);

        return [$province_id, $district_id, $area_id];
    }

}
