<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;


class UpdatePoi extends Command {
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:poi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update poi type randomly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function handle() {
        
        $ids = DB::table('search_pois')->pluck('id');
        $faker = \Faker\Factory::create();
        
        foreach($ids as $id) {
            $type = $faker->randomElement(['bts', 'mrt', 'apl', 'bank', 'dept-store', 'hospital', 'school']);
            DB::table('search_pois')->where('id', $id)->update(['type' => $type]);
        }
        
    }
}