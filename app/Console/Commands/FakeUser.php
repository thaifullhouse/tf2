<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User\Type as UserType;
use DB;
use Hash;

class FakeUser extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'faker:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    public function handle() {
        
        //$user
        
        // 20 visitors
        
        $this->line('Creating visitor profile');
        $this->createUser('visitor', 'visitor', 2);
        
        // 20 owners
        
        $this->line('Creating owner profile');
        $this->createUser('member', 'owner', 20);
        
        // 20 frelance
        
        $this->line('Creating agent profile');
        $this->createUser('member', 'agent', 20);
        
        // 20 company
        
        $this->line('Creating company profile');
        $this->createUser('member', 'company', 20);
        
        // 20 developer
        
        $this->line('Creating developer profile');
        $this->createUser('member', 'developer', 20);     
        
    }
    
    protected function createUser($type, $profile, $count = 20) {
        $faker = \Faker\Factory::create();
        
        $count = min($count, 99);
        
        $type = UserType::where('code', '=', $type)->first();
        
        $data = [];
        
        $password = Hash::make('123456');
        
        $now = date('Y-m-d H:i:s');
        
        for($i = 0; $i < $count; $i++) {
            $email = $profile . '.' . $faker->safeEmail;
            $data[] = [
                'email' => $email,
                'password' => $password,
                'type_id' => $type->id,
                'created_at' => $now,
                'updated_at' => $now,
                'email_validated' => true,
                'realname' => $faker->name(),
                'customer_type' => '',
                'active' => true
            ];
        }
        
        DB::table('users')->insert($data);
        
        $this->createMissingProfilesProfiles($profile);
    }
    
    protected function createMissingProfilesProfiles($profile) {
        
        $faker = \Faker\Factory::create();
        
        $users = DB::table('users')->select('*')
                ->whereRaw('id NOT IN (SELECT user_id FROM user_profiles)')
                ->get();
        
        $data = [];
        
        $now = date('Y-m-d H:i:s');
        
        $member_types = [];
        
        foreach($users as $user) {
            
            $type = UserType::find($user->type_id);            
            
            $data[] = [
                'user_id' => $user->id,
                'created_at' => $now,
                'updated_at' => $now,
                'company_name' => $faker->company,
                'company_registration' => $faker->swiftBicNumber,
                'firstname' => $faker->firstName(),
                'lastname' => $faker->lastName(),
                'phone' => $faker->e164PhoneNumber,
                'mobile' => $faker->e164PhoneNumber,
                'line_id' => $faker->userName,
                'citizen_id' => '',
                'member_type' => $profile,
                'contact_name' => '',
                'address' => '',
                'birthday' => $faker->date(),
                'languages' => '',
                'gender' => $faker->randomElement(['male', 'female']),
                'nationality' => '',
                'introduction' => $faker->text(),                
                'agent_verified' => true,
                'accept_quick_matching' => true,
                'has_paid' => true
            ];
        }
        
        DB::table('user_profiles')->insert($data);        
    }
    
    protected function getMemberType($type) {
        
        $_type = $type->code;
        
        if ($type->code == 'member') {
            $types = ['owner', 'agent', 'company', 'developer'];
            shuffle($types);
            $_type = $types[0];
        }        
        
        return $_type;        
    }
    
}