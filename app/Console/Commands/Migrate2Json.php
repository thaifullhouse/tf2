<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Language;
use DB;

class Migrate2Json extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'm2json:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate old field to JSON';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        
        $this->line('Starting data migration');
        
        $this->updateNewFields('condo_facilities', ['name'], 'id');
        $this->updateNewFields('condo_have_nearby_places', ['name'], 'id');
        $this->updateNewFields('condo_profile_data', ['address', 'details'], 'condo_id');        
        $this->updateNewFields('condo_list', ['name'], 'id');
        $this->updateNewFields('districts', ['name'], 'id');
        $this->updateNewFields('geo_area', ['name'], 'id');        
        $this->updateNewFields('optional_features', ['name'], 'id');
        $this->updateNewFields('property_details', ['address', 'title', 'other_details'], 'property_id');
        $this->updateNewFields('property_features', ['name'], 'id');
        $this->updateNewFields('search_pois', ['name'], 'id');        
        $this->updateNewFields('provinces', ['name'], 'id');
        
        $this->line('Data migration finished');
        
    }

    protected function updateNewFields($table, $cols, $pk) {
        
        $this->info("Updating table {$table}");
        
        $langs = Language::getActiveCodes();

        $old_data = DB::table($table)->get();
        
        $this->info("Record number: " . count($old_data));
        
        $bar = $this->output->createProgressBar(count($old_data));

        foreach ($old_data as $old) {
            foreach ($cols as $col) {                
                $data = array_fill_keys($langs, '');                
                $data['en'] = trim($old->{$col . '_en'});
                $data['th'] = trim($old->{$col . '_th'});                
                DB::table($table)->where($pk, $old->$pk)->update([
                    $col => json_encode($data)
                ]); 
            }            
            $bar->advance();
        }
        
        $bar->finish();
        
        $this->line('');
    }

}
