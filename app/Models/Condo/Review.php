<?php

namespace App\Models\Condo;

use App\Models\BaseModel;
use App\Models\User;
use App\Http\Requests\CondoReviewCreate;
use App\Helpers\ImageHelper;
use Auth;
use DB;
use Validator;

/**
 * Profiles review for agents and land developers
 */
class Review extends BaseModel {

    protected $table = 'condo_reviews';
    protected $hidden = ['condo_id', 'updated_at', 'user_id'];
    protected $appends = ['user', 'ratings', 'attachments'];

    /**
     * Create a review from user request
     */
    public static function createFromUserRequest(CondoReviewCreate $request) {

        $user = User::find($request->input('user'));

        $review = new self;
        $review->user_id = $user->id;
        $review->condo_id = $request->input('condo_id');
        $review->review = $request->input('review');
        $review->save();

        // Save scores

        //$score_fields = DB::table('condo_review_domains')->pluck('id');
        
        DB::insert('INSERT INTO condo_review_scores (review_id, domain, score) VALUES (?, ?, ?)'
                , [$review->id, 'facility', (int) $request->input('ratings.facility')]);
        
        DB::insert('INSERT INTO condo_review_scores (review_id, domain, score) VALUES (?, ?, ?)'
                , [$review->id, 'design', (int) $request->input('ratings.design')]);
        
        DB::insert('INSERT INTO condo_review_scores (review_id, domain, score) VALUES (?, ?, ?)'
                , [$review->id, 'accessibility', (int) $request->input('ratings.accessibility')]);
        
        DB::insert('INSERT INTO condo_review_scores (review_id, domain, score) VALUES (?, ?, ?)'
                , [$review->id, 'security', (int) $request->input('ratings.security')]);
        
        DB::insert('INSERT INTO condo_review_scores (review_id, domain, score) VALUES (?, ?, ?)'
                , [$review->id, 'amenities', (int) $request->input('ratings.amenities')]);

        // Save attachments

        $errors = [];

        $files = $request->file('images');

        if (is_array($files)) {
            foreach ($files as $file) {

                $rules = array('file' => 'required|mimes:png,gif,jpeg');

                $validator = Validator::make(array('file' => $file), $rules);

                if ($validator->passes()) {
                    $destinationPath = storage_path('app/public');
                    $extention = $file->getClientOriginalExtension();
                    $filename = $this->getUniqueFileName() . '.' . strtolower($extention);
                    $uploaded = $file->move($destinationPath, $filename);

                    if ($uploaded) {
                        $errors[] = $destinationPath . '/' . $filename;

                        // Resize and add watermak

                        ImageHelper::resize($destinationPath . '/' . $filename);

                        DB::insert('INSERT INTO condo_review_attachments (review_id, filepath) VALUES (?, ?)', [$review->id, $filename]);
                    } else {
                        $errors[] = ['upload_error' => $file];
                    }
                }
            }
        }        

        return $review;
    }

    public function getUserAttribute() {

        $user = User::find($this->user_id);
        return $user;

    }

    public function getRatingsAttribute() {
        
        $scores = DB::select('SELECT domain, score FROM condo_review_scores WHERE review_id = ?', [$this->id]);

        $ratings = [];

        foreach ($scores as $score) {
            $ratings[$score->domain] = $score->score;
        }

        return $ratings;

    }

    public function getAttachmentsAttribute() {

        $files = DB::select('SELECT filepath FROM condo_review_attachments WHERE review_id = ?', [$this->id]);

        $attachments = [];

        foreach($files as $file) {
            $attachments[] = url('/image/' . $file->filepath);
        }

        return $attachments;

    }

}
