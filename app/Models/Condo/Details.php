<?php

namespace App\Models\Condo;

use App\Models\BaseModel;
use DB;

/**
 * This class define user
 */
class Details extends BaseModel {
    
    /**
     * This class is associated with `condo_facilities` table
     */
    protected $table = 'condo_profile_data';
    
    protected $primaryKey = 'condo_id';
    
    protected $hidden = ['created_at', 'updated_at', 'condo_id'];
    
    public function getFacilityIds()
    {
        return DB::table('condo_have_facilities')->where('condo_id', $this->condo_id)->pluck('facility_id')->all();
    }
    
}

