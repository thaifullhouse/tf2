<?php

namespace App\Models\Acl;

use Illuminate\Database\Eloquent\Model;
use App\Models\Acl\Resource as AclResource;

/**
 * This class define the type of user that can use the website
 */
class Role extends Model
{
    /**
     * This class is associated with `acl_roles` table
     */
    protected $table = 'acl_roles';


    /**
     *
     */
    public function getResources()
    {

    }

    /**
     *
     */
    public function addPermission(AclResource $resource, array $privileges)
    {

    }
}