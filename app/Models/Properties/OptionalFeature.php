<?php

namespace App\Models\Properties;

use App\Helpers\ImageHelper;
use App\Models\BaseModel;

/**
 * This class define user
 */
class OptionalFeature extends BaseModel {

    /**
     * This class is associated with `condo_facilities` table
     */
    protected $table = 'optional_features';

    public function getUrl() {

        if ($this->icon_path) {
            return url('/image/' . $this->icon_path);
        } else {
            return 'http://placehold.it/100x100?text=No+icon';
        }
    }

    public function getThumbUrl($dimension = '100x100', $create = false, $text = 'No+icon') {

        if ($this->icon_path) {
            if ($create) {
                ImageHelper::createThumbFromFile($dimension, $this->icon_path);
            }
            return url('/thumb/' . $dimension . '/' . $this->icon_path);
        } else {
            return 'http://placehold.it/'.$dimension.'?text=' . preg_replace('/(\s+)/', '+', $text);
        }
    }

}
