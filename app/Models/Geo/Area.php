<?php

namespace App\Models\Geo;

use App\Models\BaseModel;

/**
 * This class define user
 */
class Area extends BaseModel {

    /**
     * This class is associated with `condo_list` table
     */
    protected $table = 'geo_area';
}
