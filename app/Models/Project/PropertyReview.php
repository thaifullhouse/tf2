<?php

namespace App\Models\Project;

use App\Models\BaseModel;
use DB;

class PropertyReview extends BaseModel {
    
    protected $table = 'property_project_reviews';    
    
    /**
     * 
     * @return type
     */
    public function getLocationImages() {   
        
        return DB::table('property_project_location_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFloorplanImages() {   
        
        return DB::table('property_project_floorplan_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFacilityImages() {   
        
        return DB::table('property_project_facility_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
}