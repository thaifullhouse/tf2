<?php

namespace App\Models\Project;

use App\Models\BaseModel;
use DB;

class Review extends BaseModel {
    
    protected $table = 'condo_project_reviews';    
    
    /**
     * 
     * @return type
     */
    public function getLocationImages() {   
        
        return DB::table('project_location_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFloorplanImages() {   
        
        return DB::table('project_floorplan_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
    /**
     * 
     * @return type
     */
    public function getFacilityImages() {   
        
        return DB::table('project_facility_images')
                    ->where('review_id', $this->id)->get();
        
    }
    
}