<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\Language;

class BaseModel extends Model
{
    /**
     * 
     * @param string $field
     * @param array $values
     */
    public function setJsonTranslatedField($field, $values) { 
        
        $langs = Language::getActiveCodes();  
        
        $old_options = json_decode($this->$field, true);
              
        $new_options = array_only($values, $langs);

        foreach($new_options as $lang => $value){
            $old_options[$lang] = $value;            
            if (!array_key_exists($lang, $old_options)) {
                $old_options[$lang] = '';
            }
        }
        
        $this->$field = json_encode($old_options);
    }
    
    /**
     * 
     * @param string $field
     * @param string $lang
     * @return string
     */
    public function getJsonTranslatedField($field, $lang = null, $fallback_lang = 'th') {

        $langs = Language::getActiveCodes(); 

        if ($lang == null || in_array($lang, $langs) == false) {
            $lang = \App::getLocale();
        }

        $options = json_decode($this->$field, true);

        $tr = trim(array_get($options, $lang));

        return $tr ? $tr : array_get($options, $fallback_lang);
    }
    
    /**
     * 
     * @param string $field
     * @param array $values
     */
    public function setTranslatedField($field, $values) {        
       $this->setJsonTranslatedField($field, $values);        
    }
    
    /**
     * @deprecated Please remove later
     * @param string $field
     * @return string
     */
    public function getTranslatedField($field)
    {
        return $this->getJsonTranslatedField($field);
    }
}
