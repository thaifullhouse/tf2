<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

/**
 * This class define the type of user that can use the website
 */
class Review extends Model
{
    /**
     * This class is associated with `user_types` table
     */
    protected $table = 'agent_reviews';
    
    protected $appends = ['user', 'date' ,'time'];
    
    protected $hidden = ['user_id', 'created_at', 'updated_at', 'agent_id'];
    
    public function getUserAttribute() {
        
        $user = User::find($this->user_id);
        $faker = \Faker\Factory::create();
        
        return [
            'name' => $user->realname,
            'image' => $faker->imageUrl(100, 100)
        ];
        
    }
    
    public function getDateAttribute() {        
        return date('d/m/Y', strtotime($this->created_at));        
    }
    public function getTimeAttribute() {        
        return date('H:i:s', strtotime($this->created_at));        
    }
    
}