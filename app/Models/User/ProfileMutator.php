<?php

namespace App\Models\User;

use App\Models\User;
use App\Models\Property;
//use App\Models\User\Review;
//use DB;

trait ProfileMutator {
    
    public function getPostsAttribute() {
        return 5;
    }
    
    public function getDescriptionAttribute() {
        $faker = \Faker\Factory::create();
        return $faker->realText();
    }
    
    public function getPriceRangeAttribute() {
        
        $min = mt_rand(1000000, 20000000);
        $max = mt_rand($min, ($min + 50000000));
        $avg = mt_rand($min, $max);
        
        return [
            'min' => $min,
            'max' => $max,
            'avg' => $avg
        ];
    }
    
    public function getEmailAttribute() {        
        $user = User::find($this->user_id);
        return $user->email;
    }
    
    public function getListingsAttribute() {
        return [
            'sale' => 60, 
            'rent' => 12, 
            'apartment' => mt_rand(5, 100),
            'house' => mt_rand(5, 100),
            'condominium' => mt_rand(5, 100),
            'townhouse' => mt_rand(5, 100)
            ];
    }
    
    public function getCommentsAttribute() {
        
        $comments = [];
        
        $faker = \Faker\Factory::create();
        
        $per_page = 10;
        
        for($i = 0; $i < $per_page; $i++) {
            $comments[] = [
                'id' => ($i + 1),
                'rating' => mt_rand(1, 5),                
                'date' => sprintf("%02d", $i + 2) . '/12/2016',
                'user' => [
                    'email' => $faker->email,
                    'name' => $faker->name,               
                    'message' => $faker->realText(),
                    'image' => $faker->imageUrl(100, 100)
                ]
            ];
        }
        
        /*
        $reviews = Review::where('agent_id', $this->user_id)
                    ->orderBy('created_at', 'desc')
                    ->limit(20)->get();
         */
        
        $page = array_get($_GET, 'page', 1);
        
        return [
            'data' => $comments,
            'per_page' => $per_page,
            'current_page' => $page
        ];
    }
    
    public function getProfilePictureAttribute() {
        $faker = \Faker\Factory::create();
        return $faker->imageUrl(300, 300);
    }
    
    public function getLogoAttribute() {
        $faker = \Faker\Factory::create();
        return $faker->imageUrl(300, 300);
    }
    
    
    public function getCoverPictureAttribute() {
        $faker = \Faker\Factory::create();
        return $faker->imageUrl(900, 350);
    }
    
    public function getLangsAttribute() {
        
        $langs = config('countries.languages');
        $supported = explode(',', $this->languages);
        return array_values(array_only($langs, $supported));
    }

    public function getNationalityAttribute($value) {
        
        $countries = config('countries.list');
        
        foreach($countries as $country) {
            if ($country['code'] == $value) {
                return $country['name'];
            }
        }

        return '';
    }
    
    public function getRankingAttribute() {
        return 5;
    }
    
    public function getNameAttribute() {
        return $this->getName();
    }
    
    public function getIdAttribute() {
        return $this->getPublicID();
    }
    
    public function getWebsiteAttribute() {
        $faker = \Faker\Factory::create();
        return $faker->url;
    }
    
    public function getTelAttribute() {
        return $this->phone ? $this->phone : $this->mobile;
    }
    
    public function getPropertiesAttribute() {
        /*$today = date('Y-m-d');
        return Property::where('poster_id', $this->user_id)
                ->where('published', true)
                ->where('publication_date', '<', $today)
                ->where('expiration_date', '>', $today)
                ->whereRaw(DB::raw('deleted_at IS NULL'))
                ->get()->all();*/
        
        return Property::limit(20)->get()->all();
    }
} 