<?php

namespace App\Models\User;


class FacebookData extends SocialData {
    
    protected $social_network = 'facebook';
    
}