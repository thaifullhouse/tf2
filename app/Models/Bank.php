<?php

namespace App\Models;

/**
 * Condominium class
 */
class Bank extends BaseModel {

    protected $table = 'bank_accounts';

    public static function getBankList() {
        return [
            'bbl' => 'Bangkok Bank',
            'ktb' => 'Krung Thai Bank',
            'scb' => 'Siam Commercial Bank',
            'kbank' => 'Kasikornbank',
            'bay' => 'Bank of Ayudhya',
            'tbank' => 'Thanachart Bank',
            'tmb' => 'TMB Bank'
        ];
    }
    
    public static function getBankName($code) {
        $banks = self::getBankList();        
        return array_get($banks, $code, 'Unknown bank');
    }

    public function getBankNameByCode() {
        $banks = self::getBankList();        
        return array_get($banks, $this->bank, 'Unknown bank');
    }
    

}
