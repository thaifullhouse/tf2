<?php

namespace App\Models;

use DB;
use App\Models\Condo\Details as CondoDetails;
use App\Models\Project\Review as ProjectReview;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//use App\Models\Poi;
use App\Helpers\ImageHelper;

/**
 * Condominium class
 */
class Condo extends BaseModel {

    protected $table = 'condo_list';
    protected $appends = ['details'];
    protected $hidden = ['created_at', 'updated_at', 'province_id', 'postcode', 'district_id', 'user_submited', 'new_project'];

    /**
     * Get the list of properties in this condo
     */
    public function countListedProperties() {
        return DB::table('properties_belongs_to_condos')
                        ->select('properties.*')
                        ->leftJoin('properties', 'properties.id', 'properties_belongs_to_condos.property_id')
                        ->where('properties_belongs_to_condos.condo_id', $this->id)->count();
    }

    /**
     * Get condo location.
     * @return Array An associated array with lat, lng keys
     */
    public function get_location() {
        $location = [];
        $locations = DB::select('SELECT ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng,
            ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat FROM condo_list WHERE id = ?', [$this->id]);

        if (count($locations)) {
            $location['lng'] = $locations[0]->lng;
            $location['lat'] = $locations[0]->lat;
        } else {
            $location['lng'] = 0;
            $location['lat'] = 0;
        }
        return $location;
    }

    public function getDetails() {
        $details = null;

        try {
            $details = CondoDetails::where('condo_id', $this->id)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $details = new CondoDetails;
            $details->condo_id = $this->id;
            $details->save();
        }

        return $details;
    }

    public function getFacilitiesID() {
        return DB::table('condo_have_facilities')
                        ->where('condo_id', $this->id)
                        ->pluck('facility_id')->all();
    }

    public function countImages() {
        return DB::table('condo_have_media')
                        ->where('media_type', 'image')
                        ->where('condo_id', $this->id)
                        ->count();
    }

    public function addRoomType($name, $size) {
        DB::insert('INSERT INTO condo_room_types (condo_id, name, size) VALUES (?, ?, ?)', [$this->id, $name, $size]);
    }

    public function deleteRoomType($id) {
        DB::delete('DELETE FROM condo_room_types WHERE id = ?', [$id]);
    }

    public function updateRoomType($request) {
        DB::update('UPDATE condo_room_types SET name= ?, size = ? WHERE id = ?'
                , [$request->input('name'), $request->input('size'), $request->input('id')]);
    }

    public function getRoomTypes() {
        return DB::select('SELECT * FROM condo_room_types WHERE condo_id = ? ORDER BY id ASC', [$this->id]);
    }

    public function getProjectReview() {

        $review = null;

        try {
            $review = ProjectReview::where('condo_id', $this->id)->firstOrFail();
        } catch (ModelNotFoundException $ex) {
            $review = new ProjectReview;
            $review->condo_id = $this->id;
            $review->save();
        }

        return $review;
    }

    public function getDetailsAttribute() {

        return CondoDetails::find($this->id);
    }

    public function getGooglePlacesByType($type) {

        $location = $this->get_location();

        $lang = \App::getLocale();

        $params = [
            'key' => config('google.api_key'),
            'location' => $location['lat'] . ',' . $location['lng'],
            'radius' => 1000,
            'type' => $type,
            'language' => $lang,
            'rankby' => 'distance'
        ];

        $url = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?' . http_build_query($params);

        $response = file_get_contents($url);

        //var_dump($response);

        return json_decode($response, true);
    }

    public function getNearbyPlaces() {

        $lnglat = $this->get_location();
        return DB::table('search_pois')
                        ->select('*'
                                , DB::raw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat['lng']} {$lnglat['lat']})')) as distance")
                                , DB::raw('ST_X(ST_TRANSFORM(location::geometry, 4326)) AS lng')
                                , DB::raw('ST_Y(ST_TRANSFORM(location::geometry, 4326)) AS lat'))
                        ->whereRaw("ST_Distance(location, ST_GeogFromText('SRID=4326;POINT({$lnglat['lng']} {$lnglat['lat']})')) < " . 1000)
                        ->get();
    }

    public function getReviewsAttribute() {

        $reviews = CondoReview::where('condo_id', $this->id)->orderBy('id', 'desc')->paginate(15);

        // Append ratings

        foreach ($reviews as $id => $review) {
            $ratings = DB::table('condo_review_scores')->where('review_id', $review->id)->pluck('score', 'domain');
            $review->ratings = $ratings;
            $reviews[$id] = $review;
        }

        return response()->json([
                    'status' => 'OK',
                    'reviews' => $reviews
        ]);
    }

    public function getImagesAttribute() {
        $images = DB::table('condo_have_media')
                ->where('media_type', 'image')
                ->where('condo_id', $this->id)
                ->pluck('filepath');

        foreach ($images as $key => $path) {
            $images[$key] = url('/image/' . $path);
        }


        return $images;
    }

    public function getImages() {
        $images = [];

        $media = DB::select('SELECT * FROM properties_have_media WHERE property_id = ? AND media_type = ?', [$this->id, 'image']);

        if (count($media)) {
            foreach ($media as $row) {
                $images[] = new ImageHelper($row);
            }
        }

        return $images;
    }

    public function getListedOn() {
        return Carbon::now()->subSeconds(strtotime($this->publication_date))->diffForHumans();
    }
    
    public function countListings() {
        return DB::table('properties_belongs_to_condos')
                        ->select(DB::raw('COUNT(properties.id) AS num'), 'properties.listing_type')
                        ->leftJoin('properties', 'properties.id', 'properties_belongs_to_condos.property_id')
                        ->where('properties_belongs_to_condos.condo_id', $this->id)
                        ->groupBy('properties.listing_type')
                        ->pluck('num', 'listing_type');
    }

}
