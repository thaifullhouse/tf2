<?php

namespace App\Models;

/**
 * Condominium class
 */
class Banner extends BaseModel {

    protected $table = 'banners';
    
    public static function getLocations() {
        return [
            'home' => __t('manager_banner.location-home', 'Homepage'),
            
            'map-sale' => __t('manager_banner.location-map-sale', 'Map sale'),
            'map-rent' => __t('manager_banner.location-map-rent', 'Map rent'),
            'map-project' => __t('manager_banner.location-map-project', 'Map new project'),
            'map-condo' => __t('manager_banner.location-map-condo', 'Map condo'),
            
            /*'list-sale' => __t('manager_banner.location-list-sale', 'Listing Sale'),
            'list-rent' => __t('manager_banner.location-list-rent', 'Listing Rent'),
            'list-project' => __t('manager_banner.location-list-project', 'Listing New project'),
            'list-condo' => __t('manager_banner.location-list-condo', 'Listing Condo community')*/
        ];
    }
    
    
    public function getLocation() {
        return $this->location ? trans('manager/banner.location-' . $this->location) : '';
    }

}
