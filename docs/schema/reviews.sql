--
-- Publisher reviews, replace old /agent_reviews/
--

CREATE TABLE publisher_reviews (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    rating INTEGER,
    comment TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);


--
-- Replace old /condo_project_reviews/
--

CREATE TABLE manager_property_reviews (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES property_list (id),
    user_id INTEGER REFERENCES users (id),
    domain VARCHAR(50), -- location, facility, floorplan
    details TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- Replace old /condo_review_attachments/
--

CREATE TABLE manager_property_review_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES manager_property_reviews (id) ON DELETE CASCADE,
    filepath VARCHAR(255)
);

--
-- Replace old /condo_reviews/
--

CREATE TABLE visitor_property_reviews (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES property_list (id),
    user_id INTEGER REFERENCES users (id),
    comment TEXT,
    ratings JSONB, -- facility, design, accessibility, security, amenities
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- Replace old /condo_review_attachments/
--

CREATE TABLE visitor_property_reviews_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES visitor_property_reviews (id) ON DELETE CASCADE,
    filepath VARCHAR(255)
);
