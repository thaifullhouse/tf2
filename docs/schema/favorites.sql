--
--
--

CREATE TABLE user_favorite_publishers
(
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    publisher_id  INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    UNIQUE(user_id, publisher_id)
);

CREATE TABLE user_favorite_properties
(
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    UNIQUE(user_id, property_id)
);

CREATE TABLE user_saved_searches (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    search_query VARCHAR(255),
    created_at TIMESTAMP
);