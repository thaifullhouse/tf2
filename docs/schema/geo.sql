CREATE EXTENSION postgis;

--
--
--

CREATE TABLE geo_provinces (
    id SERIAL PRIMARY KEY,
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE geo_districts (
    id SERIAL PRIMARY KEY,
    province_id INTEGER REFERENCES geo_provinces (id) ON DELETE CASCADE, 
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE geo_area (
    id SERIAL PRIMARY KEY,
    district_id INTEGER REFERENCES geo_districts (id) ON DELETE CASCADE, 
    name JSONB,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP

);

--
--
--

CREATE TABLE geo_points (
    id SERIAL PRIMARY KEY,
    name JSONB,
    external_source VARCHAR(100),
    external_id INTEGER,
    location geography(PointZ,4326),
    zoom_level INTEGER,
    type VARCHAR(100),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
); 