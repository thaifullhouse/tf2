--
--
--

CREATE TABLE banners (
    id SERIAL PRIMARY KEY,
    customer VARCHAR(255),
    name VARCHAR(255),
    related_link VARCHAR(500),
    location VARCHAR(100),
    date_start TIMESTAMP,
    date_end TIMESTAMP,
    payment_status VARCHAR(10),
    print INTEGER, 
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE banner_images (
    id SERIAL PRIMARY KEY,
    banner_id INTEGER REFERENCES banners (id) ON DELETE CASCADE,
    filepath  VARCHAR(255),
    custom_link VARCHAR(500)
);

--
--
--

CREATE TABLE listing_packages (
    id SERIAL PRIMARY KEY,
    name JSONB,
    type VARCHAR(20),
    normal_price NUMERIC(14,2),
    discounted_price NUMERIC(14,2),
    listing_count INTEGER,
    free_listing_count INTEGER,
    period INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--

CREATE TABLE publisher_package_purchases (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    invoice_id VARCHAR(255),
    package_id INTEGER REFERENCES listing_packages (id) ON DELETE CASCADE,
    bank_id INTEGER,    
    used_listing INTEGER,
    used_free_listing INTEGER,
    price NUMERIC(14,2),  
    total_amount NUMERIC(14,2), -- price + vat
    updated_amount NUMERIC(14,2),
    payment_status VARCHAR(100),
    payment_method VARCHAR(100),
    payment_updated_at TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
--
--


CREATE TABLE paysbuy_payments (
    id SERIAL PRIMARY KEY,
    purchase_id INTEGER REFERENCES publisher_package_purchases (id) ON DELETE CASCADE,
    result VARCHAR(100),
    apcode VARCHAR(100),,
    amount NUMERIC(14,2),
    fee NUMERIC(14,2),
    method VARCHAR(4),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);
  

  