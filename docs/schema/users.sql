--
--
--

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    email VARCHAR(255),
    password VARCHAR(100),
    realname VARCHAR(200),
    user_type VARCHAR(20), -- visitor, publisher, manager, admin, staff
    tfh_staff_role VARCHAR(20),
    email_validated BOOLEAN DEFAULT FALSE,
    email_validation_token VARCHAR (20),
    remember_token VARCHAR(255),
    prefered_lang VARCHAR(5),
    password_reset_token VARCHAR(200),
    active BOOLEAN,
    last_login TIMESTAMP,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(email)
);

--
--
--

CREATE TABLE publishers ( 
    id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    phone VARCHAR(255),
    mobile VARCHAR(255),
    line_id VARCHAR(255),    
    address TEXT,
    languages VARCHAR(255),    
    publisher_type VARCHAR(20), -- owner, freelance, company, developer
    total_visitors INTEGER,
    introduction TEXT,
    cover_picture_path VARCHAR(255),   
    profile_picture_path VARCHAR(255),   
    logo_picture_path VARCHAR(255),   
    verified BOOLEAN DEFAULT FALSE,
    accept_quick_matching BOOLEAN DEFAULT FALSE,
    has_made_payments BOOLEAN DEFAULT FALSE,
    search_ranking_level INTEGER,
    UNIQUE(id)
);

--
--
--

CREATE TABLE publisher_personal_profiles (
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    firstname VARCHAR(255), 
    lastname VARCHAR(255), 
    citizen_id VARCHAR(255),
    citizen_id_attachment VARCHAR(255),
    gender VARCHAR(255),
    nationality VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(publisher_id)
);

--
--
-- 

CREATE TABLE publisher_company_profiles (
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    company_name VARCHAR(255),
    company_registration VARCHAR(255),
    company_contact_name VARCHAR(255),
    UNIQUE(publisher_id)
);

--
-- Publisher addresses, replace old /address_list/
--

CREATE TABLE publisher_address_list (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    area_name JSONB,
    district_name JSONB,
    province_name JSONB,
    province_id INTEGER,
    district_id INTEGER,
    area_id INTEGER,
    street_number VARCHAR(20),
    street_name JSONB,
    postale_code VARCHAR(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);


CREATE TABLE user_social_data
(
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    social_network VARCHAR(50),
    data_key VARCHAR(255),
    data_value TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);