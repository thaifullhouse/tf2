--
--
--

CREATE TABLE message_for_publishers (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    sender_name VARCHAR(255),
    sender_phone VARCHAR(255),
    sender_email VARCHAR(255),
    message TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);


--
--
--

CREATE TABLE notification_messages (
    id SERIAL PRIMARY KEY,
    type VARCHAR(50),   
    message TEXT,
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE notification_receivers (
    message_id INTEGER REFERENCES notification_messages (id) ON DELETE CASCADE,
    publisher_id INTEGER REFERENCES publishers (id) ON DELETE CASCADE,
    read BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(message_id, publisher_id)
);