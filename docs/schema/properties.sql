--
-- List of properties
--

CREATE TABLE properties (
    id SERIAL PRIMARY KEY,
    publisher_id INTEGER REFERENCES publishers (id),
    name JSONB,   
    property_type VARCHAR(10), -- condo, condo_room, apartment, house, townhouse
    is_new_project BOOLEAN,
    published BOOLEAN,
    publication_date DATE,
    manager_review TEXT,
    video_link VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE property_address (
    property_id INTEGER REFERENCES property_list (id),
    location geography(PointZ,4326),
    area_name JSONB,
    district_name JSONB,
    province_name JSONB,
    province_id INTEGER,
    district_id INTEGER,
    area_id INTEGER,
    street_number VARCHAR(20),
    street_name JSONB,
    postale_code VARCHAR(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(property_id)
);

--
--
--

CREATE TABLE property_belongs_to_condo (
    condo_id INTEGER REFERENCES property_list (id),
    property_id INTEGER REFERENCES property_list (id),
    UNIQUE(condo_id, property_id)
);

--
-- Replace old /condo_profile_data/
--

CREATE TABLE property_extended_data (
    property_id INTEGER REFERENCES property_list (id),
    starting_sales_price INTEGER,
    office_hours VARCHAR(255),
    year_built VARCHAR(5),
    number_of_tower INTEGER,
    total_floors INTEGER,
    total_units INTEGER,
    total_size INTEGER,
    website VARCHAR(255),
    details JSONB,
    contact_person VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(property_id)
);

--
-- Replace old /condo_room_types/
--

CREATE TABLE property_have_rooms_types (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES property_list (id),
    name JSONB,
    size VARCHAR(50)
);

--
-- List of listings
--

CREATE TABLE property_listing_data (    
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    listing_type VARCHAR(10), -- rent, sale
    title JSONB,
    address JSONB,
    other_details JSONB,
    deposite NUMERIC(14,2),
    listing_price NUMERIC(14,2),
    floor_num INTEGER,
    bedroom_num INTEGER,
    bathroom_num INTEGER,
    minimal_rental_period VARCHAR(100),
    availability_date DATE,
    ylink1 VARCHAR(500),
    ylink2 VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(property_id)
);

--
-- Replace old /condo_facilities/
--

CREATE TABLE property_facilities (
    id SERIAL PRIMARY KEY,
    name JSONB,
    icon_path VARCHAR(255),
    listing_rank INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- For property_features AND replace old /optional_features/
--

CREATE TABLE property_features (
    id SERIAL PRIMARY KEY,
    name JSONB,
    icon_path VARCHAR(255),
    listing_rank INTEGER,
    optional BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- Replace old /condo_have_facilities/
--

CREATE TABLE property_have_facilities (
    facility_id INTEGER REFERENCES property_facilities (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    UNIQUE(facility_id, property_id)
);

--
-- Replace old /properties_have_features/ and /properties_have_optional_features/
--

CREATE TABLE property_have_features (
    feature_id INTEGER REFERENCES property_features (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    UNIQUE(feature_id, property_id)
);

--
-- Replace old /condo_have_media/
--

CREATE TABLE property_have_images (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    filepath VARCHAR(255),
    listing_rank INTEGER,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP
);

--
-- Replace old condo_have_nearby_places
--

CREATE TABLE property_have_nearby_places (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES property_list (id) ON DELETE CASCADE,
    place_id VARCHAR(100),
    name JSONB,
    type VARCHAR(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    deleted_at TIMESTAMP,
    UNIQUE(place_id, property_id)
);