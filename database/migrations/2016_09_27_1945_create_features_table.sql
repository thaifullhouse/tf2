--
--
--

CREATE TABLE properties_have_optional_features (
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE,
    feature_id INTEGER REFERENCES optional_features(id) ON DELETE CASCADE
);

--
--
--

CREATE TABLE properties_have_features (
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE,
    feature_id INTEGER REFERENCES property_features(id) ON DELETE CASCADE
);

--
--
--

CREATE TABLE properties_have_facilities (
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE,
    facility_id INTEGER REFERENCES condo_facilities(id) ON DELETE CASCADE
);

--
--
--

ALTER TABLE property_details DROP COLUMN other_details;
ALTER TABLE property_details DROP COLUMN address;

ALTER TABLE property_details ADD COLUMN other_details_th TEXT;
ALTER TABLE property_details ADD COLUMN other_details_en TEXT;

ALTER TABLE property_details ADD COLUMN address_th TEXT;
ALTER TABLE property_details ADD COLUMN address_en TEXT;

ALTER TABLE property_details ADD COLUMN title_th VARCHAR(200);
ALTER TABLE property_details ADD COLUMN title_en VARCHAR(200);