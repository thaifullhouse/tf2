--
--
--

ALTER TABLE condo_profile_data RENAME COLUMN address TO address_th;
ALTER TABLE condo_profile_data RENAME COLUMN details TO details_th;

ALTER TABLE condo_profile_data ADD COLUMN address_en TEXT;
ALTER TABLE condo_profile_data ADD COLUMN details_en TEXT;
