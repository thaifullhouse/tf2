--
--
--

CREATE TABLE property_details (
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE,
    deposite INTEGER,
    rental INTEGER,
    floor_num INTEGER,
    bedroom_num INTEGER,
    bathroom_num INTEGER,
    total_size INTEGER,
    year_built INTEGER,
    tower_num INTEGER,
    minimal_rental_period INTEGER,
    availability_date DATE,
    other_details TEXT,
    address TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);