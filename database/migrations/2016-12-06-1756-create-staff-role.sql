--
--
--

CREATE TABLE customer_roles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

CREATE TABLE customer_staffs (
    account_id INTEGER REFERENCES customer_accounts (id),
    staff_id INTEGER REFERENCES users (id),
    role_id INTEGER REFERENCES customer_roles (id),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

DROP TABLE acl_permissions;
DROP TABLE acl_resources;
DROP TABLE users_have_roles;
DROP TABLE acl_roles;

