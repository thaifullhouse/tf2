--
--
--

CREATE TABLE settings (
    id SERIAL PRIMARY KEY,
    domain VARCHAR(255),
    value VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(domain)
);