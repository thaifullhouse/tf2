--
--
--

CREATE TABLE customer_accounts (
    id SERIAL PRIMARy KEY,
    creator_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    name VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(creator_id)
);

--
--
--
CREATE TABLE customer_accounts_managers (
    account_id INTEGER REFERENCES customer_accounts (id) ON DELETE CASCADE,
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    UNIQUE(user_id)
);

--
--
--

CREATE TABLE packages (
    id SERIAL PRIMARY KEY,
    name_en VARCHAR(255),
    name_th VARCHAR(255),
    type VARCHAR(20),
    normal_price INTEGER,
    discounted_price INTEGER,
    discounted_percent NUMERIC(4,2),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE customer_purchases (
    id SERIAL PRIMARY KEY,
    account_id INTEGER REFERENCES customer_accounts (id) ON DELETE CASCADE,
    package_id INTEGER REFERENCES packages (id) ON DELETE CASCADE,
    created_at TIMESTAMP
);