--
--
--

ALTER TABLE condo_review_scores DROP COLUMN domain_id;
ALTER TABLE condo_review_scores ADD COLUMN domain VARCHAR(100);

DROP TABLE condo_review_domains;