--
--
--

CREATE TABLE bank_accounts (
    id SERIAL PRIMARY KEY,
    bank VARCHAR(10),
    name VARCHAR(255),
    account_no VARCHAR(255),
    branch VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);