--
--
--

CREATE TABLE translation_pages (
    id SERIAL PRIMARY KEY,
    page VARCHAR(50),
    name VARCHAR(100),
    url VARCHAR(255),
    create_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(page)
);


CREATE TABLE translation_texts (
    id SERIAL PRIMARY KEY,
    page_id INTEGER REFERENCES translation_pages (id) ON DELETE CASCADE,
    text_en TEXT,
    text_th TEXT,
    text_cn TEXT,
    text_kr TEXT,
    text_jp TEXT
);