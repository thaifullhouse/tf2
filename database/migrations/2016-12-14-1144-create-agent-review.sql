--
--
--

CREATE TABLE agent_reviews
(
    id SERIAL PRIMARY KEY NOT NULL,
    user_id integer,
    agent_id integer,
    rating integer,
    review text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);

--
--
--

CREATE TABLE agent_review_scores
(
    id serial NOT NULL,
    review_id integer references agent_reviews (id),
    score integer,
    domain character varying(100)
);