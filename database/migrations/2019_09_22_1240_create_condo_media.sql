--
--
--

CREATE TABLE condo_have_media
(
  id serial primary key,
  condo_id integer references condo_list(id) on delete cascade,
  media_type character varying(10),
  filepath character varying(200),
  created_at timestamp without time zone,
  updated_at timestamp without time zone
);