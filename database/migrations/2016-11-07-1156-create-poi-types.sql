--
--
--

CREATE TABLE search_pois_type (
    id SERIAL PRIMARY KEY,
    code VARCHAR(100),
    name_en VARCHAR(255),
    name_th VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

ALTER TABLE search_pois DROP COLUMN type;
ALTER TABLE search_pois ADD COLUMN type_id INTEGER REFERENCES search_pois_type (id) ON DELETE CASCADE;