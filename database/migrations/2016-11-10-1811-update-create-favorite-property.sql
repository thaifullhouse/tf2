--
--
--

CREATE TABLE user_favorite_properties (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE user_favorite_agents (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    agent_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE user_saved_searches (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    search_data TEXT,
    created_at TIMESTAMP
);