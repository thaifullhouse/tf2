--
--
--

CREATE TABLE user_favorite_condos (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    condo_id INTEGER REFERENCES condo_list (id) ON DELETE CASCADE,
    created_at TIMESTAMP
);