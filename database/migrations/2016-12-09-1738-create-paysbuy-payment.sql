--
--
--

CREATE TABLE paysbuy_payments (
    id SERIAL PRIMARY KEY,
    purchase_id INTEGER REFERENCES customer_purchases (id),
    result VARCHAR(100),
    apcode VARCHAR(100),
    amount INTEGER,
    fee INTEGER,
    method VARCHAR(4),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);