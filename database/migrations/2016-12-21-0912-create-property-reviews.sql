--
--
--

CREATE TABLE property_project_reviews (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,   
    location_details TEXT,
    facility_details TEXT,
    floor_plan_details TEXT,
    overview TEXT, 
    video_link VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE property_project_review_attachments (
    review_id INTEGER REFERENCES property_project_reviews (id),
    filepath VARCHAR(255)
);

--
--
--

CREATE TABLE condo_project_review_attachments (
    review_id INTEGER REFERENCES condo_project_reviews (id),
    filepath VARCHAR(255)
);