--
--
--

ALTER TABLE customer_staffs RENAME TO business_staffs;

ALTER TABLE business_staffs DROP COLUMN account_id;