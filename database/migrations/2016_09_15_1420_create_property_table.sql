--
--
--

CREATE TABLE properties (
    id SERIAL PRIMARY KEY,
    poster_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    property_type VARCHAR(10),
    listing_type VARCHAR(10),
    property_name VARCHAR(500),
    province_id INTEGER REFERENCES provinces(id),
    district_id INTEGER REFERENCES districts(id),
    area_id INTEGER REFERENCES geo_area(id),
    street_number VARCHAR(20),
    street_name VARCHAR(200),
    postale_code VARCHAR(10),
    location geography(PointZ,4326),
    created_at TIMESTAMP,
    updated_at TIMESTAMP    
);

--
--
--

CREATE TABLE properties_belongs_to_condos (
    condo_id INTEGER REFERENCES condo_list(id) ON DELETE CASCADE,
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE
);

--
--
--

CREATE TABLE properties_have_media (
    id SERIAL PRIMARY KEY,
    property_id INTEGER REFERENCES properties(id) ON DELETE CASCADE,
    media_type VARCHAR(10),
    filepath VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);