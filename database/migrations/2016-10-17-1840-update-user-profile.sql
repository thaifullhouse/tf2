--
--
--

ALTER TABLE user_profiles ADD COLUMN contact_name VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN address TEXT;
ALTER TABLE user_profiles ADD COLUMN birthday DATE;
ALTER TABLE user_profiles ADD COLUMN languages VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN gender VARCHAR(50);
ALTER TABLE user_profiles ADD COLUMN nationality VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN introduction TEXT;