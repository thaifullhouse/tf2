--
--
--

ALTER TABLE condo_list DROP COLUMN name_en;
ALTER TABLE condo_list DROP COLUMN name_th;

--
--
--

ALTER TABLE condo_facilities DROP COLUMN name_en;
ALTER TABLE condo_facilities DROP COLUMN name_th;

--
--
--

ALTER TABLE condo_profile_data DROP COLUMN address_en;
ALTER TABLE condo_profile_data DROP COLUMN address_th;

ALTER TABLE condo_profile_data DROP COLUMN details_en;
ALTER TABLE condo_profile_data DROP COLUMN details_th;

--
--
--

ALTER TABLE districts DROP COLUMN name_en;
ALTER TABLE districts DROP COLUMN name_th;

--
--
--

ALTER TABLE geo_area DROP COLUMN name_en;
ALTER TABLE geo_area DROP COLUMN name_th;

--
--
--

ALTER TABLE optional_features DROP COLUMN name_en;
ALTER TABLE optional_features DROP COLUMN name_th;


--
--
--

ALTER TABLE properties DROP COLUMN property_name;
--
--
--

ALTER TABLE property_details DROP COLUMN address_en;
ALTER TABLE property_details DROP COLUMN address_th;

ALTER TABLE property_details DROP COLUMN title_en;
ALTER TABLE property_details DROP COLUMN title_th;

ALTER TABLE property_details DROP COLUMN other_details_en;
ALTER TABLE property_details DROP COLUMN other_details_th;

--
--
--

ALTER TABLE property_features DROP COLUMN name_en;
ALTER TABLE property_features DROP COLUMN name_th;

--
--
--

ALTER TABLE provinces DROP COLUMN name_en;
ALTER TABLE provinces DROP COLUMN name_th;

--
--
--

ALTER TABLE search_pois DROP COLUMN name_en;
ALTER TABLE search_pois DROP COLUMN name_th;