--
--
--

ALTER TABLE users ADD COLUMN email_validated BOOLEAN DEFAULT FALSE;
ALTER TABLE users ADD COLUMN email_validation_token VARCHAR(20);
