--
--
--

CREATE TABLE notification_types (
    id SERIAL PRIMARY KEY,
    name VARCHAR(50)
);

--
--
--
CREATE TABLE notification_messages (
    id SERIAL PRIMARY KEY,
    type_id INTEGER REFERENCES notification_types (id) ON DELETE CASCADE,
    message TEXT,
    created_at TIMESTAMP
);

--
--
--

CREATE TABLE notification_receivers (
    message_id INTEGER REFERENCES notification_messages (id) ON DELETE CASCADE,
    receiver_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    read BOOLEAN DEFAULT FALSE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
)
