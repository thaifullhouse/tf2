--
--
--

ALTER TABLE condo_list ADD COLUMN project_start DATE;
ALTER TABLE condo_list ADD COLUMN project_end DATE;

ALTER TABLE properties ADD COLUMN project_start DATE;
ALTER TABLE properties ADD COLUMN project_end DATE;