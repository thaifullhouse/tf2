--
--
--

ALTER TABLE user_profiles ADD COLUMN agent_verified BOOLEAN DEFAULT FALSE;
ALTER TABLE user_profiles ADD COLUMN accept_quick_matching BOOLEAN DEFAULT FALSE;
ALTER TABLE user_profiles ADD COLUMN has_paid BOOLEAN DEFAULT FALSE;

