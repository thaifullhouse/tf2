--
--
--

CREATE TABLE condo_room_types (
    id SERIAL PRIMARY KEY,
    condo_id INTEGER REFERENCES condo_list(id) ON DELETE CASCADE,
    name VARCHAR(255),
    size VARCHAR(10)
);
