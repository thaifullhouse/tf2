--
--
--

ALTER TABLE property_details ALTER COLUMN deposite TYPE VARCHAR(100);
ALTER TABLE property_details ALTER COLUMN minimal_rental_period TYPE VARCHAR(100);

