--
-- User types
--

CREATE TABLE user_types (
    id SERIAL PRIMARy KEY,
    code VARCHAR(10),
    name VARCHAR(100)
);

--
-- Users
--

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    login VARCHAR(30),
    password VARCHAR(100),
    type_id INTEGER REFERENCES user_types (id) ON DELETE CASCADE,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(login)
);

--
--
--

CREATE TABLE user_profiles (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    realname VARCHAR(200),
    email VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);