--
--
--

CREATE TABLE optional_features
(
    id serial primary key,
    name_th character varying(200),
    name_en character varying(200),
    icon_path character varying(200),
    created_at timestamp without time zone,
    updated_at timestamp without time zone  
);