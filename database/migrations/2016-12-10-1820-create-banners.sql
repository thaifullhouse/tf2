--
--
--

CREATE TABLE banners (
    id SERIAL PRIMARy KEY,
    name VARCHAR(255),
    developer_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    filepath VARCHAR(255),
    related_link VARCHAR(500),
    effect VARCHAR(50),
    location VARCHAR(50),
    date_start DATE,
    date_end DATE,
    payment_status VARCHAR(10),
    print INTEGER,   
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);