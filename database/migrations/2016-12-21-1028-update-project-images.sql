--
--
--

DROP TABLE property_project_review_attachments;

--
--
--


CREATE TABLE property_project_facility_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES property_project_reviews,
    filepath VARCHAR(500)
);

--
--
--

CREATE TABLE property_project_floorplan_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES property_project_reviews,
    filepath VARCHAR(500)
);

--
--
--

CREATE TABLE property_project_location_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES property_project_reviews,
    filepath VARCHAR(500)
);