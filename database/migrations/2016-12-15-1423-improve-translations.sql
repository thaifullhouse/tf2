--
--
--

DROP TABLE translation_texts;
DROP TABLE translation_pages;

CREATE TABLE translations (
    id SERIAL PRIMARY KEY,
    section VARCHAR(100),
    label VARCHAR(100),
    translation JSON,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(section, label)
);