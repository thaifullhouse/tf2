--
--
--
CREATE TABLE acl_resources (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    description TEXT,
    privileges TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--
CREATE TABLE acl_roles (
    id INTEGER PRIMARY KEY,
    name VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--
CREATE TABLE acl_permissions (
    role_id INTEGER REFERENCES acl_roles (id) ON DELETE CASCADE,
    resource_id INTEGER REFERENCES acl_resources (id) ON DELETE CASCADE,
    privileges TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE (role_id, resource_id)
);

--
--
--
CREATE TABLE users_have_roles (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    role_id INTEGER REFERENCES acl_roles (id) ON DELETE CASCADE,
    UNIQUE (user_id, role_id)
);
