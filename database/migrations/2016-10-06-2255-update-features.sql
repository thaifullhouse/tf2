--
--
--

ALTER TABLE property_features ADD COLUMN rank INTEGER;

ALTER TABLE optional_features ADD COLUMN rank INTEGER;

ALTER TABLE condo_facilities ADD COLUMN rank INTEGER;
