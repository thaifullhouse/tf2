--
--
--

CREATE TABLE condo_facilities (
    id SERIAL PRIMARY KEY,
    name_th VARCHAR(200),
    name_en VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE condo_have_facilities (
    condo_id INTEGER REFERENCES condo_list(id) ON DELETE CASCADE,
    facility_id INTEGER REFERENCES condo_facilities(id) ON DELETE CASCADE
);

--
--
--

CREATE TABLE condo_profile_data (
    condo_id INTEGER REFERENCES condo_list(id) ON DELETE CASCADE,
    starting_sales_price INTEGER,
    office_hours VARCHAR(100),
    year_built INTEGER,
    number_of_tower INTEGER,
    total_floors INTEGER,
    total_units INTEGER,
    room_types VARCHAR(200),
    website VARCHAR(200),
    district VARCHAR(200),
    address TEXT,
    details TEXT,
    contact_person VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE condo_have_nearby_places (
    condo_id INTEGER REFERENCES condo_list(id) ON DELETE CASCADE,
    place_id VARCHAR(100),
    name VARCHAR(200),
    custom_name VARCHAR(200),
    type VARCHAR(20),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);