--
--
--

ALTER TABLE user_profiles DROP COLUMN realname;
ALTER TABLE user_profiles DROP COLUMN email;

ALTER TABLE user_profiles ADD COLUMN company_name VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN company_registration VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN firstname VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN lastname VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN phone VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN mobile VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN line_id VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN citizen_id VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN citizen_id_attachment VARCHAR(255);

CREATE TABLE address_list (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    street_no VARCHAR(50),
    street_name VARCHAR(255),
    area VARCHAR(255),       
    district_id INTEGER REFERENCES districts (id),
    province_id INTEGER REFERENCES provinces (id),
    created_at TIMESTAMP,
    updated_at TIMESTAMP    
);