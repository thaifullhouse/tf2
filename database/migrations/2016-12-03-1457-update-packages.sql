--
--
--

ALTER TABLE packages DROP COLUMN name_en;
ALTER TABLE packages DROP COLUMN name_th;
ALTER TABLE packages DROP COLUMN discounted_percent;

ALTER TABLE packages ADD COLUMN name VARCHAR(255);
ALTER TABLE packages ADD COLUMN listing_count INTEGER;
ALTER TABLE packages ADD COLUMN free_listing_count INTEGER;
