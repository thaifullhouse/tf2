--
--
--

ALTER TABLE user_profiles ADD COLUMN cover_picture_path VARCHAR(255);
ALTER TABLE user_profiles ADD COLUMN profile_picture_path VARCHAR(255);