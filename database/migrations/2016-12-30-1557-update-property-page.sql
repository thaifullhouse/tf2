--
--
--

CREATE TABLE property_nearby_transportations (
    property_id INTEGER REFERENCES properties (id) ON DELETE CASCADE,
    trans_id INTEGER REFERENCES search_pois (id) ON DELETE CASCADE,
    type VARCHAR(20)
);

ALTER TABLE properties ADD COLUMN psf INTEGER;
ALTER TABLE properties ADD COLUMN developer_id INTEGER;