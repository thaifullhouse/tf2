--
--
--

ALTER TABLE customer_accounts ADD COLUMN country VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN address VARCHAR(500);
ALTER TABLE customer_accounts ADD COLUMN city VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN state VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN zipcode VARCHAR(50);
ALTER TABLE customer_accounts ADD COLUMN firstname VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN lastname VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN cardnum VARCHAR(255);
ALTER TABLE customer_accounts ADD COLUMN expire_month VARCHAR(20);
ALTER TABLE customer_accounts ADD COLUMN expire_year VARCHAR(5);
ALTER TABLE customer_accounts ADD COLUMN securitynum VARCHAR(255);

ALTER TABLE customer_purchases ADD COLUMN bank VARCHAR(50);
ALTER TABLE customer_purchases ADD COLUMN updated_amount INTEGER;