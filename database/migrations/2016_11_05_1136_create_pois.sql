--
--
--

CREATE TABLE search_pois (
    id SERIAL PRIMARY KEY,
    external_source VARCHAR(100),
    external_id INTEGER,
    name_en VARCHAR(255),
    name_th VARCHAR(255),
    type VARCHAR(20),
    location geography(PointZ,4326),
    zoom_level INTEGER
);
