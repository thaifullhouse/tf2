--
--
--

ALTER TABLE user_favorite_properties ADD UNIQUE (user_id, property_id); 

ALTER TABLE user_favorite_agents ADD UNIQUE (user_id, agent_id); 