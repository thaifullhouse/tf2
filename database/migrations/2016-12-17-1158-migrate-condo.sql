--
--
--

UPDATE condo_list SET name = jsonb_set(name, '{en}', name_en) WHERE true;

ALTER TABLE condo_facilities ADD COLUMN name JSONB;
ALTER TABLE condo_have_nearby_places ADD COLUMN name JSONB;

ALTER TABLE condo_list DROP COLUMN name;
ALTER TABLE condo_list ADD COLUMN name JSONB;

ALTER TABLE condo_profile_data ADD COLUMN address JSONB;
ALTER TABLE condo_profile_data ADD COLUMN details JSONB;

ALTER TABLE districts ADD COLUMN name JSONB;
ALTER TABLE geo_area ADD COLUMN name JSONB;
ALTER TABLE optional_features ADD COLUMN name JSONB;
ALTER TABLE property_features ADD COLUMN name JSONB;
ALTER TABLE provinces ADD COLUMN name JSONB;
ALTER TABLE search_pois ADD COLUMN name JSONB;

ALTER TABLE property_details ADD COLUMN address JSONB;
ALTER TABLE property_details ADD COLUMN title JSONB;
ALTER TABLE property_details ADD COLUMN other_details JSONB;
