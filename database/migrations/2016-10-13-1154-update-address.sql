--
--
--

ALTER TABLE address_list ADD COLUMN district VARCHAR(255);
ALTER TABLE address_list ADD COLUMN province VARCHAR(255);
ALTER TABLE address_list ADD COLUMN postal_code VARCHAR(25);