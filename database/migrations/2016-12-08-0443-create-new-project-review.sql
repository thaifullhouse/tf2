--
--
--

CREATE TABLE condo_project_reviews (
    id SERIAL PRIMARY KEY,
    condo_id INTEGER REFERENCES condo_list (id) ON DELETE CASCADE,
    location_details TEXT,
    facility_details TEXT,
    floor_plan_details TEXT,
    overview TEXT,
    video_link VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE project_location_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES condo_project_reviews (id) ON DELETE CASCADE,
    filepath VARCHAR(255)
);

--
--
--

CREATE TABLE project_facility_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES condo_project_reviews (id) ON DELETE CASCADE,
    filepath VARCHAR(255)
);

--
--
--

CREATE TABLE project_floorplan_images (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES condo_project_reviews (id) ON DELETE CASCADE,
    filepath VARCHAR(255)
);