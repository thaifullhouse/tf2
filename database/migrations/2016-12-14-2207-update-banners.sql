--
--
--


ALTER TABLE banners DROP COLUMN developer_id;
ALTER TABLE banners DROP COLUMN effect;
ALTER TABLE banners DROP COLUMN filepath;
ALTER TABLE banners ALTER COLUMN date_start TYPE TIMESTAMP;
ALTER TABLE banners ALTER COLUMN date_end TYPE TIMESTAMP;

ALTER TABLE banners ADD COLUMN customer VARCHAR(255); 

CREATE TABLE banner_images (
    id SERIAL PRIMARY KEY,
    banner_id INTEGER REFERENCES banners (id) ON DELETE CASCADE,
    lang VARCHAR(5),
    filepath VARCHAR(255),
    related_link VARCHAR(500),
    UNIQUE(banner_id, lang)
);