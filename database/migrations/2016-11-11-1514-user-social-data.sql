--
--
--

CREATE TABLE user_social_data (
    user_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    social_network VARCHAR(50),
    data_key VARCHAR(255),
    data_value TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    UNIQUE(user_id, social_network, data_key)
);