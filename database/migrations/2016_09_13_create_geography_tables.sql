--
--
--

CREATE TABLE provinces (
    id SERIAL PRIMARY KEY,
    name_th VARCHAR(1000),
    name_en VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE districts (
    id SERIAL PRIMARY KEY,
    province_id INTEGER REFERENCES provinces(id) ON DELETE CASCADE,
    name_th VARCHAR(1000),
    name_en VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE geo_area (
    id SERIAL PRIMARY KEY,
    district_id INTEGER REFERENCES districts(id) ON DELETE CASCADE,
    name_th VARCHAR(1000),
    name_en VARCHAR(500),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);