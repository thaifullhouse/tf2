--
--
--

CREATE TABLE condo_review_domains (
    id SERIAL PRIMARY KEY,
    name_en VARCHAR(255),
    name_th VARCHAR(255)
);

--
--
--

CREATE TABLE condo_reviews (
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id),
    condo_id INTEGER REFERENCES condo_list(id),
    review TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);

--
--
--

CREATE TABLE condo_review_scores (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES condo_reviews (id),
    domain_id INTEGER REFERENCES condo_review_domains (id),
    score INTEGER
);

--
--
--

CREATE TABLE condo_review_attachments (
    id SERIAL PRIMARY KEY,
    review_id INTEGER REFERENCES condo_reviews (id),
    filepath VARCHAR(255)
);