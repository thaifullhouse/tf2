--
--
--

CREATE EXTENSION Postgis;

CREATE TABLE condo_list
(
    id SERIAL PRIMARY KEY,
    name_en VARCHAR(200),
    name_th VARCHAR(200),
    location geography(PointZ,4326),
    location_name VARCHAR(200),
    created_at TIMESTAMP,
    updated_at TIMESTAMP
)
