--
--
--

ALTER TABLE user_saved_searches DROP COLUMN search_data;
ALTER TABLE user_saved_searches ADD COLUMN search_data JSONB;