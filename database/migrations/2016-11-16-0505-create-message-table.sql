--
--
--

CREATE TABLE messages (
    id SERIAL PRIMARY KEY,
    sender_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    receiver_id INTEGER REFERENCES users (id) ON DELETE CASCADE,
    name VARCHAR(255),
    phone VARCHAR(50),
    message_body TEXT,
    created_at TIMESTAMP,
    updated_at TIMESTAMP
);