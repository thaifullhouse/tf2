--
--
--

ALTER TABLE customer_staffs DROP COLUMN role_id;
ALTER TABLE customer_staffs ADD COLUMN role VARCHAR(20);

DROP TABLE customer_roles;