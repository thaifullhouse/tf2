--
--
--

ALTER TABLE customer_purchases ADD COLUMN package_name VARCHAR(255);
ALTER TABLE customer_purchases ADD COLUMN price INTEGER;
ALTER TABLE customer_purchases ADD COLUMN listing_count INTEGER;
ALTER TABLE customer_purchases ADD COLUMN free_listing_count INTEGER;
ALTER TABLE customer_purchases ADD COLUMN used_listing INTEGER;
ALTER TABLE customer_purchases ADD COLUMN used_free_listing INTEGER;
ALTER TABLE customer_purchases ADD COLUMN period INTEGER;
ALTER TABLE customer_purchases ADD COLUMN paid BOOLEAN DEFAULT FALSE;
ALTER TABLE customer_purchases ADD COLUMN paid_on TIMESTAMP;
ALTER TABLE customer_purchases ADD COLUMN updated_at TIMESTAMP;
ALTER TABLE customer_purchases ADD COLUMN payment_method VARCHAR(255);

ALTER TABLE customer_purchases DROP COLUMN package_id;
ALTER TABLE customer_purchases ADD COLUMN package_id INTEGER;



