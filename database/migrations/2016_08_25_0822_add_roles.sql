--
--
--

INSERT INTO acl_roles (id, name, created_at, updated_at) VALUES
(1, 'tech_admin', NOW(), NOW()),
(2, 'business_manager', NOW(), NOW()),
(3, 'accountant', NOW(), NOW()),
(4, 'content_editor', NOW(), NOW()),
(5, 'project_reviewer', NOW(), NOW()),
(6, 'real_estate_agent', NOW(), NOW()),
(7, 'freelance_agent', NOW(), NOW()),
(8, 'land_developer', NOW(), NOW()),
(9, 'owner', NOW(), NOW()),
(10, 'member', NOW(), NOW());
