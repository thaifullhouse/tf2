#!/usr/bin/env bash
#
# deploy.sh - Run deployment task for
#
# - push last source code to remote git repository
# - connect to server via ssh
# - update website code
# - install dependencies
# - run database migration
#
# Copyright (C) 2015 Wishwon System

# Push last update to master branch

echo ""
printf "\e[0;36mPushing last update to master branch\n"

printf "\e[0m\n"

git push origin master

printf "\e[0m\n"

# Update server source fron master branch

printf "\e[0;36mConnecting to remote server\n"

printf "\e[0m\n"

ssh -t -t root@188.166.251.245 '
cd /var/www/fth.ladargroup.com/
chmod +x ./scripts/update.sh
./scripts/update.sh
'
