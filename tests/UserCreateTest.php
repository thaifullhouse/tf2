<?php


use App\Models\User;
use App\Models\User\Profile as UserProfile;
use App\Models\User\Type as UserType;

class UserCreateTest extends TestCase {
    
    public function testRegister() {
        $type = UserType::where('code', '=', 'visitor')->first();
        
        $email = str_random() . '@mail.com';

        $user = new User();
        $user->email = $email;
        $user->password = Hash::make(str_random());
        $user->type_id = $type->id;
        $user->email_validated = false;
        $user->email_validation_token = str_random(20);
        $user->save();
        
        $this->assertGreaterThan(0, $user->id);
        
        // Create user profile
        
        $profile = new UserProfile;
        $profile->user_id = $user->id;
        $profile->member_type = UserProfile::VISITOR;
        $profile->save();
        
        $this->assertGreaterThan(0, $profile->user_id);
    }
    
}