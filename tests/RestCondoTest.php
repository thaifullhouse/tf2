<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class RestCondoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostReview()
    {
        
        $user = User::find(1);
        
        $this->actingAs($user)->json('POST', '/rest/condo/review', [
            'review' => 'Sally', 
            'condo_id' => 1,
            'ratings' => [
                'facility' => 3,
                'design' => 4,
                'accessibility' => 5,
                'security' => 6,
                'aminities' => 7
            ]
            ])->seeJson([
                 'status' => 'OK'
             ]);
    }
    
    /*public function testGetReview() {
        $this->json('GET', '/rest/reviews/1')->seeJson([
                 'status' => 'OK'
             ]);
    }*/
}
