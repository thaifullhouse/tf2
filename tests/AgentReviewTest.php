<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class AgentReviewTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testPostReview()
    {
        
        $user = User::find(1);
        
        $faker = \Faker\Factory::create();
        
        $this->actingAs($user)->json('POST', '/rest/agent/review', [
            'review' => $faker->realText(), 
            'agent_id' => 1,
            'rating' => 2
            ])->seeJson([
                 'status' => 'OK'
             ]);
    }
}
