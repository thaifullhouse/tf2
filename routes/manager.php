<?php

Route::group(['middleware' => 'auth.manager', 'prefix' => 'manager', 'namespace' => 'Manager'], function () {
    
    // Banner
    
    Route::get('/banner', 'BannerController@index');
    Route::get('/banner/create', 'BannerController@getCreate');
    
    Route::get('/banner/info/{id}', 'BannerController@getInfo');
    Route::get('/banner/image/{id}', 'BannerController@getImage');
    Route::get('/banner/create', 'BannerController@getCreate');
    
    Route::post('/banner/create', 'BannerController@postCreate');
    Route::post('/banner/update', 'BannerController@postUpdate');
    Route::post('/banner/upload', 'BannerController@postImage');
    
    // Condo
    
    Route::get('/condo/list', 'CondoController@getList');
    Route::get('/condo/create', 'CondoController@getCreate');
    Route::post('/condo/create', 'CondoController@postCreate');
    Route::get('/condo/info/{id}', 'CondoController@getInfo');
    Route::get('/condo/details/{id}', 'CondoController@getDetails');
    Route::get('/condo/media/{id}', 'CondoController@getMedia');
    Route::get('/condo/places/{id}', 'CondoController@getPlaces');
    Route::get('/condo/listing/{id}', 'CondoController@getListings');
    
    Route::post('/condo/media/upload', 'CondoController@uploadMedia');
    Route::post('/condo/media/sort', 'CondoController@sortMedia');
    Route::get('/condo/media/delete/{id}', 'CondoController@deleteMedia');
    
    Route::post('/condo/info', 'CondoController@postInfo');
    Route::post('/condo/general-info/{id}', 'CondoController@postGeneralInfo');
    Route::post('/condo/add-room/{id}', 'CondoController@postRoom');
    Route::get('/condo/delete-room/{id}/{room}', 'CondoController@deleteRoom');
    Route::post('/condo/update-room/{id}/{room}', 'CondoController@updateRoom');
    Route::post('/condo/update-facilities/{id}', 'CondoController@postUpdateFacilities');
    Route::post('/condo/update-others/{id}', 'CondoController@postUpdateOthers');
    
    Route::get('/new-condo/list', 'CondoController@getNewList');
    
    // Customer
    
    Route::get('/visitors', 'CustomerController@getVisitors');
    Route::get('/owners', 'CustomerController@getOwners');
    Route::get('/agents', 'CustomerController@getFreelanceAgents');
    Route::get('/agents/realestate', 'CustomerController@getRealestateAgents');
    Route::get('/developers', 'CustomerController@getDevelopers');
    
    Route::post('/user/status', 'CustomerController@updateStatus');
    
    // Dashboard
    
    Route::get('/dashboard', 'DashboardController@index');
    
    // Facility
    
    Route::get('/facility/list', 'FacilityController@getList');
    Route::post('/facility/create', 'FacilityController@postCreate');
    Route::get('/facility/edit/{id}', 'FacilityController@getEdit');
    Route::get('/facility/delete/{id}', 'FacilityController@getDelete');
    Route::post('/facility/upload', 'FacilityController@postUpload');
    Route::post('/facility/update/{id}', 'FacilityController@postUpdate');
    Route::post('/facility/order', 'FacilityController@order');
    
    // Feature
    
    Route::get('/feature/list', 'FeatureController@getList');
    Route::post('/feature/create', 'FeatureController@postCreate');
    Route::post('/feature/update/{id}', 'FeatureController@postUpdate');
    Route::post('/feature/upload', 'FeatureController@postUpload');
    Route::get('/feature/edit/{id}', 'FeatureController@getEdit');
    Route::get('/feature/delete/{id}', 'FeatureController@getDelete');
    Route::post('/feature/order', 'FeatureController@order');
    
    Route::get('/optional-feature/list', 'OptionalFeatureController@getList');
    Route::post('/optional-feature/create', 'OptionalFeatureController@postCreate');
    Route::post('/optional-feature/update/{id}', 'OptionalFeatureController@postUpdate');
    Route::post('/optional-feature/upload', 'OptionalFeatureController@postUpload');
    Route::get('/optional-feature/edit/{id}', 'OptionalFeatureController@getEdit');
    Route::get('/optional-feature/delete/{id}', 'OptionalFeatureController@getDelete');
    Route::post('/optional-feature/order', 'OptionalFeatureController@order');
    
    // Listing
    
    Route::get('/listing', 'ListingController@index');
    Route::get('/listing/form/{id}', 'ListingController@getForm');
    Route::post('/listing/update', 'ListingController@postUpdate');
    Route::get('/listing/toggle/{id}', 'ListingController@toggleVisibility');
    Route::get('/listing/delete/{id}', 'ListingController@getDelete');
    
    Route::get('/apartment/list', 'ListingController@apartmentList');
    Route::get('/house/list', 'ListingController@houseList');
    Route::get('/townhouse/list', 'ListingController@townhouseList');
    
    // Location
    
    Route::get('/location/province-list', 'LocationController@getProvinceList');
    Route::get('/location/province-details/{id}', 'LocationController@getProvinceDetails');
    Route::get('/location/district-list/{id}', 'LocationController@getDistrictList');
    Route::get('/location/district-details/{id}', 'LocationController@getDistrictDetails');
    
    Route::get('/location/area-list/{id}', 'LocationController@getAreaList');
    
    // New project
    Route::get('/project/create', 'ProjectController@getCreate');
    
    Route::get('/project/list/apartment', 'Project\ApartmentController@getList');
    Route::get('/project/list/condo', 'Project\CondoController@getList');
    Route::get('/project/list/house', 'Project\HouseController@getList');
    Route::get('/project/list/townhouse', 'Project\TownhouseController@getList');
    
    Route::get('/project/create/apartment', 'ProjectController@getCreateApartment');
    Route::get('/project/create/condo', 'ProjectController@getCreateCondo');
    Route::get('/project/create/house', 'ProjectController@getCreateHouse');
    Route::get('/project/create/townhouse', 'ProjectController@getCreateTownhouse');
    
    Route::post('/project/create/apartment', 'ProjectController@postCreateApartment');
    Route::post('/project/create/condo', 'ProjectController@postCreateCondo');
    Route::post('/project/create/house', 'ProjectController@postCreateHouse');
    Route::post('/project/create/townhouse', 'ProjectController@postCreateTownhouse');
    
    Route::get('/project/condo/info/{id}', 'ProjectController@getCondoInfo');
    Route::get('/project/condo/details/{id}', 'ProjectController@getCondoDetails');
    Route::get('/project/condo/media/{id}', 'ProjectController@getCondoMedia');
    Route::get('/project/condo/review/{id}', 'ProjectController@getCondoReviews');
    Route::get('/project/condo/places/{id}', 'ProjectController@getCondoPlaces');
    
    Route::get('/project/apartment/info/{id}', 'ProjectController@getApartmentInfo');
    Route::get('/project/apartment/review/{id}', 'ProjectController@getApartmentReviews');
    Route::post('/project/apartment/info/{id}', 'ProjectController@postApartmentInfo');
    Route::get('/project/apartment/details/{id}', 'ProjectController@getApartmentDetails');
    Route::get('/project/apartment/places/{id}', 'ProjectController@getApartmentPlaces');
    
    
    Route::get('/project/house/info/{id}', 'ProjectController@getHouseInfo');
    Route::get('/project/house/review/{id}', 'ProjectController@getHouseReviews');
    Route::post('/project/house/info/{id}', 'ProjectController@postHouseInfo');
    Route::get('/project/house/details/{id}', 'ProjectController@getHouseDetails');
    Route::get('/project/house/places/{id}', 'ProjectController@getHousePlaces');
    
    Route::get('/project/townhouse/info/{id}', 'ProjectController@getTownhouseInfo');
    Route::get('/project/townhouse/review/{id}', 'ProjectController@getTownhouseReviews');
    Route::post('/project/townhouse/info/{id}', 'ProjectController@postTownhouseInfo');
    Route::get('/project/townhouse/details/{id}', 'ProjectController@getTownhouseDetails');
    Route::get('/project/townhouse/places/{id}', 'ProjectController@getTownhousePlaces');
    
    Route::post('/project/condo/info/{id}', 'ProjectController@postCondoInfo');
    Route::post('/project/condo/details/{id}', 'ProjectController@postCondoDetails');
    Route::post('/project/condo/facilities/{id}', 'ProjectController@postCondoFacilities');
    Route::post('/project/condo/others/{id}', 'ProjectController@postCondoOthers');
    
    Route::post('/project/condo/review/{id}', 'ProjectController@postCondoReviews');
    Route::post('/project/condo/places/{id}', 'ProjectController@postCondoPlaces');
    
    Route::post('/project/condo/upload/{id}/{domain}', 'ProjectController@uploadReviewImage');
    Route::get('/project/condo/rm-image/{id}/{domain}/{condo}', 'ProjectController@removeReviewImage');
    
    Route::post('/project/property/upload/{id}/{domain}', 'ProjectController@uploadPropertyReviewImage');
    Route::get('/project/property/rm-image/{id}/{domain}/{condo}', 'ProjectController@removePropertyReviewImage');
    
    Route::post('/project/condo/review/{id}/{domain}', 'ProjectController@updateReviewDetails');
    Route::post('/project/property/review/{id}/{domain}', 'ProjectController@updatePropertyReviewDetails');
    
    
    // Order management
    
    Route::get('/order', 'OrderController@getList');
    Route::get('/order/details/{id}', 'OrderController@getDetails');
    Route::post('/order/account/{id}', 'OrderController@updateAccount');
    Route::post('/order/update/{id}', 'OrderController@updateOrder');
    
    // Package
    
    Route::get('/package/list', 'PackageController@getList');
    Route::get('/package/create-featured', 'PackageController@createFeatured');
    Route::get('/package/create-exclusive', 'PackageController@createExclusive');
    
    Route::get('/package/edit-featured/{id}', 'PackageController@editFeatured');
    Route::get('/package/edit-exclusive/{id}', 'PackageController@editExclusive');
    
    Route::get('/package/delete/{id}', 'PackageController@confirmDeletePackage');
    Route::get('/package/do-delete/{id}', 'PackageController@delete');
    
    Route::post('/package/save-featured', 'PackageController@saveFeatured');
    Route::post('/package/save-exclusive', 'PackageController@saveExclusive');
    
    Route::post('/package/update-featured/{id}', 'PackageController@updateFeatured');
    Route::post('/package/update-exclusive/{id}', 'PackageController@updateExclusive');
    
    // Point
    
    Route::get('/point', 'PointController@getEntryList');
    Route::get('/point/entries', 'PointController@getEntries');
    Route::get('/point/types', 'PointController@getTypeList');
    Route::post('/point/create', 'PointController@createNewEntry');
    
    // Setting
    
    Route::get('/setting', 'SettingController@index');
    Route::get('/setting/contact', 'SettingController@getContact');
    Route::post('/setting/contact', 'SettingController@updateContact');
    Route::post('/setting/footer', 'SettingController@updateFooter');
    
    Route::get('/setting/bank', 'SettingController@getBank');
    Route::get('/setting/add-bank', 'SettingController@getAddBank');
    Route::post('/setting/add-bank', 'SettingController@addBank');
    
    Route::post('/setting/update', 'SettingController@update');
    
    Route::get('/setting/edit-bank/{id}', 'SettingController@editBank');
    Route::post('/setting/update-bank/{id}', 'SettingController@updateBank');
    
    Route::get('/setting/delete-bank/{id}', 'SettingController@deleteBank');
    
    
    Route::get('/setting/image', 'SettingController@getImages');
    Route::post('/setting/image-upload', 'SettingController@uploadImage');
    Route::get('/setting/image-remove', 'SettingController@removeTranslatedImage');
    
    // Staff
    
    Route::get('/staff/list', 'StaffController@getList');
    Route::get('/staff/create', 'StaffController@getCreate');
    Route::get('/staff/update/{id}', 'StaffController@getUpdate');
    Route::post('/staff/update/{id}', 'StaffController@postUpdate');
    Route::post('/staff/create', 'StaffController@postCreate');
    
    Route::get('/staff/delete/{id}', 'StaffController@getDelete');
    Route::get('/staff/remove/{id}', 'StaffController@delete');
    
    // Translation
    
    Route::get('/translation/list', 'TranslationController@getList');
    Route::get('/translation/edit/{id}', 'TranslationController@getEdit');
    Route::post('/translation/update', 'TranslationController@postUpdate');
    Route::get('/translation/publish', 'TranslationController@publishTranslation');
    
});
