<?php

Route::group([/*'middleware' => 'auth.admin', */'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    
    Route::get('/user/list', 'UserController@getList');
    
    Route::get('/user/setting/{id}', 'UserController@getModalSettingContent');
    Route::post('/user/update', 'UserController@update');
    
});
