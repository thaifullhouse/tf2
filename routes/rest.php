<?php

//

Route::group(['middleware' => 'rest', 'namespace' => 'Rest'], function () {
    
    // Auth
    
    Route::post('/auth/login', 'AuthController@postLogin');
    Route::post('/auth/register', 'AuthController@postRegistration');
    
    Route::post('/message/send', 'MessageController@send');

    Route::get('/search-autocomplete', 'SearchController@getProposition');
    Route::get('/search-suggestion', 'SearchController@getSuggestion');
    Route::get('/search/pro/map', 'SearchController@findPropertiesForMap');
    Route::get('/search/pro/list', 'SearchController@findPropertiesForListing');
    
    Route::get('/user/favorite-property/{id}', 'UserController@favoriteProperty');
    Route::get('/user/favorite-agent/{id}', 'UserController@favoriteAgent');
    Route::get('/user/saved-search', 'UserController@getSavedSearch');
    
    Route::get('/search/save', 'SearchController@getSaved');
    Route::post('/search/save', 'SearchController@addSaved');

    // Condominium
    
    Route::get('/condo/details/{id}', 'CondoController@getDetails');
    Route::get('/condo/reviews/{id}', 'CondoController@getReviews');
    Route::post('/condo/review', 'CondoController@postReview');
    Route::post('/condo/comment', 'CondoController@postComment');
    Route::get('/condo/properties/{id}', 'CondoController@getProperties');
    Route::get('/condo/names', 'CondoController@getCondomium');
    
    // Agent

    Route::get('/agent/details/{id}', 'AgentController@getDetails');
    Route::get('/agent/reviews/{id}', 'AgentController@getReviews');
    Route::post('/agent/review', 'AgentController@postReview');
    
    Route::get('/project/details/{id}', 'ProjectController@getDetails');
    
    Route::get('/location/district/{id}', 'LocationController@getDistrict');
    Route::get('/location/area/{id}', 'LocationController@getArea');
    
    Route::get('/property/image/{id}', 'PropertyController@getImageMeta');
    Route::get('/property/map-info/{id}', 'PropertyController@getMapInfo');
    
    

});
