<?php

Route::group(['namespace' => 'Visitor'], function () {
    
    // Agent
    
    Route::get('/agent/details/{id}', 'AgentController@getDetails');
    Route::get('/agent/search', 'AgentController@search');
    Route::get('/agent/real-details', 'AgentController@getRealDetails');

    // Auth
    
    Route::get('/auth/validate-email/{id}', 'AuthController@validateUserEmail');
    
    Route::get('/auth/email-verify', 'AuthController@verifyUserEmail');
    
    Route::get('/auth/request-email/{id}', 'AuthController@getRequestEmail');
    Route::get('/auth/auto-redirect', 'AuthController@autoRedirect');   
    
    Route::get('/auth/request-password', 'AuthController@getRequestPassword');
    Route::post('/auth/request-reset-link', 'AuthController@createRequestPassword'); 
    
    Route::get('/auth/update-password', 'AuthController@getUpdatePassword');
    Route::post('/auth/update-password', 'AuthController@postUpdatePassword');
    
    Route::get('/auth/updated', 'AuthController@getUpdated');
    
    Route::get('/auth/facebook', 'AuthController@redirectToFacebook');
    Route::get('/auth/google', 'AuthController@redirectToGoogle');
    Route::get('/auth/twitter', 'AuthController@redirectToTwitter');
    Route::get('/auth/logout', 'AuthController@logout');
    Route::get('/auth/login', 'AuthController@login');

    Route::get('/auth/callback-facebook', 'AuthController@handleFacebookCallback');
    Route::get('/auth/callback-google', 'AuthController@handleGoogleCallback');
    Route::get('/auth/callback-twitter', 'AuthController@handleTwitterCallback');
    
    Route::get('/auth/register/type', 'RegistrationController@getType');
    Route::post('/auth/register/type', 'RegistrationController@postType');
    
    Route::get('/auth/register/owner', 'RegistrationController@showOwner');
    Route::get('/auth/register/freelance', 'RegistrationController@showAgent');
    Route::get('/auth/register/realestate', 'RegistrationController@showRealEstate');
    Route::get('/auth/register/developer', 'RegistrationController@showDeveloper');
    
    Route::post('/auth/register/owner', 'RegistrationController@postOwner');
    Route::post('/auth/register/agent', 'RegistrationController@postAgent');
    Route::post('/auth/register/company', 'RegistrationController@postRealEstate');
    Route::post('/auth/register/developer', 'RegistrationController@postDeveloper');
    
    // Condo
    
    Route::get('/condo/details/{id}', 'CondoController@showCondoDetails');
    Route::get('/condo/preview/{id}', 'CondoController@showCondoPreview');
    
    // Error
    
    Route::get('/page-not-found', 'ErrorController@pageNotFound');
    Route::get('/access-forbidden', 'ErrorController@accessForbidden');
    
    // Home
    
    Route::get('/', 'HomeController@index');
    
    // Image
    
    Route::get('/image/{file}', 'ImageController@index');
    Route::get('/thumb/{size}/{file}', 'ImageController@getThumb');
    Route::get('/property-thumb/{id}', 'ImageController@propertyThumb');
    
    // Payment
    
    Route::get('/payment/featured', 'PaymentController@getFeatured');
    Route::get('/payment/exclusive', 'PaymentController@getExclusive');
    
    // Profile
    
    Route::get('/profile-review', 'ProfileController@getReviews');
    
    // Project
    
    Route::get('/project/details', 'ProjectController@getDetails');
    
    // Property
    
    Route::get('/property/details/{id}', 'PropertyController@getDetails');
    
    Route::get('/property/details/{lt}/{type}', 'PropertyController@getDetails2');
    
    Route::get('/property-review', 'PropertyController@getReviews');
    Route::get('/property-preview/{id}', 'PropertyController@getPreview');
    
    
    // Realestate
    
    Route::get('/realestate/details', 'RealEstateController@getDetails');
    
    // Registration
    
//    Route::get('/register/type', 'RegistrationController@getType');
//    Route::get('/auth/register/details', 'RegistrationController@getDetails');
    
    // Search
    
    Route::get('/search', 'SearchController@findProperty');
    
    // Test
    
    Route::get('/test/index', 'TestController@index')->name('test/index');
    
    Route::get('/payment/token/{pid}', 'PaymentController@getPaysbuyToken');
    
    Route::get('/payment/thanks', 'PaymentController@getThanks');    
    
    
});