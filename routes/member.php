<?php

Route::group(['middleware' => 'auth.customer', 'prefix' => 'member', 'namespace' => 'Member'], function () {
    
    // Listing
    
    Route::get('listing', 'ListingController@index');
    Route::post('listing/create', 'ListingController@create');
    Route::get('listing/details/{id}', 'ListingController@getDetails');
    Route::post('listing/details/{id}', 'ListingController@postDetails');
    Route::post('listing/upload', 'ListingController@postUpload');
    
    // Message
    
    Route::get('message', 'MessageController@index');
    Route::get('notice', 'MessageController@getNotifications');
    
    // Package
    
    Route::get('package/buy-exclusive', 'PackageController@buyExclusive');
    Route::post('package/buy', 'PackageController@postBuy');
    Route::get('package/buy-featured', 'PackageController@buyFeatured');
      
    // Posting
    
    Route::get('posting-info', 'PostingController@getPostingInfo');
    Route::get('posting-details', 'PostingController@getPostingDetails');
    Route::get('favorite', 'PostingController@getLikedList');
    Route::get('posting/property-type', 'PostingController@getPropertyType');
    
    
    // Profile
    
    Route::get('profile', 'ProfileController@index');
    
    // Purchase
    
    Route::get('purchase', 'PurchaseController@index');
    
    Route::post('purchase', 'PurchaseController@index');
    
    // Registration
    
    Route::get('type', 'RegistrationController@showTypeSelect');
    Route::get('register', 'RegistrationController@showRegistration');
    
    // Settings
    Route::get('settings', 'SettingController@index');
    Route::post('settings/update', 'SettingController@update'); 
    Route::post('settings/upload-cover', 'SettingController@uploadCoverPicture');
    Route::post('settings/upload-profile', 'SettingController@uploadProfilePicture');
});